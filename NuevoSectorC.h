//---------------------------------------------------------------------------

#ifndef NuevoSectorCH
#define NuevoSectorCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TNuevoSectorF : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label7;
        TDBEdit *Sector;
        TDBEdit *Variedad;
        TGroupBox *DatosFinca;
        TDBEdit *CodFinca;
        TDBEdit *Nombre;
        TDBEdit *DstLinea;
        TDBEdit *DstArbol;
        TDBEdit *Hg;
        TDBEdit *Edad;
        TDBEdit *GoteoArbol;
        TDBEdit *CaudalGoteo;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label8;
        TLabel *Label10;
        TLabel *Label11;
        TButton *Insertar;
        TButton *Insertamas;
        TButton *Cancelar;
        TDBEdit *NumArboles;
        TLabel *Label12;
        TDBCheckBox *Es_Plantonada;
        TDBLookupComboBox *Patron;
        void __fastcall SalirClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall InsertarClick(TObject *Sender);
        void __fastcall CancelarClick(TObject *Sender);
        void __fastcall InsertamasClick(TObject *Sender);
        void __fastcall DstLineaChange(TObject *Sender);
        void __fastcall DstArbolChange(TObject *Sender);
        void __fastcall NumArbolesChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TNuevoSectorF(TComponent* Owner);
        void TNuevoSectorF::calculaSuperficie(void);
};
//---------------------------------------------------------------------------
extern PACKAGE TNuevoSectorF *NuevoSectorF;
//---------------------------------------------------------------------------
#endif
