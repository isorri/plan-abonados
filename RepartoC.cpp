//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "RepartoC.h"
#include "DatosC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PlantillaC"
#pragma resource "*.dfm"
TRepartoF *RepartoF;
//---------------------------------------------------------------------------
__fastcall TRepartoF::TRepartoF(TComponent* Owner)
        : TPlantilla(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TRepartoF::FormClose(TObject *Sender, TCloseAction &Action)
{
   Action = caFree;
   RepartoF = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TRepartoF::CerrarClick(TObject *Sender)
{
        Close();
}
//---------------------------------------------------------------------------

void __fastcall TRepartoF::NuevoClick(TObject *Sender)
{
   TDataSetState State;
   State = DatosF->Reparto->State;

   if (State == dsEdit)
        Application->MessageBoxA("Cambios no guardados.\nPulse Guardar o Cancelar.","Error",MB_ICONERROR);
   else
        if (State == dsInsert)
                Application->MessageBoxA("Registro pendiente de inserción.\nPulse Guardar o Cancelar","Error",MB_ICONERROR);
        else
        {
                Codigo->SetFocus();
                DatosF->Reparto->Insert();
        }

}
//---------------------------------------------------------------------------

void __fastcall TRepartoF::GuardarClick(TObject *Sender)
{
     DatosF->Reparto->Post();
}
//---------------------------------------------------------------------------
void __fastcall TRepartoF::CancelarClick(TObject *Sender)
{
     DatosF->Reparto->Cancel();
}
//---------------------------------------------------------------------------

void __fastcall TRepartoF::EliminarClick(TObject *Sender)
{
     DatosF->Reparto->Delete();
}
//---------------------------------------------------------------------------
void TRepartoF::bloqueaFormulario(void)
{
     ListaIzq->Enabled = false;
     NuevoB->Enabled = false;
     GuardarB->Enabled = true;
     Cancelar->Enabled = true;
     EliminarB->Enabled = false;
}
//---------------------------------------------------------------------------
void TRepartoF::desbloqueaFormulario(void)
{
     ListaIzq->Enabled = true;
     NuevoB->Enabled = true;
     GuardarB->Enabled = false;
     Cancelar->Enabled = false;
     EliminarB->Enabled = true;
}
//---------------------------------------------------------------------------
