inherited RiegoPlantF: TRiegoPlantF
  Left = 505
  Top = 297
  Width = 471
  Height = 249
  Caption = 'Riego Plantonadas'
  PixelsPerInch = 96
  TextHeight = 13
  inherited NuevoB: TBitBtn
    Width = 59
  end
  inherited GuardarB: TBitBtn
    Width = 68
  end
  inherited ListaIzq: TDBGrid
    DataSource = DatosF.RiegoPlantSource
  end
  inherited Panel: TGroupBox
    inherited Mes: TDBEdit
      DataSource = DatosF.RiegoPlantSource
    end
    inherited Lunes: TDBCheckBox
      DataSource = DatosF.RiegoPlantSource
    end
    inherited Martes: TDBCheckBox
      DataSource = DatosF.RiegoPlantSource
    end
    inherited Miercoles: TDBCheckBox
      DataSource = DatosF.RiegoPlantSource
    end
    inherited Jueves: TDBCheckBox
      DataSource = DatosF.RiegoPlantSource
    end
    inherited Viernes: TDBCheckBox
      DataSource = DatosF.RiegoPlantSource
    end
    inherited Sabado: TDBCheckBox
      DataSource = DatosF.RiegoPlantSource
    end
    inherited Domingo: TDBCheckBox
      DataSource = DatosF.RiegoPlantSource
    end
  end
end
