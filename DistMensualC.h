//---------------------------------------------------------------------------

#ifndef DistMensualCH
#define DistMensualCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "PlantillaC.h"
#include <Buttons.hpp>
#include <DBCtrls.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TDistMensualF : public TPlantilla
{
__published:	// IDE-managed Components
        TDBGrid *Detalle;
        TGroupBox *Porcentajes;
        TDBEdit *Cantidad;
        TLabel *Label2;
        TLabel *Label3;
        TDBEdit *Enero;
        TDBEdit *Febrero;
        TDBEdit *Marzo;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label10;
        TLabel *Label11;
        TLabel *Label13;
        TLabel *Label14;
        TLabel *Label16;
        TLabel *Label17;
        TLabel *Label18;
        TLabel *Label19;
        TLabel *Label15;
        TLabel *Label12;
        TLabel *Label20;
        TDBEdit *Abril;
        TDBEdit *Mayo;
        TDBEdit *Junio;
        TLabel *Label21;
        TLabel *Label22;
        TLabel *Label23;
        TLabel *Label27;
        TLabel *Label28;
        TLabel *Label31;
        TLabel *Label32;
        TLabel *Label35;
        TLabel *Label36;
        TDBEdit *Julio;
        TDBEdit *Agosto;
        TDBEdit *Septiembre;
        TLabel *Label24;
        TLabel *Label37;
        TLabel *Label38;
        TLabel *Label39;
        TLabel *Label40;
        TLabel *Label41;
        TLabel *Label42;
        TLabel *Label43;
        TLabel *Label44;
        TDBEdit *Octubre;
        TDBEdit *Noviembre;
        TDBEdit *Diciembre;
        TButton *Cancelar;
        TDBEdit *PorTotal;
        TLabel *Label25;
        TDBEdit *CTD01;
        TDBEdit *CTD02;
        TDBEdit *CTD03;
        TDBEdit *CTD04;
        TDBEdit *CTD05;
        TDBEdit *CTD06;
        TDBEdit *CTD07;
        TDBEdit *CTD08;
        TDBEdit *CTD09;
        TDBEdit *CTD10;
        TDBEdit *CTD11;
        TDBEdit *CTD12;
        TLabel *Label26;
        TButton *Aplicar;
        TDBLookupComboBox *Plantilla;
        TDBGrid *ListaIzq;
        TButton *PlantillaBtn;
        TDBText *NomCliente;
        TDBText *NomFinca;
        TDBText *Sector;
        TDBText *Anyo;
        TDBEdit *NomComercial;
        TLabel *Label1;
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall GuardarClick(TObject *Sender);
        void __fastcall CancelarClick(TObject *Sender);
        void __fastcall AplicarClick(TObject *Sender);
        void __fastcall PlantillaBtnClick(TObject *Sender);
        void __fastcall EliminarClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TDistMensualF(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TDistMensualF *DistMensualF;
//---------------------------------------------------------------------------
#endif
