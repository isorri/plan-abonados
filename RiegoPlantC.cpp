//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "RiegoPlantC.h"
#include "DatosC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "RiegoC"
#pragma resource "*.dfm"
TRiegoPlantF *RiegoPlantF;
//---------------------------------------------------------------------------
__fastcall TRiegoPlantF::TRiegoPlantF(TComponent* Owner)
        : TRiegoF(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TRiegoPlantF::CerrarClick(TObject *Sender)
{
   Close();        
}
//---------------------------------------------------------------------------
void __fastcall TRiegoPlantF::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   TDataSetState State;
   int res;
        State = DatosF->RiegoPlant->State;
        if (State == dsInsert)
        {
            res = Application->MessageBoxA("Registro pendiente de inserci�n.\n�Guardar datos?.","Pregunta",MB_ICONQUESTION+MB_YESNO);
            if (res == IDYES)
                DatosF->RiegoPlant->Post();
            else
                DatosF->RiegoPlant->Cancel();
        }
        else
        {
            if (State == dsEdit)
            {
                res=Application->MessageBoxA("Registro con cambios no guardados.\n�Guardar cambios?","Pregunta",MB_ICONQUESTION+MB_YESNO);
                if (res == IDYES)
                    DatosF->RiegoPlant->Post();
                else
                    DatosF->RiegoPlant->Cancel();
            }
        }

   Action = caFree;
   RiegoPlantF = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TRiegoPlantF::NuevoClick(TObject *Sender)
{
   TDataSetState State;
   State = DatosF->RiegoPlant->State;

   if (State == dsEdit)
   {
      Application->MessageBoxA("Registro actual pendiente de guardar cambios.\nGuarde los cambios y vuelva a pulsar el bot�n.","Error",MB_ICONERROR);
      return;
   }
   if (State == dsInsert)
   {
      Application->MessageBoxA("Registro pendiente de inserci�n.\nGuarde el cliente y vuelva a pulsar el bot�n","Error",MB_ICONERROR);
      return;
   }
   DatosF->RiegoPlant->Insert();
   Mes->SetFocus();
   Lunes->Checked = false;
   Martes->Checked = false;
   Miercoles->Checked = false;
   Jueves->Checked = false;
   Viernes->Checked = false;
   Sabado->Checked = false;
   Domingo->Checked = false;
}
//---------------------------------------------------------------------------

void __fastcall TRiegoPlantF::GuardarClick(TObject *Sender)
{
   TDataSetState State;
   int mes;
        State = DatosF->RiegoPlant->State;
        if ( State == dsEdit || State == dsInsert)
        {
           mes = (int)DatosF->RiegoPlant->FieldValues["MES"];
           if (mes <1 || mes >12)
           {
              Application->MessageBoxA("Mes fuera de rango","Error",MB_ICONERROR);
              Mes->SetFocus();
           }
           else
              DatosF->RiegoPlant->Post();
         }
        else
           ShowMessage("Nada que guardar");
}
//---------------------------------------------------------------------------

void __fastcall TRiegoPlantF::CancelarClick(TObject *Sender)
{
   TDataSetState State;
        State = DatosF->RiegoPlant->State;
        if ( State == dsEdit || State == dsInsert)
           DatosF->RiegoPlant->Cancel();
        else
           ShowMessage("Nada que cancelar");
}
//---------------------------------------------------------------------------


void __fastcall TRiegoPlantF::EliminarBClick(TObject *Sender)
{
    DatosF->RiegoPlant->Delete();
}
//---------------------------------------------------------------------------
void TRiegoPlantF::bloqueaFormulario(void)
{
     ListaIzq->Enabled = false;
     NuevoB->Enabled = false;
     GuardarB->Enabled = true;
     Cancelar->Enabled = true;
     EliminarB->Enabled = false;
}
//---------------------------------------------------------------------------
void TRiegoPlantF::desbloqueaFormulario(void)
{
     ListaIzq->Enabled = true;
     NuevoB->Enabled = true;
     GuardarB->Enabled = false;
     Cancelar->Enabled = false;
     EliminarB->Enabled = true;
}
//---------------------------------------------------------------------------
