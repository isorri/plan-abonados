//---------------------------------------------------------------------------

#ifndef ClienteCH
#define ClienteCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <DBCtrls.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <Mask.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TClienteF : public TForm
{
__published:	// IDE-managed Components
        TDBGrid *Lista_Clientes;
        TLabel *Label1;
        TLabel *Label2;
        TDBEdit *Nombre;
        TLabel *Label3;
        TDBLookupComboBox *Comercial;
        TDBEdit *CodCli;
        TLabel *Label4;
        TDBComboBox *Tipo;
        TEdit *Filtro;
        TLabel *Label5;
        TButton *Cancelar;
        TGroupBox *ClienteBox;
        TBitBtn *NuevoB;
        TBitBtn *GuardarB;
        TBitBtn *EliminarB;
        TButton *Cerrar;
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall FiltroChange(TObject *Sender);
        void __fastcall NuevoClick(TObject *Sender);
        void __fastcall GuardarClick(TObject *Sender);
        void __fastcall CancelarClick(TObject *Sender);
        void __fastcall EliminarClick(TObject *Sender);

private:	// User declarations
public:		// User declarations
        __fastcall TClienteF(TComponent* Owner);
        void bloqueaFormulario(void);
        void desbloqueaFormulario(void);        
};
//---------------------------------------------------------------------------
extern PACKAGE TClienteF *ClienteF;
//---------------------------------------------------------------------------
#endif
