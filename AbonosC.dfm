inherited ProductF: TProductF
  Left = 286
  Top = 162
  Width = 819
  Height = 494
  Caption = 'Productos'
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited Cerrar: TBitBtn
    Left = 720
    Top = 424
    Hint = 'Cierra este m'#243'dulo'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 7
    OnClick = CerrarClick
  end
  inherited NuevoB: TBitBtn
    Left = 400
    Top = 424
    TabOrder = 3
    OnClick = InsertarClick
  end
  inherited GuardarB: TBitBtn
    Left = 480
    Top = 424
    Enabled = False
    TabOrder = 4
    OnClick = GuardarClick
  end
  inherited EliminarB: TBitBtn
    Left = 640
    Top = 424
    TabOrder = 6
    OnClick = EliminarClick
  end
  object Productos: TDBGrid
    Left = 8
    Top = 8
    Width = 385
    Height = 441
    Hint = 'Listado de los productos actualmente introducidos'
    TabStop = False
    DataSource = DatosF.ProductosSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CODART'
        Title.Caption = 'C'#243'digo'
        Width = 77
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMCOMERCIAL'
        Title.Caption = 'Nombre Comercial'
        Width = 266
        Visible = True
      end>
  end
  object Campos: TPanel
    Left = 400
    Top = 8
    Width = 401
    Height = 73
    BorderStyle = bsSingle
    TabOrder = 1
    object Label9: TLabel
      Left = 32
      Top = 16
      Width = 73
      Height = 13
      Caption = 'C'#243'digo Art'#237'culo'
    end
    object Label10: TLabel
      Left = 32
      Top = 40
      Width = 56
      Height = 13
      Caption = 'Descripci'#243'n'
    end
    object CodArt: TDBEdit
      Left = 136
      Top = 16
      Width = 113
      Height = 21
      DataField = 'CODART'
      DataSource = DatosF.ProductosSource
      Enabled = False
      TabOrder = 0
    end
    object NomComercial: TDBEdit
      Left = 136
      Top = 40
      Width = 233
      Height = 21
      DataField = 'NOMCOMERCIAL'
      DataSource = DatosF.ProductosSource
      TabOrder = 1
    end
  end
  object Cancelar: TBitBtn
    Left = 560
    Top = 424
    Width = 75
    Height = 25
    Hint = 'Cancela los datos a'#241'adidos/modificados'
    Caption = '&Cancelar'
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 5
    OnClick = CancelarClick
  end
  object Paginas: TPageControl
    Left = 400
    Top = 88
    Width = 401
    Height = 329
    ActivePage = DatosP
    TabIndex = 0
    TabOrder = 2
    object DatosP: TTabSheet
      Caption = 'Da&tos'
      object Propiedades: TGroupBox
        Left = 0
        Top = 0
        Width = 385
        Height = 193
        Caption = 'Propiedades'
        TabOrder = 0
        object Label2: TLabel
          Left = 16
          Top = 16
          Width = 46
          Height = 13
          Caption = 'Nitr'#243'geno'
        end
        object Label3: TLabel
          Left = 16
          Top = 40
          Width = 35
          Height = 13
          Caption = 'F'#243'sforo'
        end
        object Label4: TLabel
          Left = 16
          Top = 64
          Width = 35
          Height = 13
          Caption = 'Potasio'
        end
        object Label5: TLabel
          Left = 16
          Top = 136
          Width = 49
          Height = 13
          Caption = 'Disoluci'#243'n'
        end
        object Label6: TLabel
          Left = 208
          Top = 136
          Width = 33
          Height = 13
          Caption = 'Estado'
        end
        object Label11: TLabel
          Left = 208
          Top = 16
          Width = 29
          Height = 13
          Caption = 'C'#225'lcio'
        end
        object Label12: TLabel
          Left = 208
          Top = 40
          Width = 46
          Height = 13
          Caption = 'Magnesio'
        end
        object Label13: TLabel
          Left = 208
          Top = 64
          Width = 28
          Height = 13
          Caption = 'Hierro'
        end
        object Label14: TLabel
          Left = 16
          Top = 88
          Width = 53
          Height = 13
          Caption = 'M.Element.'
        end
        object Label15: TLabel
          Left = 208
          Top = 88
          Width = 46
          Height = 13
          Caption = 'A.H'#250'mico'
        end
        object Label16: TLabel
          Left = 56
          Top = 112
          Width = 92
          Height = 13
          Caption = 'Componente Salino'
        end
        object N: TDBEdit
          Left = 72
          Top = 16
          Width = 121
          Height = 21
          Hint = 'Cantidad de nitr'#243'geno que tiene el compuesto'
          DataField = 'N'
          DataSource = DatosF.ProductosSource
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
        object P: TDBEdit
          Left = 72
          Top = 40
          Width = 121
          Height = 21
          Hint = 'Cantidad de f'#243'sforo que tiene el compuesto'
          DataField = 'P'
          DataSource = DatosF.ProductosSource
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object K: TDBEdit
          Left = 72
          Top = 64
          Width = 121
          Height = 21
          Hint = 'Cantidad de potasio que tiene el compuesto'
          DataField = 'K'
          DataSource = DatosF.ProductosSource
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
        end
        object Disolucion: TDBEdit
          Left = 72
          Top = 136
          Width = 121
          Height = 21
          Hint = 'Kg de compuesto que se disuelven en 100 litros de agua'
          DataField = 'DISOLUCI'#211'N'
          DataSource = DatosF.ProductosSource
          ParentShowHint = False
          ShowHint = True
          TabOrder = 9
        end
        object Estado: TDBComboBox
          Left = 264
          Top = 136
          Width = 105
          Height = 21
          Hint = 'Estado f'#237'sico del compuesto'
          DataField = 'ESTADO'
          DataSource = DatosF.ProductosSource
          ItemHeight = 13
          Items.Strings = (
            'S'#211'LIDO'
            'L'#205'QUIDO')
          ParentShowHint = False
          ShowHint = True
          TabOrder = 10
        end
        object CA: TDBEdit
          Left = 264
          Top = 16
          Width = 105
          Height = 21
          DataField = 'CA'
          DataSource = DatosF.ProductosSource
          TabOrder = 1
        end
        object MG: TDBEdit
          Left = 264
          Top = 40
          Width = 105
          Height = 21
          DataField = 'MG'
          DataSource = DatosF.ProductosSource
          TabOrder = 3
        end
        object FE: TDBEdit
          Left = 264
          Top = 64
          Width = 106
          Height = 21
          DataField = 'FE'
          DataSource = DatosF.ProductosSource
          TabOrder = 5
        end
        object MElem: TDBEdit
          Left = 72
          Top = 88
          Width = 121
          Height = 21
          DataField = 'MICROELEMENTOS'
          DataSource = DatosF.ProductosSource
          TabOrder = 6
        end
        object AH: TDBEdit
          Left = 264
          Top = 88
          Width = 106
          Height = 21
          DataField = 'AHUMICO'
          DataSource = DatosF.ProductosSource
          TabOrder = 7
        end
        object CSALINO: TDBEdit
          Left = 160
          Top = 112
          Width = 121
          Height = 21
          DataField = 'CSALINO'
          DataSource = DatosF.ProductosSource
          TabOrder = 8
        end
        object Es_Abono: TDBCheckBox
          Left = 152
          Top = 168
          Width = 97
          Height = 17
          Hint = 
            'Si esta casilla esta marcada, el producto se tendra en cuenta a ' +
            'la hora de calcular los kilogramos de abono comercial.'
          Caption = 'Es un abono'
          DataField = 'ABONO'
          DataSource = DatosF.ProductosSource
          TabOrder = 11
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
      end
      object Finanzas: TGroupBox
        Left = 0
        Top = 196
        Width = 385
        Height = 97
        Caption = 'Datos Financieros'
        TabOrder = 1
        object Label7: TLabel
          Left = 8
          Top = 32
          Width = 54
          Height = 13
          Caption = 'Precio / Kg'
        end
        object Label8: TLabel
          Left = 8
          Top = 56
          Width = 49
          Height = 13
          Caption = 'C'#243'digo X3'
        end
        object Label17: TLabel
          Left = 96
          Top = 16
          Width = 21
          Height = 13
          Alignment = taCenter
          Caption = 'PVT'
        end
        object Label18: TLabel
          Left = 168
          Top = 16
          Width = 22
          Height = 13
          Alignment = taCenter
          Caption = 'COP'
        end
        object Label19: TLabel
          Left = 240
          Top = 16
          Width = 22
          Height = 13
          Alignment = taCenter
          Caption = 'GRF'
        end
        object Label20: TLabel
          Left = 312
          Top = 16
          Width = 21
          Height = 13
          Alignment = taCenter
          Caption = 'PVP'
        end
        object Label1: TLabel
          Left = 152
          Top = 56
          Width = 87
          Height = 13
          Caption = 'Fecha importaci'#243'n'
        end
        object X3: TDBEdit
          Left = 8
          Top = 72
          Width = 121
          Height = 21
          Hint = 
            'Este c'#243'digo es necesario para realizar correctamente la importac' +
            'i'#243'n desde el sistema inform'#225'tico de PROVEFE'
          DataField = 'X3CODE'
          DataSource = DatosF.ProductosSource
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
        end
        object PVT: TDBEdit
          Left = 96
          Top = 32
          Width = 65
          Height = 21
          DataField = 'PVT'
          DataSource = DatosF.ProductosSource
          TabOrder = 0
        end
        object COP: TDBEdit
          Left = 168
          Top = 32
          Width = 64
          Height = 21
          DataField = 'COP'
          DataSource = DatosF.ProductosSource
          TabOrder = 1
        end
        object GRF: TDBEdit
          Left = 240
          Top = 32
          Width = 64
          Height = 21
          DataField = 'GRF'
          DataSource = DatosF.ProductosSource
          TabOrder = 2
        end
        object PVP: TDBEdit
          Left = 312
          Top = 32
          Width = 64
          Height = 21
          DataField = 'PVP'
          DataSource = DatosF.ProductosSource
          TabOrder = 3
        end
        object Import: TButton
          Left = 272
          Top = 64
          Width = 75
          Height = 25
          Caption = '&Importar'
          TabOrder = 5
          OnClick = ImportClick
        end
        object FImportacion: TDBEdit
          Left = 152
          Top = 72
          Width = 89
          Height = 21
          DataField = 'FIMPORTACION'
          DataSource = DatosF.ProductosSource
          Enabled = False
          TabOrder = 6
        end
      end
    end
    object RepartoP: TTabSheet
      Hint = 'Reparto mensual asociado al producto por defecto.'
      Caption = 'Repart&o'
      ImageIndex = 1
      object Reparto: TGroupBox
        Left = 32
        Top = 32
        Width = 337
        Height = 209
        Hint = 'Reparto mensual asociado al producto por defecto.'
        Caption = 'Reparto mensual'
        TabOrder = 0
        object Label21: TLabel
          Left = 24
          Top = 24
          Width = 28
          Height = 13
          Caption = 'Enero'
        end
        object Label22: TLabel
          Left = 96
          Top = 24
          Width = 36
          Height = 13
          Caption = 'Febrero'
        end
        object Label23: TLabel
          Left = 168
          Top = 24
          Width = 29
          Height = 13
          Caption = 'Marzo'
        end
        object Label24: TLabel
          Left = 240
          Top = 24
          Width = 20
          Height = 13
          Caption = 'Abril'
        end
        object Label25: TLabel
          Left = 24
          Top = 72
          Width = 26
          Height = 13
          Caption = 'Mayo'
        end
        object Label26: TLabel
          Left = 96
          Top = 72
          Width = 25
          Height = 13
          Caption = 'Junio'
        end
        object Label27: TLabel
          Left = 168
          Top = 72
          Width = 21
          Height = 13
          Caption = 'Julio'
        end
        object Label28: TLabel
          Left = 240
          Top = 72
          Width = 33
          Height = 13
          Caption = 'Agosto'
        end
        object Label29: TLabel
          Left = 24
          Top = 120
          Width = 53
          Height = 13
          Caption = 'Septiembre'
        end
        object Label30: TLabel
          Left = 96
          Top = 120
          Width = 38
          Height = 13
          Caption = 'Octubre'
        end
        object Label31: TLabel
          Left = 168
          Top = 120
          Width = 51
          Height = 13
          Caption = 'Noviembre'
        end
        object Label32: TLabel
          Left = 240
          Top = 120
          Width = 47
          Height = 13
          Caption = 'Diciembre'
        end
        object Label33: TLabel
          Left = 88
          Top = 168
          Width = 35
          Height = 13
          Caption = '% Total'
        end
        object Enero: TDBEdit
          Left = 24
          Top = 40
          Width = 65
          Height = 21
          DataField = '01_ENERO'
          DataSource = DatosF.ProductosSource
          TabOrder = 0
        end
        object Febrero: TDBEdit
          Left = 96
          Top = 40
          Width = 65
          Height = 21
          DataField = '02_FEBRERO'
          DataSource = DatosF.ProductosSource
          TabOrder = 1
        end
        object Marzo: TDBEdit
          Left = 168
          Top = 40
          Width = 65
          Height = 21
          DataField = '03_MARZO'
          DataSource = DatosF.ProductosSource
          TabOrder = 2
        end
        object Abril: TDBEdit
          Left = 240
          Top = 40
          Width = 65
          Height = 21
          DataField = '04_ABRIL'
          DataSource = DatosF.ProductosSource
          TabOrder = 3
        end
        object Mayo: TDBEdit
          Left = 24
          Top = 88
          Width = 65
          Height = 21
          DataField = '05_MAYO'
          DataSource = DatosF.ProductosSource
          TabOrder = 4
        end
        object Junio: TDBEdit
          Left = 96
          Top = 88
          Width = 65
          Height = 21
          DataField = '06_JUNIO'
          DataSource = DatosF.ProductosSource
          TabOrder = 5
        end
        object Julio: TDBEdit
          Left = 168
          Top = 88
          Width = 65
          Height = 21
          DataField = '07_JULIO'
          DataSource = DatosF.ProductosSource
          TabOrder = 6
        end
        object Agosto: TDBEdit
          Left = 240
          Top = 88
          Width = 65
          Height = 21
          DataField = '08_AGOSTO'
          DataSource = DatosF.ProductosSource
          TabOrder = 7
        end
        object Septiembre: TDBEdit
          Left = 24
          Top = 136
          Width = 65
          Height = 21
          DataField = '09_SEPTIEMBRE'
          DataSource = DatosF.ProductosSource
          TabOrder = 8
        end
        object Octubre: TDBEdit
          Left = 96
          Top = 136
          Width = 65
          Height = 21
          DataField = '10_OCTUBRE'
          DataSource = DatosF.ProductosSource
          TabOrder = 9
        end
        object Noviembre: TDBEdit
          Left = 168
          Top = 136
          Width = 65
          Height = 21
          DataField = '11_NOVIEMBRE'
          DataSource = DatosF.ProductosSource
          TabOrder = 10
        end
        object Diciembre: TDBEdit
          Left = 240
          Top = 136
          Width = 65
          Height = 21
          DataField = '12_DICIEMBRE'
          DataSource = DatosF.ProductosSource
          TabOrder = 11
        end
        object Total: TDBEdit
          Left = 136
          Top = 168
          Width = 73
          Height = 22
          Color = clInactiveCaptionText
          DataField = 'TOTAL'
          DataSource = DatosF.ProductosSource
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 12
        end
      end
    end
  end
end
