//---------------------------------------------------------------------------

#ifndef CubasCH
#define CubasCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "PlantillaC.h"
#include <Buttons.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TCubasF : public TPlantilla
{
__published:	// IDE-managed Components
        TDBEdit *Cuba1;
        TDBEdit *Cuba2;
        TDBEdit *Cuba3;
        TDBEdit *Cuba4;
        TDBEdit *Cuba5;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall CerrarClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TCubasF(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TCubasF *CubasF;
//---------------------------------------------------------------------------
#endif
