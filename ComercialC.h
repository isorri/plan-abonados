//---------------------------------------------------------------------------

#ifndef ComercialCH
#define ComercialCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "PlantillaC.h"
#include <Buttons.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TComercialF : public TPlantilla
{
__published:	// IDE-managed Components
        TDBGrid *ListaIzq;
        TGroupBox *DatosGroup;
        TLabel *Label1;
        TDBEdit *Codigo;
        TLabel *Label2;
        TDBEdit *Nombre;
        TButton *Cancelar;
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall NuevoClick(TObject *Sender);
        void __fastcall GuardarClick(TObject *Sender);
        void __fastcall CancelarClick(TObject *Sender);
        void __fastcall EliminarClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TComercialF(TComponent* Owner);
        void bloqueaFormulario(void);
        void desbloqueaFormulario(void);
};
//---------------------------------------------------------------------------
extern PACKAGE TComercialF *ComercialF;
//---------------------------------------------------------------------------
#endif
