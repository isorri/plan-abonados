//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "BuscarClienteC.h"
#include "FincasC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PlantillaC"
#pragma resource "*.dfm"
TBuscarClienteF *BuscarClienteF;
//---------------------------------------------------------------------------
__fastcall TBuscarClienteF::TBuscarClienteF(TComponent* Owner)
        : TPlantilla(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TBuscarClienteF::CerrarClick(TObject *Sender)
{
   Close();
}
//---------------------------------------------------------------------------
void __fastcall TBuscarClienteF::FormClose(TObject *Sender,
      TCloseAction &Action)
{
        //Habilitar de nuevo el formulario fincas
        FincasF->Enabled = true;

        //Cerrar la consulta
        BuscarCliente->Close();

        Action = caFree;
        BuscarClienteF = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TBuscarClienteF::BusquedaBClick(TObject *Sender)
{
    //Filtrar los resultados de la b�squeda de clientes
    BuscarCliente->Close();
    BuscarCliente->SQL->Clear();
    BuscarCliente->SQL->Add("SELECT CLI.CODCLI,CLI.NOMBRE, COM.NOMBRE");
    BuscarCliente->SQL->Add("FROM CLIENTES CLI, COMERCIALES COM");
    BuscarCliente->SQL->Add("WHERE CLI.CODCOMERCIAL = COM.CODCOMERCIAL");
    if (ClienteText->Text != "")
        BuscarCliente->SQL->Add("AND CLI.NOMBRE LIKE '%" + ClienteText->Text + "%' ");
    if (!TodosFlag->Checked)
        BuscarCliente->SQL->Add("AND COM.CODCOMERCIAL = " + ComercialLista->KeyValue);
    BuscarCliente->Open();
}
//---------------------------------------------------------------------------

void __fastcall TBuscarClienteF::ResultadoTableDblClick(TObject *Sender)
{
   if (DatosF->Fincas->State == dsBrowse)
        //Activar el modo edicion
        DatosF->Fincas->Edit();

   //Cargar el cliente en el registro finca
   DatosF->Fincas->FieldValues["CODCLI"] = BuscarCliente->FieldValues["CODCLI"];

   //Una vez seleccionamos el cliente, cerramos el formulario de busqueda
   Close();
}
//---------------------------------------------------------------------------
void __fastcall TBuscarClienteF::TodosFlagClick(TObject *Sender)
{
   //Habilitar o deshabilitar la lista en funcion del flag Todos.
   ComercialLista->Enabled = !TodosFlag->Checked;
}
//---------------------------------------------------------------------------
void __fastcall TBuscarClienteF::ResultadoTableKeyPress(TObject *Sender,
      char &Key)
{
    if (Key == VK_RETURN)
        ResultadoTableDblClick(Sender);
}
//---------------------------------------------------------------------------

