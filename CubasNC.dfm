inherited CubasNF: TCubasNF
  Left = 83
  Top = 105
  Width = 268
  Height = 255
  Caption = 'Capacidad Cubas Nodrizas'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Label2: TLabel
    Left = 16
    Top = 24
  end
  inherited Label3: TLabel
    Left = 16
    Top = 56
  end
  inherited Label4: TLabel
    Left = 16
    Top = 88
  end
  inherited Label5: TLabel
    Left = 16
    Top = 120
  end
  inherited Label6: TLabel
    Left = 16
    Top = 152
  end
  inherited Cerrar: TBitBtn
    Left = 152
    Top = 184
  end
  inherited Cuba1: TDBEdit
    Left = 104
    Top = 24
    DataField = 'CAPCUBANOD_1'
  end
  inherited Cuba2: TDBEdit
    Left = 104
    Top = 56
    DataField = 'CAPCUBANOD_2'
  end
  inherited Cuba3: TDBEdit
    Left = 104
    Top = 88
    DataField = 'CAPCUBANOD_3'
  end
  inherited Cuba4: TDBEdit
    Left = 104
    Top = 120
    DataField = 'CAPCUBANOD_4'
  end
  inherited Cuba5: TDBEdit
    Left = 104
    Top = 152
    DataField = 'CAPCUBANOD_5'
  end
end
