//---------------------------------------------------------------------------

#include <vcl.h>
#include <time.h>
#include <math.h>
#pragma hdrstop

#include "AnualC.h"
#include "DatosC.h"
#include "MainC.h"
#include "BuscarFincaC.h"
#include "BuscarProductoC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PlantillaC"
#pragma resource "*.dfm"
TAbonadoAnualF *AbonadoAnualF;
//---------------------------------------------------------------------------
__fastcall TAbonadoAnualF::TAbonadoAnualF(TComponent* Owner)
        : TPlantilla(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::FormCreate(TObject *Sender)
{
    // Variables locales
    time_t t;
    struct tm * petm;

    //Habilitar la lista de selecci�n izquierda
    ListaIzq->Enabled = true;

    //Cargar el a�o actual en el filtro
    time(&t);
    petm = localtime(&t);
    FiltroAnyo->Text = (AnsiString) (petm->tm_year + 1900);

    //Filtrar por a�o.
    DatosF->Anual->Filter = "ANYO = " + (AnsiString) FiltroAnyo->Text;
    DatosF->Anual->Filtered = true;

    //Cargar analisis
    CargarAnalisis();
    //Abrir consulta con posibles Acidos Fosforicos
    ProductosAF->Open();
}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   TDataSetState State;
   int res;
        State = DatosF->Anual->State;
        if (State == dsInsert)
        {
            res = Application->MessageBoxA("Registro pendiente de inserci�n.\n�Guardar datos?.","Pregunta",MB_ICONQUESTION+MB_YESNO);
            if (res == IDYES)
                DatosF->Anual->Post();
            else
                DatosF->Anual->Cancel();
        }
        else
        {
            if (State == dsEdit)
            {
                res=Application->MessageBoxA("Registro con cambios no guardados.\n�Guardar cambios?","Pregunta",MB_ICONQUESTION+MB_YESNO);
                if (res == IDYES)
                    DatosF->Anual->Post();
                else
                    DatosF->Anual->Cancel();
            }
        }
   //Quitar el filtro de Anual
   DatosF->Anual->Filtered = false;
   DatosF->Anual->Filter   = "";
   //Cerrar consulta con posibles Acidos Fosforicos
   ProductosAF->Close();

   Action = caFree;
   AbonadoAnualF = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::CerrarClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::CancelarClick(TObject *Sender)
{
   TDataSetState State,Detalle;
   State = DatosF->Anual->State;
   Detalle = DatosF->DetAnual->State;

   if (State == dsInsert || State == dsEdit)
   {
        DatosF->Anual->Cancel();
        this->desbloqueaFormulario();
   }
   if (Detalle == dsEdit || Detalle == dsInsert)
   {
        DatosF->DetAnual->Cancel();
   }
}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::GuardarClick(TObject *Sender)
{
   TDataSetState State,Detalle;
   State = DatosF->Anual->State;
   Detalle = DatosF->DetAnual->State;

   //Recuperar Codigo
   if (State == dsInsert && CodAnual->Text == "")
       CodAnual->Text = DatosF->generaCodigo("ANY",Anyo->Text.SubString(3,2));

   DatosF->Anual->Post();
   if (Detalle != dsBrowse)
        DatosF->DetAnual->Post();
}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::InsertarClick(TObject *Sender)
{
   TDataSetState State;

   State = DatosF->Anual->State;

   if (State == dsEdit)
        Application->MessageBoxA("Cambios no guardados.\nPulse Guardar o Cancelar.","Error",MB_ICONERROR);
   else
        if (State == dsInsert)
            Application->MessageBoxA("No ha guardado el nuevo Plan Anual.\nPulse Guardar o Cancelar","Error",MB_ICONERROR);
        else
        {
            time_t t;
            struct tm * petm;
            DatosF->Anual->Insert();
            //Por defector cargar el a�o actual
            time(&t);
            petm = localtime(&t);
            DatosF->Anual->FieldValues["ANYO"] = (AnsiString) (petm->tm_year + 1900);
            DatosF->Anual->FieldValues["IMPRESO"] = false;
            this->bloqueaFormulario();
            //Mostrar formulario de b�squeda fincas
            this->BuscaFincaBClick(this);
        }
}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::EliminarClick(TObject *Sender)
{
   TDataSetState State;
   State = DatosF->Anual->State;

   if (DatosF->SectorMes->Locate("CODANUAL",CodAnual->Text, TLocateOptions()))
   {
       Application->MessageBoxA("No se puede borrar el plan anual porque tiene alg�n plan mensual asociado.\nBorre primero el plan mensual.","Error",MB_ICONERROR);
       return;
   }
   if (State == dsBrowse)
       DatosF->Anual->Delete();
   else
       Application->MessageBoxA("Imposible borrar este registro.\nPulse Guardar o Cancelar.","Error",MB_ICONERROR);
}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::AnyadirClick(TObject *Sender)
{
    anyadirProducto(Articulos->KeyValue);
}
//---------------------------------------------------------------------------
//Bloquea el formulario cuando se va a insertar un registro
void TAbonadoAnualF::bloqueaFormulario(void)
{
    //No se puede tener acceso a la lista izquierda
    ListaIzq->Enabled  = false;
    EliminarB->Enabled = false; //No se puede eliminar

    //Deshabilito el acceso a los diferentes botones.
    EliminarB->Enabled   = false;
    Anyadir->Enabled     = false;
    CalcCtd->Enabled     = false;
    Reparto->Enabled     = false;
    ImprimeB->Enabled    = false;
    PrintFincaB->Enabled = false;
    Seleccionar->Enabled = false;
    NuevoB->Enabled      = false;
    EliminaAF->Enabled   = false;
    BuscaProductoB->Enabled = false;
    //Habilitar
    GuardarB->Enabled    = true;
    Cancelar->Enabled    = true;
}
//---------------------------------------------------------------------------
//Devuelve el formulario a su estado original
void TAbonadoAnualF::desbloqueaFormulario(void)
{
    //Habilito de nuevo selecci�n desde lista izquierda y la barra
    ListaIzq->Enabled   = true;

    //Habilito el acceso a los diferentes botones.
    EliminarB->Enabled   = true;
    Anyadir->Enabled     = true;
    CalcCtd->Enabled     = true;
    Reparto->Enabled     = true;
    ImprimeB->Enabled    = true;
    PrintFincaB->Enabled = true;
    Seleccionar->Enabled = true;
    NuevoB->Enabled      = true;
    EliminaAF->Enabled   = true;
    BuscaProductoB->Enabled = true;

    //Deshabilitar guardar y cancelar
    GuardarB->Enabled = false;
    Cancelar->Enabled = false;

}
/*-------------   C�lculo de necesidades a partir de an�lisis ----------------*/
void __fastcall TAbonadoAnualF::CalcAnalisisClick(TObject *Sender)
{
    TADOTable * Coef;
    bool hayAnalisis;//�Hay un analisis disponbile para la estimaci�n?
    //Nombre del campo de los elementos a estimar en ...
    AnsiString labelCoef [8]; // ... la tabla de coeficientes
    AnsiString labelForm [8]; // ... el formulario y tabla analisis.
    Variant sectorkey[2];
    /* true -> modo edicion iniciado por este boton
    false -> el registro ya estaba en modo edicion*/
    bool modoEdicion = false;


    //�Hay dato base de estimaci�n?
    if (Base->Text == "")
    {
        Application->MessageBoxA("Dato base estimado no introducido","Error",MB_ICONERROR);
        Base->SetFocus();
        return;
    }
    //Capturar las excepciones para saber si hay que guardar o cancelar los cambios.
    try
    {
        double c1,c2,analisis,base,resultado,aportaAgua;
        Coef = DatosF->Coeficientes;
        //Cargar Finca y Sector
        Fincas->Open();
        Sectores->Open();
        if ( ! Fincas->Locate("CODFINCA", CodFinca->Text , TLocateOptions()) )
           throw Exception("Error al cargar la finca " + CodFinca->Text);
        //Cargar el sector asociado al plan
        sectorkey[0] = CodFinca->Text;
        sectorkey[1] = Sector->Text;
        if ( !  Sectores->Locate("CODFINCA;SECTOR",VarArrayOf(sectorkey,1), TLocateOptions()) )
           throw Exception("Error al cargar el sector " + Sector->Text + " de la finca " + CodFinca->Text);
        if (MainF->pruebas)
           ShowMessage("Cargado sector " + Sectores->FieldValues["SECTOR"] + " de la finca " + Sectores->FieldValues["CODFINCA"]);
        //Cargar los coeficientes asociados al sector.
        if ( !  Coef->Locate("PATRON",Sectores->FieldValues["PATRON"],TLocateOptions()) )
           throw Exception("Error al cargar los coeficientes " + Sectores->FieldValues["PATRON"]);

        base  = Base->Text.ToDouble();
        //Inicializar etiquetas
        labelCoef[0] = labelForm[0] = "N";
        labelCoef[1] = labelForm[1] = "P";
        labelCoef[2] = labelForm[2] = "K";
        labelCoef[3] = labelForm[3] = "CA";
        labelCoef[4] = labelForm[4] = "MG";
        labelCoef[5] = labelForm[5] = "FE";
        labelCoef[6] = "MICRO";
        labelForm[6] = "MICROELEMENTOS";
        labelCoef[7] = "AH";
        labelForm[7] = "AHUMICO";
        //�Hay un analisis disponible?
        hayAnalisis = !Analisis->IsEmpty() && !AnalisisTipo->Checked;
        // En Analisis tengo cargado el an�lisis (finca o sector) en caso de que lo hubiese.
        //Si el flag analisis tipo esta marcado, utilizar el analisis tipo.

        if (DatosF->Anual->State == dsBrowse)
        {
            DatosF->Anual->Edit();
            modoEdicion = true;
        }
        /*
            Si hay un analisis foliar disponible:
                Necesidades = (DatoBase/100)* C1 * (C2/Analisis)
            Si no hay analisis foliar disponible:
                Necesidades = (DatosBase/100) * C1 [ * (C2/C2)]
                Se toma como analisis tipo los parametros introducidos en coeficientes
        */

        for (int i = 0; i< 8; i++)
        {
            resultado = 0.0;
            if(!Coef->FieldValues[labelCoef[i]+"_C1"].IsNull() && !Coef->FieldValues[labelCoef[i]+"_C2"].IsNull())
            {
               c1 = Coef->FieldValues[labelCoef[i]+"_C1"];
               c2 = Coef->FieldValues[labelCoef[i]+"_C2"];
               if(hayAnalisis && Analisis->FieldValues[labelForm[i]] != 0 && !Analisis->FieldValues[labelForm[i]].IsNull())
                  //Si hay analisis y esta el valor introducido
                  analisis = Analisis->FieldValues[labelForm[i]];
               else
                  //Hay algun problema con el analisis del elemento
                  analisis = c2;
               resultado = (base /100) * c1 * (c2/analisis);
               //Tener en cuenta la aportacion por medio del agua
               if (!Fincas->FieldValues[labelForm[i]].IsNull())
               {
                  /*Por defecto se estima que se utilizaran como media
                    5000 m3 por hectarea y a�o.
                    Luego hay que pasar a L/arbol,aplicar la riqueza y
                    el 1000 es para pasar de mg/L a gr/L.*/
                  if (Sectores->FieldValues["ARBOLNUM"] != 0)
                  {
                     aportaAgua  = 5000 * (double)Sectores->FieldValues["HG"] / (double)Sectores->FieldValues["ARBOLNUM"];//Litros/arbol
                     aportaAgua  = (aportaAgua * (double)Fincas->FieldValues[labelForm[i]])/1000; //gr/arbol
                     if (MainF->pruebas)
                        ShowMessage ("Elemento: " +AnsiString(labelForm[i]) + ". Aportacion por agua:" + AnsiString((int)aportaAgua));
                     resultado -= aportaAgua;
                  }

               }
               else
                   if (MainF->pruebas)
                      ShowMessage ("Elemento: " +AnsiString(labelForm[i]) + " sin aportacion por agua.");
               if (resultado < 0)
                  resultado = 0.0;
            }
            else
               ShowMessage("No introducido coeficiente para elemento " + labelCoef[i]);

            //Utilizar 4 decimales
            DatosF->Anual->FieldValues[labelForm[i]] = DatosF->redondea(resultado,4);
        }

        //Componente salino -> es el doble que c�lcio.
        DatosF->Anual->FieldValues["CSALINO"] = 2 * DatosF->Anual->FieldValues["CA"];

        //Almacenamos la fecha del an�lisis utilizado.
        if (hayAnalisis)
        {
           ShowMessage("Analisis " + AnsiString(Analisis->FieldValues["FECHA"]) + " utilizado.");
           DatosF->Anual->FieldValues["FANALISIS"] = Analisis->FieldValues["FECHA"];
        }
        else
        {
           ShowMessage("Analisis tipo utilizado.");
           FAnalisis->Clear();
        }

        //Guardar los cambios efectuados en Anual.
        //En caso que estuviese en modo edicion ya, el usuario debera guardar o descartar los cambios.
        if (modoEdicion)
           DatosF->Anual->Post();
        //Cerrar las tablas utilizadas
        Fincas->Close();
        Sectores->Close();
    }
    catch (Exception &e)
    {
        if (modoEdicion)
            DatosF->Anual->Cancel();
        ShowMessage(e.Message);
    }
}
//---------------------------------------------------------------------------
//Calculo de cantidad de abono comercial a partir de necesidades.
void __fastcall TAbonadoAnualF::CalcCtdClick(TObject *Sender)
{
    int NArboles; //N�mero de arboles por sector
    float necesidades[9];//N�mero de elementos 9
    int NAbonos; //Abonos a tratar
    int NElementos = 9; //Numero de elementos a tener en cuenta
    float kilogramos;
    float porcentaje; //Porcentaje asociado al acido fosforico
    int a, e, enc, i, fin, cuenta;
    int lineaAF; //Linea en la que se encuentra el acido fosforico en la matriz.
    int hayAF; //En el plan anual esta gestionado el acido fosforico
    float ** composicion;
    AnsiString mensaje; //Mensaje a mostrar al usuario
    AnsiString labelCoef[8]; //Etiquetas asociadas a los elementos en la tabla coeficientes
    float maximo; //Maximo UF a aportar por elemento.
    Variant sectorkey [2];

    //Inicializar etiquetas
    labelCoef[0] = "N";
    labelCoef[1] = "P";
    labelCoef[2] = "K";
    labelCoef[3] = "CA";
    labelCoef[4] = "MG";
    labelCoef[5] = "FE";
    labelCoef[6] = "MICRO";
    labelCoef[7] = "AH";

    if (MainF->pruebas)
        Application->MessageBoxA("Modo depura errores activado","Informaci�n",MB_ICONINFORMATION);

    //Matriz composici�n NAbonos * (NElementos +2)
    NAbonos = DatosF->DetAnual->RecordCount;
    if ( ! NAbonos)
    {
        Application->MessageBoxA("Ning�n producto introducido.","Error",MB_ICONERROR);
        return;
    }
    composicion = (float **) malloc(NAbonos * sizeof(float *)) ;
    for (int i = 0; i< NAbonos ; i ++)
        composicion[i] = (float * ) malloc((NElementos + 2)*sizeof(float));
    lineaAF = -1;
    try
    {
        DatosF->Abono->BeginTrans();
        /*--------------------------------------------------------------------
                            COMPROBACIONES ACIDO FOSFORICO
        --------------------------------------------------------------------*/
        if ( (CTD_P->Text != "" && CTD_P->Text != "0") && Codigo_AF->Text == "")
            throw Exception("�cido Fosf�rico: Ha introducido un porcentaje pero no ha indicado el producto a utilizar.");
        if ( (CTD_P->Text == "" || CTD_P->Text == "0") && Codigo_AF->Text != "")
            throw Exception("�cido Fosf�rico: Porcentaje no indicado");
        hayAF = (CTD_P->Text != "" && CTD_P->Text != "0") && Codigo_AF->Text != "";

        /*--------------------------------------------------------------------
                                PREPARACI�N DE LOS DATOS
          --------------------------------------------------------------------*/

        //Cargar Finca y Sector
        Fincas->Open();
        Sectores->Open();
        Productos->Open();
        if ( ! Fincas->Locate("CODFINCA", CodFinca->Text , TLocateOptions()) )
           throw Exception("Error al cargar la finca " + CodFinca->Text);
        //Cargar el sector asociado al plan
        sectorkey[0] = CodFinca->Text;
        sectorkey[1] = Sector->Text;
        if ( !  Sectores->Locate("CODFINCA;SECTOR",VarArrayOf(sectorkey,1), TLocateOptions()) )
           throw Exception("Error al cargar el sector " + Sector->Text + " de la finca " + CodFinca->Text);
        if (MainF->pruebas)
           ShowMessage("Cargado sector " + Sectores->FieldValues["SECTOR"] + " de la finca " + Sectores->FieldValues["CODFINCA"]);
        //Cargar los coeficientes asociados al sector.
        if ( ! DatosF->Coeficientes->Locate("PATRON",Sectores->FieldValues["PATRON"],TLocateOptions()) )
           throw Exception("Error al cargar los coeficientes " + Sectores->FieldValues["PATRON"]);

        if ( Sectores->FieldValues["ARBOLNUM"].IsNull() || Sectores->FieldValues["ARBOLNUM"] == 0)
           throw Exception("Numero de arboles del sector nulo");
        NArboles = Sectores->FieldValues["ARBOLNUM"];
        if (MainF->pruebas)
            ShowMessage("Numero Arboles:" + AnsiString(NArboles));

        //Pasar los gramos de abono puro por arbol a kilogramos por sector
        // KgSector = NArboles * Necesidades / 1000
        necesidades[0] = NArboles * N->Text.ToDouble() / 1000; //Necesidades de N
        necesidades[1] = NArboles * P->Text.ToDouble() / 1000; //f�sforo
        necesidades[2] = NArboles * K->Text.ToDouble() / 1000; //Potasio
        necesidades[3] = NArboles * CA->Text.ToDouble() /1000; //C�lcio
        necesidades[4] = NArboles * MG->Text.ToDouble() / 1000; //Magnesio
        necesidades[5] = NArboles * FE->Text.ToDouble() / 1000; //Hierro
        necesidades[6] = NArboles * Micro->Text.ToDouble() / 1000; //Microelementos
        necesidades[7] = NArboles * AH->Text.ToDouble() / 1000; //Acido Humico
        necesidades[8] = NArboles * CSALINO->Text.ToDouble() / 1000; //Componente salino

        /*Mostrar el resultado de convertir las necesidades a Kg/Sector si estamos
                                en modo debug*/
        if (MainF->pruebas)
        {
            mensaje  = "Necesidades calculadas en Kg/Sector \n";
            mensaje += "N :" + AnsiString(necesidades[0]) + "\n";
            mensaje += "P :" + AnsiString(necesidades[1]) + "\n";
            mensaje += "K :" + AnsiString(necesidades[2]) + "\n";
            mensaje += "Ca:" + AnsiString(necesidades[3]) + "\n";
            mensaje += "Mg:" + AnsiString(necesidades[4]) + "\n";
            mensaje += "Fe:" + AnsiString(necesidades[5]) + "\n";
            mensaje += "ME:" + AnsiString(necesidades[6]) + "\n";
            mensaje += "AH:" + AnsiString(necesidades[7]) + "\n";
            mensaje += "CS:" + AnsiString(necesidades[8]) + "\n";
            ShowMessage(mensaje);
        }

        //M�ximo por elemento (c1 * NArboles * 3)/1000 Kg UF por sector.
        if(MainF->pruebas)
           ShowMessage("Comprobar si hay alguna necesidad que supera el maximo te�rico.\n Maximo = (c1*N�Arboles * 3)/1000 KG UF por sector");
        if (Maximo->Checked)
           for (int i = 0; i < 8 ; i++)
           {
               if (Sectores->FieldValues["ARBOLNUM"] != 0 && !DatosF->Coeficientes->FieldValues[labelCoef[i]+"_C1"].IsNull())
               {
                 //Esta introducido el campo Hg y no es 0 y
                 maximo = ( DatosF->Coeficientes->FieldValues[labelCoef[i]+"_C1"] * Sectores->FieldValues["ARBOLNUM"] * 3 ) /1000;
                 if (necesidades[i] > maximo)
                 {
                    mensaje  = labelCoef[i] + " reducido por m�ximo alcanzado.\n";
                    mensaje += "Anterior: " + AnsiString(necesidades[i]) + " Kg/Sector.\n";
                    mensaje += "Nuevo: " + AnsiString(maximo) + " Kg/Sector.";
                    necesidades[i] = maximo;
                    ShowMessage(mensaje);
                 }
               }
               else
               {
                 if(MainF->pruebas)
                    ShowMessage(labelCoef[i] + " no limitado al maximo por no tener Hg o tener nulo el coeficiente");
               }
           }

        //Rellenar matriz composici�n
        DatosF->DetAnual->First();
        i = 0;
        while (! DatosF->DetAnual->Eof)
        {
             //Buscar el art�culo
             if (! Productos->Locate("CODART",DatosF->DetAnual->FieldValues["CODART"],TLocateOptions()) )
                throw Exception("Error al cargar el art�culo" + DatosF->DetAnual->FieldValues["CODART"]);

             if (hayAF && DatosF->DetAnual->FieldValues["CODART"] == Codigo_AF->Text)
                //En la linea i-esima esta el producto asociado al acido fosforico
                lineaAF = i;

             if (Productos->FieldValues["ABONO"])
             {
                composicion[i][0] = Productos->FieldValues["N"];
                composicion[i][1] = Productos->FieldValues["P"];
                composicion[i][2] = Productos->FieldValues["K"];
                composicion[i][3] = Productos->FieldValues["CA"];
                composicion[i][4] = Productos->FieldValues["MG"];
                composicion[i][5] = Productos->FieldValues["FE"];
                composicion[i][6] = Productos->FieldValues["MICROELEMENTOS"];
                composicion[i][7] = Productos->FieldValues["AHUMICO"];
                composicion[i][8] = Productos->FieldValues["CSALINO"];
                composicion[i][NElementos + 1] = 0;
             }
             else
             {
                //Si no es un abono lo marco como ajustado
                composicion[i][NElementos + 1] = 1;
             }
             if (MainF->pruebas)
             {
                ShowMessage(AnsiString(DatosF->DetAnual->FieldValues["NOMCOMERCIAL"]) + " " + DatosF->DetAnual->FieldValues["NLINEA"]);
                if (lineaAF == i)
                   ShowMessage("Articulo asociado al acido fosforico");
             }
             composicion[i][NElementos] = DatosF->DetAnual->FieldValues["NLINEA"];
             DatosF->DetAnual->Next();
             i++;
        }
        /*--------------------------------------------------------------------
                                ACIDO FOSFORICO
        --------------------------------------------------------------------*/
        if (hayAF)
        {
            if (lineaAF == -1)
                throw Exception("Acido fosforico no introducido en el plan anual");
            porcentaje = CTD_P->Text.ToDouble();
            if (MainF->pruebas)
                ShowMessage("Porcentaje AF-->" + AnsiString(porcentaje));
            a = lineaAF; //Abono seleccionado es Acido fosforico
            composicion[a][NElementos + 1] = 1;//Marcamos el abono como ajustado
            e = 1; //Elemento a ajustar el fosforo
            //�Cuantos kilogramos son necesarios para aportar el porcentaje de fosforo indicado?
            if (composicion[a][e])
               kilogramos = necesidades[e] *  porcentaje / composicion[a][e];
            else
               throw Exception("ATENCION! El producto indicado como acido fosforico no contiene fosforo!!");
            if (MainF->pruebas)
                ShowMessage("Kilogramos AF " + AnsiString(kilogramos));
            //Restar las aportaciones
            for (int i = 0; i <NElementos; i ++)
                if(composicion[a][i])
                {
                   necesidades[i] -= kilogramos * composicion [a][i] / 100;
                   /*Si ya hemos aportado las necesidades e incluso de m�s,
                     no hace falta ajustar este elemento ya*/
                   if (necesidades[i] < 0)
                       necesidades[i] = 0;
                   if ((kilogramos * composicion [a][i] / 100) && MainF->pruebas)
                      ShowMessage("AF Aportacion al elemento " + AnsiString(i) + " de " + AnsiString(kilogramos * composicion [a][i] / 100));
                }
           //Almacenar resultado en la base de datos
           DatosF->Consulta->SQL->Clear();
           DatosF->Consulta->SQL->Add("UPDATE DETANUAL SET CANTIDAD = "+ AnsiString().FormatFloat("0",kilogramos));
           DatosF->Consulta->SQL->Add(" WHERE NLINEA = " + AnsiString(composicion[a][NElementos]));
           DatosF->Consulta->SQL->Add(" AND CODANUAL = '" + CodAnual->Text + "';");
           DatosF->Consulta->ExecSQL();
        }
        /*--------------------------------------------------------------------
                               AJUSTAR ABONOS COMERCIALES
          --------------------------------------------------------------------*/
        //Empezamos a ajustar
        fin = 0;
        while (!fin)
        {
            i = enc = 0;
            //Buscar candidato a ajustar
            while(!enc && i < NElementos)
            {
                //No ha sido ajustado todav�a es un posible candidato
                cuenta = 0;
                for (int j = 0; j <NAbonos; j++)
                     if(composicion[j][i] && !composicion[j][NElementos +1])
                     {
                        //Si realiza aportaci�n y no ha sido utilizado el abonod
                        cuenta += 1;
                        a = j;
                     }
                if (cuenta == 1)
                {
                    //Hemos encontrado un candidato
                    enc = 1;
                    e = i;
                    //Marcamos al candidato.
                    composicion[a][NElementos + 1] = 1;
                    if (MainF->pruebas)
                        ShowMessage("Seleccionada linea " + AnsiString(composicion[a][NElementos]));
                }
                i++;
            }
            //Hemos terminado el ajuste
            if (!enc)
                fin = 1;
            else
            {
                //�Cuantos kilogramos son necesarios?
                kilogramos = necesidades[e] *  100 / composicion[a][e];
                if (MainF->pruebas)
                    ShowMessage("Kilogramos " + AnsiString(kilogramos));
                //Restar las aportaciones
                for (int i = 0; i <NElementos; i ++)
                   if(composicion[a][i])
                   {
                     necesidades[i] -= kilogramos * composicion [a][i] / 100;
                     /*Si ya hemos aportado las necesidades e incluso de m�s,
                       no hace falta ajustar este elemento ya*/
                     if (necesidades[i] < 0)
                        necesidades[i] = 0;
                     if ((kilogramos * composicion [a][i] / 100) && MainF->pruebas)
                        ShowMessage("Aportacion al elemento " + AnsiString(i) + " de " + AnsiString(kilogramos * composicion [a][i] / 100));
                   }
                //Almacenar resultado en la base de datos
                DatosF->Consulta->SQL->Clear();
                DatosF->Consulta->SQL->Add("UPDATE DETANUAL SET CANTIDAD = "+ AnsiString().FormatFloat("0",kilogramos));
                DatosF->Consulta->SQL->Add(" WHERE NLINEA = " + AnsiString(composicion[a][NElementos]));
                DatosF->Consulta->SQL->Add(" AND CODANUAL = '" + CodAnual->Text +"';");
                DatosF->Consulta->ExecSQL();
            }
        }
        //Finaliza la transacci�n
        DatosF->Abono->CommitTrans();
        DatosF->DetAnual->Close();
        DatosF->DetAnual->Open();

        /*--------------------------------------------------------------------
                                MOSTRAR RESULTADO ESTIMACION
          --------------------------------------------------------------------*/
        //Antes de mostrar las necesidades pendientes al usuario, pasarlas de
        //kilogramos/sector a gr/arbol
        for (int i = 0;i<9;i++)
            necesidades[i] = necesidades[i] * 1000 / NArboles;

        //Mostrar al usuario las necesidades que han quedado pendiente.
        mensaje  = "";
        if ((int)necesidades[0])
           mensaje += "\nNitr�geno : " + AnsiString((int)necesidades[0]);
        if ((int)necesidades[1])
           mensaje += "\nF�sforo   : " + AnsiString((int)necesidades[1]);
        if ((int)necesidades[2])
           mensaje += "\nPot�sio   : " + AnsiString((int)necesidades[2]);
        if ((int)necesidades[3])
           mensaje += "\nC�lcio    : " + AnsiString((int)necesidades[3]);
        if ((int)necesidades[4])
           mensaje += "\nMagnesio  : " + AnsiString((int)necesidades[4]);
        if ((int)necesidades[5])
           mensaje += "\nHierro    : " + AnsiString((int)necesidades[5]);
        if ((int)necesidades[6])
           mensaje += "\nMicroelementos: " + AnsiString((int)necesidades[6]);
        if ((int)necesidades[7])
           mensaje += "\n�cido H�mico : " + AnsiString((int)necesidades[7]);
        if ((int)necesidades[8])
           mensaje += "\nComponente salino : " + AnsiString((int)necesidades[8]);
        if (mensaje != "")
           ShowMessage("Necesidades no aportadas (gr/arbol):" + mensaje);
        else
           ShowMessage("Todas las necesidades aportadas.");

    }
   catch(Exception &e)
   {
        DatosF->Abono->RollbackTrans();
        ShowMessage("No se puede realizar el c�lculo.\n" + e.Message);
   }
   //Cerrar las tablas utilizadas.
   Fincas->Close();
   Sectores->Close();
   Productos->Close();
   //Liberar memoria utilizada.
   for (int i=0; i< NAbonos; i ++)
       free(composicion[i]);
   free(composicion);
}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::EliminaProducto1Click(TObject *Sender)
{
     TDataSetState Detalle;
     Detalle = DatosF->DetAnual->State;
     if (Detalle == dsBrowse)
        DatosF->DetAnual->Delete();
     else
        Application->MessageBoxA("Cambios no guardados.\nPulse Guardar Ctd.","Error",MB_ICONERROR);
}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::FiltroAnyoChange(TObject *Sender)
{
    AnsiString filtro;
    filtro = "";

    if (FiltroAnyo->Text != "")
        //Hay un a�o introducido, filtramos por a�o
        filtro = filtro + "ANYO = " + (AnsiString) FiltroAnyo->Text;
    if (FiltroFinca->Text != "")
    {
        if (filtro != "")
            filtro = filtro + " AND "; //Para enlazar los dos criterios
        //Filtramos por finca
        filtro = filtro + "NOMFINCA LIKE '%" + (AnsiString) FiltroFinca->Text + "%'";
    }

    //Activamos el filtro si hay un a�o o un mes introducido
    DatosF->Anual->Filter   = filtro;
    DatosF->Anual->Filtered = FiltroAnyo->Text != "" || FiltroFinca->Text != "" ;


}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::RepartoClick(TObject *Sender)
{
    //Lanzar la pantalla de reparto anual
    MainF->RepartoMensual1Click(this);
}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::ImprimeClick(TObject *Sender)
{
    //Imprimir plan abonado actual
   if (DatosF->Anual->IsEmpty())
   {
        Application->MessageBoxA("Ning�n plan anual cargado.","Error",MB_ICONERROR);
        return;
   }
   try
   {
        //Empieza transacci�n.

        DatosF->Abono->BeginTrans();
        //Vaciar tabla de impresi�n
        DatosF->Consulta->SQL->Clear();
        DatosF->Consulta->SQL->Add("DELETE FROM IMPRESION;") ;
        DatosF->Consulta->ExecSQL();

        //Rellenar con el plan anual actual.
        DatosF->Consulta->SQL->Clear();
        DatosF->Consulta->SQL->Add("INSERT INTO IMPRESION ") ;
        DatosF->Consulta->SQL->Add(" (CODIGO) VALUES ");
        DatosF->Consulta->SQL->Add("('"+ CodAnual->Text +"');");
        DatosF->Consulta->ExecSQL();

        //Marcar el plan abonado actual, si  no esta marcado..
        if ( !DatosF->Anual->FieldValues["IMPRESO"])
        {
            DatosF->Anual->Edit();
            DatosF->Anual->FieldValues["IMPRESO"] = true;
            DatosF->Anual->Post();
        }
        //Finaliza transacci�n.
        DatosF->Abono->CommitTrans();
   }
   catch(Exception &e)
   {
        DatosF->Abono->RollbackTrans();
        ShowMessage("No se puede lanzar impresi�n.\n" + e.Message);
        return;
   }
   //Lanzamos la impresi�n
   MainF->Report->WindowButtonBar->PrintSetupBtn = true;
   MainF->Report->ReportName = "Anual.rpt";
   MainF->Report->WindowStyle->Title = "Plan Abonado Anual";
   MainF->Report->Show();
   if(MainF->Report->LastErrorNumber)
      //Ha habido alg�n error
      ShowMessage(AnsiString(MainF->Report->LastErrorNumber) +" " + MainF->Report->LastErrorString);
   MainF->Report->ReportName = "";
}
//---------------------------------------------------------------------------
void TAbonadoAnualF::CargarAnalisis(void)
{
    //Al cargar el analisis al crear el formulario, la consulta todav�a no esta preparada
    if (Analisis->Connection == NULL)
        return;

    if (CodFinca->Text != "" && Sector->Text != "")
    {
        Analisis->Close();
        Analisis->SQL->Clear();
        Analisis->SQL->Add("SELECT FECHA, N, P, K, CA, MG, FE, MICROELEMENTOS, AHUMICO");
        Analisis->SQL->Add("FROM ANALISIS_FINCA");
        Analisis->SQL->Add("WHERE CODFINCA = " + CodFinca->Text);
        Analisis->SQL->Add("UNION");
        Analisis->SQL->Add("SELECT FECHA, N, P, K, CA, MG, FE, MICROELEMENTOS, AHUMICO");
        Analisis->SQL->Add("FROM ANALISIS");
        Analisis->SQL->Add("WHERE CODFINCA = "+CodFinca->Text + " AND SECTOR = '"+Sector->Text+"'");
        Analisis->SQL->Add("ORDER BY 1");
        Analisis->Open();
    }
    else
    {
        Analisis->Close();
        Analisis->SQL->Clear();
        Analisis->SQL->Add("SELECT FECHA, N, P, K, CA, MG, FE, MICROELEMENTOS, AHUMICO");
        Analisis->SQL->Add("FROM ANALISIS_FINCA");
        Analisis->SQL->Add("WHERE 1 = 0");
        Analisis->Open();
    }
}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::SeleccionarClick(TObject *Sender)
{
  TDataSetState Detalle;
  int maxlinea; //Numero de linea maximo
  int nlinea;   //Numero de linea a utilizar

   if (DatosF->Anual->IsEmpty())
   {
        Application->MessageBoxA("Ning�n plan anual cargado.","Error",MB_ICONERROR);
        return;
   }
   if (ListaAcidoFosforico->Text == "")
   {
        Application->MessageBoxA("Nada que a�adir","Error",MB_ICONERROR);
        return;
   }
   if (DatosF->Anual->State == dsBrowse)
   {
       DatosF->Anual->Edit();
       DatosF->Anual->FieldValues["CODIGO_AF"] = ListaAcidoFosforico->KeyValue;
       DatosF->Anual->Post();
       //A�adir al detalle el articulo seleccionado.
       anyadirProducto(ListaAcidoFosforico->KeyValue);
    }
    else
    {
        Application->MessageBoxA("Cambios no guardados.\nPulse Guardar.","Error",MB_ICONERROR);
        return;
    }
}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::EliminaAFClick(TObject *Sender)
{
   TDataSetState Detalle;

   if (DatosF->Anual->IsEmpty())
   {
        Application->MessageBoxA("Ning�n plan anual cargado.","Error",MB_ICONERROR);
        return;
   }
    Detalle = DatosF->DetAnual->State;
    if (Detalle == dsBrowse)
    {
       DatosF->DetAnual->First();
       while (!DatosF->DetAnual->Eof)
       {
           if (DatosF->DetAnual->FieldValues["CODART"] == Codigo_AF->Text)
               DatosF->DetAnual->Delete();
           DatosF->DetAnual->Next();
       }
       if (DatosF->Anual->State == dsBrowse)
       {
           DatosF->Anual->Edit();
           Codigo_AF->Text = "";
           CTD_P->Text = "";
           DatosF->Anual->Post();
       }
       else
       {
           //El registro cabecera esta en modo edicion ya
           Codigo_AF->Text = "";
           CTD_P->Text = "";
       }
    }
    else
        Application->MessageBoxA("Cambios no guardados.\nPulse Guardar.","Error",MB_ICONERROR);

}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::BuscaFincaBClick(TObject *Sender)
{
   if (BuscarFincaF == NULL)
   {
       BuscarFincaF = new TBuscarFincaF(Application);
       BuscarFincaF->ClienteText->SetFocus();
       BuscarFincaF->BuscarFinca->Open();
   }
   this->Enabled = false;
   BuscarFincaF->Show();
}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::CodFincaChange(TObject *Sender)
{
    CargarAnalisis();
}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::SectorChange(TObject *Sender)
{
    CargarAnalisis();
}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::BuscaProductoBClick(TObject *Sender)
{
    if (BuscarProductoF == NULL)
    {
        BuscarProductoF = new TBuscarProductoF(Application);
    }
    this->Enabled = false;
    BuscarProductoF->Show();

}
//---------------------------------------------------------------------------
void __fastcall TAbonadoAnualF::anyadirProducto(AnsiString codigo)
{

    TDataSetState Detalle;
    Detalle = DatosF->DetAnual->State;
    int maxlinea; //Numero de linea maximo
    int nlinea;   //Numero de linea a utilizar

    if (Detalle == dsBrowse)
    {
       try
       {
           //Recuperar el producto a anyadir
           Productos->Open();
           if ( ! Productos->Locate("CODART", codigo, TLocateOptions()) )
              throw Exception("Error al cargar el producto " + codigo);

           //Recuperar el n�mero de l�nea a utilizar
           DatosF->DetAnual->First();
           maxlinea = 0;
           while (!DatosF->DetAnual->Eof)
           {
                 nlinea = DatosF->DetAnual->FieldValues["NLINEA"];
                 if (nlinea > maxlinea)
                     maxlinea = nlinea;
                 DatosF->DetAnual->Next();
           }
           //Numero de linea a utilizar el siguiente al m�ximo
           nlinea = maxlinea + 1;
           //Crear producto
           DatosF->DetAnual->Insert();
           DatosF->DetAnual->FieldValues["CODANUAL"]     = CodAnual->Text;
           DatosF->DetAnual->FieldValues["CODART"]       = codigo;
           DatosF->DetAnual->FieldValues["NLINEA"]       = nlinea;
           DatosF->DetAnual->FieldValues["NOMCOMERCIAL"] = Productos->FieldValues["NOMCOMERCIAL"];
           DatosF->DetAnual->FieldValues["CANTIDAD"]     = 0;
           DatosF->DetAnual->FieldValues["01_ENERO"]     = Productos->FieldValues["01_ENERO"];
           DatosF->DetAnual->FieldValues["02_FEBRERO"]   = Productos->FieldValues["02_FEBRERO"];
           DatosF->DetAnual->FieldValues["03_MARZO"]     = Productos->FieldValues["03_MARZO"];
           DatosF->DetAnual->FieldValues["04_ABRIL"]     = Productos->FieldValues["04_ABRIL"];
           DatosF->DetAnual->FieldValues["05_MAYO"]      = Productos->FieldValues["05_MAYO"];
           DatosF->DetAnual->FieldValues["06_JUNIO"]     = Productos->FieldValues["06_JUNIO"];
           DatosF->DetAnual->FieldValues["07_JULIO"]     = Productos->FieldValues["07_JULIO"];
           DatosF->DetAnual->FieldValues["08_AGOSTO"]    = Productos->FieldValues["08_AGOSTO"];
           DatosF->DetAnual->FieldValues["09_SEPTIEMBRE"]= Productos->FieldValues["09_SEPTIEMBRE"];
           DatosF->DetAnual->FieldValues["10_OCTUBRE"]   = Productos->FieldValues["10_OCTUBRE"];
           DatosF->DetAnual->FieldValues["11_NOVIEMBRE"] = Productos->FieldValues["11_NOVIEMBRE"];
           DatosF->DetAnual->FieldValues["12_DICIEMBRE"] = Productos->FieldValues["12_DICIEMBRE"];
           DatosF->DetAnual->Post();
           Productos->Close();
         }
         catch (Exception &e)
         {
           if (DatosF->DetAnual->State != dsBrowse)
               DatosF->DetAnual->Cancel();
           Productos->Close();
           ShowMessage(e.Message);
         }
    }
    else
        Application->MessageBoxA("Cambios no guardados.\nPulse Guardar Ctd.","Error",MB_ICONERROR);
}
//---------------------------------------------------------------------------

void __fastcall TAbonadoAnualF::PrintFincaBClick(TObject *Sender)
{
   //Imprimir plan abonado actual
   if (DatosF->Anual->IsEmpty())
   {
        Application->MessageBoxA("Ning�n plan anual cargado.","Error",MB_ICONERROR);
        return;
   }
   try
   {
        //Empieza transacci�n.

        DatosF->Abono->BeginTrans();
        //Vaciar tabla de impresi�n
        DatosF->Consulta->SQL->Clear();
        DatosF->Consulta->SQL->Add("DELETE FROM IMPRESION;") ;
        DatosF->Consulta->ExecSQL();

        //Rellenar con el plan anual actual.
        DatosF->Consulta->SQL->Clear();
        DatosF->Consulta->SQL->Add("INSERT INTO IMPRESION ") ;
        DatosF->Consulta->SQL->Add("(CODIGO) ");
        DatosF->Consulta->SQL->Add("SELECT CODANUAL ");
        DatosF->Consulta->SQL->Add("FROM ANUAL WHERE CODFINCA = ");
        DatosF->Consulta->SQL->Add(CodFinca->Text + ";");
        //DatosF->Consulta->SQL->Add( CodFinca->Text + "AND ANYO =");
        //DatosF->Consulta->SQL->Add( Anyo->Text + ";");
        DatosF->Consulta->ExecSQL();

        //Marcar los planes abonado seleccionado como impresos
        DatosF->Consulta->SQL->Clear();
        DatosF->Consulta->SQL->Add("UPDATE ANUAL SET IMPRESO = true WHERE CODFINCA = ");
        DatosF->Consulta->SQL->Add(CodFinca->Text);
        //DatosF->Consulta->SQL->Add( CodFinca->Text + "AND ANYO =");
        //DatosF->Consulta->SQL->Add( Anyo->Text);
        DatosF->Consulta->SQL->Add(" AND not IMPRESO ;");
        DatosF->Consulta->ExecSQL();

        //Finaliza transacci�n.
        DatosF->Abono->CommitTrans();
   }
   catch(Exception &e)
   {
        DatosF->Abono->RollbackTrans();
        ShowMessage("No se puede lanzar impresi�n.\n" + e.Message);
        return;
   }
   //Lanzamos la impresi�n
   MainF->Report->WindowButtonBar->PrintSetupBtn = true;
   MainF->Report->ReportName = "AnualFinca.rpt";
   MainF->Report->WindowStyle->Title = "Resumen anual por finca";
   MainF->Report->Show();
   if(MainF->Report->LastErrorNumber)
      //Ha habido alg�n error
      ShowMessage(AnsiString(MainF->Report->LastErrorNumber) +" " + MainF->Report->LastErrorString);
   MainF->Report->ReportName = "";
}
//---------------------------------------------------------------------------

