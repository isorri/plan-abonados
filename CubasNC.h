//---------------------------------------------------------------------------

#ifndef CubasNCH
#define CubasNCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CubasC.h"
#include <Buttons.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TCubasNF : public TCubasF
{
__published:	// IDE-managed Components
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
        __fastcall TCubasNF(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TCubasNF *CubasNF;
//---------------------------------------------------------------------------
#endif
