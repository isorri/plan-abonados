//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DistMensualC.h"
#include "DatosC.h"
#include "MainC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PlantillaC"
#pragma resource "*.dfm"
TDistMensualF *DistMensualF;
//---------------------------------------------------------------------------
__fastcall TDistMensualF::TDistMensualF(TComponent* Owner)
        : TPlantilla(Owner)
{

}
//---------------------------------------------------------------------------
void __fastcall TDistMensualF::CerrarClick(TObject *Sender)
{
   Close();
}
//---------------------------------------------------------------------------
void __fastcall TDistMensualF::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   TDataSetState  State;
   TADOTable * Tabla;
   int res;
   State = DatosF->DetAnual->State;
   Tabla = DatosF->DetAnual;
   if (State == dsEdit)
   {
      res=Application->MessageBoxA("Registro con cambios no guardados.\n�Guardar cambios?","Pregunta",MB_ICONQUESTION+MB_YESNO);
      if (res == IDYES)
         Tabla->Post();
      else
         Tabla->Cancel();
       }

   Action = caFree;
   DistMensualF = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TDistMensualF::GuardarClick(TObject *Sender)
{
   TDataSetState State;
   State = DatosF->DetAnual->State;

   if (State == dsEdit)
        DatosF->DetAnual->Post();
}
//---------------------------------------------------------------------------

void __fastcall TDistMensualF::CancelarClick(TObject *Sender)
{
   TDataSetState State;
   State = DatosF->DetAnual->State;

   if (State == dsEdit)
   {
        DatosF->DetAnual->Cancel();
   }
}
//---------------------------------------------------------------------------



void __fastcall TDistMensualF::AplicarClick(TObject *Sender)
{
    //Cargar plantilla de reparto en el formulario
   TDataSetState State;
   State = DatosF->DetAnual->State;

   if(Plantilla->Text == "")
   {
      Application->MessageBoxA("Ninguna plantilla a aplicar\n","Error",MB_ICONERROR);
      return;
   }

   if (State != dsEdit && State != dsInsert)
        DatosF->DetAnual->Edit();

   DatosF->DetAnual->FieldValues["01_ENERO"] = DatosF->Reparto->FieldValues["01_ENERO"];
   DatosF->DetAnual->FieldValues["02_FEBRERO"] = DatosF->Reparto->FieldValues["02_FEBRERO"];
   DatosF->DetAnual->FieldValues["03_MARZO"] = DatosF->Reparto->FieldValues["03_MARZO"];
   DatosF->DetAnual->FieldValues["04_ABRIL"] = DatosF->Reparto->FieldValues["04_ABRIL"];
   DatosF->DetAnual->FieldValues["05_MAYO"] = DatosF->Reparto->FieldValues["05_MAYO"];
   DatosF->DetAnual->FieldValues["06_JUNIO"] = DatosF->Reparto->FieldValues["06_JUNIO"];
   DatosF->DetAnual->FieldValues["07_JULIO"] = DatosF->Reparto->FieldValues["07_JULIO"];
   DatosF->DetAnual->FieldValues["08_AGOSTO"] = DatosF->Reparto->FieldValues["08_AGOSTO"];
   DatosF->DetAnual->FieldValues["09_SEPTIEMBRE"] = DatosF->Reparto->FieldValues["09_SEPTIEMBRE"];
   DatosF->DetAnual->FieldValues["10_OCTUBRE"] = DatosF->Reparto->FieldValues["10_OCTUBRE"];
   DatosF->DetAnual->FieldValues["11_NOVIEMBRE"] = DatosF->Reparto->FieldValues["11_NOVIEMBRE"];
   DatosF->DetAnual->FieldValues["12_DICIEMBRE"] = DatosF->Reparto->FieldValues["12_DICIEMBRE"];

   DatosF->DetAnual->Post();


}
//---------------------------------------------------------------------------

void __fastcall TDistMensualF::PlantillaBtnClick(TObject *Sender)
{
   MainF->Plantillasreparto1Click(this);
}
//---------------------------------------------------------------------------

void __fastcall TDistMensualF::EliminarClick(TObject *Sender)
{
   TDataSetState State;
   int res;

   State = DatosF->DetAnual->State;

   if (State == dsBrowse)
   {
      res=Application->MessageBoxA("�Desea eliminar el producto del plan anual?","Pregunta",MB_ICONQUESTION+MB_YESNO);
      if (res == IDYES)
         DatosF->DetAnual->Delete();
   }
}
//---------------------------------------------------------------------------

