//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("MainC.cpp", MainF);
USEFORM("DatosC.cpp", DatosF); /* TDataModule: File Type */
USEFORM("ClienteC.cpp", ClienteF);
USEFORM("FincasC.cpp", FincasF);
USEFORM("AutorC.cpp", AboutBox);
USEFORM("PlantillaC.cpp", Plantilla);
USEFORM("AnalisisSectC.cpp", AnalisisSectF);
USEFORM("AnalisisFincaC.cpp", AnalisisFincaF);
USEFORM("AbonosC.cpp", ProductF);
USEFORM("CoeficientesC.cpp", CoeficienteF);
USEFORM("AnualC.cpp", AbonadoAnualF);
USEFORM("DistMensualC.cpp", DistMensualF);
USEFORM("RepartoC.cpp", RepartoF);
USEFORM("RiegoC.cpp", RiegoF);
USEFORM("RiegoPlantC.cpp", RiegoPlantF);
USEFORM("CreamesC.cpp", CreamesF);
USEFORM("MensualC.cpp", MensualF);
USEFORM("ComercialC.cpp", ComercialF);
USEFORM("ProvefeC.cpp", ProvefeF); /* TDataModule: File Type */
USEFORM("BuscarClienteC.cpp", BuscarClienteF);
USEFORM("BuscarFincaC.cpp", BuscarFincaF);
USEFORM("BuscarProductoC.cpp", BuscarProductoF);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Plan Abonados";
                 Application->CreateForm(__classid(TMainF), &MainF);
                 Application->CreateForm(__classid(TDatosF), &DatosF);
                 Application->CreateForm(__classid(TAboutBox), &AboutBox);
                 Application->CreateForm(__classid(TProvefeF), &ProvefeF);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        catch (...)
        {
                 try
                 {
                         throw Exception("");
                 }
                 catch (Exception &exception)
                 {
                         Application->ShowException(&exception);
                 }
        }
        return 0;
}
//---------------------------------------------------------------------------
