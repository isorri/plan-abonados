//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CubasNC.h"
#include "FincasC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CubasC"
#pragma resource "*.dfm"
TCubasNF *CubasNF;
//---------------------------------------------------------------------------
__fastcall TCubasNF::TCubasNF(TComponent* Owner)
        : TCubasF(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TCubasNF::CerrarClick(TObject *Sender)
{
        Close();
}
//---------------------------------------------------------------------------
void __fastcall TCubasNF::FormClose(TObject *Sender, TCloseAction &Action)
{
        //Habilitar de nuevo el formulario de fincas
        FincasF->Enabled = true;
        //Preparamos el formulario para eliminarlo.
        Action = caFree;
        CubasNF = NULL;        
}
//---------------------------------------------------------------------------

