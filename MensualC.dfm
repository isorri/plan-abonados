inherited MensualF: TMensualF
  Left = 275
  Top = 228
  Width = 852
  Height = 576
  Caption = 'Plan Mensual'
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label7: TLabel [0]
    Left = 8
    Top = 8
    Width = 56
    Height = 13
    Caption = 'Mostrar a'#241'o'
  end
  object Label8: TLabel [1]
    Left = 120
    Top = 8
    Width = 57
    Height = 13
    Caption = 'Mostrar mes'
  end
  object Label24: TLabel [2]
    Left = 224
    Top = 8
    Width = 61
    Height = 13
    Caption = 'Mostrar finca'
  end
  inherited Cerrar: TBitBtn
    Left = 752
    Top = 504
    TabOrder = 11
    OnClick = CerrarClick
  end
  inherited NuevoB: TBitBtn
    Left = 8
    Top = 488
    Width = 81
    TabOrder = 5
    Visible = False
  end
  inherited GuardarB: TBitBtn
    Left = 440
    Top = 504
    Enabled = False
    TabOrder = 7
    OnClick = GuardarClick
  end
  inherited EliminarB: TBitBtn
    Left = 680
    Top = 504
    Width = 63
    TabOrder = 9
    OnClick = EliminarBClick
  end
  object Cancelar: TButton
    Left = 520
    Top = 504
    Width = 75
    Height = 25
    Hint = 'Cancela los cambios efectuados y vuelve al plan anual guardado.'
    Caption = 'Cancelar'
    Enabled = False
    TabOrder = 8
    OnClick = CancelarClick
  end
  object ListaIzq: TDBGrid
    Left = 0
    Top = 32
    Width = 385
    Height = 449
    DataSource = DatosF.MensualSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CODMENSUAL'
        Title.Caption = 'C'#243'digo'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMFINCA'
        Title.Caption = 'Finca'
        Width = 197
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MES'
        Title.Caption = 'Mes'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QUINCENA'
        Title.Caption = 'Quincena'
        Width = 51
        Visible = True
      end>
  end
  object Paginas: TPageControl
    Left = 392
    Top = 96
    Width = 441
    Height = 401
    ActivePage = RiegoTab
    TabIndex = 0
    TabOrder = 4
    object RiegoTab: TTabSheet
      Caption = 'Riego'
      ImageIndex = 3
      object RiegoSector: TGroupBox
        Left = 128
        Top = 72
        Width = 305
        Height = 121
        Caption = 'Riego por sector'
        TabOrder = 0
        object Label4: TLabel
          Left = 16
          Top = 16
          Width = 111
          Height = 13
          Caption = 'Tiempo Riego (minutos)'
        end
        object Label5: TLabel
          Left = 16
          Top = 40
          Width = 44
          Height = 13
          Caption = '% Bomba'
        end
        object Label3: TLabel
          Left = 16
          Top = 64
          Width = 114
          Height = 13
          Caption = 'Tiempo Abono (minutos)'
        end
        object Label11: TLabel
          Left = 16
          Top = 88
          Width = 70
          Height = 13
          Caption = 'Agua estimada'
        end
        object TRiego: TDBEdit
          Left = 136
          Top = 16
          Width = 121
          Height = 21
          Hint = 'Tiempo estimado duraci'#243'n de cada riego'
          DataField = 'TRIEGO'
          DataSource = DatosF.SectorMesSource
          TabOrder = 0
        end
        object Bomba: TDBEdit
          Left = 136
          Top = 40
          Width = 121
          Height = 21
          Hint = 'Porcentaje utilizaci'#243'n de la bomba inyectora'
          DataField = 'BOMBA'
          DataSource = DatosF.SectorMesSource
          TabOrder = 1
        end
        object TAbono: TDBEdit
          Left = 136
          Top = 64
          Width = 121
          Height = 21
          Hint = 'Tiempo estimado duraci'#243'n aplicaci'#243'n del abono por riego'
          Color = clInactiveCaption
          DataField = 'TABONO'
          DataSource = DatosF.SectorMesSource
          TabOrder = 2
        end
        object AguaSector: TDBEdit
          Left = 136
          Top = 88
          Width = 121
          Height = 21
          Hint = 'Agua necesaria para aplicar el abono al sector'
          Color = clInactiveCaption
          DataField = 'AGUA'
          DataSource = DatosF.SectorMesSource
          TabOrder = 3
        end
      end
      object ListaSectores: TDBGrid
        Left = 0
        Top = 80
        Width = 121
        Height = 273
        DataSource = DatosF.SectorMesSource
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SECTOR'
            Title.Caption = 'Sector'
            Visible = True
          end>
      end
      object DiasRiego: TGroupBox
        Left = 128
        Top = 192
        Width = 305
        Height = 105
        Hint = 'D'#237'as de riego del mes'
        Caption = 'D'#237'as  de riego'
        TabOrder = 2
        object Lunes: TDBCheckBox
          Left = 8
          Top = 24
          Width = 65
          Height = 17
          Caption = 'Lunes'
          DataField = 'LUNES'
          DataSource = DatosF.SectorMesSource
          TabOrder = 0
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
        object Martes: TDBCheckBox
          Left = 88
          Top = 24
          Width = 65
          Height = 17
          Caption = 'Martes'
          DataField = 'MARTES'
          DataSource = DatosF.SectorMesSource
          TabOrder = 1
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
        object Miercoles: TDBCheckBox
          Left = 192
          Top = 24
          Width = 73
          Height = 17
          Caption = 'Mi'#233'rcoles'
          DataField = 'MIERCOLES'
          DataSource = DatosF.SectorMesSource
          TabOrder = 2
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
        object Jueves: TDBCheckBox
          Left = 8
          Top = 48
          Width = 65
          Height = 17
          Caption = 'Jueves'
          DataField = 'JUEVES'
          DataSource = DatosF.SectorMesSource
          TabOrder = 3
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
        object Viernes: TDBCheckBox
          Left = 88
          Top = 48
          Width = 65
          Height = 17
          Caption = 'Viernes'
          DataField = 'VIERNES'
          DataSource = DatosF.SectorMesSource
          TabOrder = 4
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
        object Sabado: TDBCheckBox
          Left = 192
          Top = 48
          Width = 65
          Height = 17
          Caption = 'S'#225'bado'
          DataField = 'SABADO'
          DataSource = DatosF.SectorMesSource
          TabOrder = 5
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
        object Domingo: TDBCheckBox
          Left = 88
          Top = 72
          Width = 65
          Height = 17
          Caption = 'Domingo'
          DataField = 'DOMINGO'
          DataSource = DatosF.SectorMesSource
          TabOrder = 6
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
      end
      object Riego: TGroupBox
        Left = 0
        Top = 4
        Width = 433
        Height = 69
        Caption = 'Datos de riego  '
        TabOrder = 3
        object Label2: TLabel
          Left = 16
          Top = 16
          Width = 43
          Height = 13
          Caption = 'Llenados'
        end
        object Label6: TLabel
          Left = 176
          Top = 16
          Width = 83
          Height = 13
          Caption = 'D'#237'as de abonado'
        end
        object Label9: TLabel
          Left = 272
          Top = 16
          Width = 64
          Height = 13
          Caption = 'D'#237'as de riego'
        end
        object Label10: TLabel
          Left = 96
          Top = 16
          Width = 70
          Height = 13
          Caption = 'Agua estimada'
        end
        object Llenados: TDBEdit
          Left = 16
          Top = 32
          Width = 73
          Height = 21
          Hint = 'N'#186' llenados de la/s cuba/s de disoluci'#243'n'
          Color = clInactiveCaption
          DataField = 'LLENADOS'
          DataSource = DatosF.MensualSource
          TabOrder = 0
        end
        object Calcular: TButton
          Left = 350
          Top = 32
          Width = 75
          Height = 25
          Hint = 
            'Realiza el c'#225'lculo de los par'#225'metros del riego con los nuevos da' +
            'tos introducidos.'
          Caption = 'Calcular'
          TabOrder = 4
          OnClick = CalcularClick
        end
        object DAbono: TDBEdit
          Left = 176
          Top = 32
          Width = 73
          Height = 21
          Hint = 'D'#237'as en los que se aplicar'#225' abono al riego'
          Color = clInactiveCaption
          DataField = 'DABONO'
          DataSource = DatosF.MensualSource
          TabOrder = 2
        end
        object DRiego: TDBEdit
          Left = 272
          Top = 32
          Width = 73
          Height = 21
          Hint = 'N'#250'mero de d'#237'as del mes que se efectuar'#225' riego'
          DataField = 'DRIEGO'
          DataSource = DatosF.MensualSource
          TabOrder = 3
        end
        object AguaFinca: TDBEdit
          Left = 96
          Top = 32
          Width = 73
          Height = 21
          Hint = 'Agua estimada para aplicar el abono de la finca'
          Color = clInactiveCaption
          DataField = 'AGUA'
          DataSource = DatosF.MensualSource
          TabOrder = 1
        end
      end
      object CancelaSector: TButton
        Left = 216
        Top = 344
        Width = 75
        Height = 25
        Hint = 
          'Cancela los cambios realizados en los par'#225'metros de riego por se' +
          'ctor'
        Caption = '&Cancelar'
        Enabled = False
        TabOrder = 4
        OnClick = CancelaSectorClick
      end
      object GuardaSector: TBitBtn
        Left = 136
        Top = 344
        Width = 75
        Height = 25
        Hint = 'Guarda el registro actual.'
        Caption = '&Guardar'
        Enabled = False
        TabOrder = 5
        OnClick = GuardaSectorClick
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF00FF00FF00FF000000
          0000008484000084840000000000000000000000000000000000000000000000
          0000C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
          0000008484000084840000000000000000000000000000000000000000000000
          0000C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
          0000008484000084840000000000000000000000000000000000000000000000
          0000C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
          0000008484000084840000000000000000000000000000000000000000000000
          00000000000000000000000000000084840000000000FF00FF00FF00FF000000
          0000008484000084840000848400008484000084840000848400008484000084
          84000084840000848400008484000084840000000000FF00FF00FF00FF000000
          0000008484000084840000000000000000000000000000000000000000000000
          00000000000000000000008484000084840000000000FF00FF00FF00FF000000
          00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
          00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
          00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
          00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
          00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C600C6C6C600000000000000000000000000FF00FF00FF00FF000000
          00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C600C6C6C60000000000C6C6C60000000000FF00FF00FF00FF000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      end
      object EliminaSector: TBitBtn
        Left = 360
        Top = 344
        Width = 71
        Height = 25
        Hint = 'Elimina el registro actual.'
        Caption = '&Eliminar'
        TabOrder = 6
        OnClick = EliminaSectorClick
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00
          FF00000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF0000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00000000000000000000000000FF00FF00FF00FF00FF00
          FF00FF00FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00000000000000000000000000FF00FF00FF00
          FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
          000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000000000000000
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
          000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00000000000000000000000000FF00FF00FF00
          FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF0000000000000000000000000000000000FF00FF00FF00FF00FF00
          FF00FF00FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF0000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF000000000000000000FF00FF00FF00FF00FF00FF00FF00
          FF00000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      end
      object PlanAnual: TGroupBox
        Left = 128
        Top = 296
        Width = 305
        Height = 41
        Caption = 'Plan anual asociado'
        TabOrder = 7
        object CodAnual: TDBEdit
          Left = 112
          Top = 12
          Width = 121
          Height = 21
          Hint = 
            'C'#243'digo del plan anual asociado utilizado para calcular este sect' +
            'or.'
          DataField = 'CODANUAL'
          DataSource = DatosF.SectorMesSource
          Enabled = False
          ReadOnly = True
          TabOrder = 0
        end
      end
    end
    object AF: TTabSheet
      Caption = #193'cido fosf'#243'rico'
      ImageIndex = 3
      object ListaSectorAF: TDBGrid
        Left = 0
        Top = 8
        Width = 121
        Height = 297
        DataSource = DatosF.SectorMesSource
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SECTOR'
            Title.Caption = 'Sector'
            Visible = True
          end>
      end
      object DiasRiegoAF: TGroupBox
        Left = 128
        Top = 200
        Width = 305
        Height = 105
        Hint = 'D'#237'as de riego del mes'
        Caption = 'D'#237'as  de aplicaci'#243'n'
        TabOrder = 2
        object Lunes_AF: TDBCheckBox
          Left = 8
          Top = 24
          Width = 65
          Height = 17
          Caption = 'Lunes'
          DataField = 'LUNES_AF'
          DataSource = DatosF.SectorMesSource
          TabOrder = 0
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
        object Martes_AF: TDBCheckBox
          Left = 88
          Top = 24
          Width = 65
          Height = 17
          Caption = 'Martes'
          DataField = 'MARTES_AF'
          DataSource = DatosF.SectorMesSource
          TabOrder = 1
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
        object Miercoles_AF: TDBCheckBox
          Left = 192
          Top = 24
          Width = 73
          Height = 17
          Caption = 'Mi'#233'rcoles'
          DataField = 'MIERCOLES_AF'
          DataSource = DatosF.SectorMesSource
          TabOrder = 2
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
        object Jueves_AF: TDBCheckBox
          Left = 8
          Top = 48
          Width = 65
          Height = 17
          Caption = 'Jueves'
          DataField = 'JUEVES_AF'
          DataSource = DatosF.SectorMesSource
          TabOrder = 3
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
        object Viernes_AF: TDBCheckBox
          Left = 88
          Top = 48
          Width = 65
          Height = 17
          Caption = 'Viernes'
          DataField = 'VIERNES_AF'
          DataSource = DatosF.SectorMesSource
          TabOrder = 4
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
        object Sabado_AF: TDBCheckBox
          Left = 192
          Top = 48
          Width = 65
          Height = 17
          Caption = 'S'#225'bado'
          DataField = 'SABADO_AF'
          DataSource = DatosF.SectorMesSource
          TabOrder = 5
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
        object Domingo_AF: TDBCheckBox
          Left = 88
          Top = 72
          Width = 65
          Height = 17
          Caption = 'Domingo'
          DataField = 'DOMINGO_AF'
          DataSource = DatosF.SectorMesSource
          TabOrder = 6
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
      end
      object RiegoSectorAF: TGroupBox
        Left = 128
        Top = 8
        Width = 305
        Height = 193
        Caption = 'Aplicaci'#243'n por sector'
        TabOrder = 1
        object Label12: TLabel
          Left = 8
          Top = 64
          Width = 51
          Height = 13
          Caption = 'Kilogramos'
        end
        object Label13: TLabel
          Left = 80
          Top = 64
          Width = 44
          Height = 13
          Caption = '% Bomba'
        end
        object Label14: TLabel
          Left = 144
          Top = 64
          Width = 36
          Height = 13
          Caption = 'N'#186' d'#237'as'
        end
        object Label15: TLabel
          Left = 88
          Top = 112
          Width = 111
          Height = 13
          Caption = 'Tiempo aplicaci'#243'n (min)'
        end
        object Label16: TLabel
          Left = 224
          Top = 64
          Width = 63
          Height = 13
          Caption = 'T Riego (min)'
        end
        object Label1: TLabel
          Left = 8
          Top = 16
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
        end
        object Label17: TLabel
          Left = 80
          Top = 16
          Width = 56
          Height = 13
          Caption = 'Descripci'#243'n'
        end
        object Nombre_AF: TDBEdit
          Left = 80
          Top = 32
          Width = 217
          Height = 21
          Hint = 
            'Nombre comercial del producto que act'#250'a c'#243'mo '#225'cido fosf'#243'rico. Es' +
            'ta descripci'#243'n aparecer'#225' en la hoja impresa.'
          DataField = 'NOMBRE_AF'
          DataSource = DatosF.SectorMesSource
          TabOrder = 1
        end
        object Ctd_AF: TDBEdit
          Left = 8
          Top = 80
          Width = 65
          Height = 21
          Hint = 'Kilogramos de '#225'cido fosf'#243'rico a aplicar en el sector'
          DataField = 'CTD_AF'
          DataSource = DatosF.SectorMesSource
          TabOrder = 2
        end
        object Bomba_AF: TDBEdit
          Left = 80
          Top = 80
          Width = 57
          Height = 21
          Hint = 'Porcentaje de utilizaci'#243'n de la bomba inyectora'
          DataField = 'BOMBA_AF'
          DataSource = DatosF.SectorMesSource
          TabOrder = 3
        end
        object Codart_AF: TDBEdit
          Left = 8
          Top = 32
          Width = 65
          Height = 21
          Hint = 'C'#243'digo del producto que act'#250'a como '#225'cido fosf'#243'rico'
          DataField = 'CODART_AF'
          DataSource = DatosF.SectorMesSource
          TabOrder = 0
        end
        object DAcidoF: TDBEdit
          Left = 144
          Top = 80
          Width = 73
          Height = 21
          Hint = 'N'#186' d'#237'as en los que aplicar el '#225'cido fosf'#243'rico durante el mes'
          DataField = 'DACIDOF'
          DataSource = DatosF.SectorMesSource
          TabOrder = 4
        end
        object TAcidoF: TDBEdit
          Left = 88
          Top = 128
          Width = 121
          Height = 21
          Hint = 
            'Tiempo estimado duraci'#243'n aplicaci'#243'n del '#225'cido fosf'#243'rico por sect' +
            'or'
          Color = clMenuHighlight
          DataField = 'TACIDOF'
          DataSource = DatosF.SectorMesSource
          ReadOnly = True
          TabOrder = 6
        end
        object CalcularAF: TButton
          Left = 104
          Top = 160
          Width = 75
          Height = 25
          Hint = 'Vuelve a calcular los par'#225'metros asociados al riego'
          Caption = 'Calcular'
          TabOrder = 7
          OnClick = CalcularAFClick
        end
        object TRiego_AF: TDBEdit
          Left = 224
          Top = 80
          Width = 73
          Height = 21
          Hint = 'Tiempo de riego en minutos'
          DataField = 'TRIEGO_AF'
          DataSource = DatosF.SectorMesSource
          TabOrder = 5
        end
      end
      object CancelarAF: TButton
        Left = 208
        Top = 320
        Width = 75
        Height = 25
        Hint = 
          'Cancela los cambios realizados en los par'#225'metros de riego por se' +
          'ctor'
        Caption = '&Cancelar'
        Enabled = False
        TabOrder = 3
        OnClick = CancelaSectorClick
      end
      object GuardarAF: TBitBtn
        Left = 128
        Top = 320
        Width = 75
        Height = 25
        Hint = 'Guarda el registro actual.'
        Caption = '&Guardar'
        Enabled = False
        TabOrder = 4
        OnClick = GuardaSectorClick
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF00FF00FF00FF000000
          0000008484000084840000000000000000000000000000000000000000000000
          0000C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
          0000008484000084840000000000000000000000000000000000000000000000
          0000C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
          0000008484000084840000000000000000000000000000000000000000000000
          0000C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
          0000008484000084840000000000000000000000000000000000000000000000
          00000000000000000000000000000084840000000000FF00FF00FF00FF000000
          0000008484000084840000848400008484000084840000848400008484000084
          84000084840000848400008484000084840000000000FF00FF00FF00FF000000
          0000008484000084840000000000000000000000000000000000000000000000
          00000000000000000000008484000084840000000000FF00FF00FF00FF000000
          00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
          00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
          00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
          00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
          00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C600C6C6C600000000000000000000000000FF00FF00FF00FF000000
          00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C600C6C6C60000000000C6C6C60000000000FF00FF00FF00FF000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      end
      object EliminarAF: TBitBtn
        Left = 352
        Top = 320
        Width = 71
        Height = 25
        Hint = 'Elimina el registro actual.'
        Caption = '&Eliminar'
        TabOrder = 5
        OnClick = EliminarAFClick
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00
          FF00000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF0000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00000000000000000000000000FF00FF00FF00FF00FF00
          FF00FF00FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00000000000000000000000000FF00FF00FF00
          FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
          000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000000000000000
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
          000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00000000000000000000000000FF00FF00FF00
          FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF0000000000000000000000000000000000FF00FF00FF00FF00FF00
          FF00FF00FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF0000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF000000000000000000FF00FF00FF00FF00FF00FF00FF00
          FF00000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      end
    end
    object ProductosTab: TTabSheet
      Caption = 'Productos'
      ImageIndex = 1
      object Productos: TGroupBox
        Left = 128
        Top = 12
        Width = 305
        Height = 341
        Caption = 'Productos'
        TabOrder = 0
        object Detalle: TDBGrid
          Left = 8
          Top = 48
          Width = 289
          Height = 241
          Hint = 'Lista de productos utilizados durante el mes actual.'
          DataSource = DatosF.DetMensualSource
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMCOMERCIAL'
              Title.Caption = 'Nombre'
              Width = 196
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CANTIDAD'
              Title.Caption = 'Cantidad'
              Width = 50
              Visible = True
            end>
        end
        object Articulos: TDBLookupComboBox
          Left = 8
          Top = 16
          Width = 209
          Height = 21
          Hint = 'Seleccione un producto para a'#241'adir a la lista.'
          KeyField = 'CODART'
          ListField = 'NOMCOMERCIAL'
          ListSource = DatosF.ProductosSource
          TabOrder = 1
        end
        object Anyadir: TButton
          Left = 224
          Top = 16
          Width = 75
          Height = 25
          Hint = 'A'#241'ade el producto seleccionado a la lista de productos.'
          Caption = 'A'#241'adir'
          TabOrder = 2
          OnClick = AnyadirClick
        end
        object CancelarProd: TButton
          Left = 88
          Top = 304
          Width = 75
          Height = 25
          Hint = 'Cancela los cambios realizados en la lista de productos'
          Caption = '&Cancelar'
          Enabled = False
          TabOrder = 3
          OnClick = CancelarProdClick
        end
        object GuardarProd: TBitBtn
          Left = 8
          Top = 304
          Width = 75
          Height = 25
          Hint = 'Guarda el registro actual.'
          Caption = '&Guardar'
          Enabled = False
          TabOrder = 4
          OnClick = GuardarProdClick
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00000000000000000000000000000000000000000000000000000000000000
            00000000000000000000000000000000000000000000FF00FF00FF00FF000000
            0000008484000084840000000000000000000000000000000000000000000000
            0000C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
            0000008484000084840000000000000000000000000000000000000000000000
            0000C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
            0000008484000084840000000000000000000000000000000000000000000000
            0000C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
            0000008484000084840000000000000000000000000000000000000000000000
            00000000000000000000000000000084840000000000FF00FF00FF00FF000000
            0000008484000084840000848400008484000084840000848400008484000084
            84000084840000848400008484000084840000000000FF00FF00FF00FF000000
            0000008484000084840000000000000000000000000000000000000000000000
            00000000000000000000008484000084840000000000FF00FF00FF00FF000000
            00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
            C600C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
            00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
            C600C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
            00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
            C600C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
            00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
            C600C6C6C600C6C6C600000000000084840000000000FF00FF00FF00FF000000
            00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
            C600C6C6C600C6C6C600000000000000000000000000FF00FF00FF00FF000000
            00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
            C600C6C6C600C6C6C60000000000C6C6C60000000000FF00FF00FF00FF000000
            0000000000000000000000000000000000000000000000000000000000000000
            00000000000000000000000000000000000000000000FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        end
        object EliminarProd: TBitBtn
          Left = 224
          Top = 304
          Width = 71
          Height = 25
          Hint = 'Elimina el registro actual.'
          Caption = '&Eliminar'
          TabOrder = 5
          OnClick = EliminarProdClick
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00
            FF00000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF0000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00000000000000000000000000FF00FF00FF00FF00FF00
            FF00FF00FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00000000000000000000000000FF00FF00FF00
            FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
            000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000000000000000
            0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
            000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00000000000000000000000000FF00FF00FF00
            FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF0000000000000000000000000000000000FF00FF00FF00FF00FF00
            FF00FF00FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF0000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF000000000000000000FF00FF00FF00FF00FF00FF00FF00
            FF00000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        end
      end
      object ListaSectorProd: TDBGrid
        Left = 0
        Top = 16
        Width = 121
        Height = 337
        DataSource = DatosF.SectorMesSource
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SECTOR'
            Title.Caption = 'Sector'
            Visible = True
          end>
      end
    end
    object ObservaTab: TTabSheet
      Caption = 'Observaciones'
      ImageIndex = 2
      object Descripcion: TGroupBox
        Left = 5
        Top = 4
        Width = 428
        Height = 173
        Caption = 'Observaciones'
        TabOrder = 0
        object Observaciones: TDBRichEdit
          Left = 8
          Top = 16
          Width = 409
          Height = 145
          Hint = 'Consideraciones a incluir en el documento entregado al cliente.'
          DataField = 'OBSERVACIONES'
          DataSource = DatosF.MensualSource
          ScrollBars = ssBoth
          TabOrder = 0
        end
      end
      object DescripcionAF: TGroupBox
        Left = 5
        Top = 176
        Width = 428
        Height = 185
        Caption = 'Observaciones '#225'cido fosf'#243'rico'
        TabOrder = 1
        object ObservacionesAF: TDBRichEdit
          Left = 8
          Top = 16
          Width = 409
          Height = 161
          Hint = 'Consideraciones sobre la aplicaci'#243'n del '#225'cido fosf'#243'rico'
          DataField = 'OBSERVA_AF'
          DataSource = DatosF.MensualSource
          ScrollBars = ssBoth
          TabOrder = 0
        end
      end
    end
    object AnotacionesTab: TTabSheet
      Caption = 'Anotaciones'
      ImageIndex = 4
      object Anotaciones: TDBRichEdit
        Left = 8
        Top = 8
        Width = 417
        Height = 345
        Hint = 
          'Anotaciones que no aparecer'#225'n en el documento entregado al clien' +
          'te'
        DataField = 'ANOTACIONES'
        DataSource = DatosF.MensualSource
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
  end
  object FiltroAnyo: TEdit
    Left = 72
    Top = 8
    Width = 41
    Height = 21
    TabOrder = 0
    OnChange = Filtro
  end
  object FiltroMes: TEdit
    Left = 184
    Top = 8
    Width = 25
    Height = 21
    TabOrder = 1
    OnChange = Filtro
  end
  object Imprimir: TBitBtn
    Left = 600
    Top = 504
    Width = 75
    Height = 25
    Hint = 'Imprime el plan actual.'
    Caption = 'Im&primir'
    TabOrder = 10
    OnClick = ImprimirClick
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF000000
      0000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C60000000000C6C6C60000000000FF00FF00FF00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C6C60000000000FF00FF0000000000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C60000FFFF0000FFFF0000FF
      FF00C6C6C600C6C6C600000000000000000000000000FF00FF0000000000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C60084848400848484008484
      8400C6C6C600C6C6C60000000000C6C6C60000000000FF00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C6C600C6C6C6000000000000000000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C60000000000C6C6C60000000000C6C6C60000000000FF00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C60000000000C6C6C6000000000000000000FF00FF00FF00
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000C6C6C60000000000C6C6C60000000000FF00FF00FF00
      FF00FF00FF0000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF0000000000000000000000000000000000FF00FF00FF00FF00FF00
      FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0000000000FFFFFF000000000000000000000000000000
      000000000000FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
  end
  object Principal: TGroupBox
    Left = 392
    Top = 0
    Width = 441
    Height = 89
    TabOrder = 3
    object Label18: TLabel
      Left = 8
      Top = 8
      Width = 33
      Height = 13
      Caption = 'C'#243'digo'
    end
    object Label19: TLabel
      Left = 152
      Top = 8
      Width = 19
      Height = 13
      Caption = 'A'#241'o'
    end
    object Label20: TLabel
      Left = 224
      Top = 8
      Width = 20
      Height = 13
      Caption = 'Mes'
    end
    object Label21: TLabel
      Left = 8
      Top = 56
      Width = 26
      Height = 13
      Caption = 'Finca'
    end
    object Label22: TLabel
      Left = 376
      Top = 24
      Width = 46
      Height = 13
      Caption = 'Quincena'
    end
    object Label23: TLabel
      Left = 8
      Top = 32
      Width = 32
      Height = 13
      Caption = 'Cliente'
    end
    object NomCliente: TDBText
      Left = 48
      Top = 32
      Width = 241
      Height = 17
      DataField = 'NOMCLI'
      DataSource = DatosF.MensualSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Impreso: TDBCheckBox
      Left = 296
      Top = 8
      Width = 73
      Height = 17
      Hint = 'Indica si se ha impreso el plan mensual'
      Caption = 'Impreso'
      DataField = 'IMPRESO'
      DataSource = DatosF.MensualSource
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object EsQuincena: TDBCheckBox
      Left = 296
      Top = 32
      Width = 73
      Height = 17
      Hint = 'Indica si el plan actual es mensual o quincenal'
      Caption = 'Quincenal'
      DataField = 'ES_QUINCENA'
      DataSource = DatosF.MensualSource
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object CodMensual: TDBEdit
      Left = 48
      Top = 8
      Width = 97
      Height = 21
      DataField = 'CODMENSUAL'
      DataSource = DatosF.MensualSource
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
    end
    object Anyo: TDBEdit
      Left = 176
      Top = 8
      Width = 41
      Height = 21
      Hint = 'A'#241'o del plan mensual.'
      DataField = 'ANYO'
      DataSource = DatosF.MensualSource
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
    end
    object Mes: TDBEdit
      Left = 248
      Top = 8
      Width = 41
      Height = 21
      Hint = 'Mes del plan mensual.'
      DataField = 'MES'
      DataSource = DatosF.MensualSource
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
    end
    object NomFinca: TDBEdit
      Left = 48
      Top = 56
      Width = 241
      Height = 21
      Hint = 'Nombre de la finca asociada al plan mensual.'
      DataField = 'NOMFINCA'
      DataSource = DatosF.MensualSource
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
    end
    object Quincena: TDBEdit
      Left = 376
      Top = 40
      Width = 33
      Height = 21
      Hint = 
        'En caso de ser un plan quincenal muestra la quincena a la que pe' +
        'rtenece este plan mensual.'
      DataField = 'QUINCENA'
      DataSource = DatosF.MensualSource
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
    end
  end
  object DuplicaB: TBitBtn
    Left = 368
    Top = 504
    Width = 65
    Height = 25
    Hint = 'Duplica el plan mensual actual.'
    Caption = 'Duplicar'
    TabOrder = 6
    OnClick = DuplicaBClick
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0084000000840000008400
      0000840000008400000084000000840000008400000084000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0084000000FFFFFF000000
      000000000000000000000000000000000000FFFFFF0084000000FF00FF000000
      0000000000000000000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084000000FF00FF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF000000
      000000000000000000000000000000000000FFFFFF0084000000FF00FF000000
      0000FFFFFF000000000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084000000FF00FF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF000000
      000000000000FFFFFF0084000000840000008400000084000000FF00FF000000
      0000FFFFFF000000000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000FFFFFF0084000000FF00FF00FF00FF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF008400000084000000FF00FF00FF00FF00FF00FF000000
      0000FFFFFF000000000000000000FFFFFF000000000084000000840000008400
      0000840000008400000084000000FF00FF00FF00FF00FF00FF00FF00FF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
      00000000000000000000000000000000000000000000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
  end
  object FiltroFinca: TEdit
    Left = 288
    Top = 8
    Width = 97
    Height = 21
    TabOrder = 12
    OnChange = Filtro
  end
  object MensualDupl: TADOTable
    Connection = DatosF.Abono
    LockType = ltReadOnly
    TableName = 'MENSUAL'
    Left = 96
    Top = 488
    object MensualDuplCODMENSUAL: TWideStringField
      FieldName = 'CODMENSUAL'
      Size = 15
    end
    object MensualDuplCODFINCA: TIntegerField
      FieldName = 'CODFINCA'
    end
    object MensualDuplANYO: TSmallintField
      FieldName = 'ANYO'
    end
    object MensualDuplMES: TSmallintField
      FieldName = 'MES'
    end
    object MensualDuplQUINCENA: TSmallintField
      FieldName = 'QUINCENA'
    end
    object MensualDuplES_QUINCENA: TBooleanField
      FieldName = 'ES_QUINCENA'
    end
    object MensualDuplOBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object MensualDuplLLENADOS: TSmallintField
      FieldName = 'LLENADOS'
    end
    object MensualDuplIMPRESO: TBooleanField
      FieldName = 'IMPRESO'
    end
    object MensualDuplDABONO: TSmallintField
      FieldName = 'DABONO'
    end
    object MensualDuplDRIEGO: TSmallintField
      FieldName = 'DRIEGO'
    end
    object MensualDuplAGUA: TFloatField
      FieldName = 'AGUA'
    end
    object MensualDuplOBSERVA_AF: TMemoField
      FieldName = 'OBSERVA_AF'
      BlobType = ftMemo
    end
    object MensualDuplANOTACIONES: TMemoField
      FieldName = 'ANOTACIONES'
      BlobType = ftMemo
    end
    object MensualDuplNOMFINCA: TWideStringField
      FieldName = 'NOMFINCA'
      Size = 50
    end
  end
  object SectorMesDupl: TADOTable
    Connection = DatosF.Abono
    LockType = ltReadOnly
    IndexFieldNames = 'CODMENSUAL'
    MasterFields = 'CODMENSUAL'
    MasterSource = MensualDuplSource
    TableName = 'SECTORMES'
    Left = 128
    Top = 488
    object SectorMesDuplCODMENSUAL: TWideStringField
      FieldName = 'CODMENSUAL'
      Size = 15
    end
    object SectorMesDuplSECTOR: TWideStringField
      FieldName = 'SECTOR'
    end
    object SectorMesDuplCODANUAL: TWideStringField
      FieldName = 'CODANUAL'
      Size = 15
    end
    object SectorMesDuplTABONO: TSmallintField
      FieldName = 'TABONO'
    end
    object SectorMesDuplTRIEGO: TSmallintField
      FieldName = 'TRIEGO'
    end
    object SectorMesDuplLUNES: TBooleanField
      FieldName = 'LUNES'
    end
    object SectorMesDuplMARTES: TBooleanField
      FieldName = 'MARTES'
    end
    object SectorMesDuplMIERCOLES: TBooleanField
      FieldName = 'MIERCOLES'
    end
    object SectorMesDuplJUEVES: TBooleanField
      FieldName = 'JUEVES'
    end
    object SectorMesDuplVIERNES: TBooleanField
      FieldName = 'VIERNES'
    end
    object SectorMesDuplSABADO: TBooleanField
      FieldName = 'SABADO'
    end
    object SectorMesDuplDOMINGO: TBooleanField
      FieldName = 'DOMINGO'
    end
    object SectorMesDuplBOMBA: TFloatField
      FieldName = 'BOMBA'
    end
    object SectorMesDuplAGUA: TFloatField
      FieldName = 'AGUA'
    end
    object SectorMesDuplCODART_AF: TIntegerField
      FieldName = 'CODART_AF'
    end
    object SectorMesDuplTACIDOF: TSmallintField
      FieldName = 'TACIDOF'
    end
    object SectorMesDuplBOMBA_AF: TFloatField
      FieldName = 'BOMBA_AF'
    end
    object SectorMesDuplCTD_AF: TFloatField
      FieldName = 'CTD_AF'
    end
    object SectorMesDuplNOMBRE_AF: TWideStringField
      FieldName = 'NOMBRE_AF'
      Size = 50
    end
    object SectorMesDuplDACIDOF: TSmallintField
      FieldName = 'DACIDOF'
    end
    object SectorMesDuplAGUA_AF: TFloatField
      FieldName = 'AGUA_AF'
    end
    object SectorMesDuplLUNES_AF: TBooleanField
      FieldName = 'LUNES_AF'
    end
    object SectorMesDuplMARTES_AF: TBooleanField
      FieldName = 'MARTES_AF'
    end
    object SectorMesDuplMIERCOLES_AF: TBooleanField
      FieldName = 'MIERCOLES_AF'
    end
    object SectorMesDuplJUEVES_AF: TBooleanField
      FieldName = 'JUEVES_AF'
    end
    object SectorMesDuplVIERNES_AF: TBooleanField
      FieldName = 'VIERNES_AF'
    end
    object SectorMesDuplSABADO_AF: TBooleanField
      FieldName = 'SABADO_AF'
    end
    object SectorMesDuplDOMINGO_AF: TBooleanField
      FieldName = 'DOMINGO_AF'
    end
    object SectorMesDuplTRIEGO_AF: TSmallintField
      FieldName = 'TRIEGO_AF'
    end
  end
  object DetMensualDupl: TADOTable
    Connection = DatosF.Abono
    IndexFieldNames = 'CODMENSUAL;SECTOR'
    MasterFields = 'CODMENSUAL;SECTOR'
    MasterSource = SectorMesDuplSource
    TableName = 'DETMENSUAL'
    Left = 160
    Top = 488
    object DetMensualDuplCODMENSUAL: TWideStringField
      FieldName = 'CODMENSUAL'
      Size = 15
    end
    object DetMensualDuplSECTOR: TWideStringField
      FieldName = 'SECTOR'
    end
    object DetMensualDuplNLINEA: TSmallintField
      FieldName = 'NLINEA'
    end
    object DetMensualDuplCODART: TIntegerField
      FieldName = 'CODART'
    end
    object DetMensualDuplCANTIDAD: TFloatField
      FieldName = 'CANTIDAD'
    end
    object DetMensualDuplNOMCOMERCIAL: TWideStringField
      FieldName = 'NOMCOMERCIAL'
      Size = 50
    end
  end
  object MensualDuplSource: TDataSource
    DataSet = MensualDupl
    Left = 192
    Top = 488
  end
  object SectorMesDuplSource: TDataSource
    DataSet = SectorMesDupl
    Left = 224
    Top = 488
  end
end
