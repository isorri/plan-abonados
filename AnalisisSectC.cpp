//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AnalisisSectC.h"
#include "DatosC.h"
#include "FincasC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PlantillaC"
#pragma resource "*.dfm"
TAnalisisSectF *AnalisisSectF;
//---------------------------------------------------------------------------
__fastcall TAnalisisSectF::TAnalisisSectF(TComponent* Owner)
        : TPlantilla(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TAnalisisSectF::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   int res;
        if (DatosF->AnalisisSector->State == dsInsert || DatosF->AnalisisSector->State == dsEdit)
        {
            res = Application->MessageBoxA("Analisis pendiente de inserci�n.\n�Guardar los datos?","Informaci�n",MB_ICONINFORMATION+MB_YESNO);
            if (res == IDYES)
               DatosF->AnalisisSector->Post();
            else
               DatosF->AnalisisSector->Cancel();
        }
        //Habilitar de nuevo el formulario fincas
        FincasF->Enabled = true;
        //Deshacer el filtro
        DatosF->AnalisisSector->Filtered = false;
        DatosF->AnalisisSector->Filter = "";

        Action = caFree;
        AnalisisSectF = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TAnalisisSectF::CerrarClick(TObject *Sender)
{
   Close();
}
//---------------------------------------------------------------------------
void __fastcall TAnalisisSectF::FechaChange(TObject *Sender)
{
        this->FechaBD->Text = Fecha->Date.operator AnsiString();
}
//---------------------------------------------------------------------------
void __fastcall TAnalisisSectF::AnyadirClick(TObject *Sender)
{
   if (DatosF->AnalisisSector->State != dsBrowse)
   {
       Application->MessageBoxA("Hay un an�lisis de sector pendiente de inserci�n/modificaci�n.","Error",MB_ICONERROR);
       return;
   }
   DatosF->AnalisisSector->Insert();
   DatosF->AnalisisSector->FieldValues["CODFINCA"] = CodFinca->Text.ToInt();
   DatosF->AnalisisSector->FieldValues["SECTOR"]   = Sector->Caption;
   Fecha->Date = Fecha->Date.CurrentDate();
   FechaBD->Text = Fecha->Date.operator AnsiString();
   GroupBox1->Enabled = true;
   N->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TAnalisisSectF::GuardarClick(TObject *Sender)
{
    if (FechaBD->Text == "")
    {
        Application->MessageBoxA("Fecha obligatoria.\nSeleccione una fecha","Error",MB_ICONERROR);
        Fecha->SetFocus();
        return;
    }
    if (DatosF->AnalisisSector->State == dsInsert || DatosF->AnalisisSector->State == dsEdit)
        DatosF->AnalisisSector->Post();
    else
        Application->MessageBoxA("Nada que guardar.","Informaci�n",MB_ICONINFORMATION);

}
//---------------------------------------------------------------------------

void __fastcall TAnalisisSectF::CancelarClick(TObject *Sender)
{
    if (DatosF->AnalisisSector->State == dsInsert || DatosF->AnalisisSector->State == dsEdit)
        DatosF->AnalisisSector->Cancel();
    else
        Application->MessageBoxA("Nada que cancelar.","Informaci�n",MB_ICONINFORMATION);

}
//---------------------------------------------------------------------------

void __fastcall TAnalisisSectF::EliminarClick(TObject *Sender)
{
   if(DatosF->AnalisisSector->State == dsBrowse)
      DatosF->AnalisisSector->Delete();
}
//---------------------------------------------------------------------------

