//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DatosC.h"
#include "FincasC.h"
#include "AbonosC.h"
#include "AnualC.h"
#include "MensualC.h"
#include "ComercialC.h"
#include "ClienteC.h"
#include "CoeficientesC.h"
#include "RepartoC.h"
#include "RiegoC.h"
#include "RiegoPlantC.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TDatosF *DatosF;
//---------------------------------------------------------------------------
__fastcall TDatosF::TDatosF(TComponent* Owner)
        : TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::DataModuleCreate(TObject *Sender)
{
        //Realiza la conexi�n con la base de datos
        Abono->Connected = true;
        //Abre las tablas oportunas
        Clientes->Open();
        Comerciales->Open();
        Fincas->Open();
        Sectores->Open();
        AnalisisSector->Open();
        AnalisisFinca->Open();
        Productos->Open();
        Coeficientes->Open();
        Anual->Open();
        DetAnual->Open();
        Reparto->Open();
        DiasRiego->Open();
        RiegoPlant->Open();
        Mensual->Open();
        SectorMes->Open();
        DetMensual->Open();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::DataModuleDestroy(TObject *Sender)
{
        //Cierra las tablas utilizadas
        DetMensual->Close();
        SectorMes->Close();
        Mensual->Close();
        RiegoPlant->Close();
        DiasRiego->Close();
        DetAnual->Close();
        Anual->Close();
        Comerciales->Close();
        Clientes->Close();
        Fincas->Close();
        Sectores->Close();
        AnalisisSector->Close();
        AnalisisFinca->Close();
        Productos->Close();
        Coeficientes->Close();
        Reparto->Close();
        //Desconecto la base de datos
        Abono->Connected = false;
}
//---------------------------------------------------------------------------

void __fastcall TDatosF::FincasCalcFields(TDataSet *DataSet)
{
   int maxcubas = 5; //Numero maximo de cubas permitidas
   double cubas = 0;
   double cubasn = 0;
   int ncubas = 0;
   int ncubasn = 0;

   // Calcular la capacidad total de las cubas y el numero de cubas
   for (int i = 1; i<= maxcubas ; i++)
   {
     if (! Fincas->FieldValues["CAPCUBA_"+AnsiString(i)].IsNull())
     {
        cubas += (double)Fincas->FieldValues["CAPCUBA_"+AnsiString(i)];
        ncubas ++;
     }
     if (! Fincas->FieldValues["CAPCUBANOD_"+AnsiString(i)].IsNull())
     {
        cubasn += (double)Fincas->FieldValues["CAPCUBANOD_"+AnsiString(i)];
        ncubasn ++;
     }

   }
   Fincas->FieldValues["CAPACIDADCUBAS"] = cubas;
   Fincas->FieldValues["CAPCUBANOD"] = cubasn;
   Fincas->FieldValues["NUMCUBAS"] = ncubas;
   Fincas->FieldValues["NUMCUBASNOD"] = ncubasn;
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::FincasBloquea(TDataSet *DataSet)
{
        if (FincasF != NULL)
            FincasF->bloqueaFormulario();

}
//---------------------------------------------------------------------------
void __fastcall TDatosF::FincasDesbloquea(TDataSet *DataSet)
{
        if (FincasF != NULL)
            FincasF->desbloqueaFormulario();

}
//---------------------------------------------------------------------------
void __fastcall TDatosF::AnualAfterEdit(TDataSet *DataSet)
{
         if (AbonadoAnualF != NULL)
             AbonadoAnualF->bloqueaFormulario();
}
//---------------------------------------------------------------------------

void __fastcall TDatosF::AnualBeforeCancel(TDataSet *DataSet)
{
   if (AbonadoAnualF != NULL)
        AbonadoAnualF->desbloqueaFormulario();
}
//---------------------------------------------------------------------------

void __fastcall TDatosF::AnualBeforePost(TDataSet *DataSet)
{
   if (AbonadoAnualF != NULL)
        AbonadoAnualF->desbloqueaFormulario();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::DetAnualCalcFields(TDataSet *DataSet)
{
  //Calcular % Total
  float resultado;
  float cantidad;
  resultado  = DetAnualDSDesigner01_ENERO->Value;
  resultado += DetAnualDSDesigner02_FEBRERO->Value;
  resultado += DetAnualDSDesigner03_MARZO->Value;
  resultado += DetAnualDSDesigner04_ABRIL->Value;
  resultado += DetAnualDSDesigner05_MAYO->Value;
  resultado += DetAnualDSDesigner06_JUNIO->Value;
  resultado += DetAnualDSDesigner07_JULIO->Value;
  resultado += DetAnualDSDesigner08_AGOSTO->Value;
  resultado += DetAnualDSDesigner09_SEPTIEMBRE->Value;
  resultado += DetAnualDSDesigner10_OCTUBRE->Value;
  resultado += DetAnualDSDesigner11_NOVIEMBRE->Value;
  resultado += DetAnualDSDesigner12_DICIEMBRE->Value;
  DetAnualTOTAL->Value = resultado;

  //Calcular Cantidades relativas
  cantidad = DetAnualCANTIDAD->Value;
  CTD01->Value = DetAnualDSDesigner01_ENERO->Value * cantidad / 100;
  CTD02->Value =  redondea(DetAnualDSDesigner02_FEBRERO->Value * cantidad / 100,2);
  CTD03->Value =  redondea(DetAnualDSDesigner03_MARZO->Value * cantidad / 100,2);
  CTD04->Value =  redondea(DetAnualDSDesigner04_ABRIL->Value * cantidad / 100,2);
  CTD05->Value =  redondea(DetAnualDSDesigner05_MAYO->Value * cantidad / 100,2);
  CTD06->Value =  redondea(DetAnualDSDesigner06_JUNIO->Value * cantidad / 100,2);
  CTD07->Value =  redondea(DetAnualDSDesigner07_JULIO->Value * cantidad / 100,2);
  CTD08->Value =  redondea(DetAnualDSDesigner08_AGOSTO->Value * cantidad / 100,2);
  CTD09->Value =  redondea(DetAnualDSDesigner09_SEPTIEMBRE->Value * cantidad / 100,2);
  CTD10->Value =  redondea(DetAnualDSDesigner10_OCTUBRE->Value * cantidad / 100,2);
  CTD11->Value =  redondea(DetAnualDSDesigner11_NOVIEMBRE->Value * cantidad / 100,2);
  CTD12->Value =  redondea(DetAnualDSDesigner12_DICIEMBRE->Value * cantidad / 100,2);

}
//---------------------------------------------------------------------------
void __fastcall TDatosF::RepartoCalcFields(TDataSet *DataSet)
{
   float resultado;

   resultado  = RepartoDSDesigner01_ENERO->Value;
   resultado += RepartoDSDesigner02_FEBRERO->Value;
   resultado += RepartoDSDesigner03_MARZO->Value;
   resultado += RepartoDSDesigner04_ABRIL->Value;
   resultado += RepartoDSDesigner05_MAYO->Value;
   resultado += RepartoDSDesigner06_JUNIO->Value;
   resultado += RepartoDSDesigner07_JULIO->Value;
   resultado += RepartoDSDesigner08_AGOSTO->Value;
   resultado += RepartoDSDesigner09_SEPTIEMBRE->Value;
   resultado += RepartoDSDesigner10_OCTUBRE->Value;
   resultado += RepartoDSDesigner11_NOVIEMBRE->Value;
   resultado += RepartoDSDesigner12_DICIEMBRE->Value;
   TOTAL->Value = resultado;
}
//---------------------------------------------------------------------------

void __fastcall TDatosF::MensualAfterEdit(TDataSet *DataSet)
{
    /* Bloquea el formulario preparado para la modificaci�n*/
    if (MensualF != NULL)
        MensualF->bloqueaFormulario();
}
//---------------------------------------------------------------------------

void __fastcall TDatosF::MensualAfterPost(TDataSet *DataSet)
{
    /* Desbloquea el formulario una vez validada la modificaci�n*/
    if (MensualF != NULL)
        MensualF->desbloqueaFormulario();
}
//---------------------------------------------------------------------------

void __fastcall TDatosF::MensualAfterCancel(TDataSet *DataSet)
{
    /*Desbloquea el formulario una vez cancelados los cambios realizados*/
    if (MensualF != NULL)
        MensualF->desbloqueaFormulario();
}
//---------------------------------------------------------------------------

void __fastcall TDatosF::ProductosNewRecord(TDataSet *DataSet)
{
   //Inicializar composici�n para evitar que se introduzcan nulos
   Productos->FieldValues["N"] = 0;
   Productos->FieldValues["P"] = 0;
   Productos->FieldValues["K"] = 0;
   Productos->FieldValues["CA"] = 0;
   Productos->FieldValues["MG"] = 0;
   Productos->FieldValues["FE"] = 0;
   Productos->FieldValues["MICROELEMENTOS"] = 0;
   Productos->FieldValues["AHUMICO"] = 0;
   Productos->FieldValues["CSALINO"] = 0;
   Productos->FieldValues["DISOLUCI�N"]  = 0;
   Productos->FieldValues["ABONO"] = true;
   Productos->FieldValues["ESTADO"] = "S�LIDO";
   Productos->FieldValues["01_ENERO"] = 0;
   Productos->FieldValues["02_FEBRERO"] = 0;
   Productos->FieldValues["03_MARZO"] = 0;
   Productos->FieldValues["04_ABRIL"] = 0;
   Productos->FieldValues["05_MAYO"] = 0;
   Productos->FieldValues["06_JUNIO"] = 0;
   Productos->FieldValues["07_JULIO"] = 0;
   Productos->FieldValues["08_AGOSTO"] = 0;
   Productos->FieldValues["09_SEPTIEMBRE"] = 0;
   Productos->FieldValues["10_OCTUBRE"] = 0;
   Productos->FieldValues["11_NOVIEMBRE"] = 0;
   Productos->FieldValues["12_DICIEMBRE"] = 0;
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::SectoresNewRecord(TDataSet *DataSet)
{
   //Inicializar plantonada a falso
   Sectores->FieldValues["PLANTONADA"] = false;
}
//---------------------------------------------------------------------------
//Inicializar plantillas de reparto
void __fastcall TDatosF::RepartoNewRecord(TDataSet *DataSet)
{
    Reparto->FieldValues["01_ENERO"] = 0;
    Reparto->FieldValues["02_FEBRERO"] = 0;
    Reparto->FieldValues["03_MARZO"] = 0;
    Reparto->FieldValues["04_ABRIL"] = 0;
    Reparto->FieldValues["05_MAYO"] = 0;
    Reparto->FieldValues["06_JUNIO"] = 0;
    Reparto->FieldValues["07_JULIO"] = 0;
    Reparto->FieldValues["08_AGOSTO"] = 0;
    Reparto->FieldValues["09_SEPTIEMBRE"] = 0;
    Reparto->FieldValues["10_OCTUBRE"] = 0;
    Reparto->FieldValues["11_NOVIEMBRE"] = 0;
    Reparto->FieldValues["12_DICIEMBRE"] = 0;
}
//---------------------------------------------------------------------------
//Redondea un double al n�mero de decimales indicado como par�metro
double TDatosF::redondea(double valor, int ndec)
{
   double dTemp, dFract, dInt = 0, dRes;

   dTemp = valor*pow(10,ndec);
   dFract = modf (dTemp, &dInt);
   if (dFract >= 0.5)
      dInt++;
   dRes = dInt/pow(10,ndec);

 return dRes;
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::DetMensualBloquea(TDataSet *DataSet)
{
    /* Bloquea el formulario preparado para la modificaci�n*/
    if (MensualF != NULL)
        MensualF->bloqueaProductos();
}
//---------------------------------------------------------------------------

void __fastcall TDatosF::DetMensualDesbloquea(TDataSet *DataSet)
{
    /* Bloquea el formulario preparado para la modificaci�n*/
    if (MensualF != NULL)
        MensualF->desbloqueaProductos();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::SectorMesBloquea(TDataSet *DataSet)
{
    /* Bloquea el formulario preparado para la modificaci�n*/
    if (MensualF != NULL)
        MensualF->bloqueaSectores();
}
//---------------------------------------------------------------------------

void __fastcall TDatosF::SectorMesDesbloquea(TDataSet *DataSet)
{
    /* Bloquea el formulario preparado para la modificaci�n*/
    if (MensualF != NULL)
        MensualF->desbloqueaSectores();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::SectoresBloquea (TDataSet *DataSet)
{
    if (FincasF != NULL)
        FincasF->bloqueaSector();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::SectoresDesbloquea (TDataSet *DataSet)
{
   if (FincasF != NULL)
       FincasF->desbloqueaSector();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::ComercialesBloquea (TDataSet *DataSet)
{
    if (ComercialF != NULL)
        ComercialF->bloqueaFormulario();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::ComercialesDesbloquea (TDataSet *DataSet)
{
   if (ComercialF != NULL)
       ComercialF->desbloqueaFormulario();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::ClientesBloquea (TDataSet *DataSet)
{
    if (ClienteF != NULL)
        ClienteF->bloqueaFormulario();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::ClientesDesbloquea (TDataSet *DataSet)
{
   if (ClienteF != NULL)
       ClienteF->desbloqueaFormulario();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::ProductosBloquea (TDataSet *DataSet)
{
    if (ProductF != NULL)
        ProductF->bloqueaFormulario();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::ProductosDesbloquea (TDataSet *DataSet)
{
   if (ProductF != NULL)
       ProductF->desbloqueaFormulario();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::CoeficientesBloquea (TDataSet *DataSet)
{
    if (CoeficienteF != NULL)
        CoeficienteF->bloqueaFormulario();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::CoeficientesDesbloquea (TDataSet *DataSet)
{
   if (CoeficienteF != NULL)
       CoeficienteF->desbloqueaFormulario();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::RepartoBloquea (TDataSet *DataSet)
{
    if (RepartoF != NULL)
        RepartoF->bloqueaFormulario();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::RepartoDesbloquea (TDataSet *DataSet)
{
   if (RepartoF != NULL)
       RepartoF->desbloqueaFormulario();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::DiasRiegoBloquea (TDataSet *DataSet)
{
    if (RiegoF != NULL)
        RiegoF->bloqueaFormulario();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::DiasRiegoDesbloquea (TDataSet *DataSet)
{
   if (RiegoF != NULL)
       RiegoF->desbloqueaFormulario();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::RiegoPlantBloquea (TDataSet *DataSet)
{
    if (RiegoPlantF != NULL)
        RiegoPlantF->bloqueaFormulario();
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::RiegoPlantDesbloquea (TDataSet *DataSet)
{
   if (RiegoPlantF != NULL)
       RiegoPlantF->desbloqueaFormulario();
}
//---------------------------------------------------------------------------
//Genera un codigo para un plan anual o mensual del anyo indicado
AnsiString TDatosF::generaCodigo(AnsiString Cod, AnsiString Anyo)
{
   Variant key[2];
   AnsiString codigo;
    //Buscar registro del codigo y del a�o
      key[0] = Cod;
      key[1] = Anyo;
      DatosF->Contador->Open();
      if (DatosF->Contador->Locate("CODIGO;ANYO",VarArrayOf(key,1),TLocateOptions()))
      {  //recuperar e incrementar el codigo guardado
         AnsiString numero;
         numero = AnsiString(DatosF->ContadorNSECUENCIAL->Value);
         while (numero.Length() < 5)
               numero = "0" + numero;
         codigo = Cod + Anyo + numero;
         DatosF->Contador->Edit();
         DatosF->ContadorNSECUENCIAL->Value += 1;
         DatosF->Contador->Post();
      }
      else //Sino existe: crear registro del anyo y generar codigo.
      {
         codigo = Cod + Anyo + "00001";
         DatosF->Contador->Insert();
         DatosF->ContadorCODIGO->Value = Cod;
         DatosF->ContadorANYO->Value = Anyo;
         DatosF->ContadorNSECUENCIAL->Value = 2;
         DatosF->Contador->Post();
      }
      DatosF->Contador->Close();
      return codigo;
}
//---------------------------------------------------------------------------
void __fastcall TDatosF::ProductosCalcFields(TDataSet *DataSet)
{
   float resultado;



   resultado  = ProductosDSDesigner01_ENERO->Value;
   resultado += ProductosDSDesigner02_FEBRERO->Value;
   resultado += ProductosDSDesigner03_MARZO->Value;
   resultado += ProductosDSDesigner04_ABRIL->Value;
   resultado += ProductosDSDesigner05_MAYO->Value;
   resultado += ProductosDSDesigner06_JUNIO->Value;
   resultado += ProductosDSDesigner07_JULIO->Value;
   resultado += ProductosDSDesigner08_AGOSTO->Value;
   resultado += ProductosDSDesigner09_SEPTIEMBRE->Value;
   resultado += ProductosDSDesigner10_OCTUBRE->Value;
   resultado += ProductosDSDesigner11_NOVIEMBRE->Value;
   resultado += ProductosDSDesigner12_DICIEMBRE->Value;

   ProductosTOTAL->Value = resultado;
}
//---------------------------------------------------------------------------



