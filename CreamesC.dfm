inherited CreamesF: TCreamesF
  Left = 539
  Top = 397
  Width = 245
  Height = 258
  Caption = 'Crear mes'
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel [0]
    Left = 8
    Top = 40
    Width = 19
    Height = 13
    Caption = 'A'#241'o'
  end
  object Label3: TLabel [1]
    Left = 112
    Top = 40
    Width = 20
    Height = 13
    Caption = 'Mes'
  end
  object Label4: TLabel [2]
    Left = 40
    Top = 96
    Width = 71
    Height = 13
    Caption = 'D'#237'as riego mes'
  end
  object New: TLabel [3]
    Left = 8
    Top = 144
    Width = 209
    Height = 13
    AutoSize = False
  end
  object Skip: TLabel [4]
    Left = 8
    Top = 160
    Width = 209
    Height = 13
    AutoSize = False
  end
  inherited Cerrar: TBitBtn
    Left = 144
    Top = 184
    TabOrder = 7
    OnClick = CerrarClick
  end
  inherited NuevoB: TBitBtn
    Left = 8
    Top = 216
    TabOrder = 8
    Visible = False
  end
  inherited GuardarB: TBitBtn
    Left = 8
    Top = 216
    TabOrder = 9
    Visible = False
  end
  inherited EliminarB: TBitBtn
    Left = 8
    Top = 216
    TabOrder = 10
    Visible = False
  end
  object Anyo: TEdit
    Left = 40
    Top = 40
    Width = 65
    Height = 21
    Hint = 'A'#241'o'
    TabOrder = 0
  end
  object Mes: TEdit
    Left = 136
    Top = 40
    Width = 65
    Height = 21
    TabOrder = 1
  end
  object Crear: TButton
    Left = 8
    Top = 184
    Width = 75
    Height = 25
    Caption = 'Crear'
    TabOrder = 6
    OnClick = CrearClick
  end
  object Progreso: TProgressBar
    Left = 8
    Top = 128
    Width = 209
    Height = 13
    Min = 0
    Max = 100
    TabOrder = 5
  end
  object EsQuincena: TCheckBox
    Left = 40
    Top = 64
    Width = 73
    Height = 17
    Caption = 'Quincena'
    TabOrder = 2
    OnClick = EsQuincenaClick
  end
  object Quincena: TEdit
    Left = 136
    Top = 64
    Width = 41
    Height = 21
    TabOrder = 3
    Text = '1'
    Visible = False
  end
  object DRiego: TEdit
    Left = 136
    Top = 96
    Width = 65
    Height = 21
    TabOrder = 4
  end
  object Productos: TADOTable
    Connection = DatosF.Abono
    TableName = 'PRODUCTOS'
    object ProductosCODART: TAutoIncField
      FieldName = 'CODART'
      ReadOnly = True
    end
    object ProductosNOMCOMERCIAL: TWideStringField
      FieldName = 'NOMCOMERCIAL'
      Size = 50
    end
    object ProductosDISOLUCIN: TFloatField
      FieldName = 'DISOLUCI'#211'N'
    end
  end
  object Fincas: TADOTable
    Connection = DatosF.Abono
    TableName = 'FINCAS'
    Left = 32
    object FincasCAPACIDADCUBAS: TFloatField
      FieldName = 'CAPACIDADCUBAS'
    end
    object FincasCAUDALCUBAS: TFloatField
      FieldName = 'CAUDALCUBAS'
    end
    object FincasCODFINCA: TAutoIncField
      FieldName = 'CODFINCA'
      ReadOnly = True
    end
    object FincasNOMFINCA: TWideStringField
      FieldName = 'NOMFINCA'
      Size = 50
    end
  end
  object ListaFincas: TADOQuery
    Connection = DatosF.Abono
    Parameters = <>
    Left = 104
  end
  object CtdMensual: TADOQuery
    Connection = DatosF.Abono
    Parameters = <>
    Left = 136
  end
  object DetAnual: TADOTable
    Connection = DatosF.Abono
    TableName = 'DETANUAL'
    Left = 64
  end
  object ListaSectores: TADOQuery
    Connection = DatosF.Abono
    Parameters = <>
    SQL.Strings = (
      'SELECT S.SECTOR,S.TRIEGO,S.PLANTONADA,A.CODIGO_AF,A.CODANUAL'
      'FROM SECTORES S, ANUAL A'
      'WHERE A.CODFINCA = S.CODFINCA AND'
      '      A.SECTOR   = S.SECTOR')
    Left = 168
    object ListaSectoresSECTOR: TWideStringField
      FieldName = 'SECTOR'
    end
    object ListaSectoresTRIEGO: TSmallintField
      FieldName = 'TRIEGO'
    end
    object ListaSectoresPLANTONADA: TBooleanField
      FieldName = 'PLANTONADA'
    end
    object ListaSectoresCODIGO_AF: TIntegerField
      FieldName = 'CODIGO_AF'
    end
    object ListaSectoresCODANUAL: TWideStringField
      FieldName = 'CODANUAL'
      Size = 15
    end
  end
end
