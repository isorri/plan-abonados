//---------------------------------------------------------------------------

#ifndef ProvefeCH
#define ProvefeCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <DB.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TProvefeF : public TDataModule
{
__published:	// IDE-managed Components
        TDatabase *X3;
        TQuery *Precio;
        TStringField *PrecioTARIFA;
        TFloatField *PrecioPRECIO;
private:	// User declarations
public:		// User declarations
        __fastcall TProvefeF(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TProvefeF *ProvefeF;
//---------------------------------------------------------------------------
#endif
