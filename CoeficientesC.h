//---------------------------------------------------------------------------

#ifndef CoeficientesCH
#define CoeficientesCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "PlantillaC.h"
#include <Buttons.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TCoeficienteF : public TPlantilla
{
__published:	// IDE-managed Components
        TDBGrid *Coeficientes;
        TDBEdit *Patron;
        TLabel *Label2;
        TGroupBox *GroupBox1;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label10;
        TDBEdit *N1;
        TDBEdit *N2;
        TDBEdit *P2;
        TDBEdit *K2;
        TDBEdit *CA2;
        TDBEdit *MG2;
        TDBEdit *FE2;
        TDBEdit *MICRO2;
        TDBEdit *AH2;
        TDBEdit *P1;
        TDBEdit *K1;
        TDBEdit *CA1;
        TDBEdit *MG1;
        TDBEdit *FE1;
        TDBEdit *MICRO1;
        TDBEdit *AH1;
        TButton *Cancelar;
        TLabel *Label11;
        TLabel *Label12;
        TLabel *Label13;
        TLabel *Label14;
        TLabel *Label15;
        TLabel *Label16;
        TLabel *Label17;
        TLabel *Label18;
        TLabel *Label19;
        TLabel *Label20;
        TLabel *Label21;
        TLabel *Label22;
        TLabel *Label23;
        TLabel *Label24;
        TLabel *Label25;
        TLabel *Label26;
        TLabel *Label27;
        TLabel *Label28;
        TLabel *Label29;
        TLabel *Label30;
        void __fastcall InsertarClick(TObject *Sender);
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall GuardarClick(TObject *Sender);
        void __fastcall CancelarClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall EliminarClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TCoeficienteF(TComponent* Owner);
        void bloqueaFormulario(void);
        void desbloqueaFormulario(void);        
};
//---------------------------------------------------------------------------
extern PACKAGE TCoeficienteF *CoeficienteF;
//---------------------------------------------------------------------------
#endif
