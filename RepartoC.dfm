inherited RepartoF: TRepartoF
  Left = 364
  Top = 233
  Width = 602
  Height = 368
  Caption = 'Plantilla reparto'
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited Cerrar: TBitBtn
    Left = 528
    Top = 296
    Width = 57
    Caption = '&Cerrar'
    OnClick = CerrarClick
  end
  inherited NuevoB: TBitBtn
    Left = 240
    Top = 296
    Width = 65
    TabOrder = 5
    OnClick = NuevoClick
  end
  inherited GuardarB: TBitBtn
    Left = 312
    Top = 296
    Width = 65
    Enabled = False
    TabOrder = 6
    OnClick = GuardarClick
  end
  inherited EliminarB: TBitBtn
    Left = 456
    Top = 296
    Width = 65
    TabOrder = 7
    OnClick = EliminarClick
  end
  object ListaIzq: TDBGrid
    Left = 8
    Top = 8
    Width = 225
    Height = 313
    DataSource = DatosF.RepartoSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CODIGO'
        Title.Caption = 'C'#243'digo plantilla'
        Width = 190
        Visible = True
      end>
  end
  object GroupBox1: TGroupBox
    Left = 240
    Top = 8
    Width = 337
    Height = 65
    TabOrder = 2
    object Label2: TLabel
      Left = 64
      Top = 24
      Width = 33
      Height = 13
      Caption = 'C'#243'digo'
    end
    object Codigo: TDBEdit
      Left = 104
      Top = 24
      Width = 137
      Height = 21
      DataField = 'CODIGO'
      DataSource = DatosF.RepartoSource
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 240
    Top = 80
    Width = 337
    Height = 209
    Caption = 'Reparto mensual'
    TabOrder = 3
    object Label3: TLabel
      Left = 24
      Top = 24
      Width = 28
      Height = 13
      Caption = 'Enero'
    end
    object Label4: TLabel
      Left = 96
      Top = 24
      Width = 36
      Height = 13
      Caption = 'Febrero'
    end
    object Label5: TLabel
      Left = 168
      Top = 24
      Width = 29
      Height = 13
      Caption = 'Marzo'
    end
    object Label6: TLabel
      Left = 240
      Top = 24
      Width = 20
      Height = 13
      Caption = 'Abril'
    end
    object Label7: TLabel
      Left = 24
      Top = 72
      Width = 26
      Height = 13
      Caption = 'Mayo'
    end
    object Label8: TLabel
      Left = 96
      Top = 72
      Width = 25
      Height = 13
      Caption = 'Junio'
    end
    object Label9: TLabel
      Left = 168
      Top = 72
      Width = 21
      Height = 13
      Caption = 'Julio'
    end
    object Label10: TLabel
      Left = 240
      Top = 72
      Width = 33
      Height = 13
      Caption = 'Agosto'
    end
    object Label11: TLabel
      Left = 24
      Top = 120
      Width = 53
      Height = 13
      Caption = 'Septiembre'
    end
    object Label12: TLabel
      Left = 96
      Top = 120
      Width = 38
      Height = 13
      Caption = 'Octubre'
    end
    object Label13: TLabel
      Left = 168
      Top = 120
      Width = 51
      Height = 13
      Caption = 'Noviembre'
    end
    object Label14: TLabel
      Left = 240
      Top = 120
      Width = 47
      Height = 13
      Caption = 'Diciembre'
    end
    object Label15: TLabel
      Left = 88
      Top = 168
      Width = 35
      Height = 13
      Caption = '% Total'
    end
    object Enero: TDBEdit
      Left = 24
      Top = 40
      Width = 65
      Height = 21
      DataField = '01_ENERO'
      DataSource = DatosF.RepartoSource
      TabOrder = 0
    end
    object Febrero: TDBEdit
      Left = 96
      Top = 40
      Width = 65
      Height = 21
      DataField = '02_FEBRERO'
      DataSource = DatosF.RepartoSource
      TabOrder = 1
    end
    object Marzo: TDBEdit
      Left = 168
      Top = 40
      Width = 65
      Height = 21
      DataField = '03_MARZO'
      DataSource = DatosF.RepartoSource
      TabOrder = 2
    end
    object Abril: TDBEdit
      Left = 240
      Top = 40
      Width = 65
      Height = 21
      DataField = '04_ABRIL'
      DataSource = DatosF.RepartoSource
      TabOrder = 3
    end
    object Mayo: TDBEdit
      Left = 24
      Top = 88
      Width = 65
      Height = 21
      DataField = '05_MAYO'
      DataSource = DatosF.RepartoSource
      TabOrder = 4
    end
    object Junio: TDBEdit
      Left = 96
      Top = 88
      Width = 65
      Height = 21
      DataField = '06_JUNIO'
      DataSource = DatosF.RepartoSource
      TabOrder = 5
    end
    object Julio: TDBEdit
      Left = 168
      Top = 88
      Width = 65
      Height = 21
      DataField = '07_JULIO'
      DataSource = DatosF.RepartoSource
      TabOrder = 6
    end
    object Agosto: TDBEdit
      Left = 240
      Top = 88
      Width = 65
      Height = 21
      DataField = '08_AGOSTO'
      DataSource = DatosF.RepartoSource
      TabOrder = 7
    end
    object Septiembre: TDBEdit
      Left = 24
      Top = 136
      Width = 65
      Height = 21
      DataField = '09_SEPTIEMBRE'
      DataSource = DatosF.RepartoSource
      TabOrder = 8
    end
    object Octubre: TDBEdit
      Left = 96
      Top = 136
      Width = 65
      Height = 21
      DataField = '10_OCTUBRE'
      DataSource = DatosF.RepartoSource
      TabOrder = 9
    end
    object Noviembre: TDBEdit
      Left = 168
      Top = 136
      Width = 65
      Height = 21
      DataField = '11_NOVIEMBRE'
      DataSource = DatosF.RepartoSource
      TabOrder = 10
    end
    object Diciembre: TDBEdit
      Left = 240
      Top = 136
      Width = 65
      Height = 21
      DataField = '12_DICIEMBRE'
      DataSource = DatosF.RepartoSource
      TabOrder = 11
    end
    object Total: TDBEdit
      Left = 136
      Top = 168
      Width = 73
      Height = 21
      DataField = 'TOTAL'
      DataSource = DatosF.RepartoSource
      ReadOnly = True
      TabOrder = 12
    end
  end
  object Cancelar: TButton
    Left = 384
    Top = 296
    Width = 65
    Height = 25
    Caption = '&Cancelar'
    Enabled = False
    TabOrder = 4
    OnClick = CancelarClick
  end
end
