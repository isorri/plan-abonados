inherited ComercialF: TComercialF
  Left = 345
  Top = 424
  Width = 718
  Height = 306
  Caption = 'Comerciales'
  OldCreateOrder = True
  PrintScale = poPrintToFit
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited Cerrar: TBitBtn
    Left = 648
    Top = 216
    Width = 48
    OnClick = CerrarClick
  end
  object ListaIzq: TDBGrid [1]
    Left = 0
    Top = 0
    Width = 353
    Height = 265
    DataSource = DatosF.ComercialesSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CODCOMERCIAL'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMBRE'
        Title.Caption = 'Nombre'
        Width = 248
        Visible = True
      end>
  end
  object DatosGroup: TGroupBox [2]
    Left = 360
    Top = 0
    Width = 337
    Height = 209
    TabOrder = 2
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 33
      Height = 13
      Caption = 'C'#243'digo'
    end
    object Label2: TLabel
      Left = 16
      Top = 64
      Width = 37
      Height = 13
      Caption = 'Nombre'
    end
    object Codigo: TDBEdit
      Left = 16
      Top = 32
      Width = 89
      Height = 21
      Hint = 'C'#243'digo del comercial'
      DataField = 'CODCOMERCIAL'
      DataSource = DatosF.ComercialesSource
      Enabled = False
      ReadOnly = True
      TabOrder = 0
    end
    object Nombre: TDBEdit
      Left = 16
      Top = 80
      Width = 308
      Height = 21
      DataField = 'NOMBRE'
      DataSource = DatosF.ComercialesSource
      TabOrder = 1
    end
  end
  object Cancelar: TButton [3]
    Left = 496
    Top = 216
    Width = 58
    Height = 25
    Caption = '&Cancelar'
    Enabled = False
    TabOrder = 3
    OnClick = CancelarClick
  end
  inherited NuevoB: TBitBtn
    Left = 360
    Top = 216
    Width = 61
    TabOrder = 4
    OnClick = NuevoClick
  end
  inherited GuardarB: TBitBtn
    Left = 424
    Top = 216
    Width = 65
    Enabled = False
    TabOrder = 5
    OnClick = GuardarClick
  end
  inherited EliminarB: TBitBtn
    Left = 560
    Top = 216
    Width = 63
    TabOrder = 6
    OnClick = EliminarClick
  end
end
