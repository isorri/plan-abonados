//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FincasC.h"
#include "DatosC.h"
#include "MainC.h"
#include "AnalisisSectC.h"
#include "AnalisisFincaC.h"
#include "BuscarClienteC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFincasF *FincasF ;
//---------------------------------------------------------------------------
__fastcall TFincasF::TFincasF(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TFincasF::FormShow(TObject *Sender)
{
    Nombre->SetFocus();
}
//---------------------------------------------------------------------------
void __fastcall TFincasF::RegistraFincaClick(TObject *Sender)
{
    DatosF->Fincas->Post();
}
//---------------------------------------------------------------------------

void __fastcall TFincasF::CancelaClick(TObject *Sender)
{
    DatosF->Fincas->Cancel();
}
//---------------------------------------------------------------------------

void __fastcall TFincasF::NuevaClick(TObject *Sender)
{
    Nombre->SetFocus();
    DatosF->Fincas->Insert();
    DatosF->Fincas->FieldValues["CAUDALCUBAS"] = 0;
    bloqueaFormulario();
}
//---------------------------------------------------------------------------

void __fastcall TFincasF::GuardaSectorClick(TObject *Sender)
{
    //Comprobaciones antes de guardar el sector
    if (DatosF->Sectores->State == dsEdit || DatosF->Sectores->State == dsInsert)
    {
        if (Sector->Text == "")
        {
            Application->MessageBoxA("Sector no puede ser nulo","Error",MB_ICONERROR);
            Sector->SetFocus();
            return;
        }
        if (Patron->Text == "")
        {
            Application->MessageBoxA("Patr�n no puede ser nulo","Error",MB_ICONERROR);
            Patron->SetFocus();
            return;
        }
        if (NumArboles->Text == "")
        {
           Application->MessageBoxA("El numero de arboles no puede ser nulo","Error",MB_ICONERROR);
           NumArboles->SetFocus();
           return;
        }
        if (Hg->Text == "")
        {
           Application->MessageBoxA("Las hanegadas no pueden ser nulas","Error",MB_ICONERROR);
           Hg->SetFocus();
           return;
        }
        DatosF->Sectores->Post();
        ListSectores->Enabled = true;
    }
    else
        Application->MessageBoxA("Nada que guardar.","Informaci�n",MB_ICONINFORMATION);
}
//---------------------------------------------------------------------------
void __fastcall TFincasF::NuevoSectorClick(TObject *Sender)
{
    if (DatosF->Fincas->State == dsInsert)
        Application->MessageBoxA("Finca pendiente de inserci�n.","Error",MB_ICONERROR);
    else
        if (DatosF->Fincas->State == dsEdit)
            Application->MessageBoxA("Finca modificada con valores no guardados","Error",MB_ICONERROR);
        else
            if (DatosF->Sectores->State == dsEdit)
                Application->MessageBoxA("Sector modificado con valores no guardados","Error",MB_ICONERROR);
            else
            {
                ListSectores->Enabled = false;
                DatosF->Sectores->Insert();
                Sector->SetFocus();
            }
}
//---------------------------------------------------------------------------

void __fastcall TFincasF::FormClose(TObject *Sender, TCloseAction &Action)
{
    int opcion;

        if (DatosF->Fincas->State == dsInsert )
        {
           opcion = Application->MessageBoxA("�Guardar la finca nueva?","Pregunta",MB_YESNO + MB_ICONQUESTION);
           if (opcion == IDYES)
               DatosF->Fincas->Post();
           else
               DatosF->Fincas->Cancel();
        }
        if ( DatosF->Fincas->State == dsEdit )
        {
           opcion = Application->MessageBoxA("�Guardar los cambios realizados de la Finca?","Pregunta",MB_YESNO + MB_ICONQUESTION);
           if (opcion == IDYES)

               DatosF->Fincas->Post();
           else
               DatosF->Fincas->Cancel();
        }
        if (DatosF->Sectores->State == dsEdit)
        {
                opcion = Application->MessageBoxA("�Guardar los cambios de los sectores?","Pregunta",MB_YESNO + MB_ICONQUESTION);
                if (opcion == IDYES)
                   DatosF->Sectores->Post();
                else
                   DatosF->Sectores->Cancel();
        }
        if (DatosF->Sectores->State == dsInsert)
        {
                opcion = Application->MessageBoxA("�Guardar el sector nuevo introducido?","Pregunta",MB_YESNO + MB_ICONQUESTION);
                if (opcion == IDYES)
                   DatosF->Sectores->Post();
                else
                   DatosF->Sectores->Cancel();
        }

       //Formulario preparado para ser destruido.
       Action = caFree;
       FincasF = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TFincasF::CerrarClick(TObject *Sender)
{
        Close();
}
//---------------------------------------------------------------------------

void __fastcall TFincasF::AnalisisFincaClick(TObject *Sender)
{
        if (AnalisisFincaF == NULL)
        {
            AnalisisFincaF = new TAnalisisFincaF(Application);
            //Realizar el filtro para que solo muestre an�lisis de la finca
            DatosF->AnalisisFinca->Filter     = "CODFINCA = " + CODFINCA->Text ;
            DatosF->AnalisisFinca->Filtered   = true;
            AnalisisFincaF->Nomcli->Caption   = NomCliente->Text;
            AnalisisFincaF->Nomfinca->Caption = Nombre->Text;
            AnalisisFincaF->CodFinca->Text    = CODFINCA->Text;
            if (!DatosF->AnalisisFinca->RecordCount)
            {
                //No hay ning�n an�lisis introducido.
                 ShowMessage("Para crear un an�lisis pulse el bot�n Nuevo");
                 AnalisisFincaF->GroupBox2->Enabled = false;
            }
        }
        FincasF->Enabled = false;
        AnalisisFincaF->Show();
}

//---------------------------------------------------------------------------
void __fastcall TFincasF::EliminaFincaClick(TObject *Sender)
{
   if ( DatosF->Anual->Locate("CODFINCA",CODFINCA->Text, TLocateOptions()))
      Application->MessageBoxA("No se puede borrar la finca porque tiene alg�n plan anual asociado.\nBorre primero el plan anual","Error",MB_ICONERROR);
   else
      DatosF->Fincas->Delete();
}
//---------------------------------------------------------------------------

void __fastcall TFincasF::CancelaSectorClick(TObject *Sender)
{
    if (DatosF->Sectores->State == dsEdit || DatosF->Sectores->State == dsInsert)
    {
        DatosF->Sectores->Cancel();
        ListSectores->Enabled = true;
    }
    else
        Application->MessageBoxA("Nada que cancelar.","Informaci�n",MB_ICONINFORMATION);
}
//---------------------------------------------------------------------------

void __fastcall TFincasF::EliminaSectorClick(TObject *Sender)
{
     TDataSetState StSector;
        if (DatosF->Sectores->IsEmpty())
        {
            Application->MessageBoxA("Nada que eliminar","Informaci�n",MB_ICONINFORMATION);
            return;
        }
        StSector = DatosF->Sectores->State;
        if (AnalisisSectF == NULL)
        {
            if (StSector == dsInsert || StSector == dsEdit)
            {
                Application->MessageBoxA("No se puede borrar el sector.\nSector pendiente de modificaci�n/inserci�n.","Error",MB_ICONERROR);
                return;
            }
            DatosF->Sectores->Delete();
        }
        else
        {
            Application->MessageBoxA("Hay un an�lisis de sector pendiente de inserci�n.\nDebe finalizar primero ese an�lisis.","Error",MB_ICONERROR);
            AnalisisSectF->Show();
        }
}
//---------------------------------------------------------------------------

void __fastcall TFincasF::AnalisisSectorClick(TObject *Sender)
{
        if (DatosF->Sectores->IsEmpty())
        {
            Application->MessageBoxA("Ning�n sector introducido","Informaci�n",MB_ICONINFORMATION);
            return;
        }
        if (AnalisisSectF == NULL)
        {
            AnalisisSectF = new TAnalisisSectF(Application);
            //Definir el filtro para que se muestren unicamente los analisis del sector
            DatosF->AnalisisSector->Filter  = "CODFINCA = " + CODFINCA->Text;
            DatosF->AnalisisSector->Filter += " AND SECTOR = '" + DatosF->SectoresSECTOR->Value + "'";
            DatosF->AnalisisSector->Filtered = true;

            AnalisisSectF->NomCli->Caption = NomCliente->Text;
            AnalisisSectF->NomFinca->Caption = Nombre->Text;
            AnalisisSectF->Sector->Caption = DatosF->Sectores->FieldValues["SECTOR"];
            AnalisisSectF->CodFinca->Text = CODFINCA->Text;
            if (!DatosF->AnalisisSector->RecordCount)
            {
                //No hay ning�n an�lisis introducido.
                 ShowMessage("Para crear un an�lisis pulse el bot�n Nuevo");
                 AnalisisSectF->GroupBox1->Enabled = false;
            }
        }
        this->Enabled = false;
        AnalisisSectF->Show();
}
//---------------------------------------------------------------------------

void __fastcall TFincasF::DstLineaChange(TObject *Sender)
{
        calculaSuperficie();
}
//---------------------------------------------------------------------------

void __fastcall TFincasF::DstArbolChange(TObject *Sender)
{
        calculaSuperficie();
}
//---------------------------------------------------------------------------

void __fastcall TFincasF::NumArbolesChange(TObject *Sender)
{
        calculaSuperficie();
}
//---------------------------------------------------------------------------
void TFincasF::calculaSuperficie(void)
{
  //Si no se comprueba que este en modo de busqueda, al cargar el formulario
  //aparece un error.
  if (DatosF->Sectores->State == dsBrowse)
      return;
  //Realiza el calculo de la superficie del sector con los datos introducidos
  if (NumArboles->Text != "" && DstArbol->Text != "" && DstLinea->Text != "")
      DatosF->Sectores->FieldValues["HG"] = ceil((float)(NumArboles->Text * DstArbol->Text * DstLinea->Text) / 10000.0);
}
//---------------------------------------------------------------------------
void TFincasF::desbloqueaFormulario(void)
{
   //Habilitar lista izquierda
   Fincas->Enabled = true;

   //Habilitar pesta�a sectores
   Sectores->Enabled = true;

   //Tratamiento de botones
   NuevoSector->Enabled = true;
   DuplicaB->Enabled = true;
   GuardarB->Enabled = false;
   EliminarB->Enabled = true;
   Cancela->Enabled = false;
   AnalisisFinca->Enabled = true;
   AnalisisSector->Enabled = true;
   NuevoB->Enabled = true;
}
//---------------------------------------------------------------------------
void TFincasF::bloqueaFormulario(void)
{
   //Deshabilitar lista izquierda
   Fincas->Enabled = false;

   //Deshabilitar sectores.
   Sectores->Enabled = false;

   //Tratamiento de botones
   NuevoSector->Enabled = false;
   DuplicaB->Enabled = false;
   GuardarB->Enabled = true;
   Cancela->Enabled = true;
   EliminarB->Enabled = false;
   AnalisisFinca->Enabled = false;
   AnalisisSector->Enabled = false;
   NuevoB->Enabled = false;
}
//---------------------------------------------------------------------------
void TFincasF::bloqueaSector(void)
{
   //Deshabilitar lista izquierda
   Fincas->Enabled = false;
   ListSectores->Enabled = true;

   //Tratamiento de botones
   NuevoSector->Enabled = false;
   GuardaSector->Enabled = true;
   CancelaSector->Enabled = true;
   EliminaSector->Enabled = false;
   AnalisisFinca->Enabled = false;
   AnalisisSector->Enabled = false;
   NuevoB->Enabled = false;
   DuplicaB->Enabled = false;
}
//---------------------------------------------------------------------------
void TFincasF::desbloqueaSector(void)
{
   //Deshabilitar lista izquierda
   Fincas->Enabled = true;
   ListSectores->Enabled = true;

   //Tratamiento de botones
   NuevoSector->Enabled = true;
   GuardaSector->Enabled = false;
   CancelaSector->Enabled = false;
   EliminaSector->Enabled = true;
   AnalisisFinca->Enabled = true;
   AnalisisSector->Enabled = true;
   NuevoB->Enabled = true;
   DuplicaB->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFincasF::DuplicaBClick(TObject *Sender)
{
   int res;
   Variant sectorkey[2];

   res = Application->MessageBoxA("�Esta seguro de duplicar este sector?","Pregunta",MB_ICONQUESTION+MB_YESNO);
   if (res != IDYES)
      return;

   //Abrir tabla auxiliar
   SectorDupl->Open();
   //Cargar sector actual
   sectorkey[0] = DatosF->SectoresCODFINCA->Value;
   sectorkey[1] = DatosF->SectoresSECTOR->Value;
   if ( !SectorDupl->Locate("CODFINCA;SECTOR",VarArrayOf(sectorkey,1), TLocateOptions()) )
   {
      Application->MessageBoxA("Error al cargar el sector.","Error",MB_ICONERROR);
      return;
   }
   //Duplicar el sector actual
   DatosF->Sectores->Insert();
   DatosF->SectoresVARIEDAD->Value = SectorDuplVARIEDAD->Value;
   DatosF->SectoresPATRON->Value = SectorDuplPATRON->Value;
   DatosF->SectoresTRIEGO->Value = SectorDuplTRIEGO->Value;
   DatosF->SectoresDSTLINEA->Value = SectorDuplDSTLINEA->Value;
   DatosF->SectoresDSTARBOL->Value = SectorDuplDSTARBOL->Value;
   DatosF->SectoresARBOLNUM->Value = SectorDuplARBOLNUM->Value;
   DatosF->SectoresHG->Value = SectorDuplHG->Value;
   DatosF->SectoresEdad->Value = SectorDuplEdad->Value;
   DatosF->SectoresGOTEOXARB->Value = SectorDuplGOTEOXARB->Value;
   DatosF->SectoresCAUDALGOT->Value = SectorDuplCAUDALGOT->Value;
   Sector->SetFocus();

   //Cerrar la tabla temporal
   SectorDupl->Close();
   //El usuario tiene que introducir el c�digo del sector y guardar.
}
//---------------------------------------------------------------------------

void __fastcall TFincasF::BuscarClienteBClick(TObject *Sender)
{
   if (BuscarClienteF == NULL)
   {
       BuscarClienteF = new TBuscarClienteF(Application);
       BuscarClienteF->TodosFlag->Checked = true;
       BuscarClienteF->ComercialLista->Enabled = false;
       BuscarClienteF->ClienteText->SetFocus();
       BuscarClienteF->BuscarCliente->Open();
   }
   FincasF->Enabled = false;
   BuscarClienteF->Show();
}
//---------------------------------------------------------------------------
void __fastcall TFincasF::NombreExit(TObject *Sender)
{
    if(CodCli->Text == "")
        BuscarClienteBClick(this);
}
//---------------------------------------------------------------------------


