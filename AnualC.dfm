inherited AbonadoAnualF: TAbonadoAnualF
  Left = 198
  Top = 202
  Width = 920
  Height = 618
  Caption = 'Plan Abonado Anual'
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label20: TLabel [0]
    Left = 8
    Top = 0
    Width = 57
    Height = 13
    Caption = 'Mostrar A'#241'o'
  end
  object Label16: TLabel [1]
    Left = 152
    Top = 0
    Width = 61
    Height = 13
    Caption = 'Mostrar finca'
  end
  inherited Cerrar: TBitBtn
    Left = 840
    Top = 544
    Width = 51
    TabOrder = 11
    OnClick = CerrarClick
  end
  object Clave: TGroupBox [3]
    Left = 488
    Top = 8
    Width = 409
    Height = 113
    TabOrder = 3
    object Label12: TLabel
      Left = 8
      Top = 48
      Width = 29
      Height = 13
      Caption = 'Finca '
    end
    object Label13: TLabel
      Left = 272
      Top = 48
      Width = 31
      Height = 13
      Caption = 'Sector'
    end
    object Label14: TLabel
      Left = 352
      Top = 48
      Width = 19
      Height = 13
      Caption = 'A'#241'o'
    end
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 33
      Height = 13
      Caption = 'C'#243'digo'
    end
    object NombreCliente: TDBText
      Left = 120
      Top = 24
      Width = 265
      Height = 17
      Hint = 'Nombre del cliente due'#241'o de la finca'
      DataField = 'NOMCLI'
      DataSource = DatosF.AnualSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 120
      Top = 8
      Width = 32
      Height = 13
      Caption = 'Cliente'
    end
    object BuscaFincaB: TSpeedButton
      Left = 376
      Top = 88
      Width = 23
      Height = 22
      Hint = 'Realiza la b'#250'squeda de la finca y sector.'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF008400000084000000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF0084000000C6C6C6008400000084000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF0084000000C6C6C600840000008400000084000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008400
        0000C6C6C600840000008400000084000000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF000000000000000000000000000000000084000000C6C6
        C600840000008400000084000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF000000000000000000C6C6C600C6C6C600C6C6C600FFFFFF00848484008400
        00008400000084000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
        000084848400C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600FFFFFF008484
        840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
        0000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600FFFF
        FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000C6C6
        C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
        C600C6C6C60000000000FF00FF00FF00FF00FF00FF00FF00FF0000000000C6C6
        C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
        C600C6C6C60000000000FF00FF00FF00FF00FF00FF00FF00FF0000000000C6C6
        C600FFFFFF00FFFF0000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
        C600C6C6C60000000000FF00FF00FF00FF00FF00FF00FF00FF0000000000C6C6
        C600FFFFFF00FFFF0000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
        C600C6C6C60000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
        0000FFFFFF00FFFFFF00FFFF0000FFFF0000C6C6C600C6C6C600C6C6C600C6C6
        C60000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
        000084848400FFFFFF00FFFFFF00FFFFFF00C6C6C600C6C6C600C6C6C6008484
        840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF000000000000000000C6C6C600C6C6C600C6C6C600C6C6C600000000000000
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      OnClick = BuscaFincaBClick
    end
    object TDBEdit
      Left = 352
      Top = 64
      Width = 49
      Height = 21
      DataField = 'ANYO'
      DataSource = DatosF.AnualSource
      TabOrder = 5
    end
    object Sector: TDBEdit
      Left = 272
      Top = 64
      Width = 73
      Height = 21
      DataField = 'SECTOR'
      DataSource = DatosF.AnualSource
      Enabled = False
      ReadOnly = True
      TabOrder = 4
      OnChange = SectorChange
    end
    object CodFinca: TDBEdit
      Left = 8
      Top = 64
      Width = 41
      Height = 21
      DataField = 'CODFINCA'
      DataSource = DatosF.AnualSource
      Enabled = False
      ReadOnly = True
      TabOrder = 2
      OnChange = CodFincaChange
    end
    object NomFinca: TDBEdit
      Left = 48
      Top = 64
      Width = 217
      Height = 21
      Hint = 
        'Nombre de la finca, puede ser modificado para este plan de abona' +
        'do.'
      DataField = 'NOMFINCA'
      DataSource = DatosF.AnualSource
      TabOrder = 3
    end
    object Impreso: TDBCheckBox
      Left = 336
      Top = 8
      Width = 65
      Height = 17
      Hint = 'Muestra si este plan anual ha sido anteriormente impreso.'
      Caption = 'Impreso'
      Color = clBtnFace
      DataField = 'IMPRESO'
      DataSource = DatosF.AnualSource
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object CodAnual: TDBEdit
      Left = 8
      Top = 24
      Width = 105
      Height = 21
      DataField = 'CODANUAL'
      DataSource = DatosF.AnualSource
      Enabled = False
      ReadOnly = True
      TabOrder = 0
    end
  end
  object Cancelar: TButton [4]
    Left = 568
    Top = 544
    Width = 56
    Height = 25
    Hint = 'Cancela la inserci'#243'n o modificaci'#243'n de datos actual.'
    Caption = 'Cancelar'
    Enabled = False
    TabOrder = 7
    OnClick = CancelarClick
  end
  object ListaIzq: TDBGrid [5]
    Left = 0
    Top = 24
    Width = 489
    Height = 513
    DataSource = DatosF.AnualSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CODANUAL'
        Title.Caption = 'C'#243'digo'
        Width = 69
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMCLI'
        Title.Caption = 'Cliente'
        Width = 149
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMFINCA'
        Title.Caption = 'Finca'
        Width = 146
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SECTOR'
        Title.Caption = 'Sector'
        Width = 42
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ANYO'
        Title.Caption = 'A'#241'o'
        Width = 39
        Visible = True
      end>
  end
  object FiltroAnyo: TEdit [6]
    Left = 72
    Top = 0
    Width = 73
    Height = 21
    Hint = 'Filtra la lista izquierda seg'#250'n el a'#241'o introducido'
    TabOrder = 1
    OnChange = FiltroAnyoChange
  end
  object Paginas: TPageControl [7]
    Left = 488
    Top = 128
    Width = 409
    Height = 409
    ActivePage = NecesidadesP
    TabIndex = 0
    TabOrder = 4
    object NecesidadesP: TTabSheet
      Caption = 'Necesidades'
      ImageIndex = 4
      object Estimacion: TGroupBox
        Left = 0
        Top = 0
        Width = 401
        Height = 233
        Caption = 'Analisis disponibles'
        TabOrder = 0
        object SelAnalisis: TDBGrid
          Left = 0
          Top = 16
          Width = 401
          Height = 193
          Hint = 
            'Seleccione la fecha de an'#225'lisis que desea utilizar para la estim' +
            'aci'#243'n de necesidades.'
          DataSource = AnalisisSource
          Options = [dgTitles, dgIndicator, dgColumnResize, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'FECHA'
              Title.Caption = 'Fecha'
              Width = 73
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'N'
              Width = 34
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'P'
              Width = 31
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'K'
              Width = 29
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CA'
              Title.Caption = 'Ca'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MG'
              Title.Caption = 'Mg'
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FE'
              Title.Caption = 'Fe'
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MICROELEMENTOS'
              Title.Caption = 'Micro'
              Width = 37
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AHUMICO'
              Title.Caption = 'AH'#250'mico'
              Width = 48
              Visible = True
            end>
        end
        object AnalisisTipo: TCheckBox
          Left = 16
          Top = 208
          Width = 185
          Height = 17
          Hint = 
            'Utilizar un an'#225'lisis tipo en vez de uno de los an'#225'lisis disponib' +
            'le'
          Caption = 'Utilizar an'#225'lisis tipo'
          TabOrder = 1
        end
      end
      object Necesidades: TGroupBox
        Left = 0
        Top = 232
        Width = 393
        Height = 145
        Caption = 'Necesidades (gr/arbol)'
        TabOrder = 1
        object Label3: TLabel
          Left = 216
          Top = 16
          Width = 35
          Height = 13
          Caption = 'F'#243'sforo'
        end
        object Label2: TLabel
          Left = 136
          Top = 16
          Width = 46
          Height = 13
          Caption = 'Nitr'#243'geno'
        end
        object Label5: TLabel
          Left = 136
          Top = 56
          Width = 29
          Height = 13
          Caption = 'Calcio'
        end
        object Label4: TLabel
          Left = 296
          Top = 16
          Width = 35
          Height = 13
          Caption = 'Potasio'
        end
        object Label6: TLabel
          Left = 216
          Top = 56
          Width = 46
          Height = 13
          Caption = 'Magnesio'
        end
        object Label7: TLabel
          Left = 296
          Top = 56
          Width = 28
          Height = 13
          Caption = 'Hierro'
        end
        object Label9: TLabel
          Left = 215
          Top = 96
          Width = 66
          Height = 13
          Caption = #193'cido H'#250'mico'
        end
        object Label8: TLabel
          Left = 136
          Top = 96
          Width = 74
          Height = 13
          Caption = 'Microelementos'
        end
        object Label15: TLabel
          Left = 296
          Top = 96
          Width = 73
          Height = 13
          Caption = 'Corrector salino'
        end
        object Label19: TLabel
          Left = 8
          Top = 56
          Width = 67
          Height = 13
          Caption = 'Fecha an'#225'lisis'
        end
        object Label10: TLabel
          Left = 8
          Top = 16
          Width = 104
          Height = 13
          Caption = 'Producci'#243'n (Kg/arbol)'
        end
        object N: TDBEdit
          Left = 136
          Top = 32
          Width = 73
          Height = 21
          DataField = 'N'
          DataSource = DatosF.AnualSource
          TabOrder = 0
        end
        object P: TDBEdit
          Left = 216
          Top = 32
          Width = 73
          Height = 21
          DataField = 'P'
          DataSource = DatosF.AnualSource
          TabOrder = 1
        end
        object K: TDBEdit
          Left = 296
          Top = 32
          Width = 73
          Height = 21
          DataField = 'K'
          DataSource = DatosF.AnualSource
          TabOrder = 2
        end
        object CA: TDBEdit
          Left = 136
          Top = 72
          Width = 73
          Height = 21
          DataField = 'CA'
          DataSource = DatosF.AnualSource
          TabOrder = 3
        end
        object MG: TDBEdit
          Left = 216
          Top = 72
          Width = 73
          Height = 21
          DataField = 'MG'
          DataSource = DatosF.AnualSource
          TabOrder = 4
        end
        object FE: TDBEdit
          Left = 296
          Top = 72
          Width = 73
          Height = 21
          DataField = 'FE'
          DataSource = DatosF.AnualSource
          TabOrder = 5
        end
        object Micro: TDBEdit
          Left = 136
          Top = 116
          Width = 73
          Height = 21
          DataField = 'MICROELEMENTOS'
          DataSource = DatosF.AnualSource
          TabOrder = 6
        end
        object AH: TDBEdit
          Left = 216
          Top = 116
          Width = 73
          Height = 21
          DataField = 'AHUMICO'
          DataSource = DatosF.AnualSource
          TabOrder = 7
        end
        object CSALINO: TDBEdit
          Left = 296
          Top = 116
          Width = 73
          Height = 21
          DataField = 'CSALINO'
          DataSource = DatosF.AnualSource
          TabOrder = 8
        end
        object FAnalisis: TDBEdit
          Left = 8
          Top = 72
          Width = 89
          Height = 21
          Hint = 'Fecha del an'#225'lisis realizado para la estimaci'#243'n '
          DataField = 'FANALISIS'
          DataSource = DatosF.AnualSource
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 9
        end
        object CalcAnalisis: TButton
          Left = 32
          Top = 104
          Width = 75
          Height = 25
          Hint = 
            'Realiza la estimaci'#243'n de necesidades a partir del an'#225'lisis folia' +
            'r seleccionado.'
          Caption = 'Calcular'
          TabOrder = 11
          OnClick = CalcAnalisisClick
        end
        object Base: TDBEdit
          Left = 8
          Top = 32
          Width = 89
          Height = 21
          Hint = 'C'#225'lculo base para realizar la estimaci'#243'n de necesidades.'
          DataField = 'BASE'
          DataSource = DatosF.AnualSource
          TabOrder = 10
        end
      end
    end
    object ProductosPage: TTabSheet
      Caption = 'Productos'
      ImageIndex = 2
      object Label17: TLabel
        Left = 10
        Top = 0
        Width = 103
        Height = 13
        Caption = 'Productos disponibles'
      end
      object Articulos: TDBLookupComboBox
        Left = 8
        Top = 16
        Width = 225
        Height = 21
        Hint = 'Lista de productos disponibles.'
        KeyField = 'CODART'
        ListField = 'NOMCOMERCIAL'
        ListSource = DatosF.ProductosSource
        TabOrder = 0
      end
      object Detalle: TDBGrid
        Left = 8
        Top = 44
        Width = 385
        Height = 181
        Hint = 'Lista de productos utilizados para satisfacer las necesidades.'
        DataSource = DatosF.DetAnualSource
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        PopupMenu = EliminaProd
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NOMCOMERCIAL'
            Title.Caption = 'Nombre'
            Width = 210
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CANTIDAD'
            Title.Alignment = taRightJustify
            Title.Caption = 'Cantidad'
            Width = 63
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TOTAL'
            Title.Alignment = taRightJustify
            Title.Caption = '% Total'
            Visible = True
          end>
      end
      object Anyadir: TButton
        Left = 238
        Top = 16
        Width = 67
        Height = 25
        Hint = 'A'#241'ade el producto seleccionado al plan de abonado anual.'
        Caption = 'A'#241'adir'
        TabOrder = 2
        OnClick = AnyadirClick
      end
      object CalcCtd: TButton
        Left = 6
        Top = 344
        Width = 75
        Height = 25
        Hint = 
          'Calcula las cantidades necesarias de los productos seleccionados' +
          ' que satisfacen las necesidades.'
        Caption = 'Calcular'
        TabOrder = 3
        OnClick = CalcCtdClick
      end
      object Reparto: TButton
        Left = 86
        Top = 344
        Width = 75
        Height = 25
        Hint = 
          'Lanza la pantalla de reparto mensual para el plan anual mostrado' +
          '.'
        Caption = 'Reparto'
        TabOrder = 4
        OnClick = RepartoClick
      end
      object AFosforico: TGroupBox
        Left = 8
        Top = 224
        Width = 377
        Height = 97
        Caption = #193'cido fosf'#243'rico'
        TabOrder = 5
        object Label18: TLabel
          Left = 288
          Top = 48
          Width = 51
          Height = 13
          Caption = 'Porcentaje'
        end
        object Nombre_AF: TDBText
          Left = 32
          Top = 64
          Width = 241
          Height = 17
          Hint = 'Nombre comercial del producto seleccionado'
          DataField = 'NOMBRE_AF'
          DataSource = DatosF.AnualSource
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object CTD_P: TDBEdit
          Left = 288
          Top = 64
          Width = 73
          Height = 21
          Hint = '% de necesidades de f'#243'sforo a aportar mediante '#225'cido fosf'#243'rico.'
          DataField = 'CTD_P'
          DataSource = DatosF.AnualSource
          TabOrder = 0
        end
        object ListaAcidoFosforico: TDBLookupComboBox
          Left = 32
          Top = 16
          Width = 249
          Height = 21
          Hint = 
            'Lista de selecci'#243'n del producto correspondiente al '#225'cido fosf'#243'ri' +
            'co'
          KeyField = 'CODART'
          ListField = 'NOMCOMERCIAL'
          ListSource = ProductosAFSource
          TabOrder = 1
        end
        object Seleccionar: TButton
          Left = 288
          Top = 16
          Width = 75
          Height = 25
          Hint = 
            'Asocia el producto de la lista como el acido fosf'#243'rico a utiliza' +
            'r.'
          Caption = 'Seleccionar'
          TabOrder = 2
          OnClick = SeleccionarClick
        end
        object Codigo_AF: TDBEdit
          Left = 32
          Top = 40
          Width = 121
          Height = 21
          DataField = 'CODIGO_AF'
          DataSource = DatosF.AnualSource
          TabOrder = 3
          Visible = False
        end
      end
      object EliminaAF: TButton
        Left = 272
        Top = 344
        Width = 113
        Height = 25
        Hint = 'Elimina el producto que act'#250'a como '#225'cido fosf'#243'rico.'
        Caption = 'Elimina '#225'cido fosf'#243'rico'
        TabOrder = 6
        OnClick = EliminaAFClick
      end
      object Maximo: TCheckBox
        Left = 8
        Top = 320
        Width = 153
        Height = 17
        Hint = 
          'Aplica el m'#225'ximo por elemento a la hora de realizar el calculo d' +
          'e cantidad de abono comercial.'
        Caption = 'M'#225'ximo por elemento'
        Checked = True
        State = cbChecked
        TabOrder = 7
      end
      object BuscaProductoB: TBitBtn
        Left = 312
        Top = 16
        Width = 81
        Height = 25
        Hint = 'Busca y a'#241'ade productos al plan anual.'
        Caption = 'B'#250'squeda'
        TabOrder = 8
        OnClick = BuscaProductoBClick
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF008400000084000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF0084000000C6C6C6008400000084000000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF0084000000C6C6C600840000008400000084000000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008400
          0000C6C6C600840000008400000084000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF000000000000000000000000000000000084000000C6C6
          C600840000008400000084000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF000000000000000000C6C6C600C6C6C600C6C6C600FFFFFF00848484008400
          00008400000084000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
          000084848400C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600FFFFFF008484
          840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
          0000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600FFFF
          FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000C6C6
          C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C60000000000FF00FF00FF00FF00FF00FF00FF00FF0000000000C6C6
          C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C60000000000FF00FF00FF00FF00FF00FF00FF00FF0000000000C6C6
          C600FFFFFF00FFFF0000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C60000000000FF00FF00FF00FF00FF00FF00FF00FF0000000000C6C6
          C600FFFFFF00FFFF0000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C60000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
          0000FFFFFF00FFFFFF00FFFF0000FFFF0000C6C6C600C6C6C600C6C6C600C6C6
          C60000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
          000084848400FFFFFF00FFFFFF00FFFFFF00C6C6C600C6C6C600C6C6C6008484
          840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF000000000000000000C6C6C600C6C6C600C6C6C600C6C6C600000000000000
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      end
    end
    object Descripcion: TTabSheet
      Caption = 'Observaciones'
      ImageIndex = 1
      object Observaciones: TDBRichEdit
        Left = 16
        Top = 8
        Width = 377
        Height = 353
        Hint = 'Consideraciones a incluir en el documento entregado al cliente.'
        DataField = 'OBSERVACIONES'
        DataSource = DatosF.AnualSource
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
    object AnotacionesP: TTabSheet
      Caption = 'Anotaciones'
      ImageIndex = 4
      object Anotaciones: TDBRichEdit
        Left = 16
        Top = 8
        Width = 377
        Height = 353
        Hint = 
          'Anotaciones realizadas por el usuario que no apareceran en el do' +
          'cumento entregado al cliente.'
        DataField = 'ANOTACIONES'
        DataSource = DatosF.AnualSource
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
  end
  inherited NuevoB: TBitBtn
    Left = 424
    Top = 544
    Width = 68
    TabOrder = 5
    OnClick = InsertarClick
  end
  inherited GuardarB: TBitBtn
    Left = 496
    Top = 544
    Width = 68
    Enabled = False
    TabOrder = 6
    OnClick = GuardarClick
  end
  inherited EliminarB: TBitBtn
    Left = 632
    Top = 544
    Width = 64
    TabOrder = 8
    OnClick = EliminarClick
  end
  object ImprimeB: TBitBtn
    Left = 704
    Top = 544
    Width = 64
    Height = 25
    Caption = 'Im&prime'
    TabOrder = 9
    OnClick = ImprimeClick
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF000000
      0000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C60000000000C6C6C60000000000FF00FF00FF00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C6C60000000000FF00FF0000000000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C60000FFFF0000FFFF0000FF
      FF00C6C6C600C6C6C600000000000000000000000000FF00FF0000000000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C60084848400848484008484
      8400C6C6C600C6C6C60000000000C6C6C60000000000FF00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C6C600C6C6C6000000000000000000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C60000000000C6C6C60000000000C6C6C60000000000FF00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C60000000000C6C6C6000000000000000000FF00FF00FF00
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000C6C6C60000000000C6C6C60000000000FF00FF00FF00
      FF00FF00FF0000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF0000000000000000000000000000000000FF00FF00FF00FF00FF00
      FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0000000000FFFFFF000000000000000000000000000000
      000000000000FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
  end
  object PrintFincaB: TBitBtn
    Left = 776
    Top = 544
    Width = 57
    Height = 25
    Hint = 'Imprime el resumen anual por finca'
    Caption = 'Finca'
    TabOrder = 10
    OnClick = PrintFincaBClick
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF000000
      0000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C60000000000C6C6C60000000000FF00FF00FF00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C6C60000000000FF00FF0000000000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C60000FFFF0000FFFF0000FF
      FF00C6C6C600C6C6C600000000000000000000000000FF00FF0000000000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C60084848400848484008484
      8400C6C6C600C6C6C60000000000C6C6C60000000000FF00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C6C600C6C6C6000000000000000000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C60000000000C6C6C60000000000C6C6C60000000000FF00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C60000000000C6C6C6000000000000000000FF00FF00FF00
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000C6C6C60000000000C6C6C60000000000FF00FF00FF00
      FF00FF00FF0000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF0000000000000000000000000000000000FF00FF00FF00FF00FF00
      FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0000000000FFFFFF000000000000000000000000000000
      000000000000FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
  end
  object FiltroFinca: TEdit
    Left = 224
    Top = 0
    Width = 185
    Height = 21
    Hint = 'Filtra la lista izquierda seg'#250'n la finca introducida'
    TabOrder = 2
    OnChange = FiltroAnyoChange
  end
  object Fincas: TADOTable
    Connection = DatosF.Abono
    LockType = ltReadOnly
    TableName = 'FINCAS'
    Left = 224
    Top = 536
    object FincasCODFINCA: TAutoIncField
      FieldName = 'CODFINCA'
      ReadOnly = True
    end
    object FincasNOMFINCA: TWideStringField
      FieldName = 'NOMFINCA'
      Size = 50
    end
    object FincasCODCLI: TWideStringField
      FieldName = 'CODCLI'
      Size = 10
    end
    object FincasNOMCLI: TStringField
      FieldKind = fkLookup
      FieldName = 'NOMCLI'
      LookupDataSet = DatosF.Clientes
      LookupKeyFields = 'CODCLI'
      LookupResultField = 'NOMBRE'
      KeyFields = 'CODCLI'
      Size = 50
      Lookup = True
    end
    object FincasN: TFloatField
      FieldName = 'N'
    end
    object FincasP: TFloatField
      FieldName = 'P'
    end
    object FincasK: TFloatField
      FieldName = 'K'
    end
    object FincasCA: TFloatField
      FieldName = 'CA'
    end
    object FincasMG: TFloatField
      FieldName = 'MG'
    end
    object FincasFE: TFloatField
      FieldName = 'FE'
    end
    object FincasMICROELEMENTOS: TFloatField
      FieldName = 'MICROELEMENTOS'
    end
    object FincasAHUMICO: TFloatField
      FieldName = 'AHUMICO'
    end
  end
  object Sectores: TADOTable
    Connection = DatosF.Abono
    LockType = ltReadOnly
    IndexFieldNames = 'CODFINCA'
    MasterFields = 'CODFINCA'
    TableName = 'SECTORES'
    Left = 256
    Top = 536
    object SectoresSECTOR: TWideStringField
      FieldName = 'SECTOR'
    end
    object SectoresCODFINCA: TIntegerField
      FieldName = 'CODFINCA'
    end
    object SectoresPATRON: TWideStringField
      FieldName = 'PATRON'
      Size = 50
    end
    object SectoresARBOLNUM: TSmallintField
      FieldName = 'ARBOLNUM'
    end
    object SectoresHG: TFloatField
      FieldName = 'HG'
    end
  end
  object ProductosAF: TADOQuery
    Connection = DatosF.Abono
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT CODART,NOMCOMERCIAL'
      'FROM PRODUCTOS'
      'WHERE P <> 0')
    Left = 8
    Top = 536
    object ProductosAFCODART: TAutoIncField
      FieldName = 'CODART'
      ReadOnly = True
    end
    object ProductosAFNOMCOMERCIAL: TWideStringField
      FieldName = 'NOMCOMERCIAL'
      Size = 50
    end
  end
  object EliminaProd: TPopupMenu
    Left = 344
    Top = 536
    object EliminaProducto1: TMenuItem
      Caption = 'Elimina Producto'
      Hint = 'Elimina el producto seleccionado'
      OnClick = EliminaProducto1Click
    end
  end
  object Analisis: TADOQuery
    Connection = DatosF.Abono
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT FECHA , N, P, K, CA, MG, FE, MICROELEMENTOS, AHUMICO'
      'FROM ANALISIS_FINCA '
      'UNION'
      'SELECT FECHA ,N, P, K, CA, MG, FE, MICROELEMENTOS, AHUMICO'
      'FROM ANALISIS'
      'ORDER BY 1')
    Left = 104
    Top = 536
    object AnalisisFECHA: TDateTimeField
      FieldName = 'FECHA'
    end
    object AnalisisN: TFloatField
      FieldName = 'N'
    end
    object AnalisisP: TFloatField
      FieldName = 'P'
    end
    object AnalisisK: TFloatField
      FieldName = 'K'
    end
    object AnalisisCA: TFloatField
      FieldName = 'CA'
    end
    object AnalisisMG: TFloatField
      FieldName = 'MG'
    end
    object AnalisisFE: TFloatField
      FieldName = 'FE'
    end
    object AnalisisMICROELEMENTOS: TFloatField
      FieldName = 'MICROELEMENTOS'
    end
    object AnalisisAHUMICO: TFloatField
      FieldName = 'AHUMICO'
    end
  end
  object AnalisisSource: TDataSource
    DataSet = Analisis
    Left = 128
    Top = 536
  end
  object Productos: TADOTable
    Connection = DatosF.Abono
    TableName = 'PRODUCTOS'
    Left = 288
    Top = 536
  end
  object ProductosAFSource: TDataSource
    DataSet = ProductosAF
    Left = 40
    Top = 536
  end
end
