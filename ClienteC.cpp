//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ClienteC.h"
#include "DatosC.h"
#include "MainC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TClienteF *ClienteF;

//---------------------------------------------------------------------------
__fastcall TClienteF::TClienteF(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TClienteF::FormClose(TObject *Sender, TCloseAction &Action)
{
  int opcion;

        if (DatosF->Clientes->State == dsEdit)
        {
            opcion = Application->MessageBoxA("�Desea guardar las modificaciones de este cliente?","Atenci�n",MB_ICONWARNING + MB_YESNO);
            if (opcion == ID_YES)
                DatosF->Clientes->Post();
            else
                DatosF->Clientes->Cancel();
        }
        if (DatosF->Clientes->State == dsInsert)
        {
            opcion = Application->MessageBoxA("�Desea guardar el cliente nuevo?","Atenci�n",MB_ICONWARNING + MB_YESNO);
            if (opcion == ID_YES)
                DatosF->Clientes->Post();
            else
                DatosF->Clientes->Cancel();
        }
        Action = caFree;
        ClienteF = NULL;
}
//---------------------------------------------------------------------------


void __fastcall TClienteF::FormShow(TObject *Sender)
{
        Nombre->SetFocus();
}
//---------------------------------------------------------------------------
void __fastcall TClienteF::CerrarClick(TObject *Sender)
{
        Close();        
}
//---------------------------------------------------------------------------

void __fastcall TClienteF::FiltroChange(TObject *Sender)
{
   if (Filtro->Text != "")
   {
       AnsiString Fin;
       DatosF->Clientes->Filtered = false;
       Fin = Filtro->Text;
       ++Fin[Fin.Length()];
       DatosF->Clientes->Filter  = "NOMBRE >= '" + Filtro->Text + "'";
       DatosF->Clientes->Filter += " AND NOMBRE < '" + Fin + "'";
       DatosF->Clientes->Filtered = true;
   }
   else
       DatosF->Clientes->Filtered = false;

}
//---------------------------------------------------------------------------

void __fastcall TClienteF::NuevoClick(TObject *Sender)
{
   TDataSetState State;
   State = DatosF->Clientes->State;

   if (State == dsEdit)
   {
      Application->MessageBoxA("Cliente actual pendiente de guardar cambios.\nGuarde los cambios y vuelva a pulsar el bot�n.","Error",MB_ICONERROR);
      return;
   }
   if (State == dsInsert)
   {
      Application->MessageBoxA("Cliente pendiente de inserci�n.\nGuarde el cliente y vuelva a pulsar el bot�n","Error",MB_ICONERROR);
      return;
   }
   CodCli->SetFocus();
   DatosF->Clientes->Insert();
}
//---------------------------------------------------------------------------

void __fastcall TClienteF::GuardarClick(TObject *Sender)
{
   TDataSetState State;
        State = DatosF->Clientes->State;
        if ( State == dsEdit || State == dsInsert)
        {
           if (Comercial->Text == "")
           {
               Application->MessageBoxA("Comercial obligatorio","Error",MB_ICONERROR);
               Comercial->SetFocus();
               return;
           }
           if (CodCli->Text == "")
           {
                Application->MessageBoxA("C�digo cliente obligatorio","Error",MB_ICONERROR);
                CodCli->SetFocus();
                return;
           }
           DatosF->Clientes->Post();
        }
        else
           ShowMessage("Nada que guardar");
}
//---------------------------------------------------------------------------

void __fastcall TClienteF::CancelarClick(TObject *Sender)
{
   TDataSetState State;
        State = DatosF->Clientes->State;
        if ( State == dsEdit || State == dsInsert)
           DatosF->Clientes->Cancel();
        else
           ShowMessage("Nada que cancelar");
}
//---------------------------------------------------------------------------

void __fastcall TClienteF::EliminarClick(TObject *Sender)
{
   TDataSetState State;
   State = DatosF->Clientes->State;

   if (State == dsEdit)
   {
      Application->MessageBoxA("Cliente actual pendiente de guardar cambios.\nGuarde los cambios y vuelva a pulsar el bot�n.","Error",MB_ICONERROR);
      return;
   }
   if (State == dsInsert)
   {
      Application->MessageBoxA("Cliente pendiente de inserci�n.\nGuarde el cliente y vuelva a pulsar el bot�n","Error",MB_ICONERROR);
      return;
   }
   if ( DatosF->Fincas->Locate("CODCLI",CodCli->Text, TLocateOptions()))
   {
       Application->MessageBoxA("No se puede borrar el cliente porque tiene fincas asociadas.\nBorre primero la finca","Error",MB_ICONERROR);
       return;
   }

   DatosF->Clientes->Delete();
}
//---------------------------------------------------------------------------
void TClienteF::bloqueaFormulario(void)
{
     Lista_Clientes->Enabled = false;
     NuevoB->Enabled = false;
     GuardarB->Enabled = true;
     Cancelar->Enabled = true;
     EliminarB->Enabled = false;
}
//---------------------------------------------------------------------------
void TClienteF::desbloqueaFormulario(void)
{
     Lista_Clientes->Enabled = true;
     NuevoB->Enabled = true;
     GuardarB->Enabled = false;
     Cancelar->Enabled = false;
     EliminarB->Enabled = true;
}
//---------------------------------------------------------------------------

