inherited AnalisisFincaF: TAnalisisFincaF
  Left = 430
  Top = 282
  Width = 503
  Height = 434
  Caption = 'An'#225'lisis Foliar Finca'
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel [0]
    Left = 168
    Top = 80
    Width = 68
    Height = 13
    Caption = 'Fecha An'#225'lisis'
  end
  inherited Cerrar: TBitBtn
    Left = 416
    Top = 360
    Width = 64
    Hint = 'Cierra esta ventana'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    OnClick = CerrarClick
  end
  inherited NuevoB: TBitBtn
    Left = 168
    Top = 328
    Width = 66
    TabOrder = 8
    OnClick = AnyadirClick
  end
  inherited GuardarB: TBitBtn
    Left = 240
    Top = 328
    Width = 67
    TabOrder = 9
    OnClick = GuardarClick
  end
  inherited EliminarB: TBitBtn
    Left = 376
    Top = 328
    Width = 62
    TabOrder = 10
    OnClick = EliminarClick
  end
  object GroupBox1: TGroupBox
    Left = 192
    Top = 8
    Width = 289
    Height = 65
    Caption = 'Datos Finca'
    TabOrder = 0
    object Label3: TLabel
      Left = 16
      Top = 24
      Width = 26
      Height = 13
      Caption = 'Finca'
    end
    object Nomcli: TLabel
      Left = 56
      Top = 48
      Width = 32
      Height = 13
      Hint = 'Cliente de la finca'
      Caption = 'Nomcli'
      ParentShowHint = False
      ShowHint = True
    end
    object Nomfinca: TLabel
      Left = 56
      Top = 24
      Width = 45
      Height = 13
      Hint = 'Nombre de la finca'
      Caption = 'Nomfinca'
      ParentShowHint = False
      ShowHint = True
    end
    object Label2: TLabel
      Left = 16
      Top = 48
      Width = 32
      Height = 13
      Caption = 'Cliente'
    end
  end
  object GroupBox2: TGroupBox
    Left = 184
    Top = 104
    Width = 289
    Height = 217
    Caption = 'Par'#225'metros An'#225'lisis'
    TabOrder = 3
    object Label5: TLabel
      Left = 16
      Top = 16
      Width = 46
      Height = 13
      Caption = 'Nitr'#243'geno'
    end
    object Label6: TLabel
      Left = 16
      Top = 64
      Width = 35
      Height = 13
      Caption = 'Potasio'
    end
    object Label7: TLabel
      Left = 16
      Top = 40
      Width = 35
      Height = 13
      Caption = 'F'#243'sforo'
    end
    object Label8: TLabel
      Left = 16
      Top = 88
      Width = 29
      Height = 13
      Caption = 'Calcio'
    end
    object Label9: TLabel
      Left = 16
      Top = 112
      Width = 46
      Height = 13
      Caption = 'Magnesio'
    end
    object Label10: TLabel
      Left = 16
      Top = 136
      Width = 28
      Height = 13
      Caption = 'Hierro'
    end
    object Label11: TLabel
      Left = 16
      Top = 160
      Width = 74
      Height = 13
      Caption = 'Microelementos'
    end
    object Label12: TLabel
      Left = 16
      Top = 184
      Width = 46
      Height = 13
      Caption = 'A.H'#250'mico'
    end
    object Label13: TLabel
      Left = 240
      Top = 16
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label14: TLabel
      Left = 240
      Top = 40
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label15: TLabel
      Left = 240
      Top = 64
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label16: TLabel
      Left = 240
      Top = 88
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label18: TLabel
      Left = 240
      Top = 112
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label19: TLabel
      Left = 240
      Top = 136
      Width = 32
      Height = 13
      Caption = 'mg/Kg'
    end
    object Label20: TLabel
      Left = 240
      Top = 160
      Width = 32
      Height = 13
      Caption = 'mg/Kg'
    end
    object Label21: TLabel
      Left = 240
      Top = 184
      Width = 32
      Height = 13
      Caption = 'mg/Kg'
    end
    object N: TDBEdit
      Left = 112
      Top = 16
      Width = 121
      Height = 21
      DataField = 'N'
      DataSource = DatosF.AnalisisFincaSource
      TabOrder = 0
    end
    object K: TDBEdit
      Left = 112
      Top = 64
      Width = 121
      Height = 21
      DataField = 'K'
      DataSource = DatosF.AnalisisFincaSource
      TabOrder = 2
    end
    object P: TDBEdit
      Left = 112
      Top = 40
      Width = 121
      Height = 21
      DataField = 'P'
      DataSource = DatosF.AnalisisFincaSource
      TabOrder = 1
    end
    object Ca: TDBEdit
      Left = 112
      Top = 88
      Width = 121
      Height = 21
      DataField = 'CA'
      DataSource = DatosF.AnalisisFincaSource
      TabOrder = 3
    end
    object Mg: TDBEdit
      Left = 112
      Top = 112
      Width = 121
      Height = 21
      DataField = 'MG'
      DataSource = DatosF.AnalisisFincaSource
      TabOrder = 4
    end
    object Fe: TDBEdit
      Left = 112
      Top = 136
      Width = 121
      Height = 21
      DataField = 'FE'
      DataSource = DatosF.AnalisisFincaSource
      TabOrder = 5
    end
    object Micro: TDBEdit
      Left = 112
      Top = 160
      Width = 121
      Height = 21
      DataField = 'MICROELEMENTOS'
      DataSource = DatosF.AnalisisFincaSource
      TabOrder = 6
    end
    object AH: TDBEdit
      Left = 112
      Top = 184
      Width = 121
      Height = 21
      DataField = 'AHUMICO'
      DataSource = DatosF.AnalisisFincaSource
      TabOrder = 7
    end
  end
  object Fecha: TDateTimePicker
    Left = 328
    Top = 80
    Width = 97
    Height = 21
    Hint = 'Fecha en la que se realiz'#243' el an'#225'lisis'
    CalAlignment = dtaLeft
    Date = 38387
    Time = 38387
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnChange = FechaChange
  end
  object FechaBD: TDBEdit
    Left = 240
    Top = 80
    Width = 81
    Height = 21
    DataField = 'FECHA'
    DataSource = DatosF.AnalisisFincaSource
    Enabled = False
    ReadOnly = True
    TabOrder = 2
  end
  object ListaIzq: TDBGrid
    Left = 8
    Top = 8
    Width = 153
    Height = 353
    DataSource = DatosF.AnalisisFincaSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'FECHA'
        Title.Caption = 'Fecha del an'#225'lisis'
        Visible = True
      end>
  end
  object CodFinca: TEdit
    Left = 400
    Top = 32
    Width = 73
    Height = 21
    TabOrder = 6
    Text = 'CodFinca'
    Visible = False
  end
  object Cancelar: TButton
    Left = 312
    Top = 328
    Width = 60
    Height = 25
    Hint = 'Cancela la modificaci'#243'n actual.'
    Caption = 'Cancelar'
    TabOrder = 7
    OnClick = CancelarClick
  end
end
