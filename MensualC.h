//---------------------------------------------------------------------------

#ifndef MensualCH
#define MensualCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "PlantillaC.h"
#include <Buttons.hpp>
#include <DBCtrls.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <Mask.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>
#include "UCrpe32.hpp"
#include "UCrpeClasses.hpp"
#include <ADODB.hpp>
#include <DB.hpp>
//---------------------------------------------------------------------------
class TMensualF : public TPlantilla
{
__published:	// IDE-managed Components
        TGroupBox *Descripcion;
        TButton *Cancelar;
        TDBGrid *ListaIzq;
        TDBGrid *Detalle;
        TGroupBox *Productos;
        TDBLookupComboBox *Articulos;
        TButton *Anyadir;
        TButton *CancelarProd;
        TPageControl *Paginas;
        TTabSheet *ProductosTab;
        TTabSheet *ObservaTab;
        TDBCheckBox *EsQuincena;
        TEdit *FiltroAnyo;
        TLabel *Label7;
        TLabel *Label8;
        TEdit *FiltroMes;
        TTabSheet *RiegoTab;
        TGroupBox *RiegoSector;
        TDBGrid *ListaSectorProd;
        TDBCheckBox *Impreso;
        TDBGrid *ListaSectores;
        TDBEdit *TRiego;
        TLabel *Label4;
        TLabel *Label5;
        TDBEdit *Bomba;
        TDBEdit *TAbono;
        TLabel *Label3;
        TGroupBox *DiasRiego;
        TDBCheckBox *Lunes;
        TDBCheckBox *Martes;
        TDBCheckBox *Miercoles;
        TDBCheckBox *Jueves;
        TDBCheckBox *Viernes;
        TDBCheckBox *Sabado;
        TDBCheckBox *Domingo;
        TGroupBox *Riego;
        TLabel *Label2;
        TLabel *Label6;
        TDBEdit *Llenados;
        TButton *Calcular;
        TDBEdit *DAbono;
        TLabel *Label9;
        TDBEdit *DRiego;
        TButton *CancelaSector;
        TDBEdit *AguaSector;
        TDBEdit *AguaFinca;
        TLabel *Label10;
        TLabel *Label11;
        TGroupBox *DescripcionAF;
        TTabSheet *AF;
        TDBGrid *ListaSectorAF;
        TGroupBox *DiasRiegoAF;
        TDBCheckBox *Lunes_AF;
        TDBCheckBox *Martes_AF;
        TDBCheckBox *Miercoles_AF;
        TDBCheckBox *Jueves_AF;
        TDBCheckBox *Viernes_AF;
        TDBCheckBox *Sabado_AF;
        TDBCheckBox *Domingo_AF;
        TGroupBox *RiegoSectorAF;
        TDBEdit *Nombre_AF;
        TLabel *Label12;
        TDBEdit *Ctd_AF;
        TDBEdit *Bomba_AF;
        TLabel *Label13;
        TDBEdit *Codart_AF;
        TDBEdit *DAcidoF;
        TLabel *Label14;
        TDBEdit *TAcidoF;
        TLabel *Label15;
        TButton *CalcularAF;
        TButton *CancelarAF;
        TDBEdit *TRiego_AF;
        TLabel *Label16;
        TLabel *Label1;
        TLabel *Label17;
        TDBRichEdit *Observaciones;
        TDBRichEdit *ObservacionesAF;
        TBitBtn *Imprimir;
        TBitBtn *GuardaSector;
        TBitBtn *EliminaSector;
        TBitBtn *GuardarAF;
        TBitBtn *EliminarAF;
        TBitBtn *GuardarProd;
        TBitBtn *EliminarProd;
        TGroupBox *Principal;
        TDBEdit *CodMensual;
        TLabel *Label18;
        TTabSheet *AnotacionesTab;
        TDBRichEdit *Anotaciones;
        TDBEdit *CodAnual;
        TGroupBox *PlanAnual;
        TADOTable *MensualDupl;
        TADOTable *SectorMesDupl;
        TADOTable *DetMensualDupl;
        TWideStringField *MensualDuplCODMENSUAL;
        TIntegerField *MensualDuplCODFINCA;
        TSmallintField *MensualDuplANYO;
        TSmallintField *MensualDuplMES;
        TSmallintField *MensualDuplQUINCENA;
        TBooleanField *MensualDuplES_QUINCENA;
        TMemoField *MensualDuplOBSERVACIONES;
        TSmallintField *MensualDuplLLENADOS;
        TBooleanField *MensualDuplIMPRESO;
        TSmallintField *MensualDuplDABONO;
        TSmallintField *MensualDuplDRIEGO;
        TFloatField *MensualDuplAGUA;
        TMemoField *MensualDuplOBSERVA_AF;
        TMemoField *MensualDuplANOTACIONES;
        TWideStringField *SectorMesDuplCODMENSUAL;
        TWideStringField *SectorMesDuplSECTOR;
        TWideStringField *SectorMesDuplCODANUAL;
        TSmallintField *SectorMesDuplTABONO;
        TSmallintField *SectorMesDuplTRIEGO;
        TBooleanField *SectorMesDuplLUNES;
        TBooleanField *SectorMesDuplMARTES;
        TBooleanField *SectorMesDuplMIERCOLES;
        TBooleanField *SectorMesDuplJUEVES;
        TBooleanField *SectorMesDuplVIERNES;
        TBooleanField *SectorMesDuplSABADO;
        TBooleanField *SectorMesDuplDOMINGO;
        TFloatField *SectorMesDuplBOMBA;
        TFloatField *SectorMesDuplAGUA;
        TIntegerField *SectorMesDuplCODART_AF;
        TSmallintField *SectorMesDuplTACIDOF;
        TFloatField *SectorMesDuplBOMBA_AF;
        TFloatField *SectorMesDuplCTD_AF;
        TWideStringField *SectorMesDuplNOMBRE_AF;
        TSmallintField *SectorMesDuplDACIDOF;
        TFloatField *SectorMesDuplAGUA_AF;
        TBooleanField *SectorMesDuplLUNES_AF;
        TBooleanField *SectorMesDuplMARTES_AF;
        TBooleanField *SectorMesDuplMIERCOLES_AF;
        TBooleanField *SectorMesDuplJUEVES_AF;
        TBooleanField *SectorMesDuplVIERNES_AF;
        TBooleanField *SectorMesDuplSABADO_AF;
        TBooleanField *SectorMesDuplDOMINGO_AF;
        TSmallintField *SectorMesDuplTRIEGO_AF;
        TWideStringField *DetMensualDuplCODMENSUAL;
        TWideStringField *DetMensualDuplSECTOR;
        TSmallintField *DetMensualDuplNLINEA;
        TIntegerField *DetMensualDuplCODART;
        TFloatField *DetMensualDuplCANTIDAD;
        TWideStringField *DetMensualDuplNOMCOMERCIAL;
        TBitBtn *DuplicaB;
        TDataSource *MensualDuplSource;
        TDataSource *SectorMesDuplSource;
        TLabel *Label19;
        TDBEdit *Anyo;
        TLabel *Label20;
        TDBEdit *Mes;
        TLabel *Label21;
        TDBEdit *NomFinca;
        TLabel *Label22;
        TDBEdit *Quincena;
        TLabel *Label23;
        TDBText *NomCliente;
        TLabel *Label24;
        TEdit *FiltroFinca;
        TWideStringField *MensualDuplNOMFINCA;
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall ImprimirClick(TObject *Sender);
        void __fastcall GuardarClick(TObject *Sender);
        void __fastcall CancelarClick(TObject *Sender);
        void __fastcall CalcularClick(TObject *Sender);
        void __fastcall AnyadirClick(TObject *Sender);
        void __fastcall GuardarProdClick(TObject *Sender);
        void __fastcall CancelarProdClick(TObject *Sender);
        void __fastcall Filtro(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall GuardaSectorClick(TObject *Sender);
        void __fastcall CancelaSectorClick(TObject *Sender);
        void __fastcall EliminaSectorClick(TObject *Sender);
        void __fastcall CalcularAFClick(TObject *Sender);
        void __fastcall EliminarAFClick(TObject *Sender);
        void __fastcall EliminarProdClick(TObject *Sender);
        void __fastcall EliminarBClick(TObject *Sender);
        void __fastcall DuplicaBClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TMensualF(TComponent* Owner);
        void bloqueaFormulario(void);
        void desbloqueaFormulario(void);
        void bloqueaSectores(void);
        void desbloqueaSectores(void);
        void bloqueaProductos(void);
        void desbloqueaProductos(void);
};
//---------------------------------------------------------------------------
extern PACKAGE TMensualF *MensualF;
//---------------------------------------------------------------------------
#endif
