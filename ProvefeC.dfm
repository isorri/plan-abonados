object ProvefeF: TProvefeF
  OldCreateOrder = False
  Left = 362
  Top = 213
  Height = 150
  Width = 215
  object X3: TDatabase
    AliasName = 'X73'
    DatabaseName = 'X3V5'
    LoginPrompt = False
    Params.Strings = (
      'USER NAME=PROVEFE'
      'PASSWORD=TIGER')
    ReadOnly = True
    SessionName = 'Default'
    Left = 8
    Top = 8
  end
  object Precio: TQuery
    DatabaseName = 'X3V5'
    SessionName = 'Default'
    SQL.Strings = (
      'SELECT PLICRI2_0 TARIFA,PRI_0 PRECIO'
      'FROM SPRICLIST'
      'WHERE PLI_0 = '#39'T001'#39' AND'
      '      PLISTRDAT_0 <= SYSDATE AND'
      '      PLIENDDAT_0 >= SYSDATE AND'
      '      PLICRI1_0 =  :CodArticulo')
    Left = 80
    Top = 48
    ParamData = <
      item
        DataType = ftString
        Name = 'CodArticulo'
        ParamType = ptInput
      end>
    object PrecioTARIFA: TStringField
      FieldName = 'TARIFA'
      Origin = 'V145.SPRICLIST.PLICRI2_0'
      Size = 90
    end
    object PrecioPRECIO: TFloatField
      FieldName = 'PRECIO'
      Origin = 'V145.SPRICLIST.PRI_0'
    end
  end
end
