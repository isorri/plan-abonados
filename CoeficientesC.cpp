//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CoeficientesC.h"
#include "DatosC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PlantillaC"
#pragma resource "*.dfm"
TCoeficienteF *CoeficienteF;
//---------------------------------------------------------------------------
__fastcall TCoeficienteF::TCoeficienteF(TComponent* Owner)
        : TPlantilla(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TCoeficienteF::InsertarClick(TObject *Sender)
{
   TDataSetState State;
   State = DatosF->Coeficientes->State;

   if (State == dsEdit)
        Application->MessageBoxA("Cambios no guardados.\nPulse Guardar o Cancelar.","Error",MB_ICONERROR);
   else
        if (State == dsInsert)
                Application->MessageBoxA("No ha guardado el nuevo conjunto de coeficientes.\nPulse Guardar o Cancelar","Error",MB_ICONERROR);
        else
        {
                DatosF->Coeficientes->Insert();
                Coeficientes->Enabled = false;
                EliminarB->Enabled = false;
        }


}
//---------------------------------------------------------------------------
void __fastcall TCoeficienteF::CerrarClick(TObject *Sender)
{
        Close();
}
//---------------------------------------------------------------------------
void __fastcall TCoeficienteF::GuardarClick(TObject *Sender)
{
   TDataSetState State;
   State = DatosF->Coeficientes->State;

   if (State == dsInsert || State == dsEdit)
   {
        if (Patron->Text != "")
        {
                DatosF->Coeficientes->Post();
                Coeficientes->Enabled = true;
                EliminarB->Enabled = true;
        }
        else
        {
                Application->MessageBoxA("El patr�n debe estar rellenado.","Error",MB_ICONERROR);
                Patron->SetFocus();
        }


   }
}
//---------------------------------------------------------------------------
void __fastcall TCoeficienteF::CancelarClick(TObject *Sender)
{
   TDataSetState State;
   State = DatosF->Coeficientes->State;
   if (State == dsInsert || State == dsEdit)
   {
        DatosF->Coeficientes->Cancel();
        Coeficientes->Enabled = true;
        EliminarB->Enabled = true;
   }
}
//---------------------------------------------------------------------------
void __fastcall TCoeficienteF::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   TDataSetState State;
   int res;
        State = DatosF->Coeficientes->State;
        if (State == dsInsert)
        {
            res = Application->MessageBoxA("Registro pendiente de inserci�n.\n�Guardar datos?.","Pregunta",MB_ICONQUESTION+MB_YESNO);
            if (res == IDYES)
                DatosF->Coeficientes->Post();
            else
                DatosF->Coeficientes->Cancel();
        }
        else
        {
            if (State == dsEdit)
            {
                res=Application->MessageBoxA("Registro con cambios no guardados.\n�Guardar cambios?","Pregunta",MB_ICONQUESTION+MB_YESNO);
                if (res == IDYES)
                    DatosF->Coeficientes->Post();
                else
                    DatosF->Coeficientes->Cancel();
            }
        }
        Action = caFree;
        CoeficienteF = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TCoeficienteF::EliminarClick(TObject *Sender)
{
   DatosF->Coeficientes->Delete();        
}
//---------------------------------------------------------------------------
void TCoeficienteF::bloqueaFormulario(void)
{
     Coeficientes->Enabled = false;
     NuevoB->Enabled = false;
     GuardarB->Enabled = true;
     Cancelar->Enabled = true;
     EliminarB->Enabled = false;
}
//---------------------------------------------------------------------------
void TCoeficienteF::desbloqueaFormulario(void)
{
     Coeficientes->Enabled = true;
     NuevoB->Enabled = true;
     GuardarB->Enabled = false;
     Cancelar->Enabled = false;
     EliminarB->Enabled = true;
}
//---------------------------------------------------------------------------

