inherited DistMensualF: TDistMensualF
  Left = 223
  Top = 132
  Width = 941
  Height = 528
  Caption = 'Distribuci'#243'n Mensual'
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label26: TLabel [0]
    Left = 736
    Top = 80
    Width = 87
    Height = 13
    Caption = 'Plantilla de reparto'
  end
  object NomCliente: TDBText [1]
    Left = 736
    Top = 8
    Width = 185
    Height = 17
    Hint = 'Nombre del cliente asociado a este plan anual'
    DataField = 'NOMCLI'
    DataSource = DatosF.AnualSource
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object NomFinca: TDBText [2]
    Left = 736
    Top = 32
    Width = 191
    Height = 17
    Hint = 'Finca'
    DataField = 'NOMFINCA'
    DataSource = DatosF.AnualSource
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Sector: TDBText [3]
    Left = 736
    Top = 56
    Width = 105
    Height = 17
    Hint = 'Sector'
    DataField = 'SECTOR'
    DataSource = DatosF.AnualSource
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Anyo: TDBText [4]
    Left = 840
    Top = 56
    Width = 81
    Height = 17
    DataField = 'ANYO'
    DataSource = DatosF.AnualSource
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited Cerrar: TBitBtn
    Left = 848
    Top = 456
    OnClick = CerrarClick
  end
  object Detalle: TDBGrid [6]
    Left = 374
    Top = 8
    Width = 355
    Height = 145
    DataSource = DatosF.DetAnualSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'NOMCOMERCIAL'
        ReadOnly = True
        Title.Caption = 'Producto utilizado'
        Width = 256
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TOTAL'
        Title.Caption = '%Total'
        Width = 45
        Visible = True
      end>
  end
  object Porcentajes: TGroupBox [7]
    Left = 376
    Top = 160
    Width = 545
    Height = 289
    TabOrder = 2
    object Label2: TLabel
      Left = 312
      Top = 8
      Width = 85
      Height = 13
      Caption = 'Cantidad a aplicar'
    end
    object Label3: TLabel
      Left = 32
      Top = 56
      Width = 153
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Caption = 'Enero'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 200
      Top = 56
      Width = 145
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Caption = 'Febrero'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 368
      Top = 56
      Width = 137
      Height = 25
      Alignment = taCenter
      AutoSize = False
      Caption = 'Marzo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 32
      Top = 72
      Width = 73
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = '%'
    end
    object Label7: TLabel
      Left = 104
      Top = 72
      Width = 79
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = 'Cantidad'
    end
    object Label8: TLabel
      Left = 200
      Top = 72
      Width = 73
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = '%'
    end
    object Label9: TLabel
      Left = 368
      Top = 72
      Width = 73
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = '%'
    end
    object Label10: TLabel
      Left = 272
      Top = 72
      Width = 79
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = 'Cantidad'
    end
    object Label11: TLabel
      Left = 440
      Top = 72
      Width = 79
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = 'Cantidad'
    end
    object Label13: TLabel
      Left = 200
      Top = 112
      Width = 153
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Caption = 'Mayo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel
      Left = 368
      Top = 112
      Width = 153
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Caption = 'Junio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label16: TLabel
      Left = 200
      Top = 128
      Width = 73
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = '%'
    end
    object Label17: TLabel
      Left = 368
      Top = 128
      Width = 73
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = '%'
    end
    object Label18: TLabel
      Left = 272
      Top = 128
      Width = 79
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = 'Cantidad'
    end
    object Label19: TLabel
      Left = 440
      Top = 128
      Width = 79
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = 'Cantidad'
    end
    object Label15: TLabel
      Left = 32
      Top = 128
      Width = 73
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = '%'
    end
    object Label12: TLabel
      Left = 32
      Top = 112
      Width = 153
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Caption = 'Abril'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel
      Left = 104
      Top = 128
      Width = 79
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = 'Cantidad'
    end
    object Label21: TLabel
      Left = 32
      Top = 168
      Width = 153
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Caption = 'Julio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label22: TLabel
      Left = 200
      Top = 168
      Width = 153
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Caption = 'Agosto'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label23: TLabel
      Left = 368
      Top = 168
      Width = 153
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Caption = 'Septiembre'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label27: TLabel
      Left = 32
      Top = 184
      Width = 73
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = '%'
    end
    object Label28: TLabel
      Left = 104
      Top = 184
      Width = 79
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = 'Cantidad'
    end
    object Label31: TLabel
      Left = 200
      Top = 184
      Width = 73
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = '%'
    end
    object Label32: TLabel
      Left = 272
      Top = 184
      Width = 79
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = 'Cantidad'
    end
    object Label35: TLabel
      Left = 368
      Top = 184
      Width = 73
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = '%'
    end
    object Label36: TLabel
      Left = 440
      Top = 184
      Width = 79
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = 'Cantidad'
    end
    object Label24: TLabel
      Left = 32
      Top = 224
      Width = 153
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Caption = 'Octubre'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label37: TLabel
      Left = 200
      Top = 224
      Width = 153
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Caption = 'Noviembre'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label38: TLabel
      Left = 368
      Top = 224
      Width = 153
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Caption = 'Diciembre'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label39: TLabel
      Left = 32
      Top = 240
      Width = 73
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = '%'
    end
    object Label40: TLabel
      Left = 104
      Top = 240
      Width = 79
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = 'Cantidad'
    end
    object Label41: TLabel
      Left = 200
      Top = 240
      Width = 73
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = '%'
    end
    object Label42: TLabel
      Left = 272
      Top = 240
      Width = 79
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = 'Cantidad'
    end
    object Label43: TLabel
      Left = 368
      Top = 240
      Width = 73
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = '%'
    end
    object Label44: TLabel
      Left = 440
      Top = 240
      Width = 79
      Height = 17
      Alignment = taCenter
      AutoSize = False
      Caption = 'Cantidad'
    end
    object Label25: TLabel
      Left = 424
      Top = 8
      Width = 35
      Height = 13
      Caption = '% Total'
    end
    object Label1: TLabel
      Left = 32
      Top = 8
      Width = 85
      Height = 13
      Caption = 'Nombre comercial'
    end
    object Cantidad: TDBEdit
      Left = 312
      Top = 24
      Width = 105
      Height = 21
      Hint = 'Cantidad a aplicar durante todo el a'#241'o.'
      DataField = 'CANTIDAD'
      DataSource = DatosF.DetAnualSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object Enero: TDBEdit
      Left = 32
      Top = 88
      Width = 73
      Height = 21
      Hint = 'Porcentaje a aplicar durante el mes'
      DataField = '01_ENERO'
      DataSource = DatosF.DetAnualSource
      TabOrder = 2
    end
    object Febrero: TDBEdit
      Left = 200
      Top = 88
      Width = 73
      Height = 21
      Hint = 'Porcentaje a aplicar durante el mes'
      DataField = '02_FEBRERO'
      DataSource = DatosF.DetAnualSource
      TabOrder = 3
    end
    object Marzo: TDBEdit
      Left = 368
      Top = 88
      Width = 73
      Height = 21
      Hint = 'Porcentaje a aplicar durante el mes'
      DataField = '03_MARZO'
      DataSource = DatosF.DetAnualSource
      TabOrder = 4
    end
    object Abril: TDBEdit
      Left = 32
      Top = 144
      Width = 73
      Height = 21
      Hint = 'Porcentaje a aplicar durante el mes'
      DataField = '04_ABRIL'
      DataSource = DatosF.DetAnualSource
      TabOrder = 5
    end
    object Mayo: TDBEdit
      Left = 200
      Top = 144
      Width = 73
      Height = 21
      Hint = 'Porcentaje a aplicar durante el mes'
      DataField = '05_MAYO'
      DataSource = DatosF.DetAnualSource
      TabOrder = 6
    end
    object Junio: TDBEdit
      Left = 368
      Top = 144
      Width = 73
      Height = 21
      Hint = 'Porcentaje a aplicar durante el mes'
      DataField = '06_JUNIO'
      DataSource = DatosF.DetAnualSource
      TabOrder = 7
    end
    object Julio: TDBEdit
      Left = 32
      Top = 200
      Width = 73
      Height = 21
      Hint = 'Porcentaje a aplicar durante el mes'
      DataField = '07_JULIO'
      DataSource = DatosF.DetAnualSource
      TabOrder = 8
    end
    object Agosto: TDBEdit
      Left = 200
      Top = 200
      Width = 73
      Height = 21
      Hint = 'Porcentaje a aplicar durante el mes'
      DataField = '08_AGOSTO'
      DataSource = DatosF.DetAnualSource
      TabOrder = 9
    end
    object Septiembre: TDBEdit
      Left = 368
      Top = 200
      Width = 73
      Height = 21
      Hint = 'Porcentaje a aplicar durante el mes'
      DataField = '09_SEPTIEMBRE'
      DataSource = DatosF.DetAnualSource
      TabOrder = 10
    end
    object Octubre: TDBEdit
      Left = 32
      Top = 256
      Width = 73
      Height = 21
      Hint = 'Porcentaje a aplicar durante el mes'
      DataField = '10_OCTUBRE'
      DataSource = DatosF.DetAnualSource
      TabOrder = 11
    end
    object Noviembre: TDBEdit
      Left = 200
      Top = 256
      Width = 73
      Height = 21
      Hint = 'Porcentaje a aplicar durante el mes'
      DataField = '11_NOVIEMBRE'
      DataSource = DatosF.DetAnualSource
      TabOrder = 12
    end
    object Diciembre: TDBEdit
      Left = 368
      Top = 256
      Width = 73
      Height = 21
      Hint = 'Porcentaje a aplicar durante el mes'
      DataField = '12_DICIEMBRE'
      DataSource = DatosF.DetAnualSource
      TabOrder = 13
    end
    object PorTotal: TDBEdit
      Left = 424
      Top = 24
      Width = 73
      Height = 21
      Hint = 'Cantidad a aplicar durante todo el a'#241'o.'
      Color = clInactiveCaption
      DataField = 'TOTAL'
      DataSource = DatosF.DetAnualSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 14
    end
    object CTD01: TDBEdit
      Left = 104
      Top = 88
      Width = 81
      Height = 21
      Hint = 'Cantidad a aplicar durante enero'
      Color = clInactiveCaption
      DataField = 'CTD01'
      DataSource = DatosF.DetAnualSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 15
    end
    object CTD02: TDBEdit
      Left = 272
      Top = 88
      Width = 81
      Height = 21
      Hint = 'Cantidad a aplicar durante febrero'
      Color = clInactiveCaption
      DataField = 'CTD02'
      DataSource = DatosF.DetAnualSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 16
    end
    object CTD03: TDBEdit
      Left = 440
      Top = 88
      Width = 81
      Height = 21
      Hint = 'Cantidad a aplicar durante marzo'
      Color = clInactiveCaption
      DataField = 'CTD03'
      DataSource = DatosF.DetAnualSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 17
    end
    object CTD04: TDBEdit
      Left = 104
      Top = 144
      Width = 81
      Height = 21
      Hint = 'Cantidad a aplicar durante abril'
      Color = clInactiveCaption
      DataField = 'CTD04'
      DataSource = DatosF.DetAnualSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 18
    end
    object CTD05: TDBEdit
      Left = 272
      Top = 144
      Width = 81
      Height = 21
      Hint = 'Cantidad a aplicar durante mayo'
      Color = clInactiveCaption
      DataField = 'CTD05'
      DataSource = DatosF.DetAnualSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 19
    end
    object CTD06: TDBEdit
      Left = 440
      Top = 144
      Width = 81
      Height = 21
      Hint = 'Cantidad a aplicar durante junio'
      Color = clInactiveCaption
      DataField = 'CTD06'
      DataSource = DatosF.DetAnualSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 20
    end
    object CTD07: TDBEdit
      Left = 104
      Top = 200
      Width = 81
      Height = 21
      Hint = 'Cantidad a aplicar durante julio'
      Color = clInactiveCaption
      DataField = 'CTD07'
      DataSource = DatosF.DetAnualSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 21
    end
    object CTD08: TDBEdit
      Left = 272
      Top = 200
      Width = 81
      Height = 21
      Hint = 'Cantidad a aplicar durante agosto'
      Color = clInactiveCaption
      DataField = 'CTD08'
      DataSource = DatosF.DetAnualSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 22
    end
    object CTD09: TDBEdit
      Left = 440
      Top = 200
      Width = 81
      Height = 21
      Hint = 'Cantidad a aplicar durante septiembre'
      Color = clInactiveCaption
      DataField = 'CTD09'
      DataSource = DatosF.DetAnualSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 23
    end
    object CTD10: TDBEdit
      Left = 104
      Top = 256
      Width = 81
      Height = 21
      Hint = 'Cantidad a aplicar durante octubre'
      Color = clInactiveCaption
      DataField = 'CTD10'
      DataSource = DatosF.DetAnualSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 24
    end
    object CTD11: TDBEdit
      Left = 272
      Top = 256
      Width = 81
      Height = 21
      Hint = 'Cantidad a aplicar durante noviembre'
      Color = clInactiveCaption
      DataField = 'CTD11'
      DataSource = DatosF.DetAnualSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 25
    end
    object CTD12: TDBEdit
      Left = 440
      Top = 256
      Width = 81
      Height = 21
      Hint = 'Cantidad a aplicar durante diciembre'
      Color = clInactiveCaption
      DataField = 'CTD12'
      DataSource = DatosF.DetAnualSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 26
    end
    object NomComercial: TDBEdit
      Left = 32
      Top = 24
      Width = 249
      Height = 21
      Hint = 'Nombre comercial del producto que aparecer'#225' en este plan anual.'
      DataField = 'NOMCOMERCIAL'
      DataSource = DatosF.DetAnualSource
      TabOrder = 0
    end
  end
  object Cancelar: TButton [8]
    Left = 464
    Top = 456
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 3
    OnClick = CancelarClick
  end
  object Aplicar: TButton [9]
    Left = 736
    Top = 128
    Width = 75
    Height = 25
    Caption = '&Aplicar'
    TabOrder = 4
    OnClick = AplicarClick
  end
  object Plantilla: TDBLookupComboBox [10]
    Left = 736
    Top = 96
    Width = 145
    Height = 21
    KeyField = 'CODIGO'
    ListField = 'CODIGO'
    ListSource = DatosF.RepartoSource
    TabOrder = 5
  end
  object ListaIzq: TDBGrid [11]
    Left = 8
    Top = 8
    Width = 361
    Height = 465
    DataSource = DatosF.AnualSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 6
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'NOMFINCA'
        Title.Caption = 'Finca'
        Width = 146
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SECTOR'
        Title.Caption = 'Sector'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ANYO'
        Title.Caption = 'A'#241'o'
        Width = 43
        Visible = True
      end>
  end
  object PlantillaBtn: TButton [12]
    Left = 816
    Top = 128
    Width = 75
    Height = 25
    Caption = '&Plantillas'
    TabOrder = 7
    OnClick = PlantillaBtnClick
  end
  inherited EliminarB: TBitBtn [13]
    Left = 544
    Top = 456
    TabOrder = 10
    OnClick = EliminarClick
  end
  inherited GuardarB: TBitBtn
    Left = 376
    Top = 456
    TabOrder = 9
    OnClick = GuardarClick
  end
  inherited NuevoB: TBitBtn [15]
    Left = 664
    Top = 456
    TabOrder = 8
    Visible = False
  end
end
