//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainC.h"
#include "DatosC.h"
#include "ClienteC.h"
#include "FincasC.h"
#include "AutorC.h"
#include "AbonosC.h"
#include "CoeficientesC.h"
#include "AnualC.h"
#include "DistMensualC.h"
#include "RepartoC.h"
#include "RiegoC.h"
#include "RiegoPlantC.h"
#include "CreamesC.h"
#include "MensualC.h"
#include "ComercialC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma link "UCrpe32"
//#pragma link "UCrpeClasses"
#pragma link "UCrpe32"
#pragma link "UCrpeClasses"
#pragma resource "*.dfm"
TMainF *MainF;

//---------------------------------------------------------------------------
__fastcall TMainF::TMainF(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TMainF::ClientesMenuClick(TObject *Sender)
{
        if (ClienteF == NULL)
           ClienteF = new TClienteF(Application);
        else
        {
           ClienteF->Show();
           if(ClienteF->WindowState == wsMinimized)
              ClienteF->WindowState = wsNormal;
        }
}
//---------------------------------------------------------------------------

void __fastcall TMainF::FincasMenuClick(TObject *Sender)
{
        if (FincasF == NULL)
           FincasF = new TFincasF(Application);
        else
        {
           FincasF->Show();
           if(FincasF->WindowState == wsMinimized)
              FincasF->WindowState = wsNormal;
        }
}
//---------------------------------------------------------------------------
void __fastcall TMainF::Salir1Click(TObject *Sender)
{
        Close();
}
//---------------------------------------------------------------------------
void __fastcall TMainF::AutorMenuClick(TObject *Sender)
{
        AboutBox->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TMainF::FormClose(TObject *Sender, TCloseAction &Action)
{
        if (ClienteF != NULL)
           //Esta el m�dulo Clientes activo, por lo que vamos a cerrarlo
           ClienteF->Close();
        if (FincasF != NULL)
           // Esta el m�dulo Fincas activo, por lo que vamos a cerrarlo
           FincasF->Close();
        if (ProductF != NULL)
            ProductF->Close();
        if (CoeficienteF != NULL)
            CoeficienteF->Close();
        if (AbonadoAnualF != NULL)
            AbonadoAnualF->Close();
        if (DistMensualF != NULL)
            DistMensualF->Close();
        if (RepartoF != NULL)
            RepartoF->Close();
        if (MensualF != NULL)
            MensualF->Close();
}
//---------------------------------------------------------------------------
void __fastcall TMainF::Abonos1Click(TObject *Sender)
{
//Lanzar pantalla abonos
        if (ProductF == NULL)
            ProductF = new TProductF(Application);
        else
        {
            ProductF->Show();
            if(ProductF->WindowState == wsMinimized)
              ProductF->WindowState = wsNormal;
        }

}
//---------------------------------------------------------------------------
void __fastcall TMainF::CoeficientesClick(TObject *Sender)
{
//Lanzar pantalla coeficientes
        if (CoeficienteF == NULL)
            CoeficienteF = new TCoeficienteF(Application);
        else
        {
            CoeficienteF->Show();
            if(CoeficienteF->WindowState == wsMinimized)
              CoeficienteF->WindowState = wsNormal;
        }
}
//---------------------------------------------------------------------------
void __fastcall TMainF::GenerarPlanAbonadoAnual1Click(TObject *Sender)
{
        //Lanzar pantalla plan de abonados.
        if (AbonadoAnualF == NULL)
            AbonadoAnualF = new TAbonadoAnualF(Application);
        else
            AbonadoAnualF->Show();
            if(AbonadoAnualF->WindowState == wsMinimized)
              AbonadoAnualF->WindowState = wsNormal;
}
//---------------------------------------------------------------------------
void __fastcall TMainF::RepartoMensual1Click(TObject *Sender)
{
        //Lanzar Pantalla de reparto mensual
        if (DistMensualF == NULL)
            DistMensualF = new TDistMensualF(Application);
        else
        {
            DistMensualF->Show();
            if(DistMensualF->WindowState == wsMinimized)
              DistMensualF->WindowState = wsNormal;
        }
}
//---------------------------------------------------------------------------

void __fastcall TMainF::Plantillasreparto1Click(TObject *Sender)
{
        //Lanzar pantalla de plantilla de reparto
        if (RepartoF == NULL)
            RepartoF = new TRepartoF(Application);
        else
        {
            RepartoF->Show();
            if(RepartoF->WindowState == wsMinimized)
              RepartoF->WindowState = wsNormal;
        }
}
//---------------------------------------------------------------------------
void __fastcall TMainF::Impresionmasiva1Click(TObject *Sender)
{
/* Realizar impresi�n masiva de los planes de abonados
   pdtes de imprimir*/
   TDataSetState State, StateDet;

   //�Esta pdte de inserci�n/modificaci�n alg�n plan de abonado?
   State = DatosF->Anual->State;
   StateDet = DatosF->DetAnual->State;
   if (State != dsBrowse || StateDet != dsBrowse)
   {
        Application->MessageBoxA("No se puede lanzar impresi�n.\nPlan Abonado Anual pendiente de modificaci�n/inserci�n.","Error",MB_ICONERROR);
        return;
   }
   try
   {
        //Empieza transacci�n.
        DatosF->Abono->BeginTrans();
        //Vaciar tabla de impresi�n
        DatosF->Consulta->SQL->Clear();
        DatosF->Consulta->SQL->Add("DELETE FROM IMPRESION;") ;
        DatosF->Consulta->ExecSQL();

        //Rellenar con los planes de abono no impresos.
        DatosF->Consulta->SQL->Clear();
        DatosF->Consulta->SQL->Add("INSERT INTO IMPRESION ") ;
        DatosF->Consulta->SQL->Add("(CODIGO) ");
        DatosF->Consulta->SQL->Add("SELECT CODANUAL ");
        DatosF->Consulta->SQL->Add("FROM ANUAL WHERE not IMPRESO;");
        if(!DatosF->Consulta->ExecSQL())
            throw Exception("Ning�n plan anual a imprimir");

        //Marcar los planes de abono correspondientes.
        DatosF->Consulta->SQL->Clear();
        DatosF->Consulta->SQL->Add("UPDATE ANUAL SET IMPRESO = true WHERE not IMPRESO;");
        DatosF->Consulta->ExecSQL();

        //Actualizamos la tabla Anual para que muestre los cambios.
        DatosF->Anual->Close();
        DatosF->Anual->Open();

        //Finaliza transacci�n.
        DatosF->Abono->CommitTrans();
   }
   catch(Exception &e)
   {
        DatosF->Abono->RollbackTrans();
        ShowMessage("No se puede lanzar impresi�n.\n" + e.Message);
        return;
   }
   //Lanzamos la impresi�n
   Report->WindowButtonBar->PrintSetupBtn = true;
   Report->ReportName = "Anual.rpt";
   Report->WindowStyle->Title = "Plan Abonado Anual (Masivo)";
   Report->Show();
   if (Report->LastErrorNumber)
      //Ha habido alg�n error
      ShowMessage(AnsiString(Report->LastErrorNumber) +" " + Report->LastErrorString);
   Report->ReportName = "";
}
//---------------------------------------------------------------------------
void __fastcall TMainF::DiasRiegoClick(TObject *Sender)
{
        //Lanzar pantalla de d�as de riego
        if (RiegoF == NULL)
            RiegoF = new TRiegoF(Application);
        else
        {
            RiegoF->Show();
            if(RiegoF->WindowState == wsMinimized)
              RiegoF->WindowState = wsNormal;
        }
}
//---------------------------------------------------------------------------

void __fastcall TMainF::DiasRiegoPlantClick(TObject *Sender)
{
        //Lanzar pantalla de d�as de riego plantonada
        if (RiegoPlantF == NULL)
            RiegoPlantF = new TRiegoPlantF(Application);
        else
        {
            RiegoPlantF->Show();
            if(RiegoPlantF->WindowState == wsMinimized)
              RiegoPlantF->WindowState = wsNormal;
        }
}
//---------------------------------------------------------------------------

void __fastcall TMainF::CrearMesClick(TObject *Sender)
{
        //Lanzar la pantalla de creaci�n masiva del mes
        if (CreamesF == NULL)
            CreamesF = new TCreamesF(Application);
        else
        {
            CreamesF->Show();
            if(CreamesF->WindowState == wsMinimized)
              CreamesF->WindowState = wsNormal;
        }
}
//---------------------------------------------------------------------------

void __fastcall TMainF::GestionMesClick(TObject *Sender)
{
        //Pantalla de gesti�n de los planes anuales
        if (MensualF == NULL)
            MensualF = new TMensualF(Application);
        else
        {
            MensualF->Show();
            if(MensualF->WindowState == wsMinimized)
              MensualF->WindowState = wsNormal;
        }
}
//---------------------------------------------------------------------------
void __fastcall TMainF::ImpresionMensualClick(TObject *Sender)
{
/* Realizar impresi�n masiva de los planes de abonados
   pdtes de imprimir*/
   TDataSetState State;

   //�Esta pdte de inserci�n/modificaci�n alg�n plan de abonado?
   State = DatosF->Mensual->State;
   if (State != dsBrowse)
   {
        Application->MessageBoxA("No se puede lanzar impresi�n.\nPlan Abonado Mensual pendiente de modificaci�n/inserci�n.","Error",MB_ICONERROR);
        return;
   }
   try
   {
        //Empieza transacci�n.
        DatosF->Abono->BeginTrans();
        //Vaciar tabla de impresi�n
        DatosF->Consulta->SQL->Clear();
        DatosF->Consulta->SQL->Add("DELETE FROM IMPRESION;") ;
        DatosF->Consulta->ExecSQL();

        //Rellenar con los planes de abono no impresos.
        DatosF->Consulta->SQL->Clear();
        DatosF->Consulta->SQL->Add("INSERT INTO IMPRESION ") ;
        DatosF->Consulta->SQL->Add("(CODIGO) ");
        DatosF->Consulta->SQL->Add("SELECT CODMENSUAL ");
        DatosF->Consulta->SQL->Add("FROM MENSUAL WHERE not IMPRESO;");
        if(!DatosF->Consulta->ExecSQL())
            throw Exception("Ning�n plan mensual a imprimir");

        //Marcar los planes de abono correspondientes.
        DatosF->Consulta->SQL->Clear();
        DatosF->Consulta->SQL->Add("UPDATE MENSUAL SET IMPRESO = true WHERE not IMPRESO;");
        DatosF->Consulta->ExecSQL();

        //Actualizamos la tabla Mensual para que muestre los cambios.
        DatosF->Mensual->Close();
        DatosF->Mensual->Open();

        //Finaliza transacci�n.
        DatosF->Abono->CommitTrans();
   }
   catch(Exception &e)
   {
        DatosF->Abono->RollbackTrans();
        ShowMessage("No se puede lanzar impresi�n.\n" + e.Message);
        return;
   }
   //Lanzamos la impresi�n
   Report->WindowButtonBar->PrintSetupBtn = true;
   Report->ReportName = "Mensual.rpt";
   Report->WindowStyle->Title = "Plan Abonado Mensual (Masivo)";
   Report->Show();
   if (Report->LastErrorNumber)
      //Ha habido alg�n error
      ShowMessage("Error Crystal Reports.\n" + AnsiString(Report->LastErrorNumber) +" " + Report->LastErrorString);
   Report->ReportName = "";
}
//---------------------------------------------------------------------------
void __fastcall TMainF::Mododebug1Click(TObject *Sender)
{
    if (pruebas)
        Application->MessageBoxA("Modo debug off","Informaci�n",MB_ICONINFORMATION);
    else
        Application->MessageBoxA("Modo debug on","Informaci�n",MB_ICONINFORMATION);
    pruebas = !pruebas;


}
//---------------------------------------------------------------------------
void __fastcall TMainF::Comerciales1Click(TObject *Sender)
{
        //Pantalla de gesti�n de los comerciales
        if (ComercialF == NULL)
            ComercialF = new TComercialF(Application);
        else
        {
            ComercialF->Show();
            if(ComercialF->WindowState == wsMinimized)
              ComercialF->WindowState = wsNormal;
        }
}
//---------------------------------------------------------------------------

void __fastcall TMainF::MigrarDatosClick(TObject *Sender)
{
   /*En la versi�n posterior a 3.0.0 se ha creado a�adido el campo NOMFINCA
     a la tabla MENSUAL. Inicializar el campo con el nombre de la finca*/
   if (MensualF == NULL)
   {
       try
       {
          DatosF->Abono->BeginTrans();
          DatosF->Mensual->First();
          while (!DatosF->Mensual->Eof)
          {
             //Cargar la finca asociada al plan anual
             if ( ! DatosF->Fincas->Locate("CODFINCA", DatosF->Mensual->FieldValues["CODFINCA"] , TLocateOptions()) )
                 throw Exception("Error al cargar la finca " + DatosF->Mensual->FieldValues["CODFINCA"]);
             DatosF->Mensual->Edit();
             DatosF->Mensual->FieldValues["NOMFINCA"] = DatosF->Fincas->FieldValues["NOMFINCA"];
             DatosF->Mensual->Post();
             DatosF->Mensual->Next();
          }
          DatosF->Abono->CommitTrans();
          ShowMessage("Ok.Campo NOMFINCA Tabla MENSUAL inicializado");
       }
       catch(Exception &e)
       {
          DatosF->Abono->RollbackTrans();
          ShowMessage("Error al inicializar el campo NOMFINCA de la tabla MENSUAL.\n" + e.Message);
       }
   }
   else
   {
       Application->MessageBoxA("Cierre primero el plan mensual","Informaci�n",MB_ICONINFORMATION);
       MensualF->Show();
       if (MensualF->WindowState == wsMinimized)
           MensualF->WindowState = wsNormal;
   }
}
//---------------------------------------------------------------------------

