//---------------------------------------------------------------------------

#ifndef BuscarProductoCH
#define BuscarProductoCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "PlantillaC.h"
#include <Buttons.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
//---------------------------------------------------------------------------
class TBuscarProductoF : public TPlantilla
{
__published:	// IDE-managed Components
        TGroupBox *CriteriosBox;
        TGroupBox *ResultadoBox;
        TDBGrid *Resultado;
        TADOQuery *BuscarProducto;
        TAutoIncField *BuscarProductoCODART;
        TWideStringField *BuscarProductoNOMCOMERCIAL;
        TFloatField *BuscarProductoN;
        TFloatField *BuscarProductoP;
        TFloatField *BuscarProductoK;
        TFloatField *BuscarProductoCA;
        TFloatField *BuscarProductoMG;
        TFloatField *BuscarProductoMICROELEMENTOS;
        TFloatField *BuscarProductoAHUMICO;
        TFloatField *BuscarProductoCSALINO;
        TDataSource *BuscarProductoSource;
        TGroupBox *ComposicionBox;
        TCheckBox *NFlag;
        TCheckBox *PFlag;
        TCheckBox *KFlag;
        TCheckBox *CaFlag;
        TCheckBox *MgFlag;
        TCheckBox *MicroFlag;
        TCheckBox *AHumicoFlag;
        TCheckBox *CSalinoFlag;
        TFloatField *BuscarProductoFE;
        TCheckBox *FeFlag;
        TBitBtn *BusquedaB;
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall BusquedaBClick(TObject *Sender);
        void __fastcall ResultadoDblClick(TObject *Sender);
        void __fastcall ResultadoKeyPress(TObject *Sender, char &Key);
private:	// User declarations
public:		// User declarations
        __fastcall TBuscarProductoF(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TBuscarProductoF *BuscarProductoF;
//---------------------------------------------------------------------------
#endif
