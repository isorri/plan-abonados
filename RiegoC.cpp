//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "RiegoC.h"
#include "DatosC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PlantillaC"
#pragma resource "*.dfm"
TRiegoF *RiegoF;
//---------------------------------------------------------------------------
__fastcall TRiegoF::TRiegoF(TComponent* Owner)
        : TPlantilla(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TRiegoF::CerrarClick(TObject *Sender)
{
        Close();
}
//---------------------------------------------------------------------------
void __fastcall TRiegoF::FormClose(TObject *Sender, TCloseAction &Action)
{

   TDataSetState State;
   int res;
        State = DatosF->DiasRiego->State;
        if (State == dsInsert)
        {
            res = Application->MessageBoxA("Registro pendiente de inserci�n.\n�Guardar datos?.","Pregunta",MB_ICONQUESTION+MB_YESNO);
            if (res == IDYES)
                DatosF->DiasRiego->Post();
            else
                DatosF->DiasRiego->Cancel();
        }
        else
        {
            if (State == dsEdit)
            {
                res=Application->MessageBoxA("Registro con cambios no guardados.\n�Guardar cambios?","Pregunta",MB_ICONQUESTION+MB_YESNO);
                if (res == IDYES)
                    DatosF->DiasRiego->Post();
                else
                    DatosF->DiasRiego->Cancel();
            }
        }

   Action = caFree;
   RiegoF = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TRiegoF::NuevoClick(TObject *Sender)
{
   TDataSetState State;
   State = DatosF->DiasRiego->State;

   if (State == dsEdit)
   {
      Application->MessageBoxA("Registro actual pendiente de guardar cambios.\nGuarde los cambios y vuelva a pulsar el bot�n.","Error",MB_ICONERROR);
      return;
   }
   if (State == dsInsert)
   {
      Application->MessageBoxA("Registro pendiente de inserci�n.\nGuarde el cliente y vuelva a pulsar el bot�n","Error",MB_ICONERROR);
      return;
   }
   DatosF->DiasRiego->Insert();
   Mes->SetFocus();
   Lunes->Checked = false;
   Martes->Checked = false;
   Miercoles->Checked = false;
   Jueves->Checked = false;
   Viernes->Checked = false;
   Sabado->Checked = false;
   Domingo->Checked = false;
}
//---------------------------------------------------------------------------

void __fastcall TRiegoF::GuardarClick(TObject *Sender)
{
   TDataSetState State;
   int mes;
        State = DatosF->DiasRiego->State;
        if ( State == dsEdit || State == dsInsert)
        {
           mes = (int)DatosF->DiasRiego->FieldValues["MES"];
           if (mes <1 || mes >12)
           {
              Application->MessageBoxA("Mes fuera de rango","Error",MB_ICONERROR);
              Mes->SetFocus();
           }
           else
              DatosF->DiasRiego->Post();
         }
        else
           ShowMessage("Nada que guardar");

}
//---------------------------------------------------------------------------

void __fastcall TRiegoF::CancelarClick(TObject *Sender)
{
   TDataSetState State;
        State = DatosF->DiasRiego->State;
        if ( State == dsEdit || State == dsInsert)
           DatosF->DiasRiego->Cancel();
        else
           ShowMessage("Nada que cancelar");

}
//---------------------------------------------------------------------------
void TRiegoF::bloqueaFormulario(void)
{
     ListaIzq->Enabled = false;
     NuevoB->Enabled = false;
     GuardarB->Enabled = true;
     Cancelar->Enabled = true;
     EliminarB->Enabled = false;
}
//---------------------------------------------------------------------------
void TRiegoF::desbloqueaFormulario(void)
{
     ListaIzq->Enabled = true;
     NuevoB->Enabled = true;
     GuardarB->Enabled = false;
     Cancelar->Enabled = false;
     EliminarB->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TRiegoF::EliminarBClick(TObject *Sender)
{
    DatosF->DiasRiego->Delete();        
}
//---------------------------------------------------------------------------

