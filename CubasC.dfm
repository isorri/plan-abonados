inherited CubasF: TCubasF
  Left = 144
  Top = 80
  Width = 242
  Height = 244
  Caption = 'Capacidad Cubas'
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel [0]
    Left = 8
    Top = 16
    Width = 34
    Height = 13
    Caption = 'Cuba 1'
  end
  object Label3: TLabel [1]
    Left = 8
    Top = 48
    Width = 34
    Height = 13
    Caption = 'Cuba 2'
  end
  object Label4: TLabel [2]
    Left = 8
    Top = 80
    Width = 34
    Height = 13
    Caption = 'Cuba 3'
  end
  object Label5: TLabel [3]
    Left = 8
    Top = 112
    Width = 34
    Height = 13
    Caption = 'Cuba 4'
  end
  object Label6: TLabel [4]
    Left = 8
    Top = 144
    Width = 34
    Height = 13
    Caption = 'Cuba 5'
  end
  inherited Cerrar: TBitBtn
    Left = 144
    Top = 176
    Caption = '&Cerrar'
    OnClick = CerrarClick
  end
  object Cuba1: TDBEdit
    Left = 96
    Top = 16
    Width = 121
    Height = 21
    Cursor = crArrow
    DataField = 'CAPCUBA_1'
    DataSource = DatosF.FincasSource
    TabOrder = 1
  end
  object Cuba2: TDBEdit
    Left = 96
    Top = 48
    Width = 121
    Height = 21
    DataField = 'CAPCUBA_2'
    DataSource = DatosF.FincasSource
    TabOrder = 2
  end
  object Cuba3: TDBEdit
    Left = 96
    Top = 80
    Width = 121
    Height = 21
    DataField = 'CAPCUBA_3'
    DataSource = DatosF.FincasSource
    TabOrder = 3
  end
  object Cuba4: TDBEdit
    Left = 96
    Top = 112
    Width = 121
    Height = 21
    DataField = 'CAPCUBA_4'
    DataSource = DatosF.FincasSource
    TabOrder = 4
  end
  object Cuba5: TDBEdit
    Left = 96
    Top = 144
    Width = 121
    Height = 21
    DataField = 'CAPCUBA_5'
    DataSource = DatosF.FincasSource
    TabOrder = 5
  end
end
