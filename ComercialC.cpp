//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ComercialC.h"
#include "DatosC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PlantillaC"
#pragma resource "*.dfm"
TComercialF *ComercialF;
//---------------------------------------------------------------------------
__fastcall TComercialF::TComercialF(TComponent* Owner)
        : TPlantilla(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TComercialF::CerrarClick(TObject *Sender)
{
   Close();        
}
//---------------------------------------------------------------------------
void __fastcall TComercialF::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   Action = caFree;
   ComercialF = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TComercialF::NuevoClick(TObject *Sender)
{
   TDataSetState State;
   State = DatosF->Comerciales->State;

   if (State == dsEdit)
   {
      Application->MessageBoxA("Comercial actual pendiente de guardar cambios.\nGuarde los cambios y vuelva a pulsar el bot�n.","Error",MB_ICONERROR);
      return;
   }
   if (State == dsInsert)
   {
      Application->MessageBoxA("Comercial pendiente de inserci�n.\nGuarde el cliente y vuelva a pulsar el bot�n","Error",MB_ICONERROR);
      return;
   }
   Nombre->SetFocus();
   DatosF->Comerciales->Insert();
}
//---------------------------------------------------------------------------
void __fastcall TComercialF::GuardarClick(TObject *Sender)
{
   TDataSetState State;
        State = DatosF->Comerciales->State;
        if ( State == dsEdit || State == dsInsert)
        {
           if (Nombre->Text == "")
           {
               Application->MessageBoxA("Nombre obligatorio","Error",MB_ICONERROR);
               Nombre->SetFocus();
           }
           else
               DatosF->Comerciales->Post();
        }
        else
           ShowMessage("Nada que guardar");
}
//---------------------------------------------------------------------------
void __fastcall TComercialF::CancelarClick(TObject *Sender)
{
   TDataSetState State;
        State = DatosF->Comerciales->State;
        if ( State == dsEdit || State == dsInsert)
           DatosF->Comerciales->Cancel();
        else
           ShowMessage("Nada que cancelar");
}
//---------------------------------------------------------------------------
void __fastcall TComercialF::EliminarClick(TObject *Sender)
{
   TDataSetState State;
   State = DatosF->Comerciales->State;

   if (State == dsEdit)
   {
      Application->MessageBoxA("Comercial actual pendiente de guardar cambios.\nGuarde los cambios y vuelva a pulsar el bot�n.","Error",MB_ICONERROR);
      return;
   }
   if (State == dsInsert)
   {
      Application->MessageBoxA("Comercial pendiente de inserci�n.\nGuarde el cliente y vuelva a pulsar el bot�n","Error",MB_ICONERROR);
      return;
   }
   DatosF->Comerciales->Delete();
}
//---------------------------------------------------------------------------
void TComercialF::bloqueaFormulario(void)
{
     ListaIzq->Enabled = false;
     NuevoB->Enabled = false;
     GuardarB->Enabled = true;
     Cancelar->Enabled = true;
     EliminarB->Enabled = false;
}
//---------------------------------------------------------------------------
void TComercialF::desbloqueaFormulario(void)
{
     ListaIzq->Enabled = true;
     NuevoB->Enabled = true;
     GuardarB->Enabled = false;
     Cancelar->Enabled = false;
     EliminarB->Enabled = true;
}
//---------------------------------------------------------------------------


