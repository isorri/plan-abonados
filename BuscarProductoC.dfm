inherited BuscarProductoF: TBuscarProductoF
  Left = 412
  Top = 323
  Width = 619
  Height = 456
  Caption = 'B'#250'squeda de productos'
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited Cerrar: TBitBtn
    Left = 520
    Top = 384
    Caption = '&Cierra'
    OnClick = CerrarClick
    Kind = bkClose
  end
  inherited NuevoB: TBitBtn
    Left = 8
    Top = 392
    Enabled = False
    Visible = False
  end
  inherited GuardarB: TBitBtn
    Left = 40
    Top = 392
    Enabled = False
    Visible = False
  end
  inherited EliminarB: TBitBtn
    Left = 72
    Top = 392
    Enabled = False
    Visible = False
  end
  object CriteriosBox: TGroupBox
    Left = 8
    Top = 8
    Width = 593
    Height = 97
    Caption = 'Criterios de b'#250'squeda'
    TabOrder = 4
    object ComposicionBox: TGroupBox
      Left = 8
      Top = 16
      Width = 577
      Height = 41
      Caption = 'Composici'#243'n'
      TabOrder = 0
      object NFlag: TCheckBox
        Left = 8
        Top = 16
        Width = 41
        Height = 17
        Caption = 'N'
        TabOrder = 0
      end
      object PFlag: TCheckBox
        Left = 56
        Top = 16
        Width = 41
        Height = 17
        Caption = 'P'
        TabOrder = 1
      end
      object KFlag: TCheckBox
        Left = 104
        Top = 16
        Width = 41
        Height = 17
        Caption = 'K'
        TabOrder = 2
      end
      object CaFlag: TCheckBox
        Left = 144
        Top = 16
        Width = 41
        Height = 17
        Caption = 'Ca'
        TabOrder = 3
      end
      object MgFlag: TCheckBox
        Left = 184
        Top = 16
        Width = 41
        Height = 17
        Caption = 'Mg'
        TabOrder = 4
      end
      object MicroFlag: TCheckBox
        Left = 272
        Top = 16
        Width = 97
        Height = 17
        Caption = 'Microelementos'
        TabOrder = 6
      end
      object AHumicoFlag: TCheckBox
        Left = 384
        Top = 16
        Width = 73
        Height = 17
        Caption = 'A.H'#250'mico'
        TabOrder = 7
      end
      object CSalinoFlag: TCheckBox
        Left = 456
        Top = 16
        Width = 113
        Height = 17
        Caption = 'Componente Salino'
        TabOrder = 8
      end
      object FeFlag: TCheckBox
        Left = 232
        Top = 16
        Width = 41
        Height = 17
        Caption = 'Fe'
        TabOrder = 5
      end
    end
    object BusquedaB: TBitBtn
      Left = 256
      Top = 64
      Width = 75
      Height = 25
      Caption = 'B'#250'squeda'
      TabOrder = 1
      OnClick = BusquedaBClick
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00840000008400
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0084000000840000008400
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF0000000000000000000000000000000000840000008400000084000000FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
        000084848400C6C6C600FFFFFF00848484000000000084000000FF00FF00FF00
        FF00FF00FF00FF00FF0000008400FF00FF00FF00FF00FF00FF00000000008484
        8400C6C6C600C6C6C600C6C6C600FFFFFF008484840000000000FF00FF00FF00
        FF00FF00FF000000840000008400FF00FF00FF00FF00FF00FF0000000000C6C6
        C600C6C6C600C6C6C600C6C6C600C6C6C600FFFFFF0000000000FF00FF00FF00
        FF000000840000008400000084000000840000008400FF00FF0000000000C6C6
        C600FFFFFF00FFFF0000C6C6C600C6C6C600C6C6C60000000000FF00FF00FF00
        FF00FF00FF000000840000008400FF00FF00FF00FF0000008400000000008484
        8400FFFFFF00FFFFFF00C6C6C600C6C6C6008484840000000000FF00FF00FF00
        FF00FF00FF00FF00FF0000008400FF00FF00FF00FF0000008400FF00FF000000
        000084848400C6C6C600C6C6C6008484840000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000008400FF00FF00FF00
        FF0000000000000000000000000000000000FF00FF00FF00FF00FF00FF000000
        8400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
        8400FF00FF00FF00FF0000008400FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
        8400FF00FF00FF00FF000000840000008400FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF000000840000008400000084000000840000008400FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF000000840000008400FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000008400FF00FF00FF00FF00FF00FF00}
    end
  end
  object ResultadoBox: TGroupBox
    Left = 8
    Top = 112
    Width = 593
    Height = 265
    Caption = 'Resultado'
    TabOrder = 5
    object Resultado: TDBGrid
      Left = 8
      Top = 16
      Width = 569
      Height = 241
      DataSource = BuscarProductoSource
      Options = [dgTitles, dgIndicator, dgColumnResize, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = ResultadoDblClick
      OnKeyPress = ResultadoKeyPress
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMCOMERCIAL'
          Title.Caption = 'Nombre'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'N'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'P'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'K'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CA'
          Title.Caption = 'Ca'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MG'
          Title.Caption = 'Mg'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FE'
          Title.Caption = 'Fe'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MICROELEMENTOS'
          Title.Caption = 'Micro'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AHUMICO'
          Title.Caption = 'AH'#250'mico'
          Width = 47
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CSALINO'
          Title.Caption = 'CSalino'
          Width = 40
          Visible = True
        end>
    end
  end
  object BuscarProducto: TADOQuery
    Connection = DatosF.Abono
    Parameters = <>
    SQL.Strings = (
      
        'SELECT  CODART,NOMCOMERCIAL,N,P,K,CA,MG,FE,MICROELEMENTOS,AHUMIC' +
        'O,CSALINO'
      'FROM PRODUCTOS'
      'ORDER BY NOMCOMERCIAL')
    Left = 152
    Top = 392
    object BuscarProductoCODART: TAutoIncField
      FieldName = 'CODART'
      ReadOnly = True
    end
    object BuscarProductoNOMCOMERCIAL: TWideStringField
      FieldName = 'NOMCOMERCIAL'
      Size = 50
    end
    object BuscarProductoN: TFloatField
      FieldName = 'N'
    end
    object BuscarProductoP: TFloatField
      FieldName = 'P'
    end
    object BuscarProductoK: TFloatField
      FieldName = 'K'
    end
    object BuscarProductoCA: TFloatField
      FieldName = 'CA'
    end
    object BuscarProductoMG: TFloatField
      FieldName = 'MG'
    end
    object BuscarProductoMICROELEMENTOS: TFloatField
      FieldName = 'MICROELEMENTOS'
    end
    object BuscarProductoAHUMICO: TFloatField
      FieldName = 'AHUMICO'
    end
    object BuscarProductoCSALINO: TFloatField
      FieldName = 'CSALINO'
    end
    object BuscarProductoFE: TFloatField
      FieldName = 'FE'
    end
  end
  object BuscarProductoSource: TDataSource
    DataSet = BuscarProducto
    Left = 184
    Top = 392
  end
end
