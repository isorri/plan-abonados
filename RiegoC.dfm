inherited RiegoF: TRiegoF
  Left = 481
  Top = 266
  Width = 472
  Height = 245
  Caption = 'Riego Adultos'
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited Cerrar: TBitBtn
    Left = 408
    Top = 160
    Width = 46
    Hint = 'Cierra esta ventana'
    OnClick = CerrarClick
  end
  inherited NuevoB: TBitBtn
    Left = 136
    Top = 160
    Width = 57
    TabOrder = 4
    OnClick = NuevoClick
  end
  inherited GuardarB: TBitBtn
    Left = 200
    Top = 160
    Width = 66
    Enabled = False
    TabOrder = 5
    OnClick = GuardarClick
  end
  inherited EliminarB: TBitBtn
    Left = 336
    Top = 160
    Width = 65
    Height = 23
    TabOrder = 6
    OnClick = EliminarBClick
  end
  object ListaIzq: TDBGrid
    Left = 8
    Top = 4
    Width = 121
    Height = 197
    DataSource = DatosF.DiasRiegoSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'MES'
        Title.Caption = 'Mes'
        Visible = True
      end>
  end
  object Panel: TGroupBox
    Left = 144
    Top = 8
    Width = 297
    Height = 145
    TabOrder = 2
    object Label2: TLabel
      Left = 88
      Top = 8
      Width = 20
      Height = 13
      Caption = 'Mes'
    end
    object Mes: TDBEdit
      Left = 88
      Top = 24
      Width = 89
      Height = 21
      DataField = 'MES'
      DataSource = DatosF.DiasRiegoSource
      TabOrder = 0
    end
    object Lunes: TDBCheckBox
      Left = 8
      Top = 64
      Width = 65
      Height = 17
      Caption = 'Lunes'
      DataField = 'LUNES'
      DataSource = DatosF.DiasRiegoSource
      TabOrder = 1
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object Martes: TDBCheckBox
      Left = 88
      Top = 64
      Width = 81
      Height = 17
      Caption = 'Martes'
      DataField = 'MARTES'
      DataSource = DatosF.DiasRiegoSource
      TabOrder = 2
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object Miercoles: TDBCheckBox
      Left = 176
      Top = 64
      Width = 97
      Height = 17
      Caption = 'Mi'#233'rcoles'
      DataField = 'MIERCOLES'
      DataSource = DatosF.DiasRiegoSource
      TabOrder = 3
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object Jueves: TDBCheckBox
      Left = 8
      Top = 88
      Width = 97
      Height = 17
      Caption = 'Jueves'
      DataField = 'JUEVES'
      DataSource = DatosF.DiasRiegoSource
      TabOrder = 4
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object Viernes: TDBCheckBox
      Left = 88
      Top = 88
      Width = 97
      Height = 17
      Caption = 'Viernes'
      DataField = 'VIERNES'
      DataSource = DatosF.DiasRiegoSource
      TabOrder = 5
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object Sabado: TDBCheckBox
      Left = 176
      Top = 88
      Width = 97
      Height = 17
      Caption = 'S'#225'bado'
      DataField = 'SABADO'
      DataSource = DatosF.DiasRiegoSource
      TabOrder = 6
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object Domingo: TDBCheckBox
      Left = 88
      Top = 112
      Width = 97
      Height = 17
      Caption = 'Domingo'
      DataField = 'DOMINGO'
      DataSource = DatosF.DiasRiegoSource
      TabOrder = 7
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
  end
  object Cancelar: TButton
    Left = 272
    Top = 160
    Width = 57
    Height = 25
    Hint = 'Cancela los cambios introducidos.'
    Caption = 'Cancelar'
    Enabled = False
    TabOrder = 3
    OnClick = CancelarClick
  end
end
