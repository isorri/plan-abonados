//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AbonosC.h"
#include "DatosC.h"
#include "ProvefeC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PlantillaC"
#pragma resource "*.dfm"
TProductF *ProductF;
//---------------------------------------------------------------------------
__fastcall TProductF::TProductF(TComponent* Owner)
        : TPlantilla(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TProductF::CerrarClick(TObject *Sender)
{
        Close();
}
//---------------------------------------------------------------------------

void __fastcall TProductF::FormClose(TObject *Sender, TCloseAction &Action)
{
   TDataSetState StProduct;
   int res;
        StProduct = DatosF->Productos->State;
        if (StProduct == dsInsert)
        {
            res = Application->MessageBoxA("Producto pendiente de inserci�n.\n�Guardar producto introducido?.","Pregunta",MB_ICONQUESTION+MB_YESNO);
            if (res == IDYES)
                DatosF->Productos->Post();
            else
                DatosF->Productos->Cancel();
        }
        else
        {
            if (StProduct == dsEdit)
            {
                res=Application->MessageBoxA("Producto con cambios no guardados.\n�Guardar cambios?","Pregunta",MB_ICONQUESTION+MB_YESNO);
                if (res == IDYES)
                    DatosF->Productos->Post();
                else
                    DatosF->Productos->Cancel();
            }
        }
        Action = caFree;
        ProductF = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TProductF::InsertarClick(TObject *Sender)
{
   TDataSetState StProduct;
   StProduct = DatosF->Productos->State;

   if (StProduct == dsEdit)
        Application->MessageBoxA("Producto con cambios no guardados.\nPulse Guardar o Cancelar.","Error",MB_ICONERROR);
   else
        if (StProduct == dsInsert)
           Application->MessageBoxA("Hay un producto nuevo que no se ha guardado.\nPulse Guardar o Cancelar","Error",MB_ICONERROR);
        else
        {
           NomComercial->SetFocus();
           DatosF->Productos->Insert();
        }

}
//---------------------------------------------------------------------------

void __fastcall TProductF::GuardarClick(TObject *Sender)
{
   if (NomComercial->Text != "")
      DatosF->Productos->Post();
   else
   {
      Application->MessageBoxA("La descripci�n debe estar rellenada.","Error",MB_ICONERROR);
      NomComercial->SetFocus();
   }
}
//---------------------------------------------------------------------------

void __fastcall TProductF::CancelarClick(TObject *Sender)
{
    DatosF->Productos->Cancel();
}
//---------------------------------------------------------------------------
void __fastcall TProductF::ImportClick(TObject *Sender)
{
  //Almacena si hemos puesto el registro en modo edici�n al realizar este proceso.
  bool modoEdicion;

  //Comprobar que se ha introducido el c�digo de art�culo X3
  if (X3->Text == "")
  {
     Application->MessageBoxA("C�digo X3 no introducido","Error",MB_ICONERROR);
     return;
  }
  try
  {
     //Si no esta en modo insercion/editar --> edit()
     if (DatosF->Productos->State == dsBrowse)
     {
         DatosF->Productos->Edit();
         modoEdicion = true;
     }
     //Conectar a la base de datos
     ProvefeF->X3->Connected = true;
     //Recuperar el precio mediante una consulta
     ProvefeF->Precio->Prepare();
     ProvefeF->Precio->ParamByName("CodArticulo")->Value = X3->Text;
     ProvefeF->Precio->ExecSQL();
     ProvefeF->Precio->Open();
     if (!ProvefeF->Precio->RecordCount)
         Application->MessageBoxA("Ning�n precio recuperado.\nCompruebe el c�digo X3.","Error",MB_ICONERROR);
     //Inicializar el campo
     DatosF->ProductosPVT->Value = 0;
     DatosF->ProductosCOP->Value = 0;
     DatosF->ProductosGRF->Value = 0;
     DatosF->ProductosPVP->Value = 0;
     //Recorremos el resultado
     ProvefeF->Precio->First();
     while (!ProvefeF->Precio->Eof)
     {
        //Actualizar campos
        if (ProvefeF->PrecioTARIFA->Value == "PVT")
            DatosF->ProductosPVT->Value = ProvefeF->PrecioPRECIO->Value;
        else
           if (ProvefeF->PrecioTARIFA->Value == "COP")
               DatosF->ProductosCOP->Value = ProvefeF->PrecioPRECIO->Value;
           else
              if (ProvefeF->PrecioTARIFA->Value == "GRF")
                  DatosF->ProductosGRF->Value = ProvefeF->PrecioPRECIO->Value;
              else
                  DatosF->ProductosPVP->Value = ProvefeF->PrecioPRECIO->Value;
        ProvefeF->Precio->Next();
     }
     ProvefeF->Precio->Close();
     ProvefeF->Precio->UnPrepare();
     //Desconectar
     ProvefeF->X3->Connected = false;
     //Si no estaba en modo insercion/editar --> post()
     //Si ya estaba, dejar que el usuario guarde los cambios.
     if (modoEdicion)
         DatosF->Productos->Post();
  }
  catch(Exception &e)
  {
     ShowMessage("Error al conectar con PROVEFE.\n" + e.Message);
     //Si no estaba en modo insercion/editar -->Cancelar estado
     //Si ya estaba, el usuario se encarga de guardar o cancelar los cambios
     if (modoEdicion)
        DatosF->Productos->Cancel();
     return;
  }
}
//---------------------------------------------------------------------------

void __fastcall TProductF::EliminarClick(TObject *Sender)
{
       TDataSetState StProduct;
   StProduct = DatosF->Productos->State;

   if (StProduct == dsInsert || StProduct == dsEdit)
        Application->MessageBoxA("Hay un producto nuevo que no se ha guardado.\nPulse Guardar o Cancelar","Error",MB_ICONERROR);
   else
        DatosF->Productos->Delete();
}
//---------------------------------------------------------------------------
void TProductF::bloqueaFormulario(void)
{
     Productos->Enabled = false;
     NuevoB->Enabled = false;
     GuardarB->Enabled = true;
     Cancelar->Enabled = true;
     EliminarB->Enabled = false;
}
//---------------------------------------------------------------------------
void TProductF::desbloqueaFormulario(void)
{
     Productos->Enabled = true;
     NuevoB->Enabled = true;
     GuardarB->Enabled = false;
     Cancelar->Enabled = false;
     EliminarB->Enabled = true;
}
//---------------------------------------------------------------------------
