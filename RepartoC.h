//---------------------------------------------------------------------------

#ifndef RepartoCH
#define RepartoCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "PlantillaC.h"
#include <Buttons.hpp>
#include <DBCtrls.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TRepartoF : public TPlantilla
{
__published:	// IDE-managed Components
        TDBGrid *ListaIzq;
        TGroupBox *GroupBox1;
        TLabel *Label2;
        TDBEdit *Codigo;
        TGroupBox *GroupBox2;
        TLabel *Label3;
        TDBEdit *Enero;
        TDBEdit *Febrero;
        TLabel *Label4;
        TDBEdit *Marzo;
        TLabel *Label5;
        TDBEdit *Abril;
        TLabel *Label6;
        TDBEdit *Mayo;
        TLabel *Label7;
        TLabel *Label8;
        TDBEdit *Junio;
        TLabel *Label9;
        TDBEdit *Julio;
        TLabel *Label10;
        TDBEdit *Agosto;
        TDBEdit *Septiembre;
        TLabel *Label11;
        TLabel *Label12;
        TDBEdit *Octubre;
        TLabel *Label13;
        TDBEdit *Noviembre;
        TLabel *Label14;
        TDBEdit *Diciembre;
        TButton *Cancelar;
        TDBEdit *Total;
        TLabel *Label15;
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall NuevoClick(TObject *Sender);
        void __fastcall GuardarClick(TObject *Sender);
        void __fastcall CancelarClick(TObject *Sender);
        void __fastcall EliminarClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TRepartoF(TComponent* Owner);
        void bloqueaFormulario(void);
        void desbloqueaFormulario(void);        
};
//---------------------------------------------------------------------------
extern PACKAGE TRepartoF *RepartoF;
//---------------------------------------------------------------------------
#endif
