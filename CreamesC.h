//---------------------------------------------------------------------------

#ifndef CreamesCH
#define CreamesCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "PlantillaC.h"
#include <Buttons.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TCreamesF : public TPlantilla
{
__published:	// IDE-managed Components
        TEdit *Anyo;
        TEdit *Mes;
        TLabel *Label2;
        TLabel *Label3;
        TButton *Crear;
        TProgressBar *Progreso;
        TADOTable *Productos;
        TADOTable *Fincas;
        TAutoIncField *ProductosCODART;
        TWideStringField *ProductosNOMCOMERCIAL;
        TFloatField *FincasCAPACIDADCUBAS;
        TFloatField *FincasCAUDALCUBAS;
        TAutoIncField *FincasCODFINCA;
        TFloatField *ProductosDISOLUCIN;
        TCheckBox *EsQuincena;
        TEdit *Quincena;
        TADOQuery *ListaFincas;
        TADOQuery *CtdMensual;
        TLabel *Label4;
        TEdit *DRiego;
        TADOTable *DetAnual;
        TADOQuery *ListaSectores;
        TWideStringField *ListaSectoresSECTOR;
        TSmallintField *ListaSectoresTRIEGO;
        TBooleanField *ListaSectoresPLANTONADA;
        TIntegerField *ListaSectoresCODIGO_AF;
        TLabel *New;
        TLabel *Skip;
        TWideStringField *ListaSectoresCODANUAL;
        TWideStringField *FincasNOMFINCA;
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall CrearClick(TObject *Sender);
        void __fastcall EsQuincenaClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TCreamesF(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TCreamesF *CreamesF;
//---------------------------------------------------------------------------
#endif
