//---------------------------------------------------------------------------

#ifndef AnualCH
#define AnualCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "PlantillaC.h"
#include <Buttons.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <DBCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Mask.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>
#include <Chart.hpp>
#include <DbChart.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
//---------------------------------------------------------------------------
class TAbonadoAnualF : public TPlantilla
{
__published:	// IDE-managed Components
        TButton *CalcAnalisis;
        TGroupBox *Clave;
        TLabel *Label10;
        TDBEdit *Base;
        TLabel *Label12;
        TLabel *Label13;
        TLabel *Label14;
        TLabel *Label17;
        TButton *Anyadir;
        TDBGrid *Detalle;
        TButton *Cancelar;
        TADOTable *Fincas;
        TADOTable *Sectores;
        TAutoIncField *FincasCODFINCA;
        TWideStringField *FincasNOMFINCA;
        TWideStringField *SectoresSECTOR;
        TIntegerField *SectoresCODFINCA;
        TDBGrid *ListaIzq;
        TDBEdit *Sector;
        TDBEdit *CodFinca;
        TDBEdit *NomFinca;
        TDBLookupComboBox *Articulos;
        TADOQuery *ProductosAF;
        TButton *CalcCtd;
        TPopupMenu *EliminaProd;
        TMenuItem *EliminaProducto1;
        TEdit *FiltroAnyo;
        TWideStringField *SectoresPATRON;
        TDBCheckBox *Impreso;
        TButton *Reparto;
        TLabel *Label20;
        TSmallintField *SectoresARBOLNUM;
        TPageControl *Paginas;
        TTabSheet *Descripcion;
        TGroupBox *Estimacion;
        TTabSheet *ProductosPage;
        TStringField *FincasNOMCLI;
        TTabSheet *NecesidadesP;
        TDBGrid *SelAnalisis;
        TADOQuery *Analisis;
        TDataSource *AnalisisSource;
        TDateTimeField *AnalisisFECHA;
        TFloatField *AnalisisN;
        TFloatField *AnalisisP;
        TFloatField *AnalisisK;
        TFloatField *AnalisisCA;
        TFloatField *AnalisisMG;
        TFloatField *AnalisisFE;
        TFloatField *AnalisisMICROELEMENTOS;
        TFloatField *AnalisisAHUMICO;
        TFloatField *FincasN;
        TFloatField *FincasP;
        TFloatField *FincasK;
        TFloatField *FincasCA;
        TFloatField *FincasMG;
        TFloatField *FincasFE;
        TFloatField *FincasMICROELEMENTOS;
        TFloatField *FincasAHUMICO;
        TFloatField *SectoresHG;
        TGroupBox *Necesidades;
        TLabel *Label3;
        TLabel *Label2;
        TLabel *Label5;
        TLabel *Label4;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label9;
        TLabel *Label8;
        TLabel *Label15;
        TLabel *Label19;
        TDBEdit *N;
        TDBEdit *P;
        TDBEdit *K;
        TDBEdit *CA;
        TDBEdit *MG;
        TDBEdit *FE;
        TDBEdit *Micro;
        TDBEdit *AH;
        TDBEdit *CSALINO;
        TDBEdit *FAnalisis;
        TGroupBox *AFosforico;
        TLabel *Label18;
        TDBText *Nombre_AF;
        TDBEdit *CTD_P;
        TDBLookupComboBox *ListaAcidoFosforico;
        TButton *Seleccionar;
        TButton *EliminaAF;
        TDBEdit *Codigo_AF;
        TDBRichEdit *Observaciones;
        TCheckBox *Maximo;
        TBitBtn *ImprimeB;
        TDBEdit *CodAnual;
        TLabel *Label1;
        TTabSheet *AnotacionesP;
        TDBRichEdit *Anotaciones;
        TDBText *NombreCliente;
        TLabel *Label11;
        TSpeedButton *BuscaFincaB;
        TBitBtn *BuscaProductoB;
        TADOTable *Productos;
        TAutoIncField *ProductosAFCODART;
        TWideStringField *ProductosAFNOMCOMERCIAL;
        TDataSource *ProductosAFSource;
        TWideStringField *FincasCODCLI;
        TBitBtn *PrintFincaB;
        TCheckBox *AnalisisTipo;
        TLabel *Label16;
        TEdit *FiltroFinca;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall CancelarClick(TObject *Sender);
        void __fastcall GuardarClick(TObject *Sender);
        void __fastcall InsertarClick(TObject *Sender);
        void __fastcall EliminarClick(TObject *Sender);
        void __fastcall AnyadirClick(TObject *Sender);
        void __fastcall CalcAnalisisClick(TObject *Sender);
        void __fastcall CalcCtdClick(TObject *Sender);
        void __fastcall EliminaProducto1Click(TObject *Sender);
        void __fastcall FiltroAnyoChange(TObject *Sender);
        void __fastcall RepartoClick(TObject *Sender);
        void __fastcall ImprimeClick(TObject *Sender);
        void __fastcall SeleccionarClick(TObject *Sender);
        void __fastcall EliminaAFClick(TObject *Sender);
        void __fastcall BuscaFincaBClick(TObject *Sender);
        void __fastcall CodFincaChange(TObject *Sender);
        void __fastcall SectorChange(TObject *Sender);
        void __fastcall BuscaProductoBClick(TObject *Sender);
        void __fastcall PrintFincaBClick(TObject *Sender);
private:	// User declarations
        void CargarAnalisis(void);
public:		// User declarations
        __fastcall TAbonadoAnualF(TComponent* Owner);
        void bloqueaFormulario(void);
        void desbloqueaFormulario(void);
        void __fastcall anyadirProducto(AnsiString codigo);
};
//---------------------------------------------------------------------------
extern PACKAGE TAbonadoAnualF *AbonadoAnualF;
//---------------------------------------------------------------------------
#endif
