inherited AnalisisSectF: TAnalisisSectF
  Left = 438
  Top = 256
  Width = 483
  Height = 443
  Caption = 'A'#241'adir An'#225'lisis Hoja en el Sector'
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel [0]
    Left = 176
    Top = 120
    Width = 68
    Height = 13
    Caption = 'Fecha An'#225'lisis'
  end
  inherited Cerrar: TBitBtn
    Left = 384
    Top = 368
    Hint = 'Cierra esta ventana'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 9
    OnClick = CerrarClick
  end
  inherited NuevoB: TBitBtn
    Left = 80
    Top = 368
    TabOrder = 5
    OnClick = AnyadirClick
  end
  inherited GuardarB: TBitBtn
    Left = 160
    Top = 368
    Width = 68
    TabOrder = 6
    OnClick = GuardarClick
  end
  inherited EliminarB: TBitBtn
    Left = 312
    Top = 368
    Width = 68
    TabOrder = 8
    OnClick = EliminarClick
  end
  object DatoSector: TGroupBox
    Left = 168
    Top = 8
    Width = 289
    Height = 105
    Caption = 'Datos Sector'
    TabOrder = 1
    object Label2: TLabel
      Left = 16
      Top = 24
      Width = 32
      Height = 13
      Caption = 'Cliente'
    end
    object Finca: TLabel
      Left = 16
      Top = 48
      Width = 26
      Height = 13
      Caption = 'Finca'
    end
    object Label3: TLabel
      Left = 16
      Top = 72
      Width = 31
      Height = 13
      Caption = 'Sector'
    end
    object NomCli: TLabel
      Left = 64
      Top = 24
      Width = 3
      Height = 13
    end
    object NomFinca: TLabel
      Left = 64
      Top = 48
      Width = 3
      Height = 13
    end
    object Sector: TLabel
      Left = 64
      Top = 72
      Width = 3
      Height = 13
    end
  end
  object Fecha: TDateTimePicker
    Left = 320
    Top = 120
    Width = 97
    Height = 21
    Hint = 'Fecha en la que se realiz'#243' el an'#225'lisis'
    CalAlignment = dtaLeft
    Date = 38387
    Time = 38387
    DateFormat = dfShort
    DateMode = dmComboBox
    ImeMode = imHanguel
    Kind = dtkDate
    ParseInput = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnChange = FechaChange
  end
  object GroupBox1: TGroupBox
    Left = 168
    Top = 144
    Width = 289
    Height = 217
    Caption = 'Par'#225'metros An'#225'lisis'
    TabOrder = 4
    object Label5: TLabel
      Left = 16
      Top = 16
      Width = 46
      Height = 13
      Caption = 'Nitr'#243'geno'
    end
    object Label6: TLabel
      Left = 16
      Top = 64
      Width = 35
      Height = 13
      Caption = 'Potasio'
    end
    object Label7: TLabel
      Left = 16
      Top = 40
      Width = 35
      Height = 13
      Caption = 'F'#243'sforo'
    end
    object Label8: TLabel
      Left = 16
      Top = 88
      Width = 29
      Height = 13
      Caption = 'Calcio'
    end
    object Label9: TLabel
      Left = 16
      Top = 112
      Width = 46
      Height = 13
      Caption = 'Magnesio'
    end
    object Label10: TLabel
      Left = 16
      Top = 136
      Width = 28
      Height = 13
      Caption = 'Hierro'
    end
    object Label11: TLabel
      Left = 16
      Top = 160
      Width = 74
      Height = 13
      Caption = 'Microelementos'
    end
    object Label12: TLabel
      Left = 16
      Top = 184
      Width = 15
      Height = 13
      Caption = 'AH'
    end
    object Label13: TLabel
      Left = 240
      Top = 16
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label14: TLabel
      Left = 240
      Top = 40
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label15: TLabel
      Left = 240
      Top = 64
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label16: TLabel
      Left = 240
      Top = 112
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label17: TLabel
      Left = 240
      Top = 88
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label18: TLabel
      Left = 240
      Top = 136
      Width = 32
      Height = 13
      Caption = 'mg/Kg'
    end
    object Label19: TLabel
      Left = 241
      Top = 160
      Width = 32
      Height = 13
      Caption = 'mg/Kg'
    end
    object Label20: TLabel
      Left = 241
      Top = 184
      Width = 32
      Height = 13
      Caption = 'mg/Kg'
    end
    object N: TDBEdit
      Left = 112
      Top = 16
      Width = 121
      Height = 21
      DataField = 'N'
      DataSource = DatosF.AnalisisSectorSource
      TabOrder = 0
    end
    object K: TDBEdit
      Left = 112
      Top = 64
      Width = 121
      Height = 21
      DataField = 'K'
      DataSource = DatosF.AnalisisSectorSource
      TabOrder = 2
    end
    object P: TDBEdit
      Left = 112
      Top = 40
      Width = 121
      Height = 21
      DataField = 'P'
      DataSource = DatosF.AnalisisSectorSource
      TabOrder = 1
    end
    object Ca: TDBEdit
      Left = 112
      Top = 88
      Width = 121
      Height = 21
      DataField = 'Ca'
      DataSource = DatosF.AnalisisSectorSource
      TabOrder = 3
    end
    object Mg: TDBEdit
      Left = 112
      Top = 112
      Width = 121
      Height = 21
      DataField = 'Mg'
      DataSource = DatosF.AnalisisSectorSource
      TabOrder = 4
    end
    object Fe: TDBEdit
      Left = 112
      Top = 136
      Width = 121
      Height = 21
      DataField = 'Fe'
      DataSource = DatosF.AnalisisSectorSource
      TabOrder = 5
    end
    object Micro: TDBEdit
      Left = 112
      Top = 160
      Width = 121
      Height = 21
      DataField = 'MICROELEMENTOS'
      DataSource = DatosF.AnalisisSectorSource
      TabOrder = 6
    end
    object AH: TDBEdit
      Left = 112
      Top = 184
      Width = 121
      Height = 21
      DataField = 'AHUMICO'
      DataSource = DatosF.AnalisisSectorSource
      TabOrder = 7
    end
  end
  object FechaBD: TDBEdit
    Left = 248
    Top = 120
    Width = 65
    Height = 21
    DataField = 'FECHA'
    DataSource = DatosF.AnalisisSectorSource
    Enabled = False
    TabOrder = 3
  end
  object ListaIzq: TDBGrid
    Left = 8
    Top = 8
    Width = 153
    Height = 353
    DataSource = DatosF.AnalisisSectorSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'FECHA'
        Title.Caption = 'Fecha del an'#225'lisis'
        Visible = True
      end>
  end
  object Cancelar: TButton
    Left = 232
    Top = 368
    Width = 75
    Height = 25
    Hint = 'Cancela la modificaci'#243'n actual.'
    Caption = 'Cancelar'
    TabOrder = 7
    OnClick = CancelarClick
  end
  object CodFinca: TEdit
    Left = 376
    Top = 32
    Width = 73
    Height = 21
    TabOrder = 10
    Text = 'CodFinca'
    Visible = False
  end
end
