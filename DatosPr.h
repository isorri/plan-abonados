//----------------------------------------------------------------------------
#ifndef DatosPrH
#define DatosPrH
//----------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Windows.hpp>
#include <Classes.hpp>
#include <Graphics.hpp>
#include <Controls.hpp>
#include <Forms.hpp>
#include <Dialogs.hpp>
#include <DB.hpp>
#include <DBTables.hpp>
//----------------------------------------------------------------------------
class TDataModule1 : public TDataModule
{
__published:
	TStringField *Table2SECTOR;
	TSmallintField *Table2ARBOLNUM;
	TStringField *Table2VARIEDAD;
	TStringField *Table2PATRON;
	TFloatField *Table2DSTLINEA;
	TFloatField *Table2DSTARBOL;
	TFloatField *Table2HG;
	TStringField *Table2Edad;
	TFloatField *Table2PRODUCCIN;
	TSmallintField *Table2GOTEOXARB;
	TFloatField *Table2CAUDALGOT;
	TFloatField *Table2N;
	TFloatField *Table2P;
	TFloatField *Table2K;
	TFloatField *Table2Ca;
	TFloatField *Table2Mg;
	TFloatField *Table2Fe;
	TFloatField *Table2WC;
	TFloatField *Table2WH;
	TFloatField *Table2Enra;
	TFloatField *Table2AMINOACIDOS;
	TFloatField *Table2ANITRICO;
	TFloatField *Table2PROUSAL;
	TAutoIncField *Table1CODFINCA;
	TStringField *Table1NOMFINCA;
	TFloatField *Table1CAPACIDADCUBAS;
	TSmallintField *Table1NUMCUBAS;
	TFloatField *Table1CAUDALCUBAS;
	TFloatField *Table1CAPCUBANOD;
	TSmallintField *Table1ORIGENAGUA;
	TStringField *Table1ORIELECTRICIDAD;
	TStringField *Table1CODCLI;
	TDataSource *DataSource1;
	TTable *Table1;
	TTable *Table2;
	TDataSource *DataSource2;
	void __fastcall DataModuleCreate(TObject *Sender);
private:
	// private declarations
public:
	// public declarations
	__fastcall TDataModule1(TComponent *Owner);
};
//----------------------------------------------------------------------------
extern TDataModule1 *DataModule1;
//----------------------------------------------------------------------------
#endif
