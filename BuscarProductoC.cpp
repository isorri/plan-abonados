//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "BuscarProductoC.h"
#include "AnualC.h"
#include "DatosC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PlantillaC"
#pragma resource "*.dfm"
TBuscarProductoF *BuscarProductoF;
//---------------------------------------------------------------------------
__fastcall TBuscarProductoF::TBuscarProductoF(TComponent* Owner)
        : TPlantilla(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TBuscarProductoF::FormClose(TObject *Sender,
      TCloseAction &Action)
{
        //Habilitar de nuevo el formulario
        AbonadoAnualF->Enabled = true;
        //Cerrar la consulta
        BuscarProducto->Close();

        Action = caFree;
        BuscarProductoF = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TBuscarProductoF::CerrarClick(TObject *Sender)
{
     Close();
}
//---------------------------------------------------------------------------
void __fastcall TBuscarProductoF::FormCreate(TObject *Sender)
{
        BuscarProducto->Open();
}
//---------------------------------------------------------------------------

void __fastcall TBuscarProductoF::BusquedaBClick(TObject *Sender)
{
     BuscarProducto->Close();
     BuscarProducto->SQL->Clear();
     BuscarProducto->SQL->Add("SELECT CODART,NOMCOMERCIAL,N,P,K,CA,");
     BuscarProducto->SQL->Add("MG,FE,MICROELEMENTOS,AHUMICO,CSALINO");
     BuscarProducto->SQL->Add("FROM PRODUCTOS");
     BuscarProducto->SQL->Add("WHERE 1=1");
     if(NFlag->Checked)
        BuscarProducto->SQL->Add("AND N <> 0");
     if(PFlag->Checked)
        BuscarProducto->SQL->Add("AND P <> 0");
     if(KFlag->Checked)
        BuscarProducto->SQL->Add("AND K <> 0");
     if(CaFlag->Checked)
        BuscarProducto->SQL->Add("AND CA <> 0");
     if(MgFlag->Checked)
        BuscarProducto->SQL->Add("AND MG <> 0");
     if(FeFlag->Checked)
        BuscarProducto->SQL->Add("AND FE <> 0");
     if(MicroFlag->Checked)
        BuscarProducto->SQL->Add("AND MICROELEMENTOS <> 0");
     if(AHumicoFlag->Checked)
        BuscarProducto->SQL->Add("AND AHUMICO <> 0");
     if(CSalinoFlag->Checked)
        BuscarProducto->SQL->Add("AND CSALINO <> 0");
     BuscarProducto->SQL->Add("ORDER BY NOMCOMERCIAL");
     BuscarProducto->Open();

}
//---------------------------------------------------------------------------

void __fastcall TBuscarProductoF::ResultadoDblClick(TObject *Sender)
{
     AbonadoAnualF->anyadirProducto(BuscarProducto->FieldValues["CODART"]);
}
//---------------------------------------------------------------------------

void __fastcall TBuscarProductoF::ResultadoKeyPress(TObject *Sender,
      char &Key)
{
     if (Key == VK_RETURN)
        AbonadoAnualF->anyadirProducto(BuscarProducto->FieldValues["CODART"]);
}
//---------------------------------------------------------------------------

