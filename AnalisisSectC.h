//---------------------------------------------------------------------------

#ifndef AnalisisSectCH
#define AnalisisSectCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "PlantillaC.h"
#include <Buttons.hpp>
#include <DBCtrls.hpp>
#include <ComCtrls.hpp>
#include <Mask.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
//---------------------------------------------------------------------------
class TAnalisisSectF : public TPlantilla
{
__published:	// IDE-managed Components
        TGroupBox *DatoSector;
        TLabel *Label2;
        TLabel *Finca;
        TLabel *Label3;
        TDateTimePicker *Fecha;
        TLabel *Label4;
        TGroupBox *GroupBox1;
        TDBEdit *FechaBD;
        TLabel *NomCli;
        TLabel *NomFinca;
        TLabel *Sector;
        TDBEdit *N;
        TDBEdit *K;
        TDBEdit *P;
        TDBEdit *Ca;
        TDBEdit *Mg;
        TDBEdit *Fe;
        TDBEdit *Micro;
        TDBEdit *AH;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label10;
        TLabel *Label11;
        TLabel *Label12;
        TDBGrid *ListaIzq;
        TButton *Cancelar;
        TEdit *CodFinca;
        TLabel *Label13;
        TLabel *Label14;
        TLabel *Label15;
        TLabel *Label16;
        TLabel *Label17;
        TLabel *Label18;
        TLabel *Label19;
        TLabel *Label20;
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall FechaChange(TObject *Sender);
        void __fastcall AnyadirClick(TObject *Sender);
        void __fastcall GuardarClick(TObject *Sender);
        void __fastcall CancelarClick(TObject *Sender);
        void __fastcall EliminarClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TAnalisisSectF(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TAnalisisSectF *AnalisisSectF;
//---------------------------------------------------------------------------
#endif
