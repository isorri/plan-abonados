object DatosF: TDatosF
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 867
  Top = 132
  Height = 643
  Width = 384
  object ClientesSource: TDataSource
    DataSet = Clientes
    Left = 85
    Top = 48
  end
  object FincasSource: TDataSource
    DataSet = Fincas
    Left = 85
    Top = 104
  end
  object SectorSource: TDataSource
    DataSet = Sectores
    Left = 237
    Top = 104
  end
  object ComercialesSource: TDataSource
    DataSet = Comerciales
    Left = 237
    Top = 48
  end
  object Abono: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;Data Source=Adoba' +
      'ts'
    ConnectionTimeout = 0
    LoginPrompt = False
    Mode = cmReadWrite
    Left = 24
  end
  object Clientes: TADOTable
    Active = True
    Connection = Abono
    CursorType = ctStatic
    AfterInsert = ClientesBloquea
    AfterEdit = ClientesBloquea
    AfterPost = ClientesDesbloquea
    AfterCancel = ClientesDesbloquea
    TableName = 'CLIENTES'
    Left = 24
    Top = 48
    object ClientesCODCLI: TWideStringField
      FieldName = 'CODCLI'
      Size = 10
    end
    object ClientesCODCOMERCIAL: TIntegerField
      FieldName = 'CODCOMERCIAL'
    end
    object ClientesNOMBRE: TWideStringField
      FieldName = 'NOMBRE'
      Size = 50
    end
    object ClientesTIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 3
    end
  end
  object Comerciales: TADOTable
    Connection = Abono
    AfterInsert = ComercialesBloquea
    AfterEdit = ComercialesBloquea
    AfterPost = ComercialesDesbloquea
    AfterCancel = ComercialesDesbloquea
    TableName = 'COMERCIALES'
    Left = 160
    Top = 48
    object ComercialesCODCOMERCIAL: TAutoIncField
      FieldName = 'CODCOMERCIAL'
      ReadOnly = True
    end
    object ComercialesNOMBRE: TWideStringField
      FieldName = 'NOMBRE'
      Size = 50
    end
  end
  object Fincas: TADOTable
    Active = True
    Connection = Abono
    CursorType = ctStatic
    AfterInsert = FincasBloquea
    AfterEdit = FincasBloquea
    AfterPost = FincasDesbloquea
    AfterCancel = FincasDesbloquea
    OnCalcFields = FincasCalcFields
    TableName = 'FINCAS'
    Left = 24
    Top = 104
    object FincasCODFINCA: TAutoIncField
      AutoGenerateValue = arAutoInc
      FieldName = 'CODFINCA'
      ReadOnly = True
    end
    object FincasNOMFINCA: TWideStringField
      FieldName = 'NOMFINCA'
      Size = 50
    end
    object FincasCAPACIDADCUBAS: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'CAPACIDADCUBAS'
    end
    object FincasNUMCUBAS: TSmallintField
      FieldKind = fkCalculated
      FieldName = 'NUMCUBAS'
      Required = True
      Calculated = True
    end
    object FincasCAUDALCUBAS: TFloatField
      FieldName = 'CAUDALCUBAS'
      Required = True
    end
    object FincasCAPCUBANOD: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CAPCUBANOD'
      Calculated = True
    end
    object FincasORIGENAGUA: TWideStringField
      FieldName = 'ORIGENAGUA'
      Size = 25
    end
    object FincasN: TFloatField
      FieldName = 'N'
    end
    object FincasP: TFloatField
      FieldName = 'P'
    end
    object FincasK: TFloatField
      FieldName = 'K'
    end
    object FincasCa: TFloatField
      FieldName = 'Ca'
    end
    object FincasMg: TFloatField
      FieldName = 'Mg'
    end
    object FincasFe: TFloatField
      FieldName = 'Fe'
    end
    object FincasOBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object FincasORIELECTRICIDAD: TWideStringField
      FieldName = 'ORIELECTRICIDAD'
      Size = 50
    end
    object FincasCODCLI: TWideStringField
      FieldName = 'CODCLI'
      Size = 10
    end
    object FincasCAPCUBA_1: TFloatField
      FieldName = 'CAPCUBA_1'
    end
    object FincasCAPCUBA_2: TFloatField
      FieldName = 'CAPCUBA_2'
    end
    object FincasCAPCUBA_3: TFloatField
      FieldName = 'CAPCUBA_3'
    end
    object FincasCAPCUBA_4: TFloatField
      FieldName = 'CAPCUBA_4'
    end
    object FincasCAPCUBA_5: TFloatField
      FieldName = 'CAPCUBA_5'
    end
    object FincasCAPCUBANOD_1: TFloatField
      FieldName = 'CAPCUBANOD_1'
    end
    object FincasCAPCUBANOD_2: TFloatField
      FieldName = 'CAPCUBANOD_2'
    end
    object FincasCAPCUBANOD_3: TFloatField
      FieldName = 'CAPCUBANOD_3'
    end
    object FincasCAPCUBANOD_4: TFloatField
      FieldName = 'CAPCUBANOD_4'
    end
    object FincasCAPCUBANOD_5: TFloatField
      FieldName = 'CAPCUBANOD_5'
    end
    object FincasNUMCUBASNOD: TFloatField
      FieldKind = fkCalculated
      FieldName = 'NUMCUBASNOD'
      Calculated = True
    end
    object FincasNOMCLI: TStringField
      FieldKind = fkLookup
      FieldName = 'NOMCLI'
      LookupDataSet = Clientes
      LookupKeyFields = 'CODCLI'
      LookupResultField = 'NOMBRE'
      KeyFields = 'CODCLI'
      Size = 50
      Lookup = True
    end
    object FincasMICROELEMENTOS: TFloatField
      FieldName = 'MICROELEMENTOS'
    end
    object FincasAHUMICO: TFloatField
      FieldName = 'AHUMICO'
    end
  end
  object Sectores: TADOTable
    Connection = Abono
    AfterInsert = SectoresBloquea
    AfterEdit = SectoresBloquea
    AfterPost = SectoresDesbloquea
    AfterCancel = SectoresDesbloquea
    OnNewRecord = SectoresNewRecord
    IndexFieldNames = 'CODFINCA'
    MasterFields = 'CODFINCA'
    MasterSource = FincasSource
    TableName = 'SECTORES'
    Left = 160
    Top = 104
    object SectoresCODFINCA: TIntegerField
      FieldName = 'CODFINCA'
    end
    object SectoresSECTOR: TWideStringField
      FieldName = 'SECTOR'
    end
    object SectoresARBOLNUM: TSmallintField
      FieldName = 'ARBOLNUM'
    end
    object SectoresVARIEDAD: TWideStringField
      FieldName = 'VARIEDAD'
      Size = 50
    end
    object SectoresPATRON: TWideStringField
      FieldName = 'PATRON'
      Size = 50
    end
    object SectoresDSTLINEA: TFloatField
      FieldName = 'DSTLINEA'
    end
    object SectoresDSTARBOL: TFloatField
      FieldName = 'DSTARBOL'
    end
    object SectoresHG: TFloatField
      FieldName = 'HG'
    end
    object SectoresEdad: TWideStringField
      FieldName = 'Edad'
      Size = 50
    end
    object SectoresCAUDALGOT: TFloatField
      FieldName = 'CAUDALGOT'
    end
    object SectoresPLANTONADA: TBooleanField
      FieldName = 'PLANTONADA'
    end
    object SectoresGOTEOXARB: TFloatField
      FieldName = 'GOTEOXARB'
    end
    object SectoresTRIEGO: TSmallintField
      FieldName = 'TRIEGO'
    end
  end
  object AnalisisSector: TADOTable
    Connection = Abono
    TableName = 'ANALISIS'
    Left = 24
    Top = 160
    object AnalisisSectorSECTOR: TWideStringField
      FieldName = 'SECTOR'
    end
    object AnalisisSectorFECHA: TDateTimeField
      FieldName = 'FECHA'
    end
    object AnalisisSectorN: TFloatField
      FieldName = 'N'
    end
    object AnalisisSectorK: TFloatField
      FieldName = 'K'
    end
    object AnalisisSectorP: TFloatField
      FieldName = 'P'
    end
    object AnalisisSectorCa: TFloatField
      FieldName = 'Ca'
    end
    object AnalisisSectorMg: TFloatField
      FieldName = 'Mg'
    end
    object AnalisisSectorFe: TFloatField
      FieldName = 'Fe'
    end
    object AnalisisSectorCODFINCA: TIntegerField
      FieldName = 'CODFINCA'
    end
    object AnalisisSectorAHUMICO: TFloatField
      FieldName = 'AHUMICO'
    end
    object AnalisisSectorMICROELEMENTOS: TFloatField
      FieldName = 'MICROELEMENTOS'
    end
  end
  object AnalisisSectorSource: TDataSource
    DataSet = AnalisisSector
    Left = 120
    Top = 160
  end
  object AnalisisFinca: TADOTable
    Connection = Abono
    TableName = 'ANALISIS_FINCA'
    Left = 208
    Top = 160
    object AnalisisFincaCODFINCA: TIntegerField
      FieldName = 'CODFINCA'
    end
    object AnalisisFincaFECHA: TDateTimeField
      FieldName = 'FECHA'
    end
    object AnalisisFincaN: TFloatField
      FieldName = 'N'
    end
    object AnalisisFincaP: TFloatField
      FieldName = 'P'
    end
    object AnalisisFincaK: TFloatField
      FieldName = 'K'
    end
    object AnalisisFincaCA: TFloatField
      FieldName = 'CA'
    end
    object AnalisisFincaMG: TFloatField
      FieldName = 'MG'
    end
    object AnalisisFincaFE: TFloatField
      FieldName = 'FE'
    end
    object AnalisisFincaMICROELEMENTOS: TFloatField
      FieldName = 'MICROELEMENTOS'
    end
    object AnalisisFincaAHUMICO: TFloatField
      FieldName = 'AHUMICO'
    end
    object AnalisisFincaFinca: TStringField
      FieldKind = fkLookup
      FieldName = 'Finca'
      LookupDataSet = Fincas
      LookupKeyFields = 'CODFINCA'
      LookupResultField = 'NOMFINCA'
      KeyFields = 'CODFINCA'
      Size = 50
      Lookup = True
    end
  end
  object AnalisisFincaSource: TDataSource
    DataSet = AnalisisFinca
    Left = 304
    Top = 160
  end
  object Productos: TADOTable
    Active = True
    Connection = Abono
    CursorType = ctStatic
    AfterInsert = ProductosBloquea
    AfterEdit = ProductosBloquea
    AfterPost = ProductosDesbloquea
    AfterCancel = ProductosDesbloquea
    OnCalcFields = ProductosCalcFields
    OnNewRecord = ProductosNewRecord
    TableName = 'PRODUCTOS'
    Left = 32
    Top = 296
    object ProductosCODART: TAutoIncField
      FieldName = 'CODART'
      ReadOnly = True
    end
    object ProductosNOMCOMERCIAL: TWideStringField
      FieldName = 'NOMCOMERCIAL'
      Size = 50
    end
    object ProductosABONO: TBooleanField
      FieldName = 'ABONO'
    end
    object ProductosDENSIDAD: TFloatField
      FieldName = 'DENSIDAD'
    end
    object ProductosN: TFloatField
      FieldName = 'N'
    end
    object ProductosP: TFloatField
      FieldName = 'P'
    end
    object ProductosK: TFloatField
      FieldName = 'K'
    end
    object ProductosCA: TFloatField
      FieldName = 'CA'
    end
    object ProductosMG: TFloatField
      FieldName = 'MG'
    end
    object ProductosFE: TFloatField
      FieldName = 'FE'
    end
    object ProductosMICROELEMENTOS: TFloatField
      FieldName = 'MICROELEMENTOS'
    end
    object ProductosAHUMICO: TFloatField
      FieldName = 'AHUMICO'
    end
    object ProductosCSALINO: TFloatField
      FieldName = 'CSALINO'
    end
    object ProductosX3CODE: TWideStringField
      FieldName = 'X3CODE'
    end
    object ProductosPVT: TFloatField
      FieldName = 'PVT'
    end
    object ProductosCOP: TFloatField
      FieldName = 'COP'
    end
    object ProductosGRF: TFloatField
      FieldName = 'GRF'
    end
    object ProductosPVP: TFloatField
      FieldName = 'PVP'
    end
    object ProductosESTADO: TWideStringField
      FieldName = 'ESTADO'
    end
    object ProductosDISOLUCIN: TFloatField
      FieldName = 'DISOLUCI'#211'N'
    end
    object ProductosFIMPORTACION: TDateTimeField
      FieldName = 'FIMPORTACION'
    end
    object ProductosDSDesigner01_ENERO: TFloatField
      FieldName = '01_ENERO'
    end
    object ProductosDSDesigner02_FEBRERO: TFloatField
      FieldName = '02_FEBRERO'
    end
    object ProductosDSDesigner03_MARZO: TFloatField
      FieldName = '03_MARZO'
    end
    object ProductosDSDesigner04_ABRIL: TFloatField
      FieldName = '04_ABRIL'
    end
    object ProductosDSDesigner05_MAYO: TFloatField
      FieldName = '05_MAYO'
    end
    object ProductosDSDesigner06_JUNIO: TFloatField
      FieldName = '06_JUNIO'
    end
    object ProductosDSDesigner07_JULIO: TFloatField
      FieldName = '07_JULIO'
    end
    object ProductosDSDesigner08_AGOSTO: TFloatField
      FieldName = '08_AGOSTO'
    end
    object ProductosDSDesigner09_SEPTIEMBRE: TFloatField
      FieldName = '09_SEPTIEMBRE'
    end
    object ProductosDSDesigner10_OCTUBRE: TFloatField
      FieldName = '10_OCTUBRE'
    end
    object ProductosDSDesigner11_NOVIEMBRE: TFloatField
      FieldName = '11_NOVIEMBRE'
    end
    object ProductosDSDesigner12_DICIEMBRE: TFloatField
      FieldName = '12_DICIEMBRE'
    end
    object ProductosTOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL'
      Calculated = True
    end
  end
  object ProductosSource: TDataSource
    DataSet = Productos
    Left = 112
    Top = 296
  end
  object Coeficientes: TADOTable
    Connection = Abono
    AfterInsert = CoeficientesBloquea
    AfterEdit = CoeficientesBloquea
    AfterPost = CoeficientesDesbloquea
    AfterCancel = CoeficientesDesbloquea
    TableName = 'COEFICIENTES'
    Left = 200
    Top = 296
    object CoeficientesPATRON: TWideStringField
      FieldName = 'PATRON'
      Size = 50
    end
    object CoeficientesN_C1: TFloatField
      FieldName = 'N_C1'
    end
    object CoeficientesN_C2: TFloatField
      FieldName = 'N_C2'
    end
    object CoeficientesP_C1: TFloatField
      FieldName = 'P_C1'
    end
    object CoeficientesP_C2: TFloatField
      FieldName = 'P_C2'
    end
    object CoeficientesK_C1: TFloatField
      FieldName = 'K_C1'
    end
    object CoeficientesK_C2: TFloatField
      FieldName = 'K_C2'
    end
    object CoeficientesCA_C1: TFloatField
      FieldName = 'CA_C1'
    end
    object CoeficientesCA_C2: TFloatField
      FieldName = 'CA_C2'
    end
    object CoeficientesMG_C1: TFloatField
      FieldName = 'MG_C1'
    end
    object CoeficientesMG_C2: TFloatField
      FieldName = 'MG_C2'
    end
    object CoeficientesFE_C1: TFloatField
      FieldName = 'FE_C1'
    end
    object CoeficientesFE_C2: TFloatField
      FieldName = 'FE_C2'
    end
    object CoeficientesMICRO_C1: TFloatField
      FieldName = 'MICRO_C1'
    end
    object CoeficientesMICRO_C2: TFloatField
      FieldName = 'MICRO_C2'
    end
    object CoeficientesAH_C1: TFloatField
      FieldName = 'AH_C1'
    end
    object CoeficientesAH_C2: TFloatField
      FieldName = 'AH_C2'
    end
  end
  object CoeficientesSource: TDataSource
    DataSet = Coeficientes
    Left = 280
    Top = 296
  end
  object Anual: TADOTable
    Connection = Abono
    AfterEdit = AnualAfterEdit
    BeforePost = AnualBeforePost
    BeforeCancel = AnualBeforeCancel
    TableName = 'ANUAL'
    Left = 32
    Top = 368
    object AnualCODANUAL: TWideStringField
      FieldName = 'CODANUAL'
      Size = 15
    end
    object AnualCODFINCA: TIntegerField
      FieldName = 'CODFINCA'
    end
    object AnualSECTOR: TWideStringField
      FieldName = 'SECTOR'
    end
    object AnualANYO: TSmallintField
      FieldName = 'ANYO'
    end
    object AnualBASE: TFloatField
      FieldName = 'BASE'
    end
    object AnualN: TFloatField
      FieldName = 'N'
    end
    object AnualP: TFloatField
      FieldName = 'P'
    end
    object AnualK: TFloatField
      FieldName = 'K'
    end
    object AnualCA: TFloatField
      FieldName = 'CA'
    end
    object AnualMG: TFloatField
      FieldName = 'MG'
    end
    object AnualFE: TFloatField
      FieldName = 'FE'
    end
    object AnualMICROELEMENTOS: TFloatField
      FieldName = 'MICROELEMENTOS'
    end
    object AnualAHUMICO: TFloatField
      FieldName = 'AHUMICO'
    end
    object AnualCSALINO: TFloatField
      FieldName = 'CSALINO'
    end
    object AnualOBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object AnualNOMFINCA: TWideStringField
      FieldName = 'NOMFINCA'
      Size = 50
    end
    object AnualFANALISIS: TDateTimeField
      FieldName = 'FANALISIS'
    end
    object AnualIMPRESO: TBooleanField
      FieldName = 'IMPRESO'
    end
    object AnualCTD_P: TFloatField
      FieldName = 'CTD_P'
    end
    object AnualCODIGO_AF: TIntegerField
      FieldName = 'CODIGO_AF'
    end
    object AnualNOMBRE_AF: TStringField
      FieldKind = fkLookup
      FieldName = 'NOMBRE_AF'
      LookupDataSet = Productos
      LookupKeyFields = 'CODART'
      LookupResultField = 'NOMCOMERCIAL'
      KeyFields = 'CODIGO_AF'
      Size = 50
      Lookup = True
    end
    object AnualANOTACIONES: TMemoField
      FieldName = 'ANOTACIONES'
      BlobType = ftMemo
    end
    object AnualCODCLI: TIntegerField
      FieldKind = fkLookup
      FieldName = 'CODCLI'
      LookupDataSet = Fincas
      LookupKeyFields = 'CODFINCA'
      LookupResultField = 'CODCLI'
      KeyFields = 'CODFINCA'
      Lookup = True
    end
    object AnualNOMCLI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMCLI'
      LookupDataSet = Clientes
      LookupKeyFields = 'CODCLI'
      LookupResultField = 'NOMBRE'
      KeyFields = 'CODCLI'
      Size = 50
      Lookup = True
    end
  end
  object AnualSource: TDataSource
    DataSet = Anual
    Left = 112
    Top = 368
  end
  object DetAnual: TADOTable
    Connection = Abono
    OnCalcFields = DetAnualCalcFields
    IndexFieldNames = 'CODANUAL'
    MasterFields = 'CODANUAL'
    MasterSource = AnualSource
    TableName = 'DETANUAL'
    Left = 200
    Top = 368
    object DetAnualCODANUAL: TWideStringField
      FieldName = 'CODANUAL'
      Size = 15
    end
    object DetAnualCODART: TIntegerField
      FieldName = 'CODART'
    end
    object DetAnualNLINEA: TIntegerField
      FieldName = 'NLINEA'
    end
    object DetAnualCANTIDAD: TFloatField
      FieldName = 'CANTIDAD'
    end
    object DetAnualNOMCOMERCIAL: TWideStringField
      FieldName = 'NOMCOMERCIAL'
      Size = 50
    end
    object DetAnualDSDesigner01_ENERO: TFloatField
      FieldName = '01_ENERO'
    end
    object DetAnualDSDesigner02_FEBRERO: TFloatField
      FieldName = '02_FEBRERO'
    end
    object DetAnualDSDesigner03_MARZO: TFloatField
      FieldName = '03_MARZO'
    end
    object DetAnualDSDesigner04_ABRIL: TFloatField
      FieldName = '04_ABRIL'
    end
    object DetAnualDSDesigner05_MAYO: TFloatField
      FieldName = '05_MAYO'
    end
    object DetAnualDSDesigner06_JUNIO: TFloatField
      FieldName = '06_JUNIO'
    end
    object DetAnualDSDesigner07_JULIO: TFloatField
      FieldName = '07_JULIO'
    end
    object DetAnualDSDesigner08_AGOSTO: TFloatField
      FieldName = '08_AGOSTO'
    end
    object DetAnualDSDesigner09_SEPTIEMBRE: TFloatField
      FieldName = '09_SEPTIEMBRE'
    end
    object DetAnualDSDesigner10_OCTUBRE: TFloatField
      FieldName = '10_OCTUBRE'
    end
    object DetAnualDSDesigner11_NOVIEMBRE: TFloatField
      FieldName = '11_NOVIEMBRE'
    end
    object DetAnualDSDesigner12_DICIEMBRE: TFloatField
      FieldName = '12_DICIEMBRE'
    end
    object DetAnualTOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL'
      Calculated = True
    end
    object CTD01: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CTD01'
      Calculated = True
    end
    object CTD02: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CTD02'
      Calculated = True
    end
    object CTD03: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CTD03'
      Calculated = True
    end
    object CTD04: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CTD04'
      Calculated = True
    end
    object CTD05: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CTD05'
      Calculated = True
    end
    object CTD06: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CTD06'
      Calculated = True
    end
    object CTD07: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CTD07'
      Calculated = True
    end
    object CTD08: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CTD08'
      Calculated = True
    end
    object CTD11: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CTD11'
      Calculated = True
    end
    object CTD09: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CTD09'
      Calculated = True
    end
    object CTD10: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CTD10'
      Calculated = True
    end
    object CTD12: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CTD12'
      Calculated = True
    end
  end
  object DetAnualSource: TDataSource
    DataSet = DetAnual
    Left = 280
    Top = 368
  end
  object Reparto: TADOTable
    Connection = Abono
    AfterInsert = RepartoBloquea
    AfterEdit = RepartoBloquea
    AfterPost = RepartoDesbloquea
    AfterCancel = RepartoDesbloquea
    OnCalcFields = RepartoCalcFields
    OnNewRecord = RepartoNewRecord
    TableName = 'REPARTO'
    Left = 32
    Top = 440
    object RepartoCODIGO: TWideStringField
      FieldName = 'CODIGO'
    end
    object RepartoDSDesigner01_ENERO: TFloatField
      FieldName = '01_ENERO'
    end
    object RepartoDSDesigner02_FEBRERO: TFloatField
      FieldName = '02_FEBRERO'
    end
    object RepartoDSDesigner03_MARZO: TFloatField
      FieldName = '03_MARZO'
    end
    object RepartoDSDesigner04_ABRIL: TFloatField
      FieldName = '04_ABRIL'
    end
    object RepartoDSDesigner05_MAYO: TFloatField
      FieldName = '05_MAYO'
    end
    object RepartoDSDesigner06_JUNIO: TFloatField
      FieldName = '06_JUNIO'
    end
    object RepartoDSDesigner07_JULIO: TFloatField
      FieldName = '07_JULIO'
    end
    object RepartoDSDesigner08_AGOSTO: TFloatField
      FieldName = '08_AGOSTO'
    end
    object RepartoDSDesigner09_SEPTIEMBRE: TFloatField
      FieldName = '09_SEPTIEMBRE'
    end
    object RepartoDSDesigner10_OCTUBRE: TFloatField
      FieldName = '10_OCTUBRE'
    end
    object RepartoDSDesigner11_NOVIEMBRE: TFloatField
      FieldName = '11_NOVIEMBRE'
    end
    object RepartoDSDesigner12_DICIEMBRE: TFloatField
      FieldName = '12_DICIEMBRE'
    end
    object TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL'
      Calculated = True
    end
  end
  object RepartoSource: TDataSource
    DataSet = Reparto
    Left = 112
    Top = 440
  end
  object Consulta: TADOQuery
    Connection = Abono
    Parameters = <>
    Prepared = True
    Left = 280
    Top = 440
  end
  object DiasRiego: TADOTable
    Connection = Abono
    AfterInsert = DiasRiegoBloquea
    AfterEdit = DiasRiegoBloquea
    AfterPost = DiasRiegoDesbloquea
    AfterCancel = DiasRiegoDesbloquea
    TableName = 'DIASRIEGO'
    Left = 208
    Top = 224
    object DiasRiegoMES: TSmallintField
      FieldName = 'MES'
    end
    object DiasRiegoLUNES: TBooleanField
      FieldName = 'LUNES'
    end
    object DiasRiegoMARTES: TBooleanField
      FieldName = 'MARTES'
    end
    object DiasRiegoMIERCOLES: TBooleanField
      FieldName = 'MIERCOLES'
    end
    object DiasRiegoJUEVES: TBooleanField
      FieldName = 'JUEVES'
    end
    object DiasRiegoVIERNES: TBooleanField
      FieldName = 'VIERNES'
    end
    object DiasRiegoSABADO: TBooleanField
      FieldName = 'SABADO'
    end
    object DiasRiegoDOMINGO: TBooleanField
      FieldName = 'DOMINGO'
    end
  end
  object DiasRiegoSource: TDataSource
    DataSet = DiasRiego
    Left = 304
    Top = 224
  end
  object RiegoPlant: TADOTable
    Connection = Abono
    AfterInsert = RiegoPlantBloquea
    AfterEdit = RiegoPlantBloquea
    AfterPost = RiegoPlantDesbloquea
    AfterCancel = RiegoPlantDesbloquea
    TableName = 'DIASRIEGOPLANT'
    Left = 24
    Top = 224
    object RiegoPlantMES: TSmallintField
      FieldName = 'MES'
    end
    object RiegoPlantLUNES: TBooleanField
      FieldName = 'LUNES'
    end
    object RiegoPlantMARTES: TBooleanField
      FieldName = 'MARTES'
    end
    object RiegoPlantMIERCOLES: TBooleanField
      FieldName = 'MIERCOLES'
    end
    object RiegoPlantJUEVES: TBooleanField
      FieldName = 'JUEVES'
    end
    object RiegoPlantVIERNES: TBooleanField
      FieldName = 'VIERNES'
    end
    object RiegoPlantSABADO: TBooleanField
      FieldName = 'SABADO'
    end
    object RiegoPlantDOMINGO: TBooleanField
      FieldName = 'DOMINGO'
    end
  end
  object RiegoPlantSource: TDataSource
    DataSet = RiegoPlant
    Left = 120
    Top = 224
  end
  object Mensual: TADOTable
    Connection = Abono
    AfterEdit = MensualAfterEdit
    AfterPost = MensualAfterPost
    AfterCancel = MensualAfterCancel
    TableName = 'MENSUAL'
    Left = 32
    Top = 512
    object MensualCODMENSUAL: TWideStringField
      FieldName = 'CODMENSUAL'
      Size = 15
    end
    object MensualCODFINCA: TIntegerField
      FieldName = 'CODFINCA'
    end
    object MensualANYO: TSmallintField
      FieldName = 'ANYO'
    end
    object MensualMES: TSmallintField
      FieldName = 'MES'
    end
    object MensualQUINCENA: TSmallintField
      FieldName = 'QUINCENA'
    end
    object MensualOBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object MensualLLENADOS: TSmallintField
      FieldName = 'LLENADOS'
    end
    object MensualIMPRESO: TBooleanField
      FieldName = 'IMPRESO'
    end
    object MensualES_QUINCENA: TBooleanField
      FieldName = 'ES_QUINCENA'
    end
    object MensualDABONO: TSmallintField
      FieldName = 'DABONO'
    end
    object MensualDRIEGO: TSmallintField
      FieldName = 'DRIEGO'
    end
    object MensualAGUA: TFloatField
      FieldName = 'AGUA'
    end
    object MensualOBSERVA_AF: TMemoField
      FieldName = 'OBSERVA_AF'
      BlobType = ftMemo
    end
    object MensualANOTACIONES: TMemoField
      FieldName = 'ANOTACIONES'
      BlobType = ftMemo
    end
    object MensualCODCLI: TIntegerField
      FieldKind = fkLookup
      FieldName = 'CODCLI'
      LookupDataSet = Fincas
      LookupKeyFields = 'CODFINCA'
      LookupResultField = 'CODCLI'
      KeyFields = 'CODFINCA'
      Lookup = True
    end
    object MensualNOMCLI: TStringField
      FieldKind = fkLookup
      FieldName = 'NOMCLI'
      LookupDataSet = Clientes
      LookupKeyFields = 'CODCLI'
      LookupResultField = 'NOMBRE'
      KeyFields = 'CODCLI'
      Size = 50
      Lookup = True
    end
    object MensualNOMFINCA: TWideStringField
      FieldName = 'NOMFINCA'
      Size = 50
    end
  end
  object MensualSource: TDataSource
    DataSet = Mensual
    Left = 112
    Top = 512
  end
  object DetMensual: TADOTable
    Connection = Abono
    AfterEdit = DetMensualBloquea
    AfterPost = DetMensualDesbloquea
    AfterCancel = DetMensualDesbloquea
    IndexFieldNames = 'CODMENSUAL;SECTOR'
    MasterFields = 'CODMENSUAL;SECTOR'
    MasterSource = SectorMesSource
    TableName = 'DETMENSUAL'
    Left = 192
    Top = 560
    object DetMensualCODMENSUAL: TWideStringField
      FieldName = 'CODMENSUAL'
      Size = 15
    end
    object DetMensualSECTOR: TWideStringField
      FieldName = 'SECTOR'
    end
    object DetMensualNLINEA: TSmallintField
      FieldName = 'NLINEA'
    end
    object DetMensualCODART: TIntegerField
      FieldName = 'CODART'
    end
    object DetMensualCANTIDAD: TFloatField
      FieldName = 'CANTIDAD'
    end
    object DetMensualNOMCOMERCIAL: TWideStringField
      FieldName = 'NOMCOMERCIAL'
      Size = 50
    end
  end
  object DetMensualSource: TDataSource
    DataSet = DetMensual
    Left = 288
    Top = 552
  end
  object SectorMes: TADOTable
    Connection = Abono
    AfterEdit = SectorMesBloquea
    AfterPost = SectorMesDesbloquea
    AfterCancel = SectorMesDesbloquea
    IndexFieldNames = 'CODMENSUAL'
    MasterFields = 'CODMENSUAL'
    MasterSource = MensualSource
    TableName = 'SECTORMES'
    Left = 192
    Top = 496
    object SectorMesCODMENSUAL: TWideStringField
      FieldName = 'CODMENSUAL'
      Size = 15
    end
    object SectorMesSECTOR: TWideStringField
      FieldName = 'SECTOR'
    end
    object SectorMesTABONO: TSmallintField
      FieldName = 'TABONO'
    end
    object SectorMesTRIEGO: TSmallintField
      FieldName = 'TRIEGO'
    end
    object SectorMesLUNES: TBooleanField
      FieldName = 'LUNES'
    end
    object SectorMesMARTES: TBooleanField
      FieldName = 'MARTES'
    end
    object SectorMesMIERCOLES: TBooleanField
      FieldName = 'MIERCOLES'
    end
    object SectorMesJUEVES: TBooleanField
      FieldName = 'JUEVES'
    end
    object SectorMesVIERNES: TBooleanField
      FieldName = 'VIERNES'
    end
    object SectorMesSABADO: TBooleanField
      FieldName = 'SABADO'
    end
    object SectorMesDOMINGO: TBooleanField
      FieldName = 'DOMINGO'
    end
    object SectorMesBOMBA: TFloatField
      FieldName = 'BOMBA'
    end
    object SectorMesAGUA: TFloatField
      FieldName = 'AGUA'
    end
    object SectorMesCODART_AF: TIntegerField
      FieldName = 'CODART_AF'
    end
    object SectorMesTACIDOF: TSmallintField
      FieldName = 'TACIDOF'
    end
    object SectorMesBOMBA_AF: TFloatField
      FieldName = 'BOMBA_AF'
    end
    object SectorMesCTD_AF: TFloatField
      FieldName = 'CTD_AF'
    end
    object SectorMesNOMBRE_AF: TWideStringField
      FieldName = 'NOMBRE_AF'
      Size = 50
    end
    object SectorMesDACIDOF: TSmallintField
      FieldName = 'DACIDOF'
    end
    object SectorMesAGUA_AF: TFloatField
      FieldName = 'AGUA_AF'
    end
    object SectorMesLUNES_AF: TBooleanField
      FieldName = 'LUNES_AF'
    end
    object SectorMesMARTES_AF: TBooleanField
      FieldName = 'MARTES_AF'
    end
    object SectorMesMIERCOLES_AF: TBooleanField
      FieldName = 'MIERCOLES_AF'
    end
    object SectorMesJUEVES_AF: TBooleanField
      FieldName = 'JUEVES_AF'
    end
    object SectorMesVIERNES_AF: TBooleanField
      FieldName = 'VIERNES_AF'
    end
    object SectorMesSABADO_AF: TBooleanField
      FieldName = 'SABADO_AF'
    end
    object SectorMesDOMINGO_AF: TBooleanField
      FieldName = 'DOMINGO_AF'
    end
    object SectorMesTRIEGO_AF: TSmallintField
      FieldName = 'TRIEGO_AF'
    end
    object SectorMesCODANUAL: TWideStringField
      FieldName = 'CODANUAL'
      Size = 15
    end
  end
  object SectorMesSource: TDataSource
    DataSet = SectorMes
    Left = 288
    Top = 496
  end
  object Contador: TADOTable
    Connection = Abono
    TableName = 'CONTADOR'
    Left = 200
    Top = 440
    object ContadorCODIGO: TWideStringField
      FieldName = 'CODIGO'
      Size = 3
    end
    object ContadorANYO: TWideStringField
      FieldName = 'ANYO'
      Size = 2
    end
    object ContadorNSECUENCIAL: TIntegerField
      FieldName = 'NSECUENCIAL'
    end
  end
end
