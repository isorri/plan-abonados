//---------------------------------------------------------------------------

#include <vcl.h>
#include <time.h>
#include <math.h>
#pragma hdrstop

#include "MensualC.h"
#include "DatosC.h"
#include "MainC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PlantillaC"
#pragma resource "*.dfm"
TMensualF *MensualF;
//---------------------------------------------------------------------------
__fastcall TMensualF::TMensualF(TComponent* Owner)
        : TPlantilla(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TMensualF::CerrarClick(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TMensualF::FormClose(TObject *Sender, TCloseAction &Action)
{
  int res;
  if (DatosF->Mensual->State != dsBrowse)
  {
      res=Application->MessageBoxA("Registro con cambios no guardados.\n�Guardar cambios?","Pregunta",MB_ICONQUESTION+MB_YESNO);
      if (res == IDYES)
          DatosF->Mensual->Post();
      else
          DatosF->Mensual->Cancel();
  }
   Action = caFree;
   MensualF = NULL;
}
//---------------------------------------------------------------------------
/*           Bloquea el formulario en inserci�n y modificaci�n              */
void TMensualF::bloqueaFormulario(void)
{
    ListaIzq->Enabled      = false;
    Calcular->Enabled = false;
    Imprimir->Enabled = false;
    GuardarB->Enabled  = true;
    Cancelar->Enabled = true;
    EliminarB->Enabled = false;
    DuplicaB->Enabled = false;
}
//---------------------------------------------------------------------------
/*      Desbloquea el formulario actual en modo b�squeda                   */
void TMensualF::desbloqueaFormulario(void)
{
    ListaIzq->Enabled      = true;
    Calcular->Enabled = true;
    Imprimir->Enabled = true;
    GuardarB->Enabled  = false;
    Cancelar->Enabled = false;
    EliminarB->Enabled = true;
    DuplicaB->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TMensualF::ImprimirClick(TObject *Sender)
{
   //Imprime el plan de abonados actual
   if (DatosF->Mensual->IsEmpty())
   {
        Application->MessageBoxA("Ning�n plan mensual cargado.","Error",MB_ICONERROR);
        return;
   }
   try
   {
        //Empieza transacci�n.
        DatosF->Abono->BeginTrans();
        //Vaciar tabla de impresi�n
        DatosF->Consulta->SQL->Clear();
        DatosF->Consulta->SQL->Add("DELETE FROM IMPRESION;") ;
        DatosF->Consulta->ExecSQL();

        //Rellenar con el plan mensual actual.
        DatosF->Consulta->SQL->Clear();
        DatosF->Consulta->SQL->Add("INSERT INTO IMPRESION ") ;
        DatosF->Consulta->SQL->Add("(CODIGO) VALUES");
        DatosF->Consulta->SQL->Add("('"+ CodMensual->Text + "');");
        DatosF->Consulta->ExecSQL();

        //Marcar el plan abonado actual, si  no esta marcado..
        if ( !DatosF->Mensual->FieldValues["IMPRESO"])
        {
                DatosF->Mensual->Edit();
                DatosF->Mensual->FieldValues["IMPRESO"] = true;
                DatosF->Mensual->Post();
        }
        //Finaliza transacci�n.
        DatosF->Abono->CommitTrans();
   }
   catch(Exception &e)
   {
        DatosF->Abono->RollbackTrans();
        ShowMessage("No se puede lanzar impresi�n.\n" + e.Message);
        return;
   }
   //Lanzamos la impresi�n
   MainF->Report->WindowButtonBar->PrintSetupBtn = true;
   MainF->Report->ReportName = "Mensual.rpt";
   MainF->Report->WindowStyle->Title = "Plan Abonado Mensual";
   MainF->Report->Show();
   if (MainF->Report->LastErrorNumber)
      //Ha habido alg�n error
      ShowMessage(AnsiString(MainF->Report->LastErrorNumber) +" " + MainF->Report->LastErrorString);
   //Preparamos el informe para la pr�xima impresi�n.
   MainF->Report->ReportName = "";
}
//---------------------------------------------------------------------------

void __fastcall TMensualF::GuardarClick(TObject *Sender)
{
    //Guarda los cambios realizados.
    DatosF->Mensual->Post();
}
//---------------------------------------------------------------------------

void __fastcall TMensualF::CancelarClick(TObject *Sender)
{
    //Cancela las modificaciones introducidas
    DatosF->Mensual->Cancel();
}
//---------------------------------------------------------------------------
//Vuelve a calcular los par�metros de riego con los datos introducidos
void __fastcall TMensualF::CalcularClick(TObject *Sender)
{
  TADOTable * Detalle;
  TADOTable * Fincas;
  TADOTable * Productos;
  TADOTable * Mensual;
  TADOTable * SectorMes;
  AnsiString mensaje;

  Detalle = DatosF->DetMensual;
  Fincas = DatosF->Fincas;
  Productos = DatosF->Productos;
  Mensual = DatosF->Mensual;
  SectorMes = DatosF->SectorMes;

  if (Detalle->State != dsBrowse)
  {
     Application->MessageBoxA("Guarde primero los cambios de cantidades de producto","Error",MB_ICONERROR);
     return;
  }
  if (SectorMes->State != dsBrowse)
  {
     Application->MessageBoxA("Guarde primero los cambios realizados en el sector","Error",MB_ICONERROR);
     return;
  }

  //Cargar la finca asociada al plan anual
  if ( ! Fincas->Locate("CODFINCA", Mensual->FieldValues["CODFINCA"] , TLocateOptions()) )
    {
      Application->MessageBoxA("Error al cargar la finca " ,"Error",MB_ICONERROR);
      return;
    }
  try
    {
      float agua; //Agua base para el c�lculo de llenados.
      float minutos; //Minutos de abonado del sector
      float aguasector; //Agua necesaria para el abono del sector
      int llenados; //Llenados de la cuba de disolucion
      int ndias; //Dias de abonados
      int dias;  //Dias de riego
      float bomba; //Rendimiento de la bomba inyectora

      dias = DRiego->Text.ToInt();

      if (MainF->pruebas)
          ShowMessage("Dias de riego mensuales: " + AnsiString(dias));
      //Empieza la transacci�n
      DatosF->Abono->BeginTrans();
      Mensual->Edit();
      agua = 0.0;
      SectorMes->First();

      /*Para cada sector calcular los minutos de abonado y calcular la cantidad
        de agua necesaria para disolver el abono a aplicar*/
      while (!SectorMes->Eof)
      {
         aguasector = minutos = 0.0;
         SectorMes->Edit();
         Detalle->First();
         //--------  Calcular la cantidad de agua necesaria ---------------
         // Agua = sumatorio (ctd abono/ solubilidad * 100)
         while (!Detalle->Eof)
         {
	     //Cargar el producto
	     if ( ! Productos->Locate("CODART", Detalle->FieldValues["CODART"], TLocateOptions()) )
	        throw Exception("Error al cargar el producto " + Detalle->FieldValues["CODART"] );
             if (MainF->pruebas)
             {
                 mensaje  = AnsiString(Productos->FieldValues["NOMCOMERCIAL"]);
                 mensaje += "\nDisoluci�n: " + AnsiString(Productos->FieldValues["DISOLUCI�N"]);
                 ShowMessage(mensaje);
             }
             if (Productos->FieldValues["DISOLUCI�N"] != 0 && ! Productos->FieldValues["DISOLUCI�N"].IsNull())
             {
                aguasector += (float)Detalle->FieldValues["CANTIDAD"] * 100 /(float)Productos->FieldValues["DISOLUCI�N"];
	        agua += (float)Detalle->FieldValues["CANTIDAD"] * 100 /(float)Productos->FieldValues["DISOLUCI�N"];
             }
             //Siguiente art�culo
             Detalle->Next();
         }
         //En aguasector tenemos el agua necesaria para el sector.
         SectorMes->FieldValues["AGUA"] = (int)aguasector;
         SectorMes->Post();
         SectorMes->Next();
      }
      //En agua tenemos el agua necesaria para la finca
      Mensual->FieldValues["AGUA"] = (int) agua;

      //-----------------Calcular n�mero de llenados---------------------
      //En agua tenemos la cantidad de agua necesaria para disolver
      if (Fincas->FieldValues["CAPACIDADCUBAS"] != 0 && !Fincas->FieldValues["CAPACIDADCUBAS"].IsNull())
	{

	  llenados = ceil((float)agua/ (float) Fincas->FieldValues["CAPACIDADCUBAS"]);
          if (MainF->pruebas)
          {
            mensaje  = "Capacidad Cubas: " + AnsiString(Fincas->FieldValues["CAPACIDADCUBAS"]);
            mensaje += "\nAgua: " + AnsiString(agua);
            mensaje += "\nLlenados: " + AnsiString(llenados);
            ShowMessage(mensaje);
          }
          Mensual->FieldValues["LLENADOS"] = llenados;
	}
      else
	Mensual->FieldValues["LLENADOS"] = 0;
      //-----------------    D�as de abono    --------------------------
      /*Calcular los d�as necesarios para aplicar el abono, �ste debe ser
        m�ltiplo del n�mero de llenados e inferior al indicado en dias y diasp*/
      if (llenados)
          if ( dias % llenados )
             ndias = dias - (dias % llenados);
          else
             ndias = dias;
      else
          ndias = 0;
      if (MainF->pruebas)
         ShowMessage("Dias abonado: " + AnsiString(ndias));
      Mensual->FieldValues["DABONO"] = ndias;
      //Guardamos los datos asociados al mes
      DatosF->Mensual->Post();
      //-----------------Calcular el tiempo de abono--------------------
      // minutos = Agua a inyectar/( % bomba * caudal * dias) * 60
      //Agua a inyectar --> es el agua proporcional que necesita cada sector para vac�ar por completo la cuba
      //Agua a inyectar = capacidad total cubas * n� llenados * agua sector /agua total
      //Este c�lculo se realiza por sector
      SectorMes->First();
      while (!SectorMes->Eof)
      {
         SectorMes->Edit();
         aguasector = Fincas->FieldValues["CAPACIDADCUBAS"] * llenados * SectorMes->FieldValues["AGUA"];
         aguasector = aguasector / agua;
         bomba = SectorMes->FieldValues["BOMBA"];
         if (Fincas->FieldValues["CAUDALCUBAS"] != 0 && ndias  )
             minutos = ceil((float)( aguasector * 60) / (float) ( (bomba / 100) * Fincas->FieldValues["CAUDALCUBAS"] * ndias));
         else
	     minutos = 0;
         SectorMes->FieldValues["TABONO"] = (int) minutos;
         SectorMes->Post();
         SectorMes->Next();

      }
      //Ponemos el foco en el primer sector
      SectorMes->First();
      //Guardamos los datos introducidos
      DatosF->Abono->CommitTrans();
    }
  catch(Exception &e)
    {
      DatosF->Abono->RollbackTrans();
      ShowMessage("Error al recalcular los datos introducidos.\n" + e.Message);
    }
}
//---------------------------------------------------------------------------

void __fastcall TMensualF::AnyadirClick(TObject *Sender)
{
   TDataSetState Detalle;
   int maxlinea; // Numero de linea
   int nlinea;

   Detalle = DatosF->DetMensual->State;

   if (Detalle != dsBrowse)
      Application->MessageBoxA("Cantidades pendientes de inserci�n. Guarde o cancele la cantidad","Error",MB_ICONERROR);
   else
   {
      //Recuperar el n�mero de l�nea a utilizar
      DatosF->DetMensual->First();
      maxlinea = 0;
      while (!DatosF->DetMensual->Eof)
      {
        nlinea = DatosF->DetMensual->FieldValues["NLINEA"];
        if (nlinea > maxlinea)
           maxlinea = nlinea;
        DatosF->DetMensual->Next();
      }
      //Numero de linea a utilizar el siguiente al m�ximo
      nlinea = maxlinea + 1;

      DatosF->DetMensual->Insert();
      DatosF->DetMensual->FieldValues["CODMENSUAL"] = DatosF->Mensual->FieldValues["CODMENSUAL"];
      DatosF->DetMensual->FieldValues["SECTOR"]   = DatosF->SectorMes->FieldValues["SECTOR"];
      DatosF->DetMensual->FieldValues["NLINEA"]   = nlinea;
      DatosF->DetMensual->FieldValues["CODART"]   = Articulos->KeyValue;
      DatosF->DetMensual->FieldValues["NOMCOMERCIAL"] = Articulos->Text;
      DatosF->DetMensual->FieldValues["CANTIDAD"] = 0;

      DatosF->DetMensual->Post();
   }
}
//---------------------------------------------------------------------------

void __fastcall TMensualF::GuardarProdClick(TObject *Sender)
{
    TDataSetState Detalle;
    Detalle = DatosF->DetMensual->State;

    if (Detalle == dsEdit)
        DatosF->DetMensual->Post();
}
//---------------------------------------------------------------------------



void __fastcall TMensualF::CancelarProdClick(TObject *Sender)
{
    TDataSetState Detalle;
    Detalle = DatosF->DetMensual->State;

    if (Detalle == dsEdit)
        DatosF->DetMensual->Cancel();
}
//---------------------------------------------------------------------------

void __fastcall TMensualF::Filtro(TObject *Sender)
{
    AnsiString filtro;
    filtro = "";

    if (FiltroAnyo->Text != "")
        //Hay un a�o introducido, filtramos por a�o
        filtro = filtro + "ANYO = " + (AnsiString) FiltroAnyo->Text;
    if (FiltroMes->Text != "")
    {
        if (filtro != "")
            filtro = filtro + " AND "; //Para enlazar los dos criterios
        //Hay un mes introducido, filtramos por mes
        filtro = filtro + "MES = " + (AnsiString) FiltroMes->Text;
    }
    if (FiltroFinca->Text != "")
    {
        if (filtro != "")
            filtro = filtro + " AND "; //Para enlazar criterios
        //Filtrar por nombre de finca
        filtro = filtro + "NOMFINCA LIKE '%" + FiltroFinca->Text + "%'" ;
    }
    //Activamos el filtro si hay un a�o o un mes introducido
    DatosF->Mensual->Filter   = filtro;
    DatosF->Mensual->Filtered = filtro != "";
}
//---------------------------------------------------------------------------

void __fastcall TMensualF::FormCreate(TObject *Sender)
{
    // Variables locales
    time_t t;
    struct tm * petm;

    //Cargar el a�o actual en el filtro
    time(&t);
    petm = localtime(&t);
    FiltroAnyo->Text = (AnsiString) (petm->tm_year + 1900);

    //Filtrar por a�o.
    DatosF->Mensual->Filter = "ANYO = " + (AnsiString) FiltroAnyo->Text;
    DatosF->Mensual->Filtered = true;
}
//---------------------------------------------------------------------------


void __fastcall TMensualF::GuardaSectorClick(TObject *Sender)
{
    //Guarda los cambios realizados.
    DatosF->SectorMes->Post();
}
//---------------------------------------------------------------------------

void __fastcall TMensualF::CancelaSectorClick(TObject *Sender)
{
    DatosF->SectorMes->Cancel();
}
//---------------------------------------------------------------------------

void __fastcall TMensualF::EliminaSectorClick(TObject *Sender)
{
     //Elimina el sector
     int res;
     TADOTable * Mensual;
     Mensual = DatosF->SectorMes;
     res = Application->MessageBoxA("Una vez borrado el registro no se puede volver a crear.\n�Desea borrar el sector?.","Pregunta",MB_ICONQUESTION+MB_YESNO);
     if (res == IDYES)
     {
        try
        {

          DatosF->DetMensual->First();
          while(! DatosF->DetMensual->Eof)
          {
              //Al borrar situa el puntero en el siguiente registro
              DatosF->DetMensual->Delete();
          }
          Mensual->Delete();
        }
        catch(Exception &e)
        {
          ShowMessage("No se puede borrar el sector\n" + e.Message);
        }
     }
}
//---------------------------------------------------------------------------

void __fastcall TMensualF::CalcularAFClick(TObject *Sender)
{
  float agua; //Agua necesaria para aplicar el acido fosforico
  float minutos; //Minutos de abonado del sector
  int ndias; //Dias de abonados
  float bomba; //Rendimiento de la bomba inyectora
  TADOTable * Productos;
  TADOTable * SectorMes;
  TADOTable * Fincas;

  Productos = DatosF->Productos;
  SectorMes = DatosF->SectorMes;
  Fincas    = DatosF->Fincas;

  if (SectorMes->State != dsBrowse)
  {
     Application->MessageBoxA("Guarde primero los cambios realizados en el sector","Error",MB_ICONERROR);
     return;
  }

  try
    {
      //Cargar la finca asociada al plan anual
      if ( ! Fincas->Locate("CODFINCA", SectorMes->FieldValues["CODFINCA"] , TLocateOptions()) )
         throw Exception("Error al cargar la finca " + SectorMes->FieldValues["CODFINCA"]);
      //Cargar el producto asociado al acido fosforico.
      if ( ! Productos->Locate("CODART", SectorMes->FieldValues["CODART_AF"], TLocateOptions()) )
         throw Exception("Error al cargar el producto " + SectorMes->FieldValues["CODART"] );
      if (MainF->pruebas)
      {
          ShowMessage(AnsiString(Productos->FieldValues["NOMCOMERCIAL"]));
          ShowMessage("Disoluci�n: " + AnsiString(Productos->FieldValues["DISOLUCI�N"]));
      }
      if (Productos->FieldValues["DISOLUCI�N"] != 0 && ! Productos->FieldValues["DISOLUCI�N"].IsNull())
          agua = (float)SectorMes->FieldValues["CTD_AF"] * 100 /(float)Productos->FieldValues["DISOLUCI�N"];
      //Calcular los minutos de abonado
      SectorMes->Edit();
      bomba = SectorMes->FieldValues["BOMBA_AF"];//% rendimiento de la bomba inyectora
      ndias = SectorMes->FieldValues["DACIDOF"]; //Dias a aplicar el acido fosforico
      if (Fincas->FieldValues["CAUDALCUBAS"] != 0 && ndias  )
          minutos = (float)( agua * 60) / (float) ( (bomba / 100) * Fincas->FieldValues["CAUDALCUBAS"] * ndias);
      else
          minutos = 0;
      SectorMes->FieldValues["TACIDOF"] = (int) minutos;
      SectorMes->Post();
    }
  catch(Exception &e)
    {
      ShowMessage("Error al recalcular los datos asociados al acido fosforico.\n" + e.Message);
    }
}
//---------------------------------------------------------------------------
/*           Bloquea el formulario sectores en   modificaci�n              */
void TMensualF::bloqueaSectores(void)
{
    //Bloquear las listas para evitar que el usuario cambie de registro
    ListaIzq->Enabled      = false;
    ListaSectores->Enabled = false;
    ListaSectorAF->Enabled = false;
    ListaSectorProd->Enabled = false;
    //Preparar los botones
    Calcular->Enabled = false;
    CalcularAF->Enabled = false;
    Imprimir->Enabled = false;
    GuardaSector->Enabled  = true;
    CancelaSector->Enabled = true;
    EliminaSector->Enabled = false;
    GuardarAF->Enabled  = true;
    CancelarAF->Enabled = true;
    EliminarAF->Enabled = false;
}
//---------------------------------------------------------------------------
/*      Desbloquea el formulario sectores                                  */
void TMensualF::desbloqueaSectores(void)
{
    //Desbloqueamos las listas
    ListaIzq->Enabled      = true;
    ListaSectores->Enabled = true;
    ListaSectorAF->Enabled = true;
    ListaSectorProd->Enabled = true;
    //Preparamos los botones
    Calcular->Enabled = true;
    CalcularAF->Enabled = true;
    Imprimir->Enabled = true;
    GuardaSector->Enabled  = false;
    CancelaSector->Enabled = false;
    EliminaSector->Enabled = true;
    GuardarAF->Enabled  = false;
    CancelarAF->Enabled = false;
    EliminarAF->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TMensualF::EliminarAFClick(TObject *Sender)
{
   /*Elimina los datos asociados al AF*/
   DatosF->SectorMes->Edit();
   DatosF->SectorMesCODART_AF->Value = NULL;
   DatosF->SectorMesCTD_AF->Value = NULL;
   DatosF->SectorMesBOMBA_AF->Value = NULL;
   DatosF->SectorMesDACIDOF->Value = NULL;
   DatosF->SectorMesNOMBRE_AF->Value = "";
   DatosF->SectorMesTACIDOF->Value = NULL;
   DatosF->SectorMesLUNES_AF->Value = NULL;
   DatosF->SectorMesMARTES_AF->Value = NULL;
   DatosF->SectorMesMIERCOLES_AF->Value = NULL;
   DatosF->SectorMesJUEVES_AF->Value = NULL;
   DatosF->SectorMesVIERNES_AF->Value = NULL;
   DatosF->SectorMesSABADO_AF->Value = NULL;
   DatosF->SectorMesDOMINGO_AF->Value = NULL;
}
//---------------------------------------------------------------------------
/* Bloquea el formulario al editar la pesta�a de productos */
void TMensualF::bloqueaProductos(void)
{
    //Bloquear las listas para evitar que el usuario cambie de registro
    ListaIzq->Enabled      = false;
    ListaSectores->Enabled = false;
    ListaSectorAF->Enabled = false;
    ListaSectorProd->Enabled = false;
    //Preparar los botones
    Calcular->Enabled = false;
    CalcularAF->Enabled = false;
    Imprimir->Enabled = false;
    GuardarProd->Enabled = true;
    CancelarProd->Enabled = true;
    EliminarProd->Enabled = false;
}
//---------------------------------------------------------------------------
/*Desbloquea el formulario despues de guardar/cancelar cambios*/
void TMensualF::desbloqueaProductos(void)
{
    //Desbloqueamos las listas
    ListaIzq->Enabled      = true;
    ListaSectores->Enabled = true;
    ListaSectorAF->Enabled = true;
    ListaSectorProd->Enabled = true;
    //Preparamos los botones
    Calcular->Enabled = true;
    CalcularAF->Enabled = true;
    Imprimir->Enabled = true;
    GuardarProd->Enabled = false;
    CancelarProd->Enabled = false;
    EliminarProd->Enabled = true;
}
//----------------------------------------------------------------------------

void __fastcall TMensualF::EliminarProdClick(TObject *Sender)
{
     TDataSetState Detalle;
     int res;

     Detalle = DatosF->DetMensual->State;
     if (Detalle == dsBrowse)
     {
        //Preguntar si el usuario quiere borrar este producto de todos los sectores
        res = Application->MessageBoxA("�Desea borrar este producto de todos los sectores de la finca?","Pregunta",MB_ICONQUESTION+MB_YESNO);
        if (res == IDYES)
        {
            //Borrar el producto de todos los sectores
            int prod2del; //C�digo del producto a borrar de todos los sectores
            prod2del = (int)DatosF->DetMensual->FieldValues["CODART"];
            DatosF->SectorMes->First();
            while (!DatosF->SectorMes->Eof)
            {
                if (MainF->pruebas)
                    ShowMessage("Borrando sector " + DatosF->SectorMes->FieldValues["SECTOR"] + "...");
                DatosF->DetMensual->First();
                while (!DatosF->DetMensual->Eof)
                {
                    if (prod2del == DatosF->DetMensual->FieldValues["CODART"])
                        DatosF->DetMensual->Delete();
                    DatosF->DetMensual->Next();
                }
                DatosF->SectorMes->Next();
            }
        }
        else //Borrar el producto del sector activo
            DatosF->DetMensual->Delete();
        //Hay que refrescar los datos de detalles.
        DatosF->DetMensual->Close();
        DatosF->DetMensual->Open();
     }
     else
     {
        Application->MessageBoxA("Cantidades pendientes de inserci�n. Guarde o cancele la cantidad","Error",MB_ICONERROR);
     }

}
//---------------------------------------------------------------------------

void __fastcall TMensualF::EliminarBClick(TObject *Sender)
{
   int res;
     res = Application->MessageBoxA("�Esta seguro de borrar este plan mensual?","Pregunta",MB_ICONQUESTION+MB_YESNO);
     if (res == IDYES)
        DatosF->Mensual->Delete();
}
//---------------------------------------------------------------------------
//Duplica el plan mensual actual
void __fastcall TMensualF::DuplicaBClick(TObject *Sender)
{
   AnsiString Anyo,Codigo;
   int res;

   res = Application->MessageBoxA("�Esta seguro de duplicar este plan mensual?","Pregunta",MB_ICONQUESTION+MB_YESNO);
   if (res != IDYES)
      return;

   //Abrir tablas auxiliares
   MensualDupl->Open();
   SectorMesDupl->Open();
   DetMensualDupl->Open();
   //Cargar plan actual.
   MensualDupl->Filter = "CODMENSUAL = '" + CodMensual->Text +"'";
   MensualDupl->Filtered = true;
   Anyo = MensualDupl->FieldValues["ANYO"];
   Codigo = DatosF->generaCodigo("MES",Anyo.SubString(3,2));
   //Crear Mensual
   DatosF->Mensual->Insert();
   DatosF->MensualCODMENSUAL->Value = Codigo;
   DatosF->Mensual->FieldValues["CODFINCA"] = MensualDupl->FieldValues["CODFINCA"];
   DatosF->Mensual->FieldValues["NOMFINCA"] = MensualDupl->FieldValues["NOMFINCA"];
   DatosF->Mensual->FieldValues["ANYO"] = MensualDupl->FieldValues["ANYO"];
   DatosF->Mensual->FieldValues["MES"] = MensualDupl->FieldValues["MES"];
   DatosF->Mensual->FieldValues["QUINCENA"] = MensualDupl->FieldValues["QUINCENA"];
   DatosF->Mensual->FieldValues["ES_QUINCENA"] = MensualDupl->FieldValues["ES_QUINCENA"];
   DatosF->Mensual->FieldValues["OBSERVACIONES"] = MensualDupl->FieldValues["OBSERVACIONES"];
   DatosF->Mensual->FieldValues["LLENADOS"] = MensualDupl->FieldValues["LLENADOS"];
   DatosF->Mensual->FieldValues["IMPRESO"] = false;
   DatosF->Mensual->FieldValues["DABONO"] = MensualDupl->FieldValues["DABONO"];
   DatosF->Mensual->FieldValues["DRIEGO"] = MensualDupl->FieldValues["DRIEGO"];
   DatosF->Mensual->FieldValues["AGUA"] = MensualDupl->FieldValues["AGUA"];
   DatosF->Mensual->FieldValues["OBSERVA_AF"] = MensualDupl->FieldValues["OBSERVA_AF"];
   DatosF->Mensual->FieldValues["ANOTACIONES"] = "";
   DatosF->Mensual->Post();
   //Crear Sectores
   SectorMesDupl->First();
   while (!SectorMesDupl->Eof)
   {
        DatosF->SectorMes->Insert();
        DatosF->SectorMes->FieldValues["CODMENSUAL"] = Codigo ;
        DatosF->SectorMes->FieldValues["SECTOR"] = SectorMesDupl->FieldValues["SECTOR"];
        DatosF->SectorMes->FieldValues["CODANUAL"] = SectorMesDupl->FieldValues["CODANUAL"];
        DatosF->SectorMes->FieldValues["TABONO"] = SectorMesDupl->FieldValues["TABONO"];
        DatosF->SectorMes->FieldValues["TRIEGO"] = SectorMesDupl->FieldValues["TRIEGO"];
        DatosF->SectorMes->FieldValues["LUNES"] = SectorMesDupl->FieldValues["LUNES"];
        DatosF->SectorMes->FieldValues["MARTES"] = SectorMesDupl->FieldValues["MARTES"];
        DatosF->SectorMes->FieldValues["MIERCOLES"] = SectorMesDupl->FieldValues["MIERCOLES"];
        DatosF->SectorMes->FieldValues["JUEVES"] = SectorMesDupl->FieldValues["JUEVES"];
        DatosF->SectorMes->FieldValues["VIERNES"] = SectorMesDupl->FieldValues["VIERNES"];
        DatosF->SectorMes->FieldValues["SABADO"] = SectorMesDupl->FieldValues["SABADO"];
        DatosF->SectorMes->FieldValues["DOMINGO"] = SectorMesDupl->FieldValues["DOMINGO"];
        DatosF->SectorMes->FieldValues["BOMBA"] = SectorMesDupl->FieldValues["BOMBA"];
        DatosF->SectorMes->FieldValues["AGUA"] = SectorMesDupl->FieldValues["AGUA"];
        DatosF->SectorMes->Post();
        //Crear productos por sector.
        DetMensualDupl->First();
        while(!DetMensualDupl->Eof)
        {
            DatosF->DetMensual->Insert();
            DatosF->DetMensual->FieldValues["CODMENSUAL"] = Codigo ;
            DatosF->DetMensual->FieldValues["SECTOR"] = DetMensualDupl->FieldValues["SECTOR"];
            DatosF->DetMensual->FieldValues["NLINEA"] = DetMensualDupl->FieldValues["NLINEA"];
            DatosF->DetMensual->FieldValues["CODART"] = DetMensualDupl->FieldValues["CODART"];
            DatosF->DetMensual->FieldValues["CANTIDAD"] = DetMensualDupl->FieldValues["CANTIDAD"];
            DatosF->DetMensual->FieldValues["NOMCOMERCIAL"] = DetMensualDupl->FieldValues["NOMCOMERCIAL"];
            DatosF->DetMensual->Post();
            DetMensualDupl->Next();
        }
        SectorMesDupl->Next();
   }
   //Cerrar tablas.
   MensualDupl->Filtered = false;
   MensualDupl->Filter = "";
   DetMensualDupl->Close();
   SectorMesDupl->Close();
   MensualDupl->Close();

   ShowMessage("Plan Mensual duplicado - " + Codigo);
}
//---------------------------------------------------------------------------

