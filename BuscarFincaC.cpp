//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "BuscarFincaC.h"
#include "DatosC.h"
#include "AnualC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PlantillaC"
#pragma resource "*.dfm"
TBuscarFincaF *BuscarFincaF;
//---------------------------------------------------------------------------
__fastcall TBuscarFincaF::TBuscarFincaF(TComponent* Owner)
        : TPlantilla(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TBuscarFincaF::CerrarClick(TObject *Sender)
{
   Close();
}
//---------------------------------------------------------------------------
void __fastcall TBuscarFincaF::FormClose(TObject *Sender,
      TCloseAction &Action)
{
        //Habilitar de nuevo el formulario
        AbonadoAnualF->Enabled = true;

        //Cerrar la consulta
        BuscarFinca->Close();

        Action = caFree;
        BuscarFincaF = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TBuscarFincaF::BusquedaBClick(TObject *Sender)
{
    //Aplicación del filtro
    BuscarFinca->Close();
    BuscarFinca->SQL->Clear();
    BuscarFinca->SQL->Add("SELECT");
    BuscarFinca->SQL->Add("CLIENTES.NOMBRE, FINCAS.CODFINCA,FINCAS.NOMFINCA,");
    BuscarFinca->SQL->Add("SECTORES.SECTOR,SECTORES.VARIEDAD");
    BuscarFinca->SQL->Add("FROM");
    BuscarFinca->SQL->Add("CLIENTES CLIENTES,FINCAS FINCAS,SECTORES SECTORES");
    BuscarFinca->SQL->Add("WHERE");
    BuscarFinca->SQL->Add(" CLIENTES.CODCLI = FINCAS.CODCLI AND FINCAS.CODFINCA = SECTORES.CODFINCA");
    if (FincaText->Text != "")
    {
       BuscarFinca->SQL->Add("AND FINCAS.NOMFINCA LIKE '%" + FincaText->Text + "%'");
    }
    if (ClienteText->Text != "")
    {
        BuscarFinca->SQL->Add("AND CLIENTES.NOMBRE LIKE '%" + ClienteText->Text + "%'");
    }
    if (VariedadText->Text != "")
    {
        BuscarFinca->SQL->Add("AND SECTORES.VARIEDAD LIKE '%" + VariedadText->Text + "%'");
    }
    BuscarFinca->Open();
}
//---------------------------------------------------------------------------
void __fastcall TBuscarFincaF::ResultadoDblClick(TObject *Sender)
{
   if (DatosF->Anual->State == dsBrowse)
        //Activar modo edición si es necesario
        DatosF->Anual->Edit();
   DatosF->Anual->FieldValues["CODFINCA"] = BuscarFinca->FieldValues["CODFINCA"];
   DatosF->Anual->FieldValues["NOMFINCA"] = BuscarFinca->FieldValues["NOMFINCA"];
   DatosF->Anual->FieldValues["SECTOR"]   = BuscarFinca->FieldValues["SECTOR"];

   //Una vez realizada la busqueda, cerramos el formulario.
   Close();
}
//---------------------------------------------------------------------------
void __fastcall TBuscarFincaF::ResultadoKeyPress(TObject *Sender,
      char &Key)
{
    if (Key == VK_RETURN)
        ResultadoDblClick(Sender);
}
//---------------------------------------------------------------------------
