//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AnalisisFincaC.h"
#include "DatosC.h"
#include "FincasC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PlantillaC"
#pragma resource "*.dfm"
TAnalisisFincaF *AnalisisFincaF;
//---------------------------------------------------------------------------
__fastcall TAnalisisFincaF::TAnalisisFincaF(TComponent* Owner)
        : TPlantilla(Owner)
{

}
//---------------------------------------------------------------------------
void __fastcall TAnalisisFincaF::FechaChange(TObject *Sender)
{
        FechaBD->Text = Fecha->Date.operator AnsiString();
}
//---------------------------------------------------------------------------
void __fastcall TAnalisisFincaF::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   int res;
        if (DatosF->AnalisisFinca->State == dsInsert || DatosF->AnalisisFinca->State == dsEdit)
        {
            res = Application->MessageBoxA("Analisis pendiente de inserci�n.\n�Guardar los datos?","Informaci�n",MB_ICONINFORMATION+MB_YESNO);
            if (res == IDYES)
               DatosF->AnalisisFinca->Post();
            else
               DatosF->AnalisisFinca->Cancel();
        }
        //Habilitar de nuevo el formulario fincas
        FincasF->Enabled = true;
        //Deshacer el filtro indicado
        DatosF->AnalisisFinca->Filtered = false;
        DatosF->AnalisisFinca->Filter   = "";

        Action = caFree;
        AnalisisFincaF = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TAnalisisFincaF::CerrarClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------
//Preparar el formulario para introducir un nuevo an�lisis finca.
void __fastcall TAnalisisFincaF::AnyadirClick(TObject *Sender)
{
   if (DatosF->AnalisisFinca->State != dsBrowse)
   {
       Application->MessageBoxA("Hay un an�lisis de finca pendiente de inserci�n/modificaci�n.","Error",MB_ICONERROR);
       return;
   }
   DatosF->AnalisisFinca->Insert();
   DatosF->AnalisisFinca->FieldValues["CODFINCA"] = CodFinca->Text.ToInt();
   Fecha->Date = Fecha->Date.CurrentDate();
   FechaBD->Text = Fecha->Date.operator AnsiString();
   GroupBox2->Enabled = true;
   N->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TAnalisisFincaF::GuardarClick(TObject *Sender)
{
    if (FechaBD->Text == "")
    {
        Application->MessageBoxA("Fecha obligatoria.\nSeleccione una fecha","Error",MB_ICONERROR);
        Fecha->SetFocus();
        return;
    }
    if (DatosF->AnalisisFinca->State == dsInsert || DatosF->AnalisisFinca->State == dsEdit)
        DatosF->AnalisisFinca->Post();
    else
        Application->MessageBoxA("Nada que guardar.","Informaci�n",MB_ICONINFORMATION);

}
//---------------------------------------------------------------------------

void __fastcall TAnalisisFincaF::CancelarClick(TObject *Sender)
{
    if (DatosF->AnalisisFinca->State == dsInsert || DatosF->AnalisisFinca->State == dsEdit)
        DatosF->AnalisisFinca->Cancel();
    else
        Application->MessageBoxA("Nada que cancelar.","Informaci�n",MB_ICONINFORMATION);
}
//---------------------------------------------------------------------------

void __fastcall TAnalisisFincaF::EliminarClick(TObject *Sender)
{
   if(DatosF->AnalisisFinca->State == dsBrowse)
      DatosF->AnalisisFinca->Delete();
}
//---------------------------------------------------------------------------

