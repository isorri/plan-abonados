inherited BuscarFincaF: TBuscarFincaF
  Left = 399
  Top = 320
  Width = 519
  Height = 456
  Caption = 'B'#250'squeda de Fincas'
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited Cerrar: TBitBtn
    Left = 424
    Top = 384
    OnClick = CerrarClick
  end
  inherited NuevoB: TBitBtn
    Left = 0
    Top = 384
    Enabled = False
    Visible = False
  end
  inherited GuardarB: TBitBtn
    Left = 32
    Top = 384
    Enabled = False
    Visible = False
  end
  inherited EliminarB: TBitBtn
    Left = 64
    Top = 384
    Enabled = False
    Visible = False
  end
  object CriteriosBox: TGroupBox
    Left = 8
    Top = 8
    Width = 489
    Height = 145
    Caption = '  Criterios de b'#250'squeda   '
    TabOrder = 4
    object ClienteText: TLabeledEdit
      Left = 16
      Top = 32
      Width = 281
      Height = 21
      EditLabel.Width = 32
      EditLabel.Height = 13
      EditLabel.Caption = 'Cliente'
      LabelPosition = lpAbove
      LabelSpacing = 3
      TabOrder = 0
    end
    object FincaText: TLabeledEdit
      Left = 16
      Top = 72
      Width = 281
      Height = 21
      EditLabel.Width = 26
      EditLabel.Height = 13
      EditLabel.Caption = 'Finca'
      LabelPosition = lpAbove
      LabelSpacing = 3
      TabOrder = 1
    end
    object VariedadText: TLabeledEdit
      Left = 16
      Top = 112
      Width = 281
      Height = 21
      EditLabel.Width = 42
      EditLabel.Height = 13
      EditLabel.Caption = 'Variedad'
      LabelPosition = lpAbove
      LabelSpacing = 3
      TabOrder = 2
    end
    object BusquedaB: TBitBtn
      Left = 360
      Top = 56
      Width = 81
      Height = 33
      Hint = 'Realiza la b'#250'squeda con los criterios indicados.'
      Caption = '&B'#250'squeda'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = BusquedaBClick
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00840000008400
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0084000000840000008400
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF0000000000000000000000000000000000840000008400000084000000FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
        000084848400C6C6C600FFFFFF00848484000000000084000000FF00FF00FF00
        FF00FF00FF00FF00FF0000008400FF00FF00FF00FF00FF00FF00000000008484
        8400C6C6C600C6C6C600C6C6C600FFFFFF008484840000000000FF00FF00FF00
        FF00FF00FF000000840000008400FF00FF00FF00FF00FF00FF0000000000C6C6
        C600C6C6C600C6C6C600C6C6C600C6C6C600FFFFFF0000000000FF00FF00FF00
        FF000000840000008400000084000000840000008400FF00FF0000000000C6C6
        C600FFFFFF00FFFF0000C6C6C600C6C6C600C6C6C60000000000FF00FF00FF00
        FF00FF00FF000000840000008400FF00FF00FF00FF0000008400000000008484
        8400FFFFFF00FFFFFF00C6C6C600C6C6C6008484840000000000FF00FF00FF00
        FF00FF00FF00FF00FF0000008400FF00FF00FF00FF0000008400FF00FF000000
        000084848400C6C6C600C6C6C6008484840000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000008400FF00FF00FF00
        FF0000000000000000000000000000000000FF00FF00FF00FF00FF00FF000000
        8400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
        8400FF00FF00FF00FF0000008400FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
        8400FF00FF00FF00FF000000840000008400FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF000000840000008400000084000000840000008400FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF000000840000008400FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000008400FF00FF00FF00FF00FF00FF00}
    end
  end
  object ResultadoBox: TGroupBox
    Left = 8
    Top = 152
    Width = 489
    Height = 225
    Caption = 'Resultado'
    TabOrder = 5
    object Resultado: TDBGrid
      Left = 8
      Top = 16
      Width = 470
      Height = 201
      Hint = 'Resultado de la b'#250'squeda.'
      DataSource = BuscarFincaSource
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = ResultadoDblClick
      OnKeyPress = ResultadoKeyPress
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMBRE'
          Title.Caption = 'Cliente'
          Width = 130
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMFINCA'
          Title.Caption = 'Finca'
          Width = 130
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SECTOR'
          Title.Caption = 'Sector'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VARIEDAD'
          Title.Caption = 'Variedad'
          Width = 130
          Visible = True
        end>
    end
  end
  object BuscarFinca: TADOQuery
    Connection = DatosF.Abono
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      '     CLIENTES.NOMBRE,'
      '     FINCAS.CODFINCA,'
      '     FINCAS.NOMFINCA,'
      '     SECTORES.SECTOR,'
      '     SECTORES.VARIEDAD'
      'FROM'
      '     CLIENTES CLIENTES,'
      '     FINCAS FINCAS,'
      '     SECTORES SECTORES'
      'WHERE'
      '     CLIENTES.CODCLI = FINCAS.CODCLI AND'
      '     FINCAS.CODFINCA = SECTORES.CODFINCA')
    Left = 144
    Top = 384
    object BuscarFincaNOMBRE: TWideStringField
      FieldName = 'NOMBRE'
      Size = 50
    end
    object BuscarFincaCODFINCA: TAutoIncField
      FieldName = 'CODFINCA'
      ReadOnly = True
    end
    object BuscarFincaNOMFINCA: TWideStringField
      FieldName = 'NOMFINCA'
      Size = 50
    end
    object BuscarFincaSECTOR: TWideStringField
      FieldName = 'SECTOR'
    end
    object BuscarFincaVARIEDAD: TWideStringField
      FieldName = 'VARIEDAD'
      Size = 50
    end
  end
  object BuscarFincaSource: TDataSource
    DataSet = BuscarFinca
    Left = 176
    Top = 384
  end
end
