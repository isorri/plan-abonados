//---------------------------------------------------------------------------

#ifndef BuscarFincaCH
#define BuscarFincaCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "PlantillaC.h"
#include <Buttons.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
//---------------------------------------------------------------------------
class TBuscarFincaF : public TPlantilla
{
__published:	// IDE-managed Components
        TGroupBox *CriteriosBox;
        TLabeledEdit *ClienteText;
        TLabeledEdit *FincaText;
        TLabeledEdit *VariedadText;
        TBitBtn *BusquedaB;
        TGroupBox *ResultadoBox;
        TDBGrid *Resultado;
        TADOQuery *BuscarFinca;
        TWideStringField *BuscarFincaNOMBRE;
        TAutoIncField *BuscarFincaCODFINCA;
        TWideStringField *BuscarFincaNOMFINCA;
        TWideStringField *BuscarFincaSECTOR;
        TWideStringField *BuscarFincaVARIEDAD;
        TDataSource *BuscarFincaSource;
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall BusquedaBClick(TObject *Sender);
        void __fastcall ResultadoDblClick(TObject *Sender);
        void __fastcall ResultadoKeyPress(TObject *Sender, char &Key);
private:	// User declarations
public:		// User declarations
        __fastcall TBuscarFincaF(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TBuscarFincaF *BuscarFincaF;
//---------------------------------------------------------------------------
#endif
