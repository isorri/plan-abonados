//---------------------------------------------------------------------------

#ifndef BuscarClienteCH
#define BuscarClienteCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "PlantillaC.h"
#include <Buttons.hpp>
#include "DatosC.h"
#include "FincasC.h"
#include <DBCtrls.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
//---------------------------------------------------------------------------
class TBuscarClienteF : public TPlantilla
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TDBLookupComboBox *ComercialLista;
        TLabel *Label2;
        TEdit *ClienteText;
        TGroupBox *CriteriosBox;
        TGroupBox *ResultadoBox;
        TDBGrid *ResultadoTable;
        TBitBtn *BusquedaB;
        TCheckBox *TodosFlag;
        TADOQuery *BuscarCliente;
        TWideStringField *BuscarClienteNOMBRE;
        TWideStringField *BuscarClienteNOMBRE_1;
        TDataSource *BuscaCliSource;
        TWideStringField *BuscarClienteCODCLI;
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall BusquedaBClick(TObject *Sender);
        void __fastcall ResultadoTableDblClick(TObject *Sender);
        void __fastcall TodosFlagClick(TObject *Sender);
        void __fastcall ResultadoTableKeyPress(TObject *Sender, char &Key);
private:	// User declarations
public:		// User declarations
        __fastcall TBuscarClienteF(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TBuscarClienteF *BuscarClienteF;
//---------------------------------------------------------------------------
#endif
