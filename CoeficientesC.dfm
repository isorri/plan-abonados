inherited CoeficienteF: TCoeficienteF
  Left = 314
  Top = 283
  Width = 679
  Height = 416
  Caption = 'Coeficientes'
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel [0]
    Left = 336
    Top = 8
    Width = 58
    Height = 13
    Caption = 'Coeficientes'
  end
  inherited Cerrar: TBitBtn
    Left = 600
    Top = 344
    Width = 57
    TabOrder = 3
    OnClick = CerrarClick
  end
  inherited NuevoB: TBitBtn
    Left = 248
    Top = 344
    TabOrder = 5
    OnClick = InsertarClick
  end
  inherited GuardarB: TBitBtn
    Left = 328
    Top = 344
    Enabled = False
    TabOrder = 6
    OnClick = GuardarClick
  end
  inherited EliminarB: TBitBtn
    Left = 480
    Top = 344
    TabOrder = 7
    OnClick = EliminarClick
  end
  object Coeficientes: TDBGrid
    Left = 0
    Top = 0
    Width = 241
    Height = 369
    DataSource = DatosF.CoeficientesSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'PATRON'
        Title.Caption = 'Coeficientes'
        Visible = True
      end>
  end
  object Patron: TDBEdit
    Left = 400
    Top = 8
    Width = 201
    Height = 21
    DataField = 'PATRON'
    DataSource = DatosF.CoeficientesSource
    TabOrder = 0
  end
  object GroupBox1: TGroupBox
    Left = 256
    Top = 32
    Width = 401
    Height = 305
    Caption = 'Par'#225'metros'
    TabOrder = 1
    object Label3: TLabel
      Left = 24
      Top = 40
      Width = 46
      Height = 13
      Caption = 'Nitr'#243'geno'
    end
    object Label4: TLabel
      Left = 24
      Top = 72
      Width = 35
      Height = 13
      Caption = 'F'#243'sforo'
    end
    object Label5: TLabel
      Left = 24
      Top = 104
      Width = 35
      Height = 13
      Caption = 'Potasio'
    end
    object Label6: TLabel
      Left = 24
      Top = 136
      Width = 29
      Height = 13
      Caption = 'Calcio'
    end
    object Label7: TLabel
      Left = 24
      Top = 168
      Width = 46
      Height = 13
      Caption = 'Magnesio'
    end
    object Label8: TLabel
      Left = 24
      Top = 200
      Width = 28
      Height = 13
      Caption = 'Hierro'
    end
    object Label9: TLabel
      Left = 24
      Top = 232
      Width = 74
      Height = 13
      Caption = 'Microelementos'
    end
    object Label10: TLabel
      Left = 24
      Top = 264
      Width = 66
      Height = 13
      Caption = #193'cido H'#250'mico'
    end
    object Label11: TLabel
      Left = 240
      Top = 8
      Width = 100
      Height = 13
      Caption = 'Nivel medio estimado'
    end
    object Label12: TLabel
      Left = 264
      Top = 24
      Width = 41
      Height = 13
      Caption = 'por '#225'rbol'
    end
    object Label13: TLabel
      Left = 104
      Top = 8
      Width = 115
      Height = 13
      Caption = 'Cantidad necesaria para'
    end
    object Label14: TLabel
      Left = 104
      Top = 24
      Width = 111
      Height = 13
      Caption = 'producir 100Kg de fruta'
    end
    object Label15: TLabel
      Left = 368
      Top = 40
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label16: TLabel
      Left = 368
      Top = 72
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label17: TLabel
      Left = 368
      Top = 104
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label18: TLabel
      Left = 368
      Top = 136
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label19: TLabel
      Left = 368
      Top = 168
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label20: TLabel
      Left = 360
      Top = 200
      Width = 32
      Height = 13
      Caption = 'mg/Kg'
    end
    object Label21: TLabel
      Left = 361
      Top = 232
      Width = 32
      Height = 13
      Caption = 'mg/Kg'
    end
    object Label22: TLabel
      Left = 361
      Top = 264
      Width = 32
      Height = 13
      Caption = 'mg/Kg'
    end
    object Label23: TLabel
      Left = 216
      Top = 40
      Width = 37
      Height = 13
      Caption = 'gr/arbol'
    end
    object Label24: TLabel
      Left = 216
      Top = 72
      Width = 37
      Height = 13
      Caption = 'gr/arbol'
    end
    object Label25: TLabel
      Left = 216
      Top = 104
      Width = 37
      Height = 13
      Caption = 'gr/arbol'
    end
    object Label26: TLabel
      Left = 216
      Top = 136
      Width = 37
      Height = 13
      Caption = 'gr/arbol'
    end
    object Label27: TLabel
      Left = 216
      Top = 168
      Width = 37
      Height = 13
      Caption = 'gr/arbol'
    end
    object Label28: TLabel
      Left = 216
      Top = 200
      Width = 37
      Height = 13
      Caption = 'gr/arbol'
    end
    object Label29: TLabel
      Left = 216
      Top = 232
      Width = 37
      Height = 13
      Caption = 'gr/arbol'
    end
    object Label30: TLabel
      Left = 216
      Top = 264
      Width = 37
      Height = 13
      Caption = 'gr/arbol'
    end
    object N1: TDBEdit
      Left = 104
      Top = 40
      Width = 106
      Height = 21
      DataField = 'N_C1'
      DataSource = DatosF.CoeficientesSource
      TabOrder = 0
    end
    object N2: TDBEdit
      Left = 264
      Top = 40
      Width = 90
      Height = 21
      DataField = 'N_C2'
      DataSource = DatosF.CoeficientesSource
      TabOrder = 1
    end
    object P2: TDBEdit
      Left = 264
      Top = 72
      Width = 91
      Height = 21
      DataField = 'P_C2'
      DataSource = DatosF.CoeficientesSource
      TabOrder = 3
    end
    object K2: TDBEdit
      Left = 264
      Top = 104
      Width = 92
      Height = 21
      DataField = 'K_C2'
      DataSource = DatosF.CoeficientesSource
      TabOrder = 5
    end
    object CA2: TDBEdit
      Left = 264
      Top = 136
      Width = 92
      Height = 21
      DataField = 'CA_C2'
      DataSource = DatosF.CoeficientesSource
      TabOrder = 7
    end
    object MG2: TDBEdit
      Left = 264
      Top = 168
      Width = 93
      Height = 21
      DataField = 'MG_C2'
      DataSource = DatosF.CoeficientesSource
      TabOrder = 9
    end
    object FE2: TDBEdit
      Left = 264
      Top = 200
      Width = 93
      Height = 21
      DataField = 'FE_C2'
      DataSource = DatosF.CoeficientesSource
      TabOrder = 11
    end
    object MICRO2: TDBEdit
      Left = 264
      Top = 232
      Width = 92
      Height = 21
      DataField = 'MICRO_C2'
      DataSource = DatosF.CoeficientesSource
      TabOrder = 13
    end
    object AH2: TDBEdit
      Left = 264
      Top = 264
      Width = 92
      Height = 21
      DataField = 'AH_C2'
      DataSource = DatosF.CoeficientesSource
      TabOrder = 15
    end
    object P1: TDBEdit
      Left = 104
      Top = 72
      Width = 106
      Height = 21
      DataField = 'P_C1'
      DataSource = DatosF.CoeficientesSource
      TabOrder = 2
    end
    object K1: TDBEdit
      Left = 104
      Top = 104
      Width = 107
      Height = 21
      DataField = 'K_C1'
      DataSource = DatosF.CoeficientesSource
      TabOrder = 4
    end
    object CA1: TDBEdit
      Left = 104
      Top = 136
      Width = 108
      Height = 21
      DataField = 'CA_C1'
      DataSource = DatosF.CoeficientesSource
      TabOrder = 6
    end
    object MG1: TDBEdit
      Left = 104
      Top = 168
      Width = 108
      Height = 21
      DataField = 'MG_C1'
      DataSource = DatosF.CoeficientesSource
      TabOrder = 8
    end
    object FE1: TDBEdit
      Left = 104
      Top = 200
      Width = 109
      Height = 21
      DataField = 'FE_C1'
      DataSource = DatosF.CoeficientesSource
      TabOrder = 10
    end
    object MICRO1: TDBEdit
      Left = 104
      Top = 232
      Width = 109
      Height = 21
      DataField = 'MICRO_C1'
      DataSource = DatosF.CoeficientesSource
      TabOrder = 12
    end
    object AH1: TDBEdit
      Left = 104
      Top = 264
      Width = 110
      Height = 21
      DataField = 'AH_C1'
      DataSource = DatosF.CoeficientesSource
      TabOrder = 14
    end
  end
  object Cancelar: TButton
    Left = 408
    Top = 344
    Width = 65
    Height = 25
    Caption = '&Cancelar'
    Enabled = False
    TabOrder = 2
    OnClick = CancelarClick
  end
end
