//---------------------------------------------------------------------------

#ifndef RiegoPlantCH
#define RiegoPlantCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "RiegoC.h"
#include <Buttons.hpp>
#include <DBCtrls.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TRiegoPlantF : public TRiegoF
{
__published:	// IDE-managed Components
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall NuevoClick(TObject *Sender);
        void __fastcall GuardarClick(TObject *Sender);
        void __fastcall CancelarClick(TObject *Sender);
        void __fastcall EliminarBClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TRiegoPlantF(TComponent* Owner);
        void bloqueaFormulario(void);
        void desbloqueaFormulario(void);        
};
//---------------------------------------------------------------------------
extern PACKAGE TRiegoPlantF *RiegoPlantF;
//---------------------------------------------------------------------------
#endif
