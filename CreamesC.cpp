//---------------------------------------------------------------------------

#include <vcl.h>
#include <time.h>
#include <math.h>
#pragma hdrstop

#include "CreamesC.h"
#include "DatosC.h"
#include "MainC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PlantillaC"
#pragma resource "*.dfm"
TCreamesF *CreamesF;
//---------------------------------------------------------------------------
__fastcall TCreamesF::TCreamesF(TComponent* Owner)
        : TPlantilla(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TCreamesF::CerrarClick(TObject *Sender)
{
        Close();        
}
//---------------------------------------------------------------------------

void __fastcall TCreamesF::FormClose(TObject *Sender, TCloseAction &Action)
{
        //Cerrar las tablas
        Productos->Close();
        Fincas->Close();
        DetAnual->Close();
        //Formulario preparado para ser destruido.
        Action = caFree;
        CreamesF = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TCreamesF::FormCreate(TObject *Sender)
{
        time_t t;
        struct tm * petm;

        //Abrir y preparar las tablas utilizadas por este formulario
        Productos->Open();
        Fincas->Open();
        DetAnual->Open();

        //Inicializar el formulario con el mes y a�o actual.
        time(&t);
        petm = localtime(&t);
        petm->tm_mon;
        Anyo->Text = (AnsiString) (petm->tm_year + 1900);
        Mes->Text  = (AnsiString) (petm->tm_mon + 1);
}
//---------------------------------------------------------------------------

void __fastcall TCreamesF::CrearClick(TObject *Sender)
{

   /*Hay que adaptar este proceso a los planes quincenales y a DETMENSUAL.
     Si el plan es mensual continuar como hasta ahora creado el detalle del
     plan mensual, si el plan es quincenal crear la quincena indicada teniendo
     en cuenta que los dias de riego son 15 y las semanas 2*/


   int mes, anyo,quincena,llenados;
   float agua ;//Agua necesaria para la finca
   float aguasector; //Agua necesaria para el sector actual
   float aguaproducto; //Agua necesaria para disolver el producto
   float minutos; //Minutos de abonado
   float cantidad; //Cantidad del producto actual en el mes (o quincena)
   int dias; //Dias de riego y dias de riego plantonada respectivamente
   int ndias; // N�mero de d�as empleados para aplicar el abono
   int nlinea; //Numero de linea del detalle del plan mensual.
   int nplan;  // Numero de planes mensuales creados.
   int nplanskip; //Numero de fincas omitidas por estar ya creadas.
   AnsiString CampoMes; //Nombre del campo a utilizar en la consulta detalle
   AnsiString temp;
   Variant compruebames [4]; //Para comprobar si la finca ya tiene un plan anual asociado
   Variant compruebasector[2]; //Para comprobar si existe el sectormes que vamos a crear

   //Comprobar los par�metros
   try
   {
        mes = Mes->Text.ToInt();
        anyo = Anyo->Text.ToInt();
        quincena = Quincena->Text.ToInt();
        dias = DRiego->Text.ToInt();
        if (mes <1 || mes >12)
            throw Exception("Mes fuera de rango");
        if (quincena <1 || quincena >2)
            throw Exception("Quincena fuera de rango (1 � 2)");
        if (EsQuincena->Checked)
        {
            if ( dias <1 || dias > 16)
                throw Exception("Dias de riego fuera de rango");
        }
        else
        {
            if (dias <1 || dias >31)
                throw Exception("Dias de riego fuera de rango");
        }
        //Inicializar CampoMes para que seleccione el campo de la tabla DetAnual correspondiente
        switch(mes)
        {
            case 1  : CampoMes = "01_ENERO";       break;
            case 2  : CampoMes = "02_FEBRERO";     break;
            case 3  : CampoMes = "03_MARZO";       break;
            case 4  : CampoMes = "04_ABRIL";       break;
            case 5  : CampoMes = "05_MAYO";        break;
            case 6  : CampoMes = "06_JUNIO";       break;
            case 7  : CampoMes = "07_JULIO";       break;
            case 8  : CampoMes = "08_AGOSTO";      break;
            case 9  : CampoMes = "09_SEPTIEMBRE";  break;
            case 10 : CampoMes = "10_OCTUBRE";     break;
            case 11 : CampoMes = "11_NOVIEMBRE";   break;
            case 12 : CampoMes = "12_DICIEMBRE";   break;
         }
   }
   catch(Exception &e)
   {
        ShowMessage("No se puede crear el mes.\n" + e.Message);
        return;
   }
   //OK. Empezamos a crear los registros.
   try
   {
        //Empieza la transacci�n
        DatosF->Abono->BeginTrans();
        //Recuperar los dias de riego
        if ( ! DatosF->DiasRiego->Locate("MES", mes , TLocateOptions() ) )
            throw Exception("Error al recuperar los d�as de riego del mes");

        //Recuperar los dias de riego de plantonadas
        if ( ! DatosF->RiegoPlant->Locate("MES", mes , TLocateOptions() ) )
            throw Exception("Error al recuperar los d�as de riego del mes para plantones");

        //Listado fincas con un plan anual asociado
        ListaFincas->SQL->Clear();
        ListaFincas->SQL->Add("SELECT DISTINCT CODFINCA");
        ListaFincas->SQL->Add("FROM ANUAL ");
        ListaFincas->SQL->Add("WHERE ANYO =" + Anyo->Text);
        ListaFincas->Open();
        ListaFincas->First();

        //Inicializaci�n barra de progreso
        Progreso->Min = 0;
        Progreso->Max = ListaFincas->RecordCount;
        Progreso->Position = 0;
        nplan = 0;
        nplanskip = 0;
        //Para cada plan anual a�adir un nuevo registro con los calculos correspondientes
        while (!ListaFincas->Eof)
        {
             //Cargar la finca asociada al plan anual
             if ( ! Fincas->Locate("CODFINCA", ListaFincas->FieldValues["CODFINCA"] , TLocateOptions()) )
                 throw Exception("Error al cargar la finca " + ListaFincas->FieldValues["CODFINCA"]);
             if (MainF->pruebas)
                 ShowMessage("Codigo finca " + AnsiString(ListaFincas->FieldValues["CODFINCA"]));

             //Comprobar si existe el plan mensual que vamos a crear
             compruebames[0] = ListaFincas->FieldValues["CODFINCA"];
             compruebames[1] = anyo;
             compruebames[2] = mes;
             compruebames[3] = quincena;
             if (DatosF->Mensual->Locate("CODFINCA;ANYO;MES;QUINCENA",VarArrayOf(compruebames,3),TLocateOptions()) )
             {
                //Existe el plan mensual que vamos a crear
                if (MainF->pruebas)
                    ShowMessage( "Omitimos la finca " + AnsiString(ListaFincas->FieldValues["CODFINCA"]) + " por tener ya el plan mensual creado");
                ListaFincas->Next();
                Progreso->Position += 1; //Avanzamos la barra de progreso
                nplanskip += 1;
                continue; //Omitimos este plan y pasamos al siguiente
             }
             //Crear el mensual
             DatosF->Mensual->Insert();
             DatosF->Mensual->FieldValues["CODMENSUAL"] = DatosF->generaCodigo("MES",Anyo->Text.SubString(3,2));
             DatosF->Mensual->FieldValues["CODFINCA"] = ListaFincas->FieldValues["CODFINCA"];
             DatosF->Mensual->FieldValues["NOMFINCA"] = Fincas->FieldValues["NOMFINCA"];
             DatosF->Mensual->FieldValues["ANYO"] = anyo;
             DatosF->Mensual->FieldValues["MES"] = mes;
             DatosF->Mensual->FieldValues["QUINCENA"] = quincena;
             DatosF->Mensual->FieldValues["ES_QUINCENA"] = EsQuincena->Checked;
             DatosF->Mensual->FieldValues["OBSERVACIONES"] = "";
             DatosF->Mensual->FieldValues["OBSERVA_AF"] = "";
             DatosF->Mensual->FieldValues["ANOTACIONES"] = "";
             DatosF->Mensual->Post();
             if (MainF->pruebas)
                ShowMessage( "Registro Mensual creado finca " + AnsiString(ListaFincas->FieldValues["CODFINCA"]));

             ListaSectores->SQL->Clear();
             ListaSectores->SQL->Add("SELECT S.SECTOR,S.TRIEGO,S.PLANTONADA,A.CODIGO_AF,A.CODANUAL ");
             ListaSectores->SQL->Add("FROM SECTORES S, ANUAL A ");
             ListaSectores->SQL->Add("WHERE A.CODFINCA = S.CODFINCA ");
             ListaSectores->SQL->Add("AND A.SECTOR   = S.SECTOR ");
             ListaSectores->SQL->Add("AND A.ANYO =" + Anyo->Text);
             ListaSectores->SQL->Add(" AND A.CODFINCA = " + ListaFincas->FieldValues["CODFINCA"]);
             ListaSectores->Open();
             ListaSectores->First();
             agua = 0.0;
             //Para cada sector de la finca seleccionada que tiene plan anual
             while (!ListaSectores->Eof)
             {
                //----------- Comprobar que no existe ya el sector que vamos a crear ----------
                compruebasector[0] = DatosF->Mensual->FieldValues["CODMENSUAL"];
                compruebasector[1] = ListaSectores->FieldValues["SECTOR"];
                if (DatosF->SectorMes->Locate("CODMENSUAL;SECTOR",VarArrayOf(compruebasector,1),TLocateOptions()) )
                   //OJO!!! El sector que vamos a crear ya existe!!!!
                   throw Exception("Sector " + AnsiString(ListaSectores->FieldValues["SECTOR"]) + " de la finca " + AnsiString(ListaFincas->FieldValues["CODFINCA"]) + " esta duplicado");

                //-----------  Crear el mes para el sector ---------
                DatosF->SectorMes->Insert();
                DatosF->SectorMes->FieldValues["CODMENSUAL"] = DatosF->Mensual->FieldValues["CODMENSUAL"];
                DatosF->SectorMes->FieldValues["SECTOR"] = ListaSectores->FieldValues["SECTOR"];
                DatosF->SectorMes->FieldValues["TRIEGO"] = ListaSectores->FieldValues["TRIEGO"];
                DatosF->SectorMes->FieldValues["BOMBA"] = 100;
                DatosF->SectorMes->FieldValues["CODANUAL"] = ListaSectores->FieldValues["CODANUAL"];
                //D�as de riego.
                {
                   TADOTable * Dias;
                   if ( ListaSectores->FieldValues["PLANTONADA"] )
                       Dias = DatosF->RiegoPlant;
                   else
                       Dias = DatosF->DiasRiego;
                   DatosF->SectorMes->FieldValues["LUNES"]     = Dias->FieldValues["LUNES"];
                   DatosF->SectorMes->FieldValues["MARTES"]    = Dias->FieldValues["MARTES"];
                   DatosF->SectorMes->FieldValues["MIERCOLES"] = Dias->FieldValues["MIERCOLES"];
                   DatosF->SectorMes->FieldValues["JUEVES"]    = Dias->FieldValues["JUEVES"];
                   DatosF->SectorMes->FieldValues["VIERNES"]   = Dias->FieldValues["VIERNES"];
                   DatosF->SectorMes->FieldValues["SABADO"]    = Dias->FieldValues["SABADO"];
                   DatosF->SectorMes->FieldValues["DOMINGO"]   = Dias->FieldValues["DOMINGO"];
                }
                DatosF->SectorMes->Post();
                if (MainF->pruebas)
                    ShowMessage( "Sector " + AnsiString(ListaSectores->FieldValues["SECTOR"]) + "creado");

                //--------  Calcular la cantidad de agua necesaria ---------------
                // Agua = sumatorio (ctd abono/ solubilidad * 100)
                aguasector = 0.0;
                //Numero de linea para detalle = 1
                nlinea = 1;
                //Seleccionar el detalle del plan anual asociado al sector, mes y a�o
                DetAnual->Filter  = "CODANUAL = '" + ListaSectores->FieldValues["CODANUAL"] + "'";
                if (MainF->pruebas)
                    ShowMessage("Filtrado por c�digo:" + AnsiString(ListaSectores->FieldValues["CODANUAL"]));
                DetAnual->Filtered = true;
                DetAnual->First();
                while(! DetAnual->Eof)
                {
                    //Cargar el producto
                    if ( ! Productos->Locate("CODART", DetAnual->FieldValues["CODART"], TLocateOptions()) )
                        throw Exception("Error al cargar el producto " + DetAnual->FieldValues["CODART"] );
                    cantidad = ceil(DetAnual->FieldValues["CANTIDAD"] * DetAnual->FieldValues[CampoMes] / 100);
                    if (EsQuincena->Checked)
                        //Si el plan es quincenal, aplicamos la mitad del producto
                        cantidad = cantidad / 2;
                    if (MainF->pruebas)
                    {
                        ShowMessage("Producto:" + AnsiString(DetAnual->FieldValues["NOMCOMERCIAL"]));
                        ShowMessage("Disoluci�n del producto: " + AnsiString(Productos->FieldValues["DISOLUCI�N"]));
                        ShowMessage("Cantidad a aplicar:" + AnsiString(cantidad));
                    }
                    if (Productos->FieldValues["DISOLUCI�N"] != 0 && ! Productos->FieldValues["DISOLUCI�N"].IsNull())
                    {
                        aguaproducto = (cantidad * 100) / (float)Productos->FieldValues["DISOLUCI�N"];
                        aguasector  += aguaproducto;
                        if (MainF->pruebas)
                        {
                            ShowMessage("Agua a aplicar: " + AnsiString(aguaproducto));
                            ShowMessage("Agua acumulada sector:" + AnsiString(aguasector));
                        }
                    }
                    //Crear el detalle del mes si hay cantidad que aplicar del producto
                    if (cantidad)
                    {
                        if (ListaSectores->FieldValues["CODIGO_AF"] == DetAnual->FieldValues["CODART"])
                        {
                            if (MainF->pruebas)
                                ShowMessage("Acido fosforico: " + AnsiString(DetAnual->FieldValues["NOMCOMERCIAL"]));
                            minutos = (float)( aguaproducto * 60) / (float) (Fincas->FieldValues["CAUDALCUBAS"]);
                            DatosF->SectorMes->Refresh();
                            DatosF->SectorMes->Edit();
                            DatosF->SectorMes->FieldValues["CODART_AF"] = DetAnual->FieldValues["CODART"];
                            DatosF->SectorMes->FieldValues["NOMBRE_AF"] = DetAnual->FieldValues["NOMCOMERCIAL"];
                            DatosF->SectorMes->FieldValues["BOMBA_AF"] = 100;
                            DatosF->SectorMes->FieldValues["CTD_AF"] = cantidad;
                            DatosF->SectorMes->FieldValues["DACIDOF"] = 1;
                            DatosF->SectorMes->FieldValues["TACIDOF"] = (int) minutos;
                            DatosF->SectorMes->FieldValues["TRIEGO_AF"] = ListaSectores->FieldValues["TRIEGO"];
                            DatosF->SectorMes->FieldValues["LUNES_AF"]     = 0;
                            DatosF->SectorMes->FieldValues["MARTES_AF"]    = 0;
                            DatosF->SectorMes->FieldValues["MIERCOLES_AF"] = 0;
                            DatosF->SectorMes->FieldValues["JUEVES_AF"]    = 0;
                            DatosF->SectorMes->FieldValues["VIERNES_AF"]   = 0;
                            DatosF->SectorMes->FieldValues["SABADO_AF"]    = 1;
                            DatosF->SectorMes->FieldValues["DOMINGO_AF"]   = 0;
                            DatosF->SectorMes->Post();
                        }
                        else
                        {
                            if (MainF->pruebas)
                                ShowMessage("Nuevo registro detalle:" + AnsiString(DatosF->Mensual->FieldValues["CODMENSUAL"]) + ";" + AnsiString(ListaSectores->FieldValues["SECTOR"]) + ";" + AnsiString(nlinea));
                            DatosF->DetMensual->Insert();
                            DatosF->DetMensual->FieldValues["CODMENSUAL"] = DatosF->Mensual->FieldValues["CODMENSUAL"];
                            DatosF->DetMensual->FieldValues["SECTOR"] = ListaSectores->FieldValues["SECTOR"];
                            DatosF->DetMensual->FieldValues["CODART"]   = DetAnual->FieldValues["CODART"];
                            DatosF->DetMensual->FieldValues["NLINEA"]   =  nlinea;
                            DatosF->DetMensual->FieldValues["CANTIDAD"] = cantidad;
                            DatosF->DetMensual->FieldValues["NOMCOMERCIAL"] = DetAnual->FieldValues["NOMCOMERCIAL"];
                            DatosF->DetMensual->Post();
                            nlinea +=1;
                            if (MainF->pruebas)
                                ShowMessage("Ok! Creado");
                        }
                    }
                    //Siguiente linea de detalle
                    if (MainF->pruebas)
                        ShowMessage("Siguiente producto.");
                    DetAnual->Next();
                }
                DetAnual->Filtered = false;
                //Guardamos la cantidad de agua en el sector
                DatosF->SectorMes->Edit();
                DatosF->SectorMes->FieldValues["AGUA"] = (int)aguasector;
                DatosF->SectorMes->Post();
                if (MainF->pruebas)
                    ShowMessage("Almacenar agua del sector: " + AnsiString(aguasector));
                //Acumulamos el agua necesaria para la finca
                agua += aguasector;
                //Siguiente sector
                if (MainF->pruebas)
                    ShowMessage("Siguiente sector.");
                ListaSectores->Next();
              }
              ListaSectores->Close();
              //-----------------Calcular n�mero de llenados---------------------
              //En agua tenemos la cantidad de agua necesaria para disolver
              DatosF->Mensual->Edit();
              if (Fincas->FieldValues["CAPACIDADCUBAS"] != 0 && ! Fincas->FieldValues["CAPACIDADCUBAS"].IsNull() )
              {
                  if (MainF->pruebas)
                  {
                      ShowMessage("Capacidad Cubas: " + AnsiString(Fincas->FieldValues["CAPACIDADCUBAS"]));
                      ShowMessage("Agua: " + AnsiString(agua));
                  }
                  llenados = ceil((float)agua / (float) Fincas->FieldValues["CAPACIDADCUBAS"]);
                  DatosF->Mensual->FieldValues["LLENADOS"] = llenados;
                  DatosF->Mensual->FieldValues["AGUA"]     = (int)agua;
              }
              else
                  DatosF->Mensual->FieldValues["LLENADOS"] = 0;
              //-----------------    D�as de abono    --------------------------
              /*Calcular los d�as necesarios para aplicar el abono, �ste debe ser
                m�ltiplo del n�mero de llenados e inferior al indicado en dias y diasp*/
              if (llenados)
              {
                  if ( dias % llenados )
                      ndias = dias - (dias % llenados);
                  else
                      ndias = dias;
              }
              else
                  ndias = 0;

              DatosF->Mensual->FieldValues["DABONO"] = ndias;
              DatosF->Mensual->FieldValues["DRIEGO"] = dias;
              DatosF->Mensual->FieldValues["IMPRESO"] = false;
              DatosF->Mensual->Post();
              //-----------------Calcular el tiempo de abono--------------------
              // minutos = Agua a inyectar/( % bomba * caudal * dias) * 60
              //Agua a inyectar --> es el agua proporcional que necesita cada sector para vac�ar por completo la cuba
              //Agua a inyectar = capacidad total cubas * n� llenados * agua sector /agua total
              //Volver a recorrer los sectores mes  y calcular los minutos de abonado
              DatosF->SectorMes->First();
              while(!DatosF->SectorMes->Eof)
              {
                  aguasector = Fincas->FieldValues["CAPACIDADCUBAS"] * llenados * DatosF->SectorMes->FieldValues["AGUA"];
                  aguasector = aguasector / agua;
                  if (Fincas->FieldValues["CAUDALCUBAS"] != 0 && ndias  )
                      minutos = ceil((float)( aguasector * 60) / (float) (Fincas->FieldValues["CAUDALCUBAS"] * ndias));
                  else
                      minutos = 0;
                  DatosF->SectorMes->Edit();
                  DatosF->SectorMes->FieldValues["TABONO"] = (int) minutos;
                  DatosF->SectorMes->Post();
                  DatosF->SectorMes->Next();
              }
              //Siguiente plan
              Progreso->Position += 1;
              nplan +=1;

              ListaFincas->Next();
        }
        //Mostramos resultado en la pantalla
        if (nplan)
            New->Caption = AnsiString(nplan) + " plan mensual creados";
        if (nplanskip)
            Skip->Caption = AnsiString(nplanskip) + " fincas omitidas";
        //Finaliza la transacci�n
        ListaFincas->Close();
        DatosF->Abono->CommitTrans();
        DatosF->DetMensual->Close();
        DatosF->DetMensual->Open();
   }
   catch(Exception &e)
   {
      DatosF->Abono->RollbackTrans();
      ShowMessage("No se puede crear el mes.\n" + e.Message);
   }
}
//---------------------------------------------------------------------------
void __fastcall TCreamesF::EsQuincenaClick(TObject *Sender)
{
   Quincena->Visible = EsQuincena->Checked;
   if (EsQuincena->Checked)
      Label4->Caption = "D�as riego quincena";
   else
      Label4->Caption = "D�as riego mes";
}
//---------------------------------------------------------------------------

