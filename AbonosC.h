//---------------------------------------------------------------------------

#ifndef AbonosCH
#define AbonosCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "PlantillaC.h"
#include <Buttons.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include <DBCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Mask.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TProductF : public TPlantilla
{
__published:	// IDE-managed Components
        TDBGrid *Productos;
        TPanel *Campos;
        TDBEdit *CodArt;
        TDBEdit *NomComercial;
        TGroupBox *Propiedades;
        TDBEdit *N;
        TDBEdit *P;
        TDBEdit *K;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TDBEdit *Disolucion;
        TDBComboBox *Estado;
        TGroupBox *Finanzas;
        TDBEdit *X3;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label10;
        TBitBtn *Cancelar;
        TLabel *Label11;
        TLabel *Label12;
        TLabel *Label13;
        TDBEdit *CA;
        TDBEdit *MG;
        TDBEdit *FE;
        TDBEdit *MElem;
        TDBEdit *AH;
        TLabel *Label14;
        TLabel *Label15;
        TDBEdit *CSALINO;
        TLabel *Label16;
        TDBEdit *PVT;
        TDBEdit *COP;
        TDBEdit *GRF;
        TDBEdit *PVP;
        TLabel *Label17;
        TLabel *Label18;
        TLabel *Label19;
        TLabel *Label20;
        TDBCheckBox *Es_Abono;
        TButton *Import;
        TLabel *Label1;
        TDBEdit *FImportacion;
        TPageControl *Paginas;
        TTabSheet *DatosP;
        TTabSheet *RepartoP;
        TGroupBox *Reparto;
        TLabel *Label21;
        TLabel *Label22;
        TLabel *Label23;
        TLabel *Label24;
        TLabel *Label25;
        TLabel *Label26;
        TLabel *Label27;
        TLabel *Label28;
        TLabel *Label29;
        TLabel *Label30;
        TLabel *Label31;
        TLabel *Label32;
        TLabel *Label33;
        TDBEdit *Enero;
        TDBEdit *Febrero;
        TDBEdit *Marzo;
        TDBEdit *Abril;
        TDBEdit *Mayo;
        TDBEdit *Junio;
        TDBEdit *Julio;
        TDBEdit *Agosto;
        TDBEdit *Septiembre;
        TDBEdit *Octubre;
        TDBEdit *Noviembre;
        TDBEdit *Diciembre;
        TDBEdit *Total;
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall InsertarClick(TObject *Sender);
        void __fastcall GuardarClick(TObject *Sender);
        void __fastcall CancelarClick(TObject *Sender);
        void __fastcall ImportClick(TObject *Sender);
        void __fastcall EliminarClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TProductF(TComponent* Owner);
        void bloqueaFormulario(void);
        void desbloqueaFormulario(void);        
};
//---------------------------------------------------------------------------
extern PACKAGE TProductF *ProductF;
//---------------------------------------------------------------------------
#endif
