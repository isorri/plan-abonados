//---------------------------------------------------------------------------

#ifndef FincasCH
#define FincasCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <DBCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Mask.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include <ComCtrls.hpp>
#include <Buttons.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <math.h>

//---------------------------------------------------------------------------
class TFincasF : public TForm
{
__published:	// IDE-managed Components
        TDBEdit *Nombre;
        TDBEdit *CODFINCA;
        TDBGrid *Fincas;
        TDBEdit *CapacidadCubas;
        TDBEdit *CaudalBomba;
        TDBEdit *CapNodriza;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label6;
        TDBComboBox *OrigenAgua;
        TDBGrid *ListSectores;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label10;
        TButton *Cancela;
        TLabel *Label12;
        TButton *Cerrar;
        TLabel *Label15;
        TLabel *Label16;
        TDBComboBox *OrigenElectricidad;
        TButton *AnalisisFinca;
        TPageControl *Paginas;
        TTabSheet *Riego;
        TTabSheet *Descripcion;
        TTabSheet *Sectores;
        TGroupBox *DatosFinca;
        TLabel *Label11;
        TDBEdit *Sector;
        TLabel *Label13;
        TDBEdit *Variedad;
        TLabel *Label14;
        TDBLookupComboBox *Patron;
        TLabel *Label17;
        TDBEdit *DstLinea;
        TLabel *Label18;
        TDBEdit *DstArbol;
        TLabel *Label19;
        TDBEdit *NumArboles;
        TLabel *Label20;
        TDBEdit *Hg;
        TLabel *Label21;
        TDBEdit *Edad;
        TLabel *Label22;
        TDBEdit *GoteoArbol;
        TLabel *Label23;
        TDBEdit *CaudalGoteo;
        TDBCheckBox *Es_Plantonada;
        TButton *CancelaSector;
        TButton *AnalisisSector;
        TLabel *Label24;
        TDBEdit *NomCliente;
        TDBEdit *CodCli;
        TDBEdit *TRiego;
        TLabel *Label25;
        TGroupBox *RiquezaAgua;
        TLabel *Label26;
        TDBEdit *N;
        TLabel *Label27;
        TDBEdit *P;
        TLabel *Label28;
        TDBEdit *K;
        TLabel *Label29;
        TDBEdit *Ca;
        TLabel *Label30;
        TDBEdit *Mg;
        TLabel *Label31;
        TDBEdit *Fe;
        TLabel *Label32;
        TDBEdit *MElementos;
        TLabel *Label33;
        TDBEdit *AHumico;
        TGroupBox *StmaRiego;
        TDBRichEdit *Observaciones;
        TDBEdit *Cuba1;
        TDBEdit *Cuba2;
        TDBEdit *Cuba3;
        TDBEdit *Cuba4;
        TDBEdit *Cuba5;
        TLabel *Label36;
        TDBEdit *CubaN1;
        TDBEdit *CubaN2;
        TDBEdit *CubaN3;
        TDBEdit *CubaN4;
        TDBEdit *CubaN5;
        TLabel *Label35;
        TLabel *Label1;
        TLabel *Label7;
        TLabel *Label34;
        TBitBtn *NuevoB;
        TBitBtn *GuardarB;
        TBitBtn *EliminarB;
        TBitBtn *NuevoSector;
        TBitBtn *GuardaSector;
        TBitBtn *EliminaSector;
        TBitBtn *DuplicaB;
        TADOTable *SectorDupl;
        TIntegerField *SectorDuplCODFINCA;
        TWideStringField *SectorDuplSECTOR;
        TSmallintField *SectorDuplARBOLNUM;
        TWideStringField *SectorDuplVARIEDAD;
        TWideStringField *SectorDuplPATRON;
        TFloatField *SectorDuplDSTLINEA;
        TFloatField *SectorDuplDSTARBOL;
        TFloatField *SectorDuplHG;
        TWideStringField *SectorDuplEdad;
        TFloatField *SectorDuplGOTEOXARB;
        TFloatField *SectorDuplCAUDALGOT;
        TBooleanField *SectorDuplPLANTONADA;
        TSmallintField *SectorDuplTRIEGO;
        TSpeedButton *BuscarClienteB;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall RegistraFincaClick(TObject *Sender);
        void __fastcall CancelaClick(TObject *Sender);
        void __fastcall NuevaClick(TObject *Sender);
        void __fastcall GuardaSectorClick(TObject *Sender);    
        void __fastcall NuevoSectorClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall AnalisisFincaClick(TObject *Sender);
        void __fastcall EliminaFincaClick(TObject *Sender);
        void __fastcall CancelaSectorClick(TObject *Sender);
        void __fastcall EliminaSectorClick(TObject *Sender);
        void __fastcall AnalisisSectorClick(TObject *Sender);
        void __fastcall DstLineaChange(TObject *Sender);
        void __fastcall DstArbolChange(TObject *Sender);
        void __fastcall NumArbolesChange(TObject *Sender);
        void __fastcall DuplicaBClick(TObject *Sender);
        void __fastcall BuscarClienteBClick(TObject *Sender);
        void __fastcall NombreExit(TObject *Sender);
private:	// User declarations
        void TFincasF::calculaSuperficie(void);
public:		// User declarations
        __fastcall TFincasF(TComponent* Owner);
        void bloqueaFormulario(void);
        void desbloqueaFormulario(void);
        void bloqueaSector(void);
        void desbloqueaSector(void);
};
//---------------------------------------------------------------------------
extern PACKAGE TFincasF *FincasF;
//---------------------------------------------------------------------------
#endif
