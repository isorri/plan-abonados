//---------------------------------------------------------------------------

#ifndef PlantillaCH
#define PlantillaCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TPlantilla : public TForm
{
__published:	// IDE-managed Components
        TBitBtn *Cerrar;
        TBitBtn *NuevoB;
        TBitBtn *GuardarB;
        TBitBtn *EliminarB;
private:	// User declarations
public:		// User declarations
        __fastcall TPlantilla(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TPlantilla *Plantilla;
//---------------------------------------------------------------------------
#endif
