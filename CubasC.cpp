//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CubasC.h"
#include "FincasC.h"
#include "DatosC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PlantillaC"
#pragma resource "*.dfm"
TCubasF *CubasF;
//---------------------------------------------------------------------------
__fastcall TCubasF::TCubasF(TComponent* Owner)
        : TPlantilla(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TCubasF::FormClose(TObject *Sender, TCloseAction &Action)
{
        //Habilitar de nuevo el formulario de fincas
        FincasF->Enabled = true;
        //Formulario preparado para ser destruido
        Action = caFree;
        CubasF = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TCubasF::CerrarClick(TObject *Sender)
{
        Close();        
}
//---------------------------------------------------------------------------

