//---------------------------------------------------------------------------

#ifndef AnalisisFincaCH
#define AnalisisFincaCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "PlantillaC.h"
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
//---------------------------------------------------------------------------
class TAnalisisFincaF : public TPlantilla
{
__published:	// IDE-managed Components
        TGroupBox *GroupBox1;
        TLabel *Nomcli;
        TLabel *Label3;
        TLabel *Nomfinca;
        TGroupBox *GroupBox2;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label10;
        TLabel *Label11;
        TLabel *Label12;
        TDBEdit *N;
        TDBEdit *K;
        TDBEdit *P;
        TDBEdit *Ca;
        TDBEdit *Mg;
        TDBEdit *Fe;
        TDBEdit *Micro;
        TDBEdit *AH;
        TLabel *Label4;
        TDateTimePicker *Fecha;
        TDBEdit *FechaBD;
        TDBGrid *ListaIzq;
        TLabel *Label2;
        TEdit *CodFinca;
        TButton *Cancelar;
        TLabel *Label13;
        TLabel *Label14;
        TLabel *Label15;
        TLabel *Label16;
        TLabel *Label18;
        TLabel *Label19;
        TLabel *Label20;
        TLabel *Label21;
        void __fastcall FechaChange(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall AnyadirClick(TObject *Sender);
        void __fastcall GuardarClick(TObject *Sender);
        void __fastcall CancelarClick(TObject *Sender);
        void __fastcall EliminarClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TAnalisisFincaF(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TAnalisisFincaF *AnalisisFincaF;
//---------------------------------------------------------------------------
#endif
