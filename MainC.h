//---------------------------------------------------------------------------

#ifndef MainCH
#define MainCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ActnList.hpp>
#include <StdActns.hpp>
#include <ToolWin.hpp>
#include <ImgList.hpp>
#include <OleCtrls.hpp>
//#include "UCrpe32.hpp"
//#include "UCrpeClasses.hpp"
#include <ExtActns.hpp>
#include <DBActns.hpp>
#include "UCrpe32.hpp"
#include "UCrpeClasses.hpp"

//---------------------------------------------------------------------------
class TMainF : public TForm
{
__published:	// IDE-managed Components
        TMainMenu *Menu;
        TMenuItem *N1;
        TMenuItem *ClientesMenu;
        TMenuItem *FincasMenu;
        TMenuItem *AyudaMenu;
        TMenuItem *AutorMenu;
        TStatusBar *BarraEstado;
        TMenuItem *Salir1;
        TMenuItem *Ventana1;
        TMenuItem *Cascada1;
        TActionList *ActionList1;
        TWindowCascade *WindowCascade1;
        TWindowTileVertical *WindowTileVertical1;
        TWindowTileHorizontal *WindowTileHorizontal1;
        TMenuItem *MosaicoHorizontal1;
        TMenuItem *MosaicoVertical1;
        TImageList *ImageList1;
        TMenuItem *Principal1;
        TMenuItem *Abonos1;
        TMenuItem *PlanAnual1;
        TMenuItem *RepartoMensual1;
        TMenuItem *GenerarPlanAbonadoAnual1;
        TMenuItem *Coeficientes;
        TMenuItem *Impresionmasiva1;
        TMenuItem *Plantillasreparto1;
        TMenuItem *PlanMensual;
        TMenuItem *DiasRiego;
        TMenuItem *GestionMes;
        TMenuItem *ImpresionMensual;
        TMenuItem *CrearMes;
        TCrpe *Report;
        TMenuItem *Mododebug1;
        TMenuItem *DiasRiegoPlant;
        TMenuItem *Editar;
        TEditCut *EditCut1;
        TEditCopy *EditCopy1;
        TEditPaste *EditPaste1;
        TMenuItem *Cortar;
        TMenuItem *Copy1;
        TMenuItem *Paste1;
        TWindowMinimizeAll *WindowMinimizeAll1;
        TMenuItem *MinimizeAll1;
        TDataSetNext *DataSetNext1;
        TMenuItem *Next1;
        TDataSetFirst *DataSetFirst1;
        TDataSetPrior *DataSetPrior1;
        TDataSetLast *DataSetLast1;
        TDataSetRefresh *DataSetRefresh1;
        TMenuItem *First1;
        TMenuItem *Last1;
        TMenuItem *Prior1;
        TEditSelectAll *EditSelectAll1;
        TEditUndo *EditUndo1;
        TMenuItem *SelectAll1;
        TMenuItem *Undo1;
        TRichEditBold *RichEditBold1;
        TMenuItem *Bold1;
        TRichEditItalic *RichEditItalic1;
        TRichEditUnderline *RichEditUnderline1;
        TRichEditStrikeOut *RichEditStrikeOut1;
        TRichEditBullets *RichEditBullets1;
        TRichEditAlignLeft *RichEditAlignLeft1;
        TRichEditAlignRight *RichEditAlignRight1;
        TRichEditAlignCenter *RichEditAlignCenter1;
        TMenuItem *Formato;
        TMenuItem *Italic1;
        TMenuItem *Underline1;
        TMenuItem *Strikeout1;
        TMenuItem *Bullets1;
        TMenuItem *AlignLeft1;
        TMenuItem *AlignLeft2;
        TMenuItem *Center1;
        TMenuItem *N3;
        TMenuItem *N4;
        TToolBar *ToolBar1;
        TToolButton *ToolButton1;
        TToolButton *ToolButton2;
        TToolButton *ToolButton3;
        TToolButton *ToolButton4;
        TToolButton *ToolButton6;
        TToolButton *ToolButton7;
        TToolButton *ToolButton8;
        TToolButton *ToolButton9;
        TToolButton *ToolButton10;
        TToolButton *ToolButton11;
        TToolButton *ToolButton12;
        TToolButton *ToolButton13;
        TToolButton *ToolButton14;
        TToolButton *ToolButton15;
        TToolButton *ToolButton16;
        TToolButton *ToolButton17;
        TToolButton *ToolButton18;
        TToolButton *ToolButton19;
        TToolButton *ToolButton20;
        TMenuItem *Comerciales1;
        TMenuItem *N2;
        TMenuItem *N5;
        TMenuItem *MigrarDatos;
        void __fastcall ClientesMenuClick(TObject *Sender);
        void __fastcall FincasMenuClick(TObject *Sender);
        void __fastcall Salir1Click(TObject *Sender);
        void __fastcall AutorMenuClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall Abonos1Click(TObject *Sender);
        void __fastcall CoeficientesClick(TObject *Sender);
        void __fastcall GenerarPlanAbonadoAnual1Click(TObject *Sender);
        void __fastcall RepartoMensual1Click(TObject *Sender);
        void __fastcall Plantillasreparto1Click(TObject *Sender);
        void __fastcall Impresionmasiva1Click(TObject *Sender);
        void __fastcall DiasRiegoClick(TObject *Sender);
        void __fastcall DiasRiegoPlantClick(TObject *Sender);
        void __fastcall CrearMesClick(TObject *Sender);
        void __fastcall GestionMesClick(TObject *Sender);
        void __fastcall ImpresionMensualClick(TObject *Sender);
        void __fastcall Mododebug1Click(TObject *Sender);
        void __fastcall Comerciales1Click(TObject *Sender);
        void __fastcall MigrarDatosClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TMainF(TComponent* Owner);
        bool pruebas;
};
//---------------------------------------------------------------------------
extern PACKAGE TMainF *MainF;
//---------------------------------------------------------------------------
#endif
