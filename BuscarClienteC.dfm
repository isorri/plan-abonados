inherited BuscarClienteF: TBuscarClienteF
  Left = 511
  Top = 312
  Width = 455
  Height = 410
  Caption = 'B'#250'squeda Clientes'
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited Cerrar: TBitBtn
    Left = 360
    Top = 336
    TabOrder = 2
    OnClick = CerrarClick
  end
  inherited NuevoB: TBitBtn
    Left = 8
    Top = 336
    Enabled = False
    TabOrder = 3
    Visible = False
  end
  inherited GuardarB: TBitBtn
    Left = 40
    Top = 336
    Enabled = False
    TabOrder = 4
    Visible = False
  end
  inherited EliminarB: TBitBtn
    Left = 64
    Top = 336
    Enabled = False
    TabOrder = 5
    Visible = False
  end
  object CriteriosBox: TGroupBox
    Left = 8
    Top = 0
    Width = 425
    Height = 113
    Hint = 'Criterios de b'#250'squeda utilizados para seleccionar clientes.'
    Caption = 'Criterios b'#250'squeda'
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 46
      Height = 13
      Caption = 'Comercial'
    end
    object Label2: TLabel
      Left = 16
      Top = 56
      Width = 32
      Height = 13
      Caption = 'Cliente'
    end
    object ComercialLista: TDBLookupComboBox
      Left = 72
      Top = 16
      Width = 281
      Height = 21
      Hint = 'Filtrar resultado por comercial.'
      KeyField = 'CODCOMERCIAL'
      ListField = 'NOMBRE'
      ListSource = DatosF.ComercialesSource
      TabOrder = 0
    end
    object ClienteText: TEdit
      Left = 72
      Top = 56
      Width = 281
      Height = 21
      Hint = 'Patr'#243'n de b'#250'squeda asociado al cliente'
      TabOrder = 2
    end
    object BusquedaB: TBitBtn
      Left = 328
      Top = 80
      Width = 83
      Height = 25
      Hint = 'Realiza la b'#250'squeda con los criterios de b'#250'squeda introducidos.'
      Caption = 'B'#250'squeda'
      TabOrder = 3
      OnClick = BusquedaBClick
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00840000008400
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0084000000840000008400
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF0000000000000000000000000000000000840000008400000084000000FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
        000084848400C6C6C600FFFFFF00848484000000000084000000FF00FF00FF00
        FF00FF00FF00FF00FF0000008400FF00FF00FF00FF00FF00FF00000000008484
        8400C6C6C600C6C6C600C6C6C600FFFFFF008484840000000000FF00FF00FF00
        FF00FF00FF000000840000008400FF00FF00FF00FF00FF00FF0000000000C6C6
        C600C6C6C600C6C6C600C6C6C600C6C6C600FFFFFF0000000000FF00FF00FF00
        FF000000840000008400000084000000840000008400FF00FF0000000000C6C6
        C600FFFFFF00FFFF0000C6C6C600C6C6C600C6C6C60000000000FF00FF00FF00
        FF00FF00FF000000840000008400FF00FF00FF00FF0000008400000000008484
        8400FFFFFF00FFFFFF00C6C6C600C6C6C6008484840000000000FF00FF00FF00
        FF00FF00FF00FF00FF0000008400FF00FF00FF00FF0000008400FF00FF000000
        000084848400C6C6C600C6C6C6008484840000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000008400FF00FF00FF00
        FF0000000000000000000000000000000000FF00FF00FF00FF00FF00FF000000
        8400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
        8400FF00FF00FF00FF0000008400FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
        8400FF00FF00FF00FF000000840000008400FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF000000840000008400000084000000840000008400FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF000000840000008400FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000008400FF00FF00FF00FF00FF00FF00}
    end
    object TodosFlag: TCheckBox
      Left = 360
      Top = 16
      Width = 57
      Height = 17
      Hint = 'Muestra los clientes de todos los comerciales si esta activado.'
      Caption = 'Todos'
      TabOrder = 1
      OnClick = TodosFlagClick
    end
  end
  object ResultadoBox: TGroupBox
    Left = 8
    Top = 112
    Width = 425
    Height = 217
    Caption = 'Resultado'
    TabOrder = 1
    object ResultadoTable: TDBGrid
      Left = 8
      Top = 16
      Width = 409
      Height = 193
      Hint = 'Muestra el resultado de la b'#250'squeda'
      DataSource = BuscaCliSource
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = ResultadoTableDblClick
      OnKeyPress = ResultadoTableKeyPress
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMBRE'
          Title.Caption = 'Nombre'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMBRE_1'
          Title.Caption = 'Comercial'
          Width = 175
          Visible = True
        end>
    end
  end
  object BuscarCliente: TADOQuery
    Connection = DatosF.Abono
    Parameters = <>
    SQL.Strings = (
      'SELECT CLI.CODCLI,CLI.NOMBRE, COM.NOMBRE '
      'FROM CLIENTES CLI, COMERCIALES COM'
      'WHERE CLI.CODCOMERCIAL = COM.CODCOMERCIAL')
    Left = 160
    Top = 336
    object BuscarClienteNOMBRE: TWideStringField
      FieldName = 'NOMBRE'
      Size = 50
    end
    object BuscarClienteNOMBRE_1: TWideStringField
      FieldName = 'NOMBRE_1'
      Size = 50
    end
    object BuscarClienteCODCLI: TWideStringField
      FieldName = 'CODCLI'
      Size = 10
    end
  end
  object BuscaCliSource: TDataSource
    DataSet = BuscarCliente
    Left = 192
    Top = 336
  end
end
