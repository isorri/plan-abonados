//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "NuevoSectorC.h"
#include "FincasC.h"
#include "DatosC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TNuevoSectorF *NuevoSectorF;
//---------------------------------------------------------------------------
__fastcall TNuevoSectorF::TNuevoSectorF(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TNuevoSectorF::SalirClick(TObject *Sender)
{
        Close();
}
//---------------------------------------------------------------------------

void __fastcall TNuevoSectorF::FormClose(TObject *Sender,
      TCloseAction &Action)
{
        if (DatosF->Sectores->State == dsInsert)
        {
                Application->MessageBoxA("Sector no insertado","Error",MB_ICONERROR);
        }
        else
        {
                Action = caFree;
                NuevoSectorF = NULL;
                if (FincasF != NULL)
                    FincasF->Sectores->Enabled = true;
        }
}
//---------------------------------------------------------------------------


void __fastcall TNuevoSectorF::InsertarClick(TObject *Sender)
{
        if (Sector->Text == "")
        {
            Application->MessageBoxA("Sector no puede ser nulo","Error",MB_ICONERROR);
            Sector->SetFocus();
            return;
        }
        if (Patron->Text == "")
        {
            Application->MessageBoxA("Patrón no puede ser nulo","Error",MB_ICONERROR);
            Patron->SetFocus();
            return;
        }
        if (DatosF->Sectores->State == dsInsert)
           DatosF->Sectores->Post();

        else
           Application->MessageBoxA("Nada que insertar","Información",MB_ICONINFORMATION);
        Close();
}
//---------------------------------------------------------------------------


void __fastcall TNuevoSectorF::CancelarClick(TObject *Sender)
{
        if (DatosF->Sectores->State == dsInsert)
        {
                DatosF->Sectores->Cancel();
                FincasF->Sectores->Enabled = true;

        }
        else
        {
                Application->MessageBoxA("Nada que cancelar","Información",MB_ICONINFORMATION);
        }
        Close();
}
//---------------------------------------------------------------------------

void __fastcall TNuevoSectorF::InsertamasClick(TObject *Sender)
{
        if (Sector->Text == "")
        {
            Application->MessageBoxA("Sector no puede ser nulo","Error",MB_ICONERROR);
            Sector->SetFocus();
            return;
        }
        if (Patron->Text == "")
        {
            Application->MessageBoxA("Patrón no puede ser nulo","Error",MB_ICONERROR);
            Patron->SetFocus();
            return;
        }
        if (DatosF->Sectores->State == dsInsert)
        {
                DatosF->Sectores->Post();
        }
        DatosF->Sectores->Insert();
        Sector->SetFocus();
        Es_Plantonada->Checked = false;
}
//---------------------------------------------------------------------------


void __fastcall TNuevoSectorF::DstLineaChange(TObject *Sender)
{
        calculaSuperficie();
}
//---------------------------------------------------------------------------

void __fastcall TNuevoSectorF::DstArbolChange(TObject *Sender)
{
        calculaSuperficie();

}
//---------------------------------------------------------------------------

void __fastcall TNuevoSectorF::NumArbolesChange(TObject *Sender)
{
        calculaSuperficie();
}
//---------------------------------------------------------------------------
void TNuevoSectorF::calculaSuperficie(void)
{
  //Realiza el calculo de la superficie del sector con los datos introducidos
  if (NumArboles->Text != "" && DstArbol->Text != "" && DstLinea->Text != "")
      DatosF->Sectores->FieldValues["HG"] =NumArboles->Text * DstArbol->Text * DstLinea->Text / 813;

}
//---------------------------------------------------------------------------
