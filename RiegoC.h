//---------------------------------------------------------------------------

#ifndef RiegoCH
#define RiegoCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "PlantillaC.h"
#include <Buttons.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TRiegoF : public TPlantilla
{
__published:	// IDE-managed Components
        TDBGrid *ListaIzq;
        TGroupBox *Panel;
        TDBEdit *Mes;
        TDBCheckBox *Lunes;
        TDBCheckBox *Martes;
        TDBCheckBox *Miercoles;
        TDBCheckBox *Jueves;
        TDBCheckBox *Viernes;
        TDBCheckBox *Sabado;
        TDBCheckBox *Domingo;
        TLabel *Label2;
        TButton *Cancelar;
        void __fastcall CerrarClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall NuevoClick(TObject *Sender);
        void __fastcall GuardarClick(TObject *Sender);
        void __fastcall CancelarClick(TObject *Sender);
        void __fastcall EliminarBClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TRiegoF(TComponent* Owner);
        void bloqueaFormulario(void);
        void desbloqueaFormulario(void);        
};
//---------------------------------------------------------------------------
extern PACKAGE TRiegoF *RiegoF;
//---------------------------------------------------------------------------
#endif
